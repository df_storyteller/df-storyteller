#![forbid(unsafe_code)]
#![deny(clippy::all)]

use colored::*;
use log::{Level, LevelFilter, Metadata, Record};
use std::io;
use std::path::PathBuf;
use structopt::StructOpt;

/// Provide custom error with links for when application panics (unrecoverable error).
#[macro_use]
mod git_panic;

// TODO: Write logs to file, this requires multi-threading safety or using an external crate
/// An instance of the `Logger`.
static LOGGER: Logger = Logger;
/// The log collector and handler for most printed messages in terminal.
struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        let enable = if !cfg!(debug_assertions) {
            // Only in release mode
            // Do the filters below unless it is a Warning, Error (or Debug)
            metadata.level() == Level::Warn
                || metadata.level() == Level::Error
                || metadata.level() == Level::Debug
                // Hide part of the Rocket messages
                || !(metadata.target() == "launch_" || metadata.target() == "_")
        } else {
            // Don't apply additional filters in debug build
            true
        };

        // All messages need to be Trace or lower
        metadata.level() <= Level::Trace
            // Don't display serde xml parsing messages (to many)
            && metadata.target() != "serde_xml_rs::de"
            // Don't display `ureq` message (can be enabled for testing)
            && metadata.target() != "ureq::unit"
            // Don't display hyper networking message (not useful in most cases)
            && !metadata.target().starts_with("hyper::")
            // If release mode filter on
            && enable
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR".bright_red(),
                    Level::Warn => "WARN".bright_yellow(),
                    Level::Info => "INFO".bright_blue(),
                    Level::Debug => "DEBUG".bright_green(),
                    Level::Trace => "TRACE".bright_magenta(),
                },
                record.target(),
                record.args()
            );
        }
        // TODO: log to file
        // TODO: add timestamps to log file
    }

    fn flush(&self) {}
}

/// The available command line parameters.
#[derive(StructOpt, Debug)]
#[structopt(
    name = "df_storyteller",
    about = "Parse Dwarf Fortress Legends and have fun with the stories."
)]
struct Opts {
    /// Activates debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Activates quiet mode, no log message in std out
    #[structopt(short, long)]
    quiet: bool,

    /// Verbose mode (-v, -vv)
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    /// All available subcommand
    #[structopt(subcommand)]
    cmd: Commands,
}

/// All available subcommands in DF Storyteller.
#[derive(StructOpt, Debug)]
enum Commands {
    /// A Guide for setting up DF Storyteller.
    /// This is also available on [our website](https://guide.dfstoryteller.com/).
    Guide {
        // No other options
    },
    /// Parse and save the Legends files
    Import {
        /// Select what world to load from DB
        #[structopt(short, long)]
        world: u32,

        /// Legends Files to process.
        /// Give one of the legend files, other files will be found automatically
        #[structopt(name = "FILE", parse(from_os_str))]
        file: PathBuf,

        /// (TODO) Show a list of unknown tags in files
        #[structopt(short, long)]
        #[allow(dead_code)]
        show_unknown: bool,
    },
    /// Start API server
    Start {
        /// Select what world to load from DB
        #[structopt(short, long)]
        world: u32,
    },
    /// List all the saved world
    List {
        /// View next page of worlds. Current page limit = 20.
        /// If the list is to long it will be broken up into pages.
        /// This allows you to get the other pages.
        #[structopt(short, long, default_value = "0")]
        page: u32,
    },
    /// (TODO) Export a saved world
    Export {
        /// Select what world to load from DB
        #[structopt(short, long)]
        #[allow(dead_code)]
        world: u32,

        /// Output the parsed legends to file
        #[structopt(name = "OUTPUT FILE", parse(from_os_str))]
        #[allow(dead_code)]
        output: PathBuf,

        /// Format of output file. Default is Json
        #[structopt(subcommand)]
        #[allow(dead_code)]
        format: Option<ExportFormats>,
    },
    /// Export OpenAPI JSON file.
    /// This file can be used together with other documentation viewers like
    /// RapiDoc to view the API documentation.
    Docs {
        /// Path to where to write the `openapi.json` file, can be a file or directory
        #[structopt(name = "OUTPUT FILE", parse(from_os_str))]
        output: PathBuf,
    },
    /// Create/overwrite persistent settings/config file.
    /// If no file is found it will create the default global config file.
    /// If a file is found it will read the file (if valid) and write it again,
    /// this will ensure it is a valid and nicely formatted config file.
    Config {
        /// Replace the port set in the config file
        /// Default is `20350`
        #[structopt(short = "p", long)]
        port: Option<u16>,

        /// Reset currently used config file (current directory or global).
        #[structopt(long)]
        reset: bool,
    },
    /// Setup the database
    Database {
        /// The username that be used to create new roles
        /// By default "postgres" is used
        #[structopt(short = "u", long)]
        db_user: Option<String>,

        /// The port used for the database connection
        /// Default is `5432`
        #[structopt(short = "p", long)]
        db_port: Option<u16>,

        /// DANGER: This will DELETE the existing `df_storyteller` database.
        /// This deletes all stored data in the database.
        /// Once deleted it will create a new database.
        #[structopt(long)]
        drop_db: bool,

        /// Reset currently used database.
        /// Remove all tables and recreate it.
        /// DANGER: The `--drop_db` flag has to be set in order to remove a postgres database.
        #[structopt(long)]
        reset: bool,
    },
    /// Sign verifiers (only available in debug build)
    #[cfg(debug_assertions)]
    Sign {
        /// Create a new pair of keys for signing verifiers.
        #[structopt(long)]
        create_new_keypair: bool,
    },
}

/// The available file formats for exporting, used by the [`Export` subcommand](Commands::Export).
#[derive(StructOpt, Debug)]
enum ExportFormats {
    Json,
    Xml,
}

/// Start of the DF Storyteller CLI application.
pub fn main() -> io::Result<()> {
    setup_panic!();
    let opts = Opts::from_args();
    // Setup logger and log level
    log::set_logger(&LOGGER).unwrap();
    if opts.debug {
        log::set_max_level(LevelFilter::Debug);
        if opts.verbose >= 2 {
            log::set_max_level(LevelFilter::Trace);
        }
    } else if opts.quiet {
        // TODO change this to logging to file one, but can not use static var. (unsafe)
        log::set_max_level(LevelFilter::Off);
    } else {
        match opts.verbose {
            0 => log::set_max_level(LevelFilter::Info),
            1 => log::set_max_level(LevelFilter::Debug),
            _ => log::set_max_level(LevelFilter::Trace),
        }
    }
    log::trace!("Command arguments: {:#?}", opts);

    // Read config file
    let config = df_st_core::config::load_config();
    log::trace!("Config loaded: {:#?}", config);

    let status = futures::executor::block_on(df_st_updater::check_version()).unwrap();
    match status {
        df_st_updater::VersionStatus::Latest => {
            log::info!("DF Storyteller is up-to-date.");
        }
        df_st_updater::VersionStatus::UpdateAvailable => {
            log::info!(
                "-----------------------------------------------\n\
                There is a new update available for DF Storyteller!\n\
                Please check https://dfstoryteller.com/ for the latest version.\n\
                -----------------------------------------------"
            );
        }
        df_st_updater::VersionStatus::UpdateRequired => {
            // log::error!(
            //     "This version of DF Storyteller is outdated.\n\
            //     This version is marked as unsafe. Updating is required!"
            // );
            // log::info!(
            //     "Please download a new version at\n\
            //     https://dfstoryteller.com/ You are blocked from using an unsafe versions.\n\
            //     Closing application now!"
            // );
            // panic!("This version is unsafe. Please update.");
        }
        df_st_updater::VersionStatus::TamperProof => {
            log::error!(
                "Something is off. We are detecting some tampering with update verification.\n\
                We do not allow tampering in this application. If you did nothing weird, \
                please open an issue: {}",
                df_st_core::git_issue::link_to_issue_page()
            );
            // log::error!(
            //     "Tamper prevention in place. Please update to the latest version.\n\
            //     Closing application now!"
            // );
            // panic!("Tamper prevention in place. Please update to the latest version.");
        }
        df_st_updater::VersionStatus::CouldNotCheck => {
            log::warn!("DF Storyteller could not check for updates.");
            log::info!(
                "Please check for updates yourself to make sure you are not \
                vulnerable to attacks."
            );
            // This is allowed because we do not want to prevent people from enjoying it without
            // an internet connection.
        }
        df_st_updater::VersionStatus::Unknown => {
            log::warn!(
                "DF Storyteller encountered an unknown status from update server.\n\
                This might be a bug, please open an issue: {}",
                df_st_core::git_issue::link_to_issue_page()
            );
        }
    }

    log::info!(
        "DF Storyteller will use the {} database.",
        df_st_db::get_db_service_name().bold()
    );

    // Check what subcommand we want to run
    match opts.cmd {
        Commands::Guide {} => {
            df_st_guide::start_guide_server();
        }
        Commands::Import {
            world,
            file,
            show_unknown: _,
        } => {
            log::debug!("Checking database connection");
            if let Some(service) = &config.database.service {
                if service == "sqlite" {
                    if !df_st_db::check_db_has_tables(&config) {
                        log::info!("Found a database without tables. Running migrations");
                        // Run migrations and create all the tables
                        df_st_db::run_migrations(&config);
                    }
                } else if !df_st_db::check_db_has_tables(&config) {
                    panic!("Found a database without tables.");
                }
            }

            if !file.is_file() && !file.is_dir() {
                log::error!(
                    "The path provided is not a file or directory: {}",
                    file.to_string_lossy()
                );
            } else {
                df_st_parser::start_import(file, &config, world as i32);
                log::info!(
                    "Done parsing all files. You can now use the command below to view you world."
                );
                if cfg!(windows) {
                    log::info!("Run: `./df_storyteller.exe start -w {}`", world);
                } else {
                    log::info!("Run: `./df_storyteller start -w {}`", world);
                }
            }
        }
        Commands::Start { world } => {
            log::debug!("Checking database connection");
            if !df_st_db::check_db_has_tables(&config) {
                if cfg!(feature = "sqlite") {
                    log::info!("Found a database without tables. Running migrations");
                    // Run migrations and create all the tables
                    df_st_db::run_migrations(&config);
                    log::warn!(
                        "You have not imported any worlds so all responses will be empty.\n\
                        It is best if you first use the `import` command to import a world."
                    );
                } else if cfg!(feature = "postgres") {
                    panic!("Found a database without tables.");
                }
            }
            df_st_api::start_server(&config, world);
        }
        Commands::List { page } => {
            //TODO: Add the following part, if `list` is run and no DB is present yet
            /*
            log::debug!("Checking database connection");
            if !df_st_db::check_db_has_tables(&config) {
                if cfg!(feature = "sqlite") {
                    log::info!("Found a database without tables. Running migrations");
                    // Run migrations and create all the tables
                    df_st_db::run_migrations(&config);
                    log::warn!(
                        "You have not imported any worlds so all responses will be empty.\n\
                        It is best if you first use the `import` command to import a world."
                    );
                } else if cfg!(feature = "postgres") {
                    panic!("Found a database without tables.");
                }
            }
            */
            use df_st_db::DBObject;
            let pool =
                df_st_db::establish_connection(&config).expect("Failed to connect to database.");
            let conn = pool.get().expect("Couldn't get db connection from pool.");
            let per_page_limit = 10;

            let world_list = df_st_db::DFWorldInfo::get_list_from_db(
                &*conn,
                df_st_db::id_filter![],
                df_st_db::string_filter![],
                page,
                per_page_limit,
                Some(df_st_db::OrderTypes::Asc),
                Some("id".to_owned()),
                None,
                true,
            )
            .expect("Could not get the list of worlds from that database.");

            let mut list_string = String::new();
            let mut message = String::new();
            if world_list.is_empty() {
                if page == 0 {
                    message = "No worlds found.".to_owned();
                } else {
                    message = "No more worlds found on this page.".to_owned();
                }
            }
            if world_list.len() >= per_page_limit as usize {
                message = format!("More world might be on next page, use `-p {}`.", page + 1);
            }
            for world in world_list {
                let year = match world.year {
                    Some(x) => x.to_string(),
                    None => "<unknown>".to_owned(),
                };
                let region_number = match world.region_number {
                    Some(x) => x.to_string(),
                    None => "<unknown>".to_owned(),
                };
                list_string = format!(
                    "{}{:>3}: {} ({}), Year: {} Region: {}\n",
                    list_string,
                    world.id,
                    world.name.unwrap_or_else(|| "<no-name>".to_owned()),
                    world
                        .alternative_name
                        .unwrap_or_else(|| "<no-name>".to_owned()),
                    year,
                    region_number
                );
            }
            println!("List of worlds in database:\n{}{}", list_string, message);
        }
        Commands::Export {
            world: _,
            output: _,
            format: _,
        } => {
            // TODO it should automatically detect if the extension is xml
            // to export it in xml format
            println!("The Export subcommand is not yet implemented. We are still working on this.");
        }
        Commands::Docs { mut output } => {
            if let Some(extension) = output.extension() {
                // TODO: To lowercase?
                if extension != "json" {
                    log::warn!(
                        "The openapi file can only be exported in json format. \
                        Replacing extension with '.json'."
                    );
                    output.set_extension("json");
                }
            }
            if output.is_dir() {
                output.set_file_name("openapi.json");
            }
            let output_name = output.to_str().unwrap_or("openapi.json");
            log::info!("Write documentation to '{}' file.", output_name);
            // let openapi = df_st_api::get_openapi_spec(); // TODO
            // df_st_api::write_json_file(output, &openapi).ok();
        }
        Commands::Config { port, reset } => {
            let mut config = if reset {
                df_st_core::config::RootConfig::default()
            } else {
                config
            };

            let config_path = df_st_core::config::get_current_use_config_path();
            if port.is_some() {
                config.server.port = port;
            }
            df_st_core::config::store_config_to_file(
                config,
                match &config_path {
                    Some(path) => Some(path),
                    None => None,
                },
            );
        }
        Commands::Database {
            db_user,
            db_port,
            drop_db,
            reset,
        } => {
            if cfg!(feature = "postgres") {
                if reset {
                    // Create or recreate database
                    df_st_db::recreate_database(&config, drop_db);
                    // Run migrations and create all the tables
                    df_st_db::run_migrations(&config);
                } else {
                    println!(
                        "The Database subcommand is still experimental. \
                        Tel us know if something does not seem right."
                    );
                    let user = db_user.unwrap_or_else(|| "postgres".to_owned());
                    let port = db_port.unwrap_or(5432);

                    // Get password
                    println!("Password for {}: ", user);
                    let mut password = String::new();
                    io::stdin().read_line(&mut password)?;
                    password = password.trim().to_owned();
                    let mut privileged_config = config;
                    privileged_config.database.service = Some("postgres".to_owned());
                    privileged_config.database.config = df_st_core::config::DBURLConfig {
                        user: Some(user),
                        password,
                        host: Some("localhost".to_owned()),
                        port: Some(port),
                        database: Some("df_storyteller".to_owned()),
                        ..Default::default()
                    };
                    // This creates the user and overwrites the config file
                    df_st_db::create_user(&privileged_config);
                    // Reload config to use new user (less privileged)
                    let config = df_st_core::config::load_config();
                    df_st_db::recreate_database(&config, drop_db);
                    // Run migrations and create all the tables
                    df_st_db::run_migrations(&config);
                }
            } else if cfg!(feature = "sqlite") {
                if reset {
                    df_st_db::delete_sqlite_db(&config);
                }
                // Run migrations and create all the tables
                df_st_db::run_migrations(&config);
            } else {
                unreachable!("No DB service selected");
            }
        }
        #[cfg(debug_assertions)]
        Commands::Sign { create_new_keypair } => {
            if create_new_keypair {
                df_st_updater::create_keypair();
            } else {
                df_st_updater::sign_all_verifiers();
            }
        }
    }
    Ok(())
}
