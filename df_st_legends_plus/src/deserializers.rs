use df_st_core::{Coordinate, Path};
use regex::Regex;
use serde::{Deserialize, Deserializer};

pub fn coordinates_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Vec<Coordinate>>, D::Error> {
    let coords: String = Deserialize::deserialize(deserializer)?;
    if coords.is_empty() {
        return Ok(None);
    }
    let coords_list = coordinates_list(coords);
    Ok(Some(coords_list))
}

fn coordinates_list(coords: String) -> Vec<Coordinate> {
    let re = Regex::new(r"(-?[0-9]+),(-?[0-9]+)\|").unwrap();
    let mut coords_list = Vec::new();
    for values in re.captures_iter(&coords) {
        coords_list.push(Coordinate {
            x: values[1].parse::<i32>().unwrap(),
            y: values[2].parse::<i32>().unwrap(),
            ..Default::default()
        })
    }
    coords_list
}

pub fn coordinate_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Coordinate>, D::Error> {
    let coords: String = Deserialize::deserialize(deserializer)?;
    if coords.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"(-?[0-9]+),(-?[0-9]+)").unwrap();
    let values = re.captures(&coords).unwrap();
    let coords_result = Coordinate {
        x: values[1].parse::<i32>().unwrap(),
        y: values[2].parse::<i32>().unwrap(),
        ..Default::default()
    };
    Ok(Some(coords_result))
}

pub fn path_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Vec<Path>>, D::Error> {
    let path: String = Deserialize::deserialize(deserializer)?;
    if path.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"(-?[0-9]+),(-?[0-9]+),(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)\|").unwrap();
    let mut path_list = Vec::new();
    for values in re.captures_iter(&path) {
        path_list.push(Path {
            x: values[1].parse::<i32>().unwrap(),
            y: values[2].parse::<i32>().unwrap(),
            flow: values[3].parse::<i32>().unwrap(),
            exit_tile: values[4].parse::<i32>().unwrap(),
            elevation: values[5].parse::<i32>().unwrap(),
            ..Default::default()
        })
    }
    Ok(Some(path_list))
}
