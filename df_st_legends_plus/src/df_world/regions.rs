use crate::deserializers::coordinates_deserializer;
use df_st_core::{ConvertingUtils, Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct Region {
    pub id: i32,
    #[serde(deserialize_with = "coordinates_deserializer")]
    #[serde(default)]
    pub coords: Option<Vec<Coordinate>>,
    pub evilness: Option<String>,
    pub force_id: Option<Vec<i32>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, HasUnknown)]
pub struct Regions {
    pub region: Option<Vec<Region>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Region, Region> for df_st_core::Region {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Region) {
        self.id.add_missing_data(&source.id);
        self.coords.add_missing_data(&source.coords);
        self.evilness.add_missing_data(&source.evilness);
        self.force_ids.add_missing_data(&source.force_id.remove_negative());
    }
}

impl PartialEq<df_st_core::Region> for Region {
    fn eq(&self, other: &df_st_core::Region) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Region>, Regions> for Vec<df_st_core::Region> {
    fn add_missing_data(&mut self, source: &Regions) {
        self.add_missing_data(&source.region);
    }
}

impl Filler<IndexMap<u64, df_st_core::Region>, Regions> for IndexMap<u64, df_st_core::Region> {
    fn add_missing_data(&mut self, source: &Regions) {
        self.add_missing_data(&source.region);
    }
}
