use df_st_core::{ConvertingUtils, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct Identity {
    pub id: i32,
    pub name: Option<String>,
    pub histfig_id: Option<i32>,
    pub race: Option<String>,
    pub caste: Option<String>,
    pub birth_year: Option<i32>,
    pub birth_second: Option<i32>,
    pub profession: Option<String>,
    pub entity_id: Option<i32>,
    pub nemesis_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Identities {
    pub identity: Option<Vec<Identity>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Identity, Identity> for df_st_core::Identity {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Identity) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.hf_id.add_missing_data(&source.histfig_id.negative_to_none());
        self.race.add_missing_data(&source.race);
        self.caste.add_missing_data(&source.caste);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_second.add_missing_data(&source.birth_second);
        self.profession.add_missing_data(&source.profession);
        self.entity_id.add_missing_data(&source.entity_id.negative_to_none());
        self.nemesis_id.add_missing_data(&source.nemesis_id.negative_to_none());
    }
}

impl PartialEq<df_st_core::Identity> for Identity {
    fn eq(&self, other: &df_st_core::Identity) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Identity>, Identities> for Vec<df_st_core::Identity> {
    fn add_missing_data(&mut self, source: &Identities) {
        self.add_missing_data(&source.identity);
    }
}

impl Filler<IndexMap<u64, df_st_core::Identity>, Identities>
    for IndexMap<u64, df_st_core::Identity>
{
    fn add_missing_data(&mut self, source: &Identities) {
        self.add_missing_data(&source.identity);
    }
}
