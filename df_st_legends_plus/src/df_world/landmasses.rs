use crate::deserializers::coordinate_deserializer;
use df_st_core::{Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct Landmass {
    pub id: i32,
    pub name: Option<String>,
    #[serde(deserialize_with = "coordinate_deserializer")]
    #[serde(default)]
    pub coord_1: Option<Coordinate>,
    #[serde(deserialize_with = "coordinate_deserializer")]
    #[serde(default)]
    pub coord_2: Option<Coordinate>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Landmasses {
    pub landmass: Option<Vec<Landmass>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Landmass, Landmass> for df_st_core::Landmass {
    fn add_missing_data(&mut self, source: &Landmass) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.coord_1.add_missing_data(&source.coord_1);
        self.coord_2.add_missing_data(&source.coord_2);
    }
}

impl PartialEq<df_st_core::Landmass> for Landmass {
    fn eq(&self, other: &df_st_core::Landmass) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Landmass>, Landmasses> for Vec<df_st_core::Landmass> {
    fn add_missing_data(&mut self, source: &Landmasses) {
        self.add_missing_data(&source.landmass);
    }
}

impl Filler<IndexMap<u64, df_st_core::Landmass>, Landmasses>
    for IndexMap<u64, df_st_core::Landmass>
{
    fn add_missing_data(&mut self, source: &Landmasses) {
        self.add_missing_data(&source.landmass);
    }
}
