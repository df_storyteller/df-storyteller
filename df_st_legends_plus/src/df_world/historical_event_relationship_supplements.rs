use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationshipSupplement {
    pub event: i32,
    pub occasion_type: Option<i32>,
    pub site: Option<i32>,
    // pub unk_1: Option<i32>,
    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationshipSupplements {
    pub historical_event_relationship_supplement: Option<Vec<HERelationshipSupplement>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEvent, HERelationshipSupplement> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HERelationshipSupplement) {
        self.id.add_missing_data(&source.event);
        self.type_.add_missing_data(&Some("hf_relationship".to_owned()));
        self.occasion_id.add_missing_data(&source.occasion_type);
        self.site_id.add_missing_data(&source.site);
        // self.unk_1.add_missing_data(&source.unk_1);
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HERelationshipSupplement {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.event == other.id
    }
}

impl std::hash::Hash for HERelationshipSupplement {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.event.hash(state);
    }
}

impl Filler<Vec<df_st_core::HistoricalEvent>, HERelationshipSupplements>
    for Vec<df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplements) {
        self.add_missing_data(&source.historical_event_relationship_supplement);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEvent>, HERelationshipSupplements>
    for IndexMap<u64, df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplements) {
        self.add_missing_data(&source.historical_event_relationship_supplement);
    }
}
