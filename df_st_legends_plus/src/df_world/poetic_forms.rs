use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct PoeticForm {
    pub id: i32,
    pub name: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct PoeticForms {
    pub poetic_form: Option<Vec<PoeticForm>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::PoeticForm, PoeticForm> for df_st_core::PoeticForm {
    fn add_missing_data(&mut self, source: &PoeticForm) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
    }
}

impl PartialEq<df_st_core::PoeticForm> for PoeticForm {
    fn eq(&self, other: &df_st_core::PoeticForm) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::PoeticForm>, PoeticForms> for Vec<df_st_core::PoeticForm> {
    fn add_missing_data(&mut self, source: &PoeticForms) {
        self.add_missing_data(&source.poetic_form);
    }
}

impl Filler<IndexMap<u64, df_st_core::PoeticForm>, PoeticForms>
    for IndexMap<u64, df_st_core::PoeticForm>
{
    fn add_missing_data(&mut self, source: &PoeticForms) {
        self.add_missing_data(&source.poetic_form);
    }
}
