use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct HistoricalEventCollection {
    pub id: i32,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HistoricalEventCollections {
    pub historical_event_collection: Option<Vec<HistoricalEventCollection>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEventCollection, HistoricalEventCollection>
    for df_st_core::HistoricalEventCollection
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollection) {
        self.id.add_missing_data(&source.id);
    }
}

impl PartialEq<df_st_core::HistoricalEventCollection> for HistoricalEventCollection {
    fn eq(&self, other: &df_st_core::HistoricalEventCollection) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalEventCollection>, HistoricalEventCollections>
    for Vec<df_st_core::HistoricalEventCollection>
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollections) {
        self.add_missing_data(&source.historical_event_collection);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEventCollection>, HistoricalEventCollections>
    for IndexMap<u64, df_st_core::HistoricalEventCollection>
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollections) {
        self.add_missing_data(&source.historical_event_collection);
    }
}
