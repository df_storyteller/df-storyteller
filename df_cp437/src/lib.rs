#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod bufreader;
mod convert_to_char;
mod cp437;
mod string_convert;

pub use bufreader::{BufReader, Progress, ProgressUpdater};
pub use convert_to_char::convert_cp437_to_utf8_char;
pub use cp437::{
    convert_cp437_byte_to_utf8_bytes_for_chars, convert_cp437_byte_to_utf8_bytes_for_texts,
};
pub use string_convert::convert_cp437_to_utf8;
