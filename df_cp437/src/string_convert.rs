/// Convert an array of bytes encoded as CP437 into a byte array encoded as UTF-8.
pub fn convert_cp437_to_utf8(input_bytes: &[u8]) -> Vec<u8> {
    // Make output at least the size of input
    let mut output_bytes: Vec<u8> = Vec::with_capacity(input_bytes.len());

    for character in input_bytes {
        let new_char = crate::convert_cp437_byte_to_utf8_bytes_for_texts(character);
        if new_char[0] != 0x00 {
            output_bytes.push(new_char[0]);
        }
        if new_char[1] != 0x00 {
            output_bytes.push(new_char[1]);
        }
        output_bytes.push(new_char[2]);
    }
    output_bytes
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_string_convert() {
        let input = b"This is \x92 test \xb0string \x85[TEST\x8b] \xf1";
        let output = "This is Æ test ░string à[TESTï] ±";

        let result = crate::convert_cp437_to_utf8(input);
        assert_eq!(result, output.as_bytes());
    }
}
