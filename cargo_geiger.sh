#!/bin/sh

# https://github.com/rust-secure-code/cargo-geiger

cd df_st_cli
cargo geiger -- --features="postgres"
# This will clean the target folder
# can not be prevented: https://github.com/rust-secure-code/cargo-geiger/issues/113
# --target-dir target/geiger
