#![forbid(unsafe_code)]
#![deny(clippy::all)]

use std::path::{Path, PathBuf};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct WorldMapImages {
    pub detailed: Option<PathBuf>,
    pub world_map: Option<PathBuf>,
    pub biome: Option<PathBuf>,
    pub diplomacy: Option<PathBuf>,
    pub drainage: Option<PathBuf>,
    pub elevation: Option<PathBuf>,
    pub elevation_water: Option<PathBuf>,
    pub evil: Option<PathBuf>,
    pub hydrologic: Option<PathBuf>,
    pub nobility: Option<PathBuf>,
    pub rainfall: Option<PathBuf>,
    pub salinity: Option<PathBuf>,
    pub savagery: Option<PathBuf>,
    pub cadaster: Option<PathBuf>,
    pub temperature: Option<PathBuf>,
    pub trade: Option<PathBuf>,
    pub vegetation: Option<PathBuf>,
    pub volcanism: Option<PathBuf>,
}

impl WorldMapImages {
    /// Create a new world map image collection
    pub fn new() -> Self {
        Self::default()
    }

    pub fn some_found(&self) -> bool {
        self.detailed.is_some()
            || self.world_map.is_some()
            || self.biome.is_some()
            || self.diplomacy.is_some()
            || self.drainage.is_some()
            || self.elevation.is_some()
            || self.elevation_water.is_some()
            || self.evil.is_some()
            || self.hydrologic.is_some()
            || self.nobility.is_some()
            || self.rainfall.is_some()
            || self.salinity.is_some()
            || self.savagery.is_some()
            || self.cadaster.is_some()
            || self.temperature.is_some()
            || self.trade.is_some()
            || self.vegetation.is_some()
            || self.volcanism.is_some()
    }
}

pub fn parse_world_map_images(files: WorldMapImages) -> df_st_core::WorldMapImages {
    let mut world_map_images = df_st_core::WorldMapImages::new();

    world_map_images.detailed = create_map_image(&files.detailed, "Detailed map", 0);
    world_map_images.world_map = create_map_image(&files.world_map, "World map", 1);
    world_map_images.biome = create_map_image(&files.biome, "Biome map", 2);
    world_map_images.diplomacy = create_map_image(&files.diplomacy, "Diplomacy map", 3);
    world_map_images.drainage = create_map_image(&files.drainage, "Drainage map", 4);
    world_map_images.elevation = create_map_image(&files.elevation, "Elevation map", 5);
    world_map_images.elevation_water =
        create_map_image(&files.elevation_water, "Elevation map with water", 6);
    world_map_images.evil = create_map_image(&files.evil, "Evil map", 7);
    world_map_images.hydrologic = create_map_image(&files.hydrologic, "Hydrological map", 8);
    world_map_images.nobility = create_map_image(&files.nobility, "Nobility map", 9);
    world_map_images.rainfall = create_map_image(&files.rainfall, "Rainfall map", 10);
    world_map_images.salinity = create_map_image(&files.salinity, "Salinity map", 11);
    world_map_images.savagery = create_map_image(&files.savagery, "Savagery map", 12);
    world_map_images.cadaster = create_map_image(&files.cadaster, "Cadaster map", 13);
    world_map_images.temperature = create_map_image(&files.temperature, "Temperature map", 14);
    world_map_images.trade = create_map_image(&files.trade, "Trade map", 15);
    world_map_images.vegetation = create_map_image(&files.vegetation, "Vegetation map", 16);
    world_map_images.volcanism = create_map_image(&files.volcanism, "Volcanism map", 17);

    world_map_images
}

fn create_map_image(file: &Option<PathBuf>, name: &str, id: i32) -> Option<df_st_core::MapImage> {
    if let Some(file) = file {
        return Some(df_st_core::MapImage {
            id,
            name: name.to_owned(),
            data: load_image(file),
            format: "png".to_owned(),
        });
    }
    None
}

fn load_image(file_path: &Path) -> Vec<u8> {
    use image::{GenericImageView, ImageEncoder};
    let img = image::open(file_path).unwrap();

    let mut data = Vec::new();

    // Encode image as PNG and return bytes.
    let bytes = img.as_bytes();
    let (width, height) = img.dimensions();
    let color = img.color();

    let p = image::codecs::png::PngEncoder::new(&mut data);
    p.write_image(bytes, width, height, color)
        .unwrap_or_else(|_| {
            panic!(
                "Image conversion went wrong for: `{}`.",
                file_path.to_string_lossy()
            )
        });

    data
}
