use rocket::form::{FromForm, FromFormField};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Debug;

/// A Query Guard for pagination of lists of items.
#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema, FromForm)]
pub struct ApiPagination {
    /// Number of the page you want to request. (starting from page 0)
    pub page: Option<u32>,
    /// Number of maximum items per page. (at least 1)
    pub per_page: Option<u32>,
    /// Tag to identify the version of the page requested.
    /// [More info here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag).
    pub etag: Option<String>,
    /// Force the page to be ordered. "asc" = A-Z, "desc" = Z-A.
    pub order: Option<OrderTypes>,
    /// What field should the result be ordered by. Example: "id" or "name".
    /// If the field does not exist it will order by the default field. (usually "id")
    pub order_by: Option<String>,
    /// Allows filtering over a property.
    /// The property has to be of type `String` or `i32`, so it will not work for all properties.
    /// Example: "type" or "year".
    pub filter_by: Option<String>,
    /// The values that is used as the filter. Example: "hf_died" or "1000".
    /// If value is wrong type a `400 Bad Request` will be returned.
    pub filter_value: Option<String>,
    /// Don't load nested items. This will speed up page loads but will not fill in all the data.
    /// Fields that can be used for ordering are usually the only field that are returned.
    /// - `Some(false)` If value is not set, or set to "false" (same result default)
    /// - `Some(true)` If value is set to ""(empty), "true"
    /// - `None` If any other value this will add all the data to the response (default)
    pub minimal_data: Option<bool>,
}

/// Possible ways of sorting data
#[derive(Serialize, Deserialize, Clone, Debug, JsonSchema, FromFormField)]
#[serde(rename_all = "snake_case")]
pub enum OrderTypes {
    Asc,
    Desc,
}

/// From DB order to Api order.
impl From<df_st_db::OrderTypes> for OrderTypes {
    fn from(order: df_st_db::OrderTypes) -> Self {
        match order {
            df_st_db::OrderTypes::Asc => OrderTypes::Asc,
            df_st_db::OrderTypes::Desc => OrderTypes::Desc,
        }
    }
}

/// From Api order to DB order.
impl From<OrderTypes> for df_st_db::OrderTypes {
    fn from(order: OrderTypes) -> Self {
        match order {
            OrderTypes::Asc => df_st_db::OrderTypes::Asc,
            OrderTypes::Desc => df_st_db::OrderTypes::Desc,
        }
    }
}

impl fmt::Display for OrderTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            OrderTypes::Asc => write!(f, "asc"),
            OrderTypes::Desc => write!(f, "desc"),
        }
    }
}
