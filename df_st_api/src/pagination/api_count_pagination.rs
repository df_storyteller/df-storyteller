use rocket::form::FromForm;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

/// A Query Guard for pagination of lists of items.
#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema, FromForm)]
pub struct ApiCountPagination {
    /// Number of the page you want to request. (starting from page 0)
    pub page: Option<u32>,
    /// Number of maximum items per page. (at least 1)
    pub per_page: Option<u32>,
    /// Tag to identify the version of the page requested.
    /// [More info here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag).
    pub etag: Option<String>,
    /// Allows filtering over a property.
    /// The property has to be of type `String` or `i32`, so it will not work for all properties.
    /// Example: "type" or "year".
    pub filter_by: Option<String>,
    /// The values that is used as the filter. Example: "hf_died" or "1000".
    /// If value is wrong type a `400 Bad Request` will be returned.
    pub filter_value: Option<String>,
}
