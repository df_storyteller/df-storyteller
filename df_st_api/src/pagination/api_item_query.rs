use rocket::form::FromForm;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

/// A Query Guard for querying of one item.
#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema, FromForm)]
pub struct ApiItemQuery {
    /// Don't load nested items. This will speed up page loads but will not fill in all the data.
    /// Fields that can be used for ordering are usually the only field that are returned.
    /// - `Some(false)` If value is not set, or set to "false" (same result default)
    /// - `Some(true)` If value is set to ""(empty), "true"
    /// - `None` If any other value this will add all the data to the response (default)
    pub minimal_data: Option<bool>,
}
