use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::ApiObject;
use crate::pagination::{
    ApiCountPage, ApiCountPagination, ApiItem, ApiItemQuery, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use okapi::openapi3::OpenApi;
use rocket::serde::json::Json;
use rocket::{get, State};
use rocket_okapi::{openapi, openapi_get_routes_spec, settings::OpenApiSettings};

impl ApiObject for df_st_core::WrittenContent {
    fn get_type() -> String {
        "written_content".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!("{}/written_contents/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/written_contents", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/written_contents/count", base_url)
    }
}

pub fn get_routes_and_docs(settings: &OpenApiSettings) -> (Vec<rocket::Route>, OpenApi) {
    openapi_get_routes_spec![
        settings: get_written_content,
        list_written_contents,
        get_written_content_count,
    ]
}

/// Request a `WrittenContent` by id.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Written Content")]
#[get("/written_contents/<written_content_id>?<item_query..>")]
pub async fn get_written_content(
    db: DfStDatabase,
    written_content_id: i32,
    item_query: ApiItemQuery,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::WrittenContent>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::WrittenContent>::new(server_info, &item_query);
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        let item_search = df_st_db::WrittenContent::get_from_db(
            conn,
            id_filter!["id" => written_content_id, "world_id" => world_id],
            api_page.get_nested_items(),
        )?;
        if let Some(item) = item_search {
            api_page.wrap(item);
            return Ok(Json(api_page));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a list of all `WrittenContent` in the world.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Written Content")]
#[get("/written_contents?<pagination..>")]
pub async fn list_written_contents(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::WrittenContent>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::WrittenContent>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::WrittenContent, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::WrittenContent::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::WrittenContent::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `WrittenContent` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Written Content")]
#[get("/written_contents/count?<group_by>&<pagination..>")]
pub async fn get_written_content_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::WrittenContent>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::WrittenContent>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::WrittenContent, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::WrittenContent::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::WrittenContent::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}
