use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::ApiObject;
use crate::pagination::{
    ApiCountPage, ApiCountPagination, ApiItem, ApiItemQuery, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use okapi::openapi3::OpenApi;
use rocket::serde::json::Json;
use rocket::{get, State};
use rocket_okapi::{openapi, openapi_get_routes_spec, settings::OpenApiSettings};

impl ApiObject for df_st_core::DFWorldInfo {
    fn get_type() -> String {
        "world_info".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!("{}/worlds_info/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/worlds_info", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/worlds_info/count", base_url)
    }
}

pub fn get_routes_and_docs(settings: &OpenApiSettings) -> (Vec<rocket::Route>, OpenApi) {
    openapi_get_routes_spec![
        settings: get_world_info,
        list_worlds_info,
        get_world_info_count,
    ]
}

/// Request a `DFWorldInfo` by id.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "DF World Info")]
#[get("/worlds_info/<world_id>?<item_query..>")]
pub async fn get_world_info(
    db: DfStDatabase,
    world_id: i32,
    item_query: ApiItemQuery,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::DFWorldInfo>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::DFWorldInfo>::new(server_info, &item_query);

    db.run(move |conn| -> Result<_, _> {
        let item_search = df_st_db::DFWorldInfo::get_from_db(
            conn,
            id_filter!["id" => world_id],
            api_page.get_nested_items(),
        )?;
        if let Some(item) = item_search {
            api_page.wrap(item);
            return Ok(Json(api_page));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a list of all `DFWorldInfo` in the database.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "DF World Info")]
#[get("/worlds_info?<pagination..>")]
pub async fn list_worlds_info(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::DFWorldInfo>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::DFWorldInfo>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::DFWorldInfo, _, _>()?;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::DFWorldInfo::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter![]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::DFWorldInfo::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter![]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `DFWorldInfo` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "DF World Info")]
#[get("/worlds_info/count?<group_by>&<pagination..>")]
pub async fn get_world_info_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::DFWorldInfo>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::DFWorldInfo>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::DFWorldInfo, _, _>()?;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::DFWorldInfo::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter![]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::DFWorldInfo::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter![]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}
