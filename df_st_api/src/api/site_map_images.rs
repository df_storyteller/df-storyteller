use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::ApiObject;
use crate::pagination::{
    ApiCountPage, ApiCountPagination, ApiItem, ApiItemQuery, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::{RawPngImage, ServerInfo};
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use okapi::openapi3::OpenApi;
use rocket::serde::json::Json;
use rocket::{get, State};
use rocket_okapi::{openapi, openapi_get_routes_spec, settings::OpenApiSettings};

impl ApiObject for df_st_core::SiteMapImage {
    fn get_type() -> String {
        "site_map_image".to_owned()
    }
    fn get_item_link(&self, base_url: &str) -> String {
        format!("{}/site_map_images/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &str) -> String {
        format!("{}/site_map_images", base_url)
    }
    fn get_count_link(base_url: &str) -> String {
        format!("{}/site_map_images/count", base_url)
    }
}

pub fn get_routes_and_docs(settings: &OpenApiSettings) -> (Vec<rocket::Route>, OpenApi) {
    openapi_get_routes_spec![
        settings: get_site_map_image,
        list_site_map_images,
        get_site_map_image_count,
        get_site_map_image_png,
    ]
}

/// Request a `SiteMapImage` by id.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Map Image")]
#[get("/site_map_images/<site_map_image_id>?<item_query..>")]
pub async fn get_site_map_image(
    db: DfStDatabase,
    site_map_image_id: i32,
    item_query: ApiItemQuery,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::SiteMapImage>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::SiteMapImage>::new(server_info, &item_query);
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        let item_search = df_st_db::SiteMapImage::get_from_db(
            conn,
            id_filter!["id" => site_map_image_id, "world_id" => world_id],
            api_page.get_nested_items(),
        )?;
        if let Some(item) = item_search {
            api_page.wrap(item);
            return Ok(Json(api_page));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a `SiteMapImage` by id.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Map Image")]
#[get("/site_map_images/<site_map_image_id>/image.png")]
pub async fn get_site_map_image_png<'r>(
    db: DfStDatabase,
    site_map_image_id: i32,
    server_info: &State<ServerInfo>,
) -> Result<RawPngImage, APIErrorNoContent> {
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        let item_search = df_st_db::SiteMapImage::get_from_db(
            conn,
            id_filter!["id" => site_map_image_id, "world_id" => world_id],
            true,
        )?;
        if let Some(item) = item_search {
            return Ok(RawPngImage(item.data));
        }
        Err(APIErrorNoContent::new())
    })
    .await
}

/// Request a list of all `SiteMapImage` in the world.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Map Image")]
#[get("/site_map_images?<pagination..>")]
pub async fn list_site_map_images(
    db: DfStDatabase,
    pagination: ApiPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::SiteMapImage>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::SiteMapImage>::new(&pagination, server_info);
    api_page.match_fields::<df_st_db::SiteMapImage, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteMapImage::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            1,
            None,
            None,
        )?
        .get(0)
        .unwrap()
        .count;

        let result_list = df_st_db::SiteMapImage::get_list_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.get_db_order(),
            api_page.order_by.clone(),
            None,
            api_page.get_nested_items(),
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}

/// Request a counts about `SiteMapImage` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "Site Map Image")]
#[get("/site_map_images/count?<group_by>&<pagination..>")]
pub async fn get_site_map_image_count(
    db: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiCountPagination,
    server_info: &State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::SiteMapImage>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::SiteMapImage>::new(&pagination, server_info);
    api_page.group_by = group_by;
    api_page.match_fields::<df_st_db::SiteMapImage, _, _>()?;
    let world_id = server_info.world_id;

    db.run(move |conn| -> Result<_, _> {
        api_page.total_item_count = df_st_db::SiteMapImage::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            0,
            u32::MAX,
            api_page.group_by.clone(),
            None,
        )?
        .len() as u32;

        let result_list = df_st_db::SiteMapImage::get_count_from_db(
            conn,
            api_page.add_int_filter(id_filter!["world_id" => world_id]),
            api_page.get_string_filter(),
            api_page.page_start,
            api_page.max_page_size,
            api_page.group_by.clone(),
            None,
        )?;

        if api_page.wrap(result_list) {
            return Err(APIErrorNotModified::new());
        }
        Ok(Json(api_page))
    })
    .await
}
