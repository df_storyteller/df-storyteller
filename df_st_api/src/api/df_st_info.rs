use crate::ServerInfo;
use okapi::openapi3::OpenApi;
use rocket::serde::json::Json;
use rocket::{get, State};
use rocket_okapi::{openapi, openapi_get_routes_spec, settings::OpenApiSettings};

pub fn get_routes_and_docs(settings: &OpenApiSettings) -> (Vec<rocket::Route>, OpenApi) {
    openapi_get_routes_spec![settings: get_df_st_info,]
}

/// Request information about DF Storyteller itself.
///
/// ( Since = "0.1.0" )
#[openapi(tag = "DF Storyteller Info")]
#[get("/df_st_info")]
pub async fn get_df_st_info(
    server_info: &State<ServerInfo>,
) -> Result<Json<df_st_core::DFSTInfo>, ()> {
    Ok(Json(df_st_core::DFSTInfo {
        version: env!("CARGO_PKG_VERSION").to_owned(),
        enabled_features: Vec::new(),
        enabled_mods: Vec::new(),
        base_url: server_info.base_url.clone(),
        world_id: server_info.world_id,
    }))
}
