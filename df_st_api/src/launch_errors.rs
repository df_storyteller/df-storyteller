use rocket::error::ErrorKind;

pub fn handle_launch_errors(error: rocket::Error) {
    match error.kind() {
        ErrorKind::Bind(err) => {
            log::error!("{}", err);
            panic!("Address/Port binding error.");
        }
        ErrorKind::Io(err) => {
            log::error!("{}", err);
            let issue = df_st_core::GitIssue::<bool> {
                title: format!("API IO error: {}", err),
                message: "While launching the API we encountered an IO error.".to_owned(),
                labels: vec!["Panic".to_owned()],
                debug_info_string: Some(format!("Error message: `{}`", err,)),
                debug_info_json: None,
                add_steps: true,
                ask_add_files: false,
                include_backtrace: false,
            };
            log::error!("{}", issue.create_message());
            panic!("API IO error.");
        }
        ErrorKind::Config(err) => {
            log::error!("{}", err);
            panic!("Config is not valid.");
        }
        ErrorKind::Collisions(ref collisions) => {
            fn collision_messages<T: std::fmt::Display>(
                kind: &str,
                collisions: &[(T, T)],
            ) -> String {
                collisions
                    .iter()
                    .map(|(route1, route2)| {
                        format!(
                            "Collision ({}) between: \n{}\nand\n{}\n",
                            kind, route1, route2
                        )
                    })
                    .collect::<Vec<String>>()
                    .join("\n")
            }
            let routes_collision_messages = collision_messages("routes", &collisions.routes);
            let catchers_collision_messages = collision_messages("catchers", &collisions.catchers);

            log::error!("Colliding routes: {}", routes_collision_messages);
            log::error!("Colliding catchers: {}", catchers_collision_messages);

            let first_route_pair: (String, String) =
                if let Some(first_collision) = collisions.routes.first() {
                    (first_collision.0.to_string(), first_collision.1.to_string())
                } else if let Some(first_collision) = collisions.catchers.first() {
                    (first_collision.0.to_string(), first_collision.1.to_string())
                } else {
                    panic!("Unknown type of collision: {:#?}", collisions);
                };
            let issue = df_st_core::GitIssue::<bool> {
                title: format!(
                    "API Collision error: {} and {}",
                    first_route_pair.0, first_route_pair.1
                ),
                message: "While launching the API we encountered an Collision error.\n\n\
                This is an error made by the developers, we are sorry."
                    .to_owned(),
                labels: vec!["Panic".to_owned()],
                debug_info_string: Some(routes_collision_messages),
                debug_info_json: None,
                add_steps: false,
                ask_add_files: false,
                include_backtrace: false,
            };
            log::error!("{}", issue.create_message());
            panic!("API Collision error.");
        }
        ErrorKind::FailedFairings(ref errors) => {
            let errored_fairings = errors
                .iter()
                .map(|fairing| fairing.name.to_owned())
                .collect::<Vec<String>>();
            let errors_string = errored_fairings.join("\n");
            log::error!("Fairing error: {}", errors_string);

            let issue = df_st_core::GitIssue::<bool> {
                title: format!(
                    "Fairing error: {}",
                    errored_fairings.first().cloned().unwrap_or_default()
                ),
                message: "While launching the API one of our fairings failed.".to_owned(),
                labels: vec!["Panic".to_owned()],
                debug_info_string: Some(errors_string),
                debug_info_json: None,
                add_steps: false,
                ask_add_files: false,
                include_backtrace: false,
            };
            log::error!("{}", issue.create_message());
            panic!("API Fairing error.");
        }
        ErrorKind::SentinelAborts(ref errors) => {
            log::error!(
                "Sentinel Aborts: {}",
                errors
                    .iter()
                    .map(|sentry| {
                        let (file, line, col) = sentry.location;
                        format!("{} ({}:{}:{})", sentry.type_name, file, line, col)
                    })
                    .collect::<Vec<String>>()
                    .join("\n")
            );
            panic!("Sentinels requested abort");
        }
        ErrorKind::InsecureSecretKey(err) => {
            log::error!("{}", err);
            panic!("The configuration profile is not debug but not secret key is configured.");
        }
        ErrorKind::Shutdown(_, err) => {
            log::error!("{:?}", err);
            panic!("API Error during shutdown.");
        }
        err => {
            log::error!("{}", err);
            let issue = df_st_core::GitIssue::<bool> {
                title: format!("API Unknown error: {}", err),
                message: "While launching the API we encountered an Unknown error.".to_owned(),
                labels: vec!["Panic".to_owned()],
                debug_info_string: None,
                debug_info_json: None,
                add_steps: false,
                ask_add_files: false,
                include_backtrace: false,
            };
            log::error!("{}", issue.create_message());
            panic!("API Unknown error.");
        }
    };
}
