#![forbid(unsafe_code)]
#![deny(clippy::all)]
// This option gives what looks like false warnings on `JsonSchema` derive fields.
#![allow(clippy::field_reassign_with_default)]
#![doc(html_root_url = "https://docs.dfstoryteller.com/rust-docs/")]
#![doc(html_favicon_url = "https://docs.dfstoryteller.com/favicon/favicon-16x16.png")]
#![doc(html_logo_url = "https://docs.dfstoryteller.com/logo.svg")]

//! # DF Storyteller - API Documentation
//!
//! This crate includes all the documentation about the API setup and API calls.
//! For the objects them self look in the [`df_st_core`](df_st_core) crate.
//!
//!

pub mod api;
pub mod api_errors;
pub mod api_objects;
// pub mod graphql;
mod cors;
pub mod item_count;
mod launch_errors;
pub mod pagination;
pub mod server_config;

use anyhow::Error;
use df_st_core::config::RootConfig;
use std::ffi::OsStr;
// use graphql::{MutationRoot, QueryRoot, Schema};
use launch_errors::handle_launch_errors;
use okapi::openapi3::Responses;
use rocket::fs::FileServer;
use rocket::get;
use rocket::http::ContentType;
use rocket::response::content::RawHtml;
use rocket_okapi::gen::OpenApiGenerator;
use rocket_okapi::mount_endpoints_and_merged_docs;
use rocket_okapi::request::{OpenApiFromRequest, RequestHeaderInput};
use rocket_okapi::response::OpenApiResponderInner;
use rocket_okapi::settings::UrlObject;
use rocket_okapi::OpenApiError;
use rocket_okapi::{rapidoc::*, swagger_ui::*};
use rust_embed::RustEmbed;
use serde::Serialize;
use server_config::{set_server_config, ServerInfo};
use std::borrow::Cow;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

#[derive(RustEmbed)]
#[folder = "./pages/"]
struct StaticAssets;

#[derive(RustEmbed)]
#[folder = "./serve-paintings/"]
struct StaticPaintings;

// Can be configured different ways:
// https://api.rocket.rs/v0.4/rocket_contrib/databases/index.html
#[rocket_sync_db_pools::database("df_st_database")]
pub struct DfStDatabase(df_st_db::DbConnection);

impl<'r> OpenApiFromRequest<'r> for DfStDatabase {
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(RequestHeaderInput::None)
    }
}

/// Main information page
#[get("/")]
async fn index_page<'r>() -> Option<RawHtml<Cow<'static, [u8]>>> {
    let asset = StaticAssets::get("index.html")?;
    Some(RawHtml(asset.data))
}

/// Serve other static file from pages folder.
#[get("/static/<file..>")]
async fn static_file<'r>(file: PathBuf) -> Option<(ContentType, Cow<'static, [u8]>)> {
    let filename = file
        .display()
        .to_string()
        // replace the Windows `\` with the unix `/` This effect the windows release build.
        .replace('\\', "/");
    let asset = StaticAssets::get(&filename)?;
    let content_type = file
        .extension()
        .and_then(OsStr::to_str)
        .and_then(ContentType::from_extension)
        .unwrap_or(ContentType::Bytes);

    Some((content_type, asset.data))
}

/// Main Docs page
#[get("/docs")]
async fn index_docs_page<'r>() -> Option<RawHtml<&'static [u8]>> {
    Some(RawHtml(include_bytes!("../docs/index.html")))
}

/// Write A Json object to a file
pub fn write_json_file<P: AsRef<std::path::Path>, C: Serialize>(
    filename: P,
    object: &C,
) -> Result<(), Error> {
    let mut file = File::create(filename)?;
    let json_string = serde_json::to_string_pretty(object)?;
    file.write_all(json_string.as_bytes())?;
    Ok(())
}

pub fn create_serve_paintings_folder() -> Result<(), Error> {
    let folder_name = "./serve-paintings";
    if !Path::new(folder_name).exists() {
        // Print folder name where it creates the serve-paintings folder
        log::info!(
            "Creating `serve-paintings` folder in: {}",
            std::env::current_dir()?
                .to_str()
                .unwrap_or("path contains special characters it could not display.")
        );
        // Create folders
        std::fs::create_dir(folder_name)?;
        std::fs::create_dir(format!("{}/timeline", folder_name))?;
        std::fs::create_dir(format!("{}/timeline/chartjs/", folder_name))?;
        // Create all files inside folder
        let file_list_copy = vec![
            "index.html",
            "paintings.json",
            "timeline/index.html",
            "timeline/chartjs/Chart.min.css",
            "timeline/chartjs/Chart.min.js",
        ];
        for filename in file_list_copy {
            let file_bytes = match StaticPaintings::get(filename) {
                Some(data) => data,
                None => {
                    panic!("Could not get file: {}", filename);
                }
            };
            let mut file = File::create(format!("{}/{}", folder_name, filename))?;
            file.write_all(&file_bytes.data)?;
        }
    }
    Ok(())
}

pub fn create_service(server_config: &RootConfig, world_id: u32) -> rocket::Rocket<rocket::Build> {
    let server_info = ServerInfo::new(server_config, "/api/", world_id);

    log::info!("Your `World_id` = `{}`", server_info.world_id);
    log::info!("Your `Base_url` = `{}`", server_info.base_url);

    // TODO create 'serve-paintings' folder and add the files to this folder that are in the repo.
    // Do this only if the folder does not already exist.

    let mut building_rocket = rocket::custom(set_server_config(server_config))
        .attach(DfStDatabase::fairing())
        .attach(cors::Cors) // TODO
        .register(
            "/",
            rocket::catchers![api_errors::internal_error, api_errors::not_found,],
        )
        .manage(server_info)
        // Index and other static pages that should not be added as part of openapi docs
        // when adding more static pages see: https://github.com/pyros2097/rust-embed/blob/master/examples/rocket.rs
        .mount(
            "/",
            rocket::routes![index_page, static_file, index_docs_page],
        )
        // Add documentation static pages.
        .mount(
            "/docs/swagger-ui/",
            make_swagger_ui(&SwaggerUIConfig {
                url: "../api/openapi.json".to_owned(),
                ..Default::default()
            }),
        )
        .mount(
            "/docs/rapidoc/",
            make_rapidoc(&RapiDocConfig {
                title: Some("DF Storyteller | RapiDoc".to_owned()),
                general: GeneralConfig {
                    spec_urls: vec![UrlObject::new("General", "/api/openapi.json")],
                    heading_text: "DF Storyteller".to_owned(),
                    ..Default::default()
                },
                hide_show: HideShowConfig {
                    allow_spec_url_load: false,
                    allow_spec_file_load: false,
                    ..Default::default()
                },
                ui: UiConfig {
                    header_color: "#9e2b25".to_owned(),
                    primary_color: "#9e2b25".to_owned(),
                    theme: Theme::Dark,
                    ..Default::default()
                },
                layout: LayoutConfig {
                    schema_style: SchemaStyle::Tree,
                    ..Default::default()
                },
                slots: SlotsConfig {
                    logo: Some("/static/logo.png".to_owned()),
                    ..Default::default()
                },
                ..Default::default()
            }),
        )
        // Allow users to put static files in the `serve-paintings` folder and the server will
        // serve them as static pages.
        .mount("/paintings", FileServer::from("./serve-paintings"));
    // Add GraphQL Schema
    // .manage(Schema::new(QueryRoot, MutationRoot))
    // Add GraphQL Routes
    // .mount(
    //     "/graphql/",
    //     rocket::routes![
    //         graphql::graphiql,
    //         graphql::get_graphql_handler,
    //         graphql::post_graphql_handler,
    //         graphql::graphiql_playground,
    //     ],
    // )

    // Add all routes for RESTful API
    let openapi_settings = rocket_okapi::settings::OpenApiSettings::default();
    let main_info_spec = (vec![], main_info_openapi_spec());
    mount_endpoints_and_merged_docs! {
        building_rocket, "/api".to_owned(), openapi_settings,
        "/" => main_info_spec,
        // General
        "/" => api::df_st_info::get_routes_and_docs(&openapi_settings),
        "/" => api::world_info::get_routes_and_docs(&openapi_settings),
        // Main
        "/" => api::artifacts::get_routes_and_docs(&openapi_settings),
        "/" => api::coordinates::get_routes_and_docs(&openapi_settings),
        "/" => api::creatures::get_routes_and_docs(&openapi_settings),
        "/" => api::dance_forms::get_routes_and_docs(&openapi_settings),
        "/" => api::entities::get_routes_and_docs(&openapi_settings),
        "/" => api::entity_populations::get_routes_and_docs(&openapi_settings),
        "/" => api::historical_eras::get_routes_and_docs(&openapi_settings),
        "/" => api::historical_event_collections::get_routes_and_docs(&openapi_settings),
        "/" => api::historical_events::get_routes_and_docs(&openapi_settings),
        "/" => api::historical_figures::get_routes_and_docs(&openapi_settings),
        "/" => api::identities::get_routes_and_docs(&openapi_settings),
        "/" => api::landmasses::get_routes_and_docs(&openapi_settings),
        "/" => api::mountain_peaks::get_routes_and_docs(&openapi_settings),
        "/" => api::musical_forms::get_routes_and_docs(&openapi_settings),
        "/" => api::poetic_forms::get_routes_and_docs(&openapi_settings),
        "/" => api::regions::get_routes_and_docs(&openapi_settings),
        "/" => api::rivers::get_routes_and_docs(&openapi_settings),
        "/" => api::sites::get_routes_and_docs(&openapi_settings),
        "/" => api::underground_regions::get_routes_and_docs(&openapi_settings),
        "/" => api::world_constructions::get_routes_and_docs(&openapi_settings),
        "/" => api::written_content::get_routes_and_docs(&openapi_settings),
        // Images
        "/" => api::map_images::get_routes_and_docs(&openapi_settings),
        "/" => api::site_map_images::get_routes_and_docs(&openapi_settings),
        // Links
        "/" => api::links::link_he_entity::get_routes_and_docs(&openapi_settings),
        "/" => api::links::link_he_hf::get_routes_and_docs(&openapi_settings),
        "/" => api::links::link_he_site::get_routes_and_docs(&openapi_settings),
    };
    building_rocket
}

fn main_info_openapi_spec() -> okapi::openapi3::OpenApi {
    use okapi::openapi3::*;
    OpenApi {
        openapi: OpenApi::default_version(),
        info: Info {
            title: "DF Storyteller".to_owned(),
            description: Some(
                r#"
DF Storyteller is an application for parsing and storing Dwarf Fortress Legends files.
It provides an interface for other apps to visualize the data.
                "#
                .to_owned(),
            ),
            license: Some(License {
                name: "GNU AGPLv3".to_owned(),
                url: Some(
                    "https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/LICENSE"
                        .to_owned(),
                ),
                ..Default::default()
            }),
            contact: Some(Contact {
                name: Some("DF Storyteller".to_owned()),
                url: Some("https://dfstoryteller.com/".to_owned()),
                email: None,
                ..Default::default()
            }),
            version: env!("CARGO_PKG_VERSION").to_owned(),
            ..Default::default()
        },
        ..Default::default()
    }
}

/// Start the Rocket API server that serves the RESTful API, GraphQL and documentation.
pub fn start_server(server_config: &RootConfig, world_id: u32) {
    log::info!("Starting server");

    match create_serve_paintings_folder() {
        Ok(_) => {}
        Err(err) => {
            log::error!("{}", err);
        }
    }

    let launch_result = rocket::execute(create_service(server_config, world_id).launch());
    match launch_result {
        Ok(_service) => log::info!("Rocket shut down gracefully."),
        Err(err) => handle_launch_errors(err),
    };
}

/// The raw data of the signature image.
#[derive(rocket::Responder)]
#[response(status = 200, content_type = "image/png")]
pub struct RawPngImage(pub Vec<u8>);

impl OpenApiResponderInner for RawPngImage {
    fn responses(gen: &mut OpenApiGenerator) -> Result<Responses, OpenApiError> {
        let mut responses = Responses::default();
        let schema = gen.json_schema::<Vec<u8>>();
        rocket_okapi::util::add_schema_response(&mut responses, 200, "image/png", schema)?;
        Ok(responses)
    }
}
