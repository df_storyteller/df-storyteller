/// This trait allows the item to be used as a response.
/// Allows the item to be nested in an `ApiPage` or `ApiItem`.
pub trait ApiObject {
    /// A unique name for the type. All in `snake_case` and singular.
    fn get_type() -> String;
    /// The URL to an item in the api.
    fn get_item_link(&self, base_url: &str) -> String;
    /// The URL to a page (list) in the api.
    /// This URL will be extended with query parameters.
    fn get_page_link(base_url: &str) -> String;
    /// The URL to the count page in the api.
    /// This URL will be extended with query parameters.
    fn get_count_link(base_url: &str) -> String;
}
