use std::fs;
use std::io;
use std::io::{Read, Seek};
use std::path::PathBuf;
use tempfile::Builder;
use zip::read::ZipArchive;

/// Unpack different types of archives.
/// Supported archives: zip
pub fn unpack_archive(file_name: PathBuf) -> (PathBuf, tempfile::TempDir) {
    let temp_dir = get_temp_dir();
    if !file_name.is_file() {
        return (file_name, temp_dir);
    }
    let extension = file_name.extension().unwrap();
    if extension == "zip" {
        log::info!("Unpacking zip archive");
        return (
            unpack_zip_archive(file_name, temp_dir.path().to_path_buf()),
            temp_dir,
        );
    }
    (file_name, temp_dir)
}

/// Crate a temporary folder
fn get_temp_dir() -> tempfile::TempDir {
    Builder::new()
        .prefix("df_st_legends_archive-")
        .tempdir()
        .unwrap()
}

/// Unpack a zip archive into the given folder and return the path of the folder
fn unpack_zip_archive(file_name: PathBuf, unpack_folder: PathBuf) -> PathBuf {
    let file = fs::File::open(&file_name).unwrap();
    let archive = ZipArchive::new(file).unwrap();

    unpack_all_zip_files(archive, unpack_folder)
}

/// Unpack all the files in the zip into the folder.
/// All files are flatten and will be in root of folder
fn unpack_all_zip_files<R>(mut archive: ZipArchive<R>, unpack_folder: PathBuf) -> PathBuf
where
    R: Read + Seek,
{
    for i in 0..archive.len() {
        let mut file = archive.by_index(i).unwrap();
        // TODO this should be fixed later
        // See: https://github.com/zip-rs/zip/issues/181
        // Although we just get the filename out of there, so don't care about the path
        #[allow(deprecated)]
        let file_name_item = file.sanitized_name();
        let mut file_path = unpack_folder.clone();
        file_path.push(file_name_item.file_name().unwrap());

        // Only add files, not directories and if it doesn't already exists
        if !(*file.name()).ends_with('/') && !file_path.exists() {
            let mut outfile = fs::File::create(&file_path).unwrap();
            log::debug!(
                "File {} extracted to \"{}\" ({} bytes)",
                i,
                file_path.display(),
                file.size()
            );
            io::copy(&mut file, &mut outfile).unwrap();
        }
    }
    unpack_folder
}
