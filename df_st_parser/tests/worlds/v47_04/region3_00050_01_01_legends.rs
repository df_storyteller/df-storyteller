use df_st_core::*;

#[allow(dead_code)]
pub fn get_world(mut world: DFWorld) -> DFWorld {
    world.world_info = DFWorldInfo {
        name: Some("Moÿoecamo".to_owned()),
        alternative_name: Some("The Mythical Universe".to_owned()),
        ..Default::default()
    };

    world.regions.add_missing_data(&vec![Region {
        id: 0,
        name: Some("the eviscerated tundra".to_owned()),
        type_: Some("Tundra".to_owned()),
        coords: vec![
            Coordinate {
                x: 0,
                y: 18,
                ..Default::default()
            },
            Coordinate {
                x: 0,
                y: 19,
                ..Default::default()
            },
            Coordinate {
                x: 0,
                y: 20,
                ..Default::default()
            },
            Coordinate {
                x: 0,
                y: 21,
                ..Default::default()
            },
        ],
        evilness: Some("neutral".to_owned()),
        ..Default::default()
    }]);

    world
        .underground_regions
        .add_missing_data(&vec![UndergroundRegion {
            id: 0,
            type_: Some("cavern".to_owned()),
            depth: Some(1),
            coords: vec![
                Coordinate {
                    x: 0,
                    y: 12,
                    ..Default::default()
                },
                Coordinate {
                    x: 0,
                    y: 13,
                    ..Default::default()
                },
                Coordinate {
                    x: 0,
                    y: 14,
                    ..Default::default()
                },
                Coordinate {
                    x: 0,
                    y: 15,
                    ..Default::default()
                },
                Coordinate {
                    x: 0,
                    y: 16,
                    ..Default::default()
                },
            ],
        }]);

    world.artifacts.add_missing_data(&vec![Artifact {
        id: 0,
        name: Some("hexwicked the doomed menace".to_owned()),
        site_id: Some(8),
        item_name: Some("necaterithesa emurasofipu".to_owned()),
        item_type: Some("slab".to_owned()),
        item_description: Some(
            "i am lim iklist bocash, \
        lim the whispers of ruthlessness, \
        once of the underworld.  by ngáthi the sin of crypts, \
        i bind myself to this place."
                .to_owned(),
        ),
        item_mat: Some("satinspar".to_owned()),
        ..Default::default()
    }]);

    world.sites.add_missing_data(&vec![Site {
        id: 1,
        type_: Some("hamlet".to_owned()),
        name: Some("spinepondered".to_owned()),
        coord: Some(Coordinate {
            x: 13,
            y: 14,
            ..Default::default()
        }),
        rectangle: Some(Rectangle {
            id: 0,
            x1: 213,
            y1: 226,
            x2: 229,
            y2: 242,
        }),
        structures: vec![Structure {
            site_id: 0,
            local_id: 0,
            type_: Some("mead hall".to_owned()),
            name: Some("The Bean of Hay".to_owned()),
            name2: Some("Seto Thel".to_owned()),
            inhabitant_hf_ids: vec![723],
            ..Default::default()
        }],
        site_properties: vec![SiteProperty {
            site_id: 0,
            local_id: 0,
            structure_id: Some(2),
            owner_hf_id: Some(442),
            ..Default::default()
        }],
        civ_id: Some(26),
        cur_owner_id: Some(27),
    }]);

    world
        .world_constructions
        .add_missing_data(&vec![WorldConstruction {
            id: 1,
            name: Some("The Pure Road".to_owned()),
            type_: Some("road".to_owned()),
            coords: vec![
                Coordinate {
                    x: 2,
                    y: 28,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 27,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 26,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 25,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 24,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 23,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 22,
                    ..Default::default()
                },
                Coordinate {
                    x: 2,
                    y: 21,
                    ..Default::default()
                },
            ],
        }]);

    world
        .entity_populations
        .add_missing_data(&vec![EntityPopulation {
            id: 0,
            civ_id: Some(0),
            races: vec!["rodent man:196".to_owned()],
        }]);

    world
        .historical_figures
        .add_missing_data(&vec![HistoricalFigure {
            id: 1,
            name: Some("klafajeedis".to_owned()),
            race: Some("kobold".to_owned()),
            race_id: Some("KOBOLD".to_owned()),
            caste: Some("male".to_owned()),
            appeared: Some(1),
            birth_year: Some(-84),
            birth_seconds72: Some(-1),
            death_year: Some(-1),
            death_seconds72: Some(-1),
            associated_type: Some("bowman".to_owned()),
            entity_link: vec![HFEntityLink {
                hf_id: 0,
                entity_id: 24,
                link_type: Some("former member".to_owned()),
                link_strength: Some(82),
            }],
            entity_position_link: vec![
                HFEntityPositionLink {
                    id: 0,
                    hf_id: 0,
                    entity_id: 26,
                    position_profile_id: Some(3),
                    start_year: Some(4),
                    end_year: Some(21),
                },
                HFEntityPositionLink {
                    id: 0,
                    hf_id: 0,
                    entity_id: 446,
                    position_profile_id: Some(0),
                    start_year: Some(37),
                    end_year: None,
                },
            ],
            site_link: vec![HFSiteLink {
                hf_id: 0,
                site_id: 1517,
                link_type: Some("lair".to_owned()),
                ..Default::default()
            }],
            sphere: vec!["murder".to_owned(), "death".to_owned()],
            interaction_knowledge: vec!["SECRET_2".to_owned(), "SECRET_3".to_owned()],
            deity: Some("".to_owned()),
            goal: Some("rule the world".to_owned()),
            relationship_profile_hf: vec![
                HFRelationshipProfileHF {
                    hf_id: 0,
                    hf_id_other: 8316,
                    historical: true,
                    love: Some(0),
                    respect: Some(0),
                    trust: Some(-50),
                    loyalty: Some(0),
                    fear: Some(0),
                    ..Default::default()
                },
                HFRelationshipProfileHF {
                    hf_id: 0,
                    hf_id_other: 100,
                    visual: true,
                    meet_count: Some(0),
                    last_meet_year: Some(-1),
                    last_meet_seconds72: Some(1048592),
                    love: Some(-100),
                    respect: Some(-50),
                    trust: Some(-100),
                    loyalty: Some(0),
                    fear: Some(-25),
                    ..Default::default()
                },
            ],
            intrigue_actor: vec![HFIntrigueActor {
                hf_id: 0,
                local_id: 0,
                hf_id_other: Some(100),
                role: Some("suspected criminal".to_owned()),
                strategy: Some("avoid".to_owned()),
                ..Default::default()
            }],
            intrigue_plot: vec![
                HFIntriguePlot {
                    hf_id: 0,
                    local_id: 0,
                    type_: Some("attain rank".to_owned()),
                    on_hold: Some(true),
                    ..Default::default()
                },
                HFIntriguePlot {
                    hf_id: 0,
                    local_id: 1,
                    type_: Some("grow funding network".to_owned()),
                    plot_actor: vec![HFPlotActor {
                        hf_id: 0,
                        local_id: 0,
                        actor_id: 8,
                        plot_role: Some("source of funds".to_owned()),
                        agreement_id: Some(138),
                        ..Default::default()
                    }],
                    ..Default::default()
                },
            ],
            ent_pop_id: Some(24),
            entity_reputation: vec![HFEntityReputation {
                hf_id: 0,
                entity_id: 257,
                ..Default::default()
            }],
            vague_relationship: vec![HFVagueRelationship {
                hf_id: 0,
                hf_id_other: 7769,
                war_buddy: Some(true),
                ..Default::default()
            }],
            entity_squad_link: vec![HFEntitySquadLink {
                hf_id: 0,
                entity_id: 629,
                squad_id: 14,
                squad_position: Some(1),
                start_year: Some(242),
                end_year: None,
            }],
            honor_entity: vec![HFHonorEntity {
                hf_id: 0,
                entity_id: 811,
                battles: Some(1),
                honor_id: vec![2],
                kills: Some(1),
            }],
            site_property: vec![HFSiteProperty {
                hf_id: 0,
                site_id: 222,
                property_id: 2,
            }],
            used_identity_id: Some(0),
            skills: vec![HFSkill {
                hf_id: 0,
                skill: "MINING".to_owned(),
                total_ip: Some(9000),
            }],
            links: vec![HFLink {
                hf_id: 0,
                hf_id_other: 129,
                link_type: Some("child".to_owned()),
                ..Default::default()
            }],
            ..Default::default()
        }]);

    world.mountain_peaks.add_missing_data(&vec![MountainPeak {
        id: 0,
        name: Some("The Finger of Growing".to_owned()),
        coord: Some(Coordinate {
            x: 75,
            y: 64,
            ..Default::default()
        }),
        height: Some(265),
        is_volcano: Some(false),
    }]);

    world.landmasses.add_missing_data(&vec![Landmass {
        id: 0,
        name: Some("The Wispy Continents".to_owned()),
        coord_1: Some(Coordinate {
            x: 0,
            y: 0,
            ..Default::default()
        }),
        coord_2: Some(Coordinate {
            x: 32,
            y: 32,
            ..Default::default()
        }),
    }]);

    world.dance_forms.add_missing_data(&vec![DanceForm { id: 0,
        name: Some("The West Wheels".to_owned()),
        description: Some("The West Wheels is a sacred solo dance originating in The Nation of Discoveries.  \
        The for\nm guides dancers during improvised performances.  The dance is accompanied by \
        The East-Wanderer of Wheeling as the dan\ncer acts out the story of Ukap the North Trades.  \
        The dancer performs slower and slower with the music.  The dance is \npunctuated by strong \
        raised right arms and sluggish forward bends.  [B]The dance begins with the introduction of \
        the m\nusic.  This section is grotesque.  [B]The dance enters a new section with the theme \
        of the music.  This section is slu\nggish.  [B]The dance enters a new section with the \
        exposition of the theme of the music.  This aggressive section is p\nunctuated by twisting \
        sway and fluid left leg lifts.  [B]The dance enters a new section with the bridge-passage of \
        the\n music.  This flamboyant section is punctuated by lively curved walks, \
        grotesque facial expressions and sinuous footwo\nrk.  [B]The dance enters a new section \
        with the recapitulation of the theme of the music.  This vigorous section is pu\nnctuated \
        by spirited straight walks and powerful spins.  [B]The dance enters a new section with the \
        coda of the music.\n  This flamboyant section is punctuated by proud leaps.".to_owned()) }]);

    world.musical_forms.add_missing_data(&vec![MusicalForm { id: 0,
        name: Some("The East Wanderer of Wheeling".to_owned()),
        description: Some("The Bewildering Lark is a form of music used for entertainment originating \
        in The Nation \nof Discoveries.  The rules of the form are applied by composers to produce \
        individual pieces of music which can be \nperformed.  The music is played on a fathri.  \
        The entire performance is consistently slowing, \
        and it is to be soft.  \nThe melody has short phrases throughout the form.  Only one pitch \
        is ever played at a time.  It is performed without \npreference for a scale and in free \
        rhythm.  [B]The fathri always does the main melody and should be broad.  \n[B]The \
        Bewildering Lark has the following structure: an introduction and three unrelated passages.".to_owned()) }]);

    world.poetic_forms.add_missing_data(&vec![PoeticForm { id: 0,
        name: Some("The Bewildering Meadow".to_owned()),
        description: Some("The Bewildering Meadow is a poetic narrative, \
        originating in The Nation of Discoveries.  \nThe rules of the form are applied by poets to \
        produce individual poems which can be recited.  The poem is divided into\n a line and four \
        to six tercets.  The Bewildering Meadow is always written from the perspective of a skunk.  \
        Use of si\nmile is characteristic of the form.  [B]The first part is intended to describe \
        the past.  It has three feet with a ton\ne pattern of uneven-uneven.  [B]The second part is \
        intended to develop the previous idea concerning the future.  The t\nhird line of each \
        tercet must expand the idea of the first line.  It has lines with five feet with a tone \
        pattern of u\nneven-uneven.  The rhyme scheme within each stanza is aab.".to_owned()) }]);

    world
        .written_contents
        .add_missing_data(&vec![WrittenContent {
            id: 0,
            title: Some("The Birth of Fanciness".to_owned()),
            author_hf_id: Some(85),
            author_roll: Some(12),
            form: Some("poem".to_owned()),
            form_id: Some(7),
            page_start: Some(1),
            page_end: Some(1),
            type_: Some("Poem".to_owned()),
            style: vec![
                WCStyle {
                    written_content_id: 0,
                    local_id: 0,
                    label: Some("mechanical".to_owned()),
                    weight: Some(1),
                },
                WCStyle {
                    written_content_id: 0,
                    local_id: 1,
                    label: Some("vicious".to_owned()),
                    weight: Some(1),
                },
            ],
            author: Some(85),
            reference: vec![WCReference {
                written_content_parent_id: 0,
                local_id: 0,
                type_: Some("WRITTEN_CONTENT".to_owned()),
                type_id: Some(2),
                written_content_id: Some(2),
                ..Default::default()
            }],
        }]);

    world.historical_eras.add_missing_data(&vec![HistoricalEra {
        id: 0,
        name: Some("Age of Hydra and Tundra Titan".to_owned()),
        start_year: Some(-1),
    }]);

    world.rivers.add_missing_data(&vec![River {
        id: 0,
        name: Some("The Fabulous Steam".to_owned()),
        path: vec![
            Path {
                id: 0,
                x: 1,
                y: 5,
                flow: 50,
                exit_tile: 8,
                elevation: 114,
            },
            Path {
                id: 0,
                x: 1,
                y: 6,
                flow: 100,
                exit_tile: 13,
                elevation: 114,
            },
            Path {
                id: 0,
                x: 0,
                y: 6,
                flow: 150,
                exit_tile: 12,
                elevation: 114,
            },
        ],
        end_pos: Some(Coordinate {
            x: -1,
            y: 6,
            ..Default::default()
        }),
    }]);

    world.creatures.add_missing_data(&vec![Creature {
        id: 0,
        creature_id: Some("TOAD".to_owned()),
        name_singular: Some("toad".to_owned()),
        name_plural: Some("toads".to_owned()),
        biomes: CreatureBiome {
            cr_id: 0,
            desert_badland: Some(false),
            desert_rock: Some(false),
            desert_sand: Some(false),
            forest_taiga: Some(false),
            forest_temperate_broadleaf: Some(false),
            forest_temperate_conifer: Some(false),
            forest_tropical_conifer: Some(false),
            forest_tropical_dry_broadleaf: Some(false),
            forest_tropical_moist_broadleaf: Some(false),
            glacier: Some(false),
            grassland_temperate: Some(false),
            grassland_tropical: Some(false),
            lake_temperate_brackishwater: Some(false),
            lake_temperate_freshwater: Some(false),
            lake_temperate_saltwater: Some(false),
            lake_tropical_brackishwater: Some(false),
            lake_tropical_freshwater: Some(false),
            lake_tropical_saltwater: Some(false),
            marsh_temperate_freshwater: Some(false),
            marsh_temperate_saltwater: Some(false),
            marsh_tropical_freshwater: Some(false),
            marsh_tropical_saltwater: Some(false),
            mountain: Some(false),
            ocean_arctic: Some(false),
            ocean_temperate: Some(false),
            ocean_tropical: Some(false),
            pool_temperate_brackishwater: Some(true),
            pool_temperate_freshwater: Some(true),
            pool_temperate_saltwater: Some(true),
            pool_tropical_brackishwater: Some(true),
            pool_tropical_freshwater: Some(true),
            pool_tropical_saltwater: Some(true),
            river_temperate_brackishwater: Some(false),
            river_temperate_freshwater: Some(false),
            river_temperate_saltwater: Some(false),
            river_tropical_brackishwater: Some(false),
            river_tropical_freshwater: Some(false),
            river_tropical_saltwater: Some(false),
            savanna_temperate: Some(false),
            savanna_tropical: Some(false),
            shrubland_temperate: Some(false),
            shrubland_tropical: Some(false),
            subterranean_chasm: Some(false),
            subterranean_lava: Some(false),
            subterranean_water: Some(false),
            swamp_mangrove: Some(false),
            swamp_temperate_freshwater: Some(false),
            swamp_temperate_saltwater: Some(false),
            swamp_tropical_freshwater: Some(false),
            swamp_tropical_saltwater: Some(false),
            tundra: Some(false),
        },
        all_castes_alive: Some(true),
        artificial_hiveable: Some(false),
        does_not_exist: Some(false),
        equipment: Some(false),
        equipment_wagon: Some(false),
        evil: Some(false),
        fanciful: Some(false),
        generated: Some(false),
        good: Some(false),
        has_any_benign: Some(false),
        has_any_can_swim: Some(true),
        has_any_cannot_breathe_air: Some(false),
        has_any_cannot_breathe_water: Some(false),
        has_any_carnivore: Some(false),
        has_any_common_domestic: Some(false),
        has_any_curious_beast: Some(false),
        has_any_demon: Some(false),
        has_any_feature_beast: Some(false),
        has_any_flier: Some(false),
        has_any_fly_race_gait: Some(false),
        has_any_grasp: Some(false),
        has_any_grazer: Some(false),
        has_any_has_blood: Some(true),
        has_any_immobile: Some(false),
        has_any_intelligent_learns: Some(false),
        has_any_intelligent_speaks: Some(false),
        has_any_large_predator: Some(false),
        has_any_local_pops_controllable: Some(false),
        has_any_local_pops_produce_heroes: Some(false),
        has_any_megabeast: Some(false),
        has_any_mischievous: Some(false),
        has_any_natural_animal: Some(true),
        has_any_night_creature: Some(false),
        has_any_night_creature_bogeyman: Some(false),
        has_any_night_creature_experimenter: Some(false),
        has_any_night_creature_hunter: Some(false),
        has_any_night_creature_nightmare: Some(false),
        has_any_not_fireimmune: Some(true),
        has_any_not_flier: Some(false),
        has_any_not_living: Some(false),
        has_any_outsider_controllable: Some(false),
        has_any_power: Some(false),
        has_any_race_gait: Some(true),
        has_any_semimegabeast: Some(false),
        has_any_slow_learner: Some(false),
        has_any_supernatural: Some(false),
        has_any_titan: Some(false),
        has_any_unique_demon: Some(false),
        has_any_utterances: Some(false),
        has_any_vermin_hateable: Some(true),
        has_any_vermin_micro: Some(false),
        has_female: Some(true),
        has_male: Some(true),
        large_roaming: Some(false),
        loose_clusters: Some(false),
        mates_to_breed: Some(true),
        mundane: Some(true),
        occurs_as_entity_race: Some(false),
        savage: Some(false),
        small_race: Some(true),
        two_genders: Some(true),
        ubiquitous: Some(false),
        vermin_eater: Some(false),
        vermin_fish: Some(false),
        vermin_grounder: Some(true),
        vermin_rotter: Some(false),
        vermin_soil: Some(false),
        vermin_soil_colony: Some(false),
    }]);

    world.identities.add_missing_data(&vec![Identity {
        id: 0,
        name: Some("Lim the Whispers of Ruthlessness".to_owned()),
        hf_id: Some(78),
        birth_year: Some(-1000000001),
        birth_second: Some(-1000000001),
        entity_id: None,
        ..Default::default()
    }]);

    world.entities.add_missing_data(&vec![Entity {
        id: 0,
        name: Some("shrokostlaynkin".to_owned()),
        honor: vec![EntityHonor {
            local_id: 0,
            name: Some("Captain".to_owned()),
            gives_precedence: Some(100),
            requires_any_melee_or_ranged_skill: Some(true),
            required_skill_ip_total: Some(12600),
            required_battles: Some(4),
            exempt_ep_id: Some(0),
            exempt_former_ep_id: Some(0),
            ..Default::default()
        }],
        race: Some("rodent man".to_owned()),
        type_: Some("civilization".to_owned()),
        entity_link: vec![EntityLink {
            local_id: 0,
            type_: Some("CHILD".to_owned()),
            target: Some(25),
            strength: Some(100),
        }],
        entity_position: vec![EntityPosition {
            local_id: 0,
            name: Some("law-giver".to_owned()),
            ..Default::default()
        }],
        entity_position_assignment: vec![EntityPositionAssignment {
            local_id: 0,
            hf_id: Some(723),
            position_id: Some(0),
            squad_id: None,
        }],
        hf_ids: vec![40],
        child_en_ids: vec![24],
        occasion: vec![EntityOccasion {
            local_id: 0,
            name: Some("The Radiant Festival".to_owned()),
            event: Some(1918),
            schedule: vec![EntityOccasionSchedule {
                local_id: 0,
                type_: Some("musical_performance".to_owned()),
                reference: Some(1),
                reference2: None,
                ..Default::default()
            }],
        }],
        ..Default::default()
    }]);

    world.historical_events.add_missing_data(&vec![
        HistoricalEvent {
            id: 1,
            type_: Some("item_stolen".to_owned()),
            year: Some(3),
            seconds72: Some(-1),
            circumstance: Some("historical event collection".to_owned()),
            circumstance_id: Some(12),
            circumstance_obj: Some(HECircumstance {
                type_: Some("233".to_owned()),
                hec_id: Some(12),
                ..Default::default()
            }),
            entity_id: Some(31),
            hf_id: Some(41),
            item_subtype: Some("-1".to_owned()),
            item_type: Some("figurine".to_owned()),
            mat: Some("chimpanzee bone".to_owned()),
            reason_obj: Some(HEReason {
                type_: Some("none".to_owned()),
                ..Default::default()
            }),
            site_id: Some(6),
            structure_id: Some(-1),
            ..Default::default()
        },
        HistoricalEvent {
            id: 0,
            type_: Some("hf_relationship".to_owned()),
            year: Some(1),
            seconds72: None,
            source_hf_id: Some(46),
            target_hf_id: Some(45),
            relationship: None, // TODO was see int to enum: Some("4".to_owned()),
            occasion_id: Some(245),
            site_id: Some(10),
            // unk_1: Some(81),
            ..Default::default()
        },
    ]);

    world
        .historical_event_collections
        .add_missing_data(&vec![HistoricalEventCollection {
            id: 0,
            type_: Some("beast attack".to_owned()),
            start_year: Some(1),
            start_seconds72: Some(84000),
            end_year: Some(1),
            end_seconds72: Some(84000),
            coords: Some("32,32".to_owned()),
            defending_en_id: Some(25),
            he_ids: vec![140, 141, 142, 143, 144],
            feature_layer_id: Some(-1),
            ordinal: Some(1),
            parent_hec_id: Some(-1),
            site_id: Some(2),
            subregion_id: Some(-1),
            ..Default::default()
        }]);

    world
}
