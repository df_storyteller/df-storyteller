pub mod artifacts;
pub mod creatures;
pub mod dance_forms;
pub mod entities;
pub mod entity_populations;
pub mod historical_eras;
pub mod identities;
pub mod landmasses;
pub mod map_images;
pub mod mountain_peaks;
pub mod musical_forms;
pub mod poetic_forms;
pub mod regions;
pub mod rivers;
pub mod site_map_images;
pub mod sites;

pub use artifacts::*;
pub use creatures::*;
pub use dance_forms::*;
pub use entities::*;
pub use entity_populations::*;
pub use historical_eras::*;
pub use identities::*;
pub use landmasses::*;
pub use map_images::*;
pub use mountain_peaks::*;
pub use musical_forms::*;
pub use poetic_forms::*;
pub use regions::*;
pub use rivers::*;
pub use site_map_images::*;
pub use sites::*;

use df_st_core::item_count::ItemCount;

#[allow(dead_code)]
pub fn get_count(count: u32) -> Vec<ItemCount> {
    vec![ItemCount {
        count,
        value: serde_json::json!("total"),
    }]
}
