use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_entity_populations() -> Vec<EntityPopulation> {
    vec![
        EntityPopulation {
            // Only use values from legends.xml
            id: 0,
            civ_id: None,
            races: vec![],
        },
        EntityPopulation {
            // Only use values from legends_plus.xml
            id: 1,
            civ_id: Some(1),
            races: vec!["rodent man:301".to_owned()],
        },
        EntityPopulation {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        EntityPopulation {
            // Full merge
            id: 3,
            civ_id: Some(3),
            races: vec!["bat_man:427".to_owned()],
        },
        EntityPopulation {
            // missing ID
            id: 4,
            ..Default::default()
        },
        EntityPopulation {
            id: 5,
            civ_id: None,
            races: vec!["serpent_man:780".to_owned(), "ant_man:310".to_owned()],
        },
        EntityPopulation {
            // empty strings
            id: 6,
            races: vec!["".to_owned()],
            ..Default::default()
        },
        EntityPopulation::new_by_id(7),
        EntityPopulation {
            id: 8,
            civ_id: Some(9),
            races: vec!["bat_man:85".to_owned()],
        },
        EntityPopulation {
            id: 9,
            civ_id: Some(2),
            races: vec!["human:2107".to_owned(), "human:27".to_owned()],
        },
        EntityPopulation {
            id: 10,
            civ_id: Some(3),
            races: vec![
                "goblin:222".to_owned(),
                "human:27".to_owned(),
                "bat_man:24".to_owned(),
                "serpent_man:445".to_owned(),
            ],
        },
    ]
}
