use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_landmasses() -> Vec<Landmass> {
    vec![
        Landmass {
            // Only use values from legends.xml
            id: 0,
            ..Default::default()
        },
        Landmass {
            // Only use values from legends_plus.xml
            id: 1,
            name: Some("The Island of Aquamarines".to_owned()),
            coord_1: Some(Coordinate {
                x: 3,
                y: 2,
                ..Default::default()
            }),
            coord_2: Some(Coordinate {
                x: 9,
                y: 5,
                ..Default::default()
            }),
        },
        Landmass {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Landmass {
            // Full merge
            id: 3,
            name: Some("The Continents of Beaching".to_owned()),
            coord_1: Some(Coordinate {
                x: 1,
                y: 3,
                ..Default::default()
            }),
            coord_2: Some(Coordinate {
                x: 63,
                y: 63,
                ..Default::default()
            }),
        },
        Landmass {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Landmass {
            // Set ID's to `-1`
            id: 5,
            name: None,
            coord_1: Some(Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }),
            coord_2: Some(Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }),
        },
        Landmass {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            ..Default::default()
        },
        Landmass::new_by_id(7),
        Landmass {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("The Island of 😈𐅋".to_owned()),
            ..Default::default()
        },
        Landmass {
            id: 9,
            name: Some("The Land of Rhyming".to_owned()),
            coord_1: Some(Coordinate {
                x: 4,
                y: 59,
                ..Default::default()
            }),
            coord_2: Some(Coordinate {
                x: 5,
                y: 60,
                ..Default::default()
            }),
        },
        Landmass {
            id: 10,
            name: Some("The Island of Shanks".to_owned()),
            coord_1: Some(Coordinate {
                x: 28,
                y: 59,
                ..Default::default()
            }),
            coord_2: Some(Coordinate {
                x: 28,
                y: 59,
                ..Default::default()
            }),
        },
    ]
}
