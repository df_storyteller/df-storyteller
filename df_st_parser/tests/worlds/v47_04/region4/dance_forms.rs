use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_dance_forms() -> Vec<DanceForm> {
    vec![
        DanceForm {
            // Only use values from legends.xml
            id: 0,
            name: None,
            description: Some("The West Wheels is a sacred solo dance originating in The Nation of \
            Discoveries.  The form guides dancers during improvised performances.  The dance is \
            accompanied by The East-Wanderer of Wheeling as the dancer acts out the story of Ukap \
            the North Trades.  The dancer performs slower and slower with the music.  The dance is \
            punctuated by strong raised right arms and sluggish forward bends.  [B]The dance begins \
            with the introduction of the music.  This section is grotesque.  [B]The dance enters a \
            new section with the theme of the music.  This section is sluggish.  [B]The dance \
            enters a new section with the exposition of the theme of the music.  This aggressive \
            section is punctuated by twisting sway and fluid left leg lifts.  [B]The dance enters \
            a new section with the bridge-passage of the music.  This flamboyant section is \
            punctuated by lively curved walks, grotesque facial expressions and sinuous footwork.  \
            [B]The dance enters a new section with the recapitulation of the theme of the music.  \
            This vigorous section is punctuated by spirited straight walks and powerful spins.  \
            [B]The dance enters a new section with the coda of the music.  This flamboyant section \
            is punctuated by proud leaps.".to_owned()),
        },
        DanceForm {
            // Only use values from legends_plus.xml
            id: 1,
            name: Some("The West Wheels".to_owned()),
            description: None,
        },
        DanceForm {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        DanceForm {
            // Full merge
            id: 3,
            name: Some("test for trim too".to_owned()),
            description: Some("test for some trim here too".to_owned()),
        },
        DanceForm {
            // missing ID
            id: 4,
            ..Default::default()
        },
        DanceForm {
            id: 5,
            name: Some("malesuada".to_owned()),
            description: Some("Mauris".to_owned()),
        },
        DanceForm {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            description: Some("".to_owned()),
        },
        DanceForm::new_by_id(7),
        DanceForm {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("jus😈to𐅋".to_owned()),
            description: Some("These are cp437 chars: ê å ╢ Θ".to_owned()),
        },
        DanceForm {
            id: 9,
            name: Some("rhoncus".to_owned()),
            description: Some("condientum".to_owned()),
        },
        DanceForm {
            id: 10,
            name: Some("Praesent".to_owned()),
            description: Some("vulputate".to_owned()),
        },
    ]
}
