use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_identities() -> Vec<Identity> {
    vec![
        Identity {
            // Only use values from legends.xml
            id: 0,
            name: None,
            hf_id: None,
            race: None,
            caste: None,
            birth_year: None,
            birth_second: None,
            profession: None,
            entity_id: None,
            nemesis_id: None,
        },
        Identity {
            // Only use values from legends_plus.xml
            id: 1,
            name: Some("Skulldeaths".to_owned()),
            hf_id: Some(3),
            race: Some("human".to_owned()),
            caste: Some("female".to_owned()),
            birth_year: Some(45),
            birth_second: Some(12),
            profession: Some("peddler".to_owned()),
            entity_id: Some(5),
            nemesis_id: Some(4),
        },
        Identity {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Identity {
            // Full merge
            id: 3,
            name: Some("The Sienna Wisp".to_owned()),
            hf_id: Some(1),
            race: Some("dwarf".to_owned()),
            caste: Some("male".to_owned()),
            birth_year: Some(7),
            birth_second: Some(9),
            profession: Some("monster_slayer".to_owned()),
            entity_id: Some(9),
            nemesis_id: Some(8),
        },
        Identity {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Identity {
            // Set ID's to `-1`
            id: 5,
            name: None,
            hf_id: None,
            race: None,
            caste: None,
            birth_year: Some(-1),
            birth_second: Some(-1),
            profession: None,
            entity_id: None,
            nemesis_id: None,
        },
        Identity {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            race: Some("".to_owned()),
            caste: Some("".to_owned()),
            profession: Some("".to_owned()),
            ..Default::default()
        },
        Identity::new_by_id(7),
        Identity {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("Cilob Washorb😈m𐅋".to_owned()),
            race: Some("Cilob 🌷".to_owned()),
            caste: Some("Cilob 🌹".to_owned()),
            profession: Some("Cilob ⁉".to_owned()),
            ..Default::default()
        },
        Identity {
            id: 9,
            name: Some("Asob Urncharcoal".to_owned()),
            race: Some("dwarf".to_owned()),
            caste: Some("male".to_owned()),
            ..Default::default()
        },
        Identity {
            id: 10,
            name: Some("Bustsneaked the God-forsaken".to_owned()),
            hf_id: Some(10),
            birth_year: Some(-1000000001),
            birth_second: Some(-1000000001),
            entity_id: None,
            ..Default::default()
        },
    ]
}
