use df_st_core::*;

fn get_control_image(file_path: String) -> Vec<u8> {
    std::fs::read(file_path).unwrap()
}

#[allow(dead_code)]
pub fn get_site_maps() -> Vec<Option<SiteMapImage>> {
    let base_path = "./tests/worlds/v47_04/region4/control_images";
    vec![
        None,
        None,
        None,
        Some(SiteMapImage {
            id: 3,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-3.png", base_path)),
            format: "png".to_owned(),
        }),
        Some(SiteMapImage {
            id: 4,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-4.png", base_path)),
            format: "png".to_owned(),
        }),
        Some(SiteMapImage {
            id: 5,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-5.png", base_path)),
            format: "png".to_owned(),
        }),
        Some(SiteMapImage {
            id: 6,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-6.png", base_path)),
            format: "png".to_owned(),
        }),
        None,
        Some(SiteMapImage {
            id: 8,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-8.png", base_path)),
            format: "png".to_owned(),
        }),
        Some(SiteMapImage {
            id: 9,
            data: get_control_image(format!("{}/region4-00050-01-01-site_map-9.png", base_path)),
            format: "png".to_owned(),
        }),
    ]
}
