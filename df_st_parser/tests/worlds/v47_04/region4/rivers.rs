use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_rivers() -> Vec<River> {
    vec![
        River {
            // Only use values from legends.xml
            id: 0,
            ..Default::default()
        },
        River {
            // Only use values from legends_plus.xml
            id: 1,
            name: Some("The Fabulous Steam".to_owned()),
            path: vec![
                Path {
                    id: 0,
                    x: 1,
                    y: 5,
                    flow: 50,
                    exit_tile: 8,
                    elevation: 114,
                },
                Path {
                    id: 1,
                    x: 1,
                    y: 6,
                    flow: 100,
                    exit_tile: 13,
                    elevation: 114,
                },
                Path {
                    id: 2,
                    x: 0,
                    y: 6,
                    flow: 150,
                    exit_tile: 12,
                    elevation: 114,
                },
            ],
            end_pos: Some(Coordinate {
                x: 1,
                y: 6,
                ..Default::default()
            }),
        },
        River {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        River {
            // Full merge
            id: 3,
            name: Some("The Future Bread".to_owned()),
            path: vec![Path {
                id: 3,
                x: 7,
                y: 4,
                flow: 0,
                exit_tile: 8,
                elevation: 99,
            }],
            end_pos: Some(Coordinate {
                x: 7,
                y: 6,
                ..Default::default()
            }),
        },
        River {
            // missing ID
            id: 4,
            ..Default::default()
        },
        River {
            // Set ID's to `-1`
            id: 5,
            path: vec![Path {
                id: 4,
                x: -1,
                y: -1,
                flow: -1,
                exit_tile: -1,
                elevation: -1,
            }],
            end_pos: Some(Coordinate {
                x: -1,
                y: -1,
                ..Default::default()
            }),
            ..Default::default()
        },
        River {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            path: vec![],
            end_pos: None,
        },
        River::new_by_id(7),
        River {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("The ê å ╢ Θ".to_owned()),
            ..Default::default()
        },
        River {
            id: 9,
            name: Some("Grainpurge the Tangled Spurts".to_owned()),
            ..Default::default()
        },
        River {
            id: 10,
            name: Some("Ivorypalace".to_owned()),
            ..Default::default()
        },
    ]
}
