use df_st_core::*;

fn get_control_image(file_path: String) -> Vec<u8> {
    std::fs::read(file_path).unwrap()
}

#[allow(dead_code)]
pub fn get_world_maps() -> Vec<MapImage> {
    let base_path = "./tests/worlds/v47_04/region4/control_images";
    vec![
        MapImage {
            id: 0,
            name: "Detailed map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-detailed.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 1,
            name: "World map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-world_map.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 2,
            name: "Biome map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-bm.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 3,
            name: "Diplomacy map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-dip.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 4,
            name: "Drainage map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-drn.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 5,
            name: "Elevation map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-el.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 6,
            name: "Elevation map with water".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-elw.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 7,
            name: "Evil map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-evil.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 8,
            name: "Hydrological map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-hyd.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 9,
            name: "Nobility map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-nob.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 10,
            name: "Rainfall map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-rain.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 11,
            name: "Salinity map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-sal.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 12,
            name: "Savagery map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-sav.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 13,
            name: "Cadaster map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-str.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 14,
            name: "Temperature map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-tmp.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 15,
            name: "Trade map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-trd.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 16,
            name: "Vegetation map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-veg.png", base_path)),
            format: "png".to_owned(),
        },
        MapImage {
            id: 17,
            name: "Volcanism map".to_owned(),
            data: get_control_image(format!("{}/region4-00050-01-01-vol.png", base_path)),
            format: "png".to_owned(),
        },
    ]
}
