use df_st_core::create_new::CreateNew;
use df_st_core::*;

#[allow(dead_code)]
pub fn get_artifacts() -> Vec<Artifact> {
    vec![
        Artifact {
            // Only use values from legends.xml
            id: 0,
            name: Some("hexwicked the doomed menace".to_owned()),
            site_id: Some(1),
            structure_local_id: Some(0),
            holder_hf_id: Some(3),

            abs_tile_x: Some(4),
            abs_tile_y: Some(5),
            abs_tile_z: Some(6),
            region_id: Some(7),

            item_name: Some("necaterithesa emurasofipu".to_owned()),
            item_type: None,
            item_subtype: None,
            item_writing: None,
            item_page_number: Some(8),
            item_page_written_content_id: Some(9),
            item_writing_written_content_id: Some(10),
            item_description: None,
            item_mat: None,
        },
        Artifact {
            // Only use values from legends_plus.xml
            id: 1,
            name: None,
            site_id: None,
            structure_local_id: None,
            holder_hf_id: None,

            abs_tile_x: None,
            abs_tile_y: None,
            abs_tile_z: None,
            region_id: None,

            item_name: None,
            item_type: Some("slab".to_owned()),
            item_subtype: Some("scroll".to_owned()),
            item_writing: Some(2),
            item_page_number: Some(3),
            item_page_written_content_id: None,
            item_writing_written_content_id: None,
            item_description: Some(
                "i am lim iklist bocash, lim the whispers of ruthlessness, once of \
                the underworld.  by ngáthi the sin of crypts, i bind myself to this place."
                    .to_owned(),
            ),
            item_mat: Some("satinspar".to_owned()),
        },
        Artifact {
            // ID merge, only ID
            id: 2,
            ..Default::default()
        },
        Artifact {
            // Full merge
            id: 3,
            name: Some("Lorem Ipsum".to_owned()),
            site_id: Some(3),
            structure_local_id: Some(0),
            holder_hf_id: Some(6),

            abs_tile_x: Some(14),
            abs_tile_y: Some(15),
            abs_tile_z: Some(16),
            region_id: Some(5),

            item_name: Some("dolor sit amet".to_owned()),
            item_type: Some("consectetur".to_owned()),
            item_subtype: Some("adipiscing".to_owned()),
            item_writing: Some(4),
            item_page_number: Some(20),
            item_page_written_content_id: Some(1),
            item_writing_written_content_id: Some(2),
            item_description: Some(
                "Nam lacus felis, mattis a pretium vitae, hendrerit at augue.".to_owned(),
            ),
            item_mat: Some("Vivamus".to_owned()),
        },
        Artifact {
            // missing ID
            id: 4,
            ..Default::default()
        },
        Artifact {
            // Set ID's to `-1`
            id: 5,
            name: Some("malesuada".to_owned()),
            item_type: Some("Mauris".to_owned()),
            site_id: None,
            structure_local_id: None,
            holder_hf_id: None,
            item_writing: None,
            item_page_written_content_id: None,
            item_writing_written_content_id: None,
            ..Default::default()
        },
        Artifact {
            // empty strings
            id: 6,
            name: Some("".to_owned()),
            item_type: Some("".to_owned()),
            ..Default::default()
        },
        Artifact::new_by_id(7),
        Artifact {
            // higher UTF-8 and CP437 chars
            id: 8,
            name: Some("These are cp437 chars: ê å ╢ Θ".to_owned()),
            item_type: Some("fermentu😈m𐅋".to_owned()),
            ..Default::default()
        },
        Artifact {
            id: 9,
            name: Some("accumsan".to_owned()),
            item_type: Some("ultricies".to_owned()),
            holder_hf_id: Some(3),
            ..Default::default()
        },
        Artifact {
            id: 10,
            name: Some("egestas".to_owned()),
            item_type: Some("metus".to_owned()),
            ..Default::default()
        },
    ]
}
