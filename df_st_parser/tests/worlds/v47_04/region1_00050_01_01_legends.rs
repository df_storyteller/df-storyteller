use df_st_core::*;

#[allow(dead_code)]
pub fn get_world(mut world: DFWorld) -> DFWorld {
    world.world_info = DFWorldInfo {
        name: Some("Moÿoecamo".to_owned()),
        alternative_name: Some("The Mythical Universe".to_owned()),
        ..Default::default()
    };

    let artifacts = vec![
        Artifact {
            id: 0,
            name: Some("hexwicked the doomed menace".to_owned()),
            site_id: Some(8),
            item_name: Some("necaterithesa emurasofipu".to_owned()),
            item_type: Some("slab".to_owned()),
            item_description: Some(
                "i am lim iklist bocash, lim the whispers of ruthlessness, \
                once of the underworld.  by ngáthi the sin of crypts, \
                i bind myself to this place."
                    .to_owned(),
            ),
            item_mat: Some("satinspar".to_owned()),
            ..Default::default()
        },
        Artifact {
            id: 1,
            name: Some("and i sang \'jades!\'".to_owned()),
            site_id: Some(5),
            item_name: Some("and i sang \'jades!\'".to_owned()),
            item_type: Some("book".to_owned()),
            item_writing: Some(4),
            item_page_number: Some(14),
            item_page_written_content_id: Some(4),
            item_mat: Some("green zircon".to_owned()),
            ..Default::default()
        },
        Artifact {
            id: 2,
            name: Some("the gutter, my life".to_owned()),
            holder_hf_id: Some(153),
            item_name: Some("the gutter, my life".to_owned()),
            item_type: Some("book".to_owned()),
            item_writing: Some(17),
            item_page_number: Some(35),
            item_page_written_content_id: Some(17),
            item_mat: Some("marble".to_owned()),
            ..Default::default()
        },
    ];

    world.artifacts.add_missing_data(&artifacts);

    world
}
