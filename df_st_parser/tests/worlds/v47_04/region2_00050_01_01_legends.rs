use df_st_core::*;

#[allow(dead_code)]
pub fn get_world(mut world: DFWorld) -> DFWorld {
    world.world_info = DFWorldInfo {
        name: Some("Moÿoecamo".to_owned()),
        alternative_name: Some("The Mythical Universe".to_owned()),
        ..Default::default()
    };

    let historical_events = vec![
        HistoricalEvent {
            id: 0,
            type_: Some("change_hf_state".to_owned()),
            year: Some(1),
            seconds72: Some(-1),
            coord: Some("-1,-1".to_owned()),
            feature_layer_id: Some(-1),
            hf_id: Some(0),
            reason_obj: Some(HEReason::default()),
            site_id: Some(1),
            state: Some("settled".to_owned()),
            region_id: Some(-1),
            ..Default::default()
        },
        HistoricalEvent {
            id: 1,
            type_: Some("ceremony".to_owned()),
            year: Some(1),
            seconds72: Some(290800),
            civ_id: Some(30),
            feature_layer_id: Some(-1),
            occasion_id: Some(0),
            schedule_id: Some(1),
            site_id: Some(6),
            region_id: Some(-1),
            ..Default::default()
        },
        HistoricalEvent {
            id: 2,
            type_: Some("hf_died".to_owned()),
            year: Some(2),
            seconds72: Some(336000),
            cause: Some("struck".to_owned()),
            death_cause: Some("struck_down".to_owned()),
            feature_layer_id: Some(-1),
            hf_id: Some(0),
            site_id: Some(7),
            slayer_caste: Some(0),
            slayer_hf_id: Some(-1),
            slayer_item_id: Some(-1),
            slayer_race: Some("ELEPHANT".to_owned()),
            slayer_shooter_item_id: Some(-1),
            region_id: Some(1),
            victim_hf_id: Some(0),
            ..Default::default()
        },
        HistoricalEvent {
            id: 3,
            type_: Some("add_hf_entity_link".to_owned()),
            year: Some(3),
            seconds72: Some(-1),
            appointer_hf_id: Some(-1),
            civ_id: Some(42),
            hf_id: Some(151),
            link: Some("position".to_owned()),
            link_type: Some("position".to_owned()),
            position_id: Some(0),
            promise_to_hf_id: Some(-1),
            position: Some("chief".to_owned()),
            ..Default::default()
        },
    ];

    world.historical_events.add_missing_data(&historical_events);

    world
}
