use df_st_core::*;

#[allow(dead_code)]
pub fn get_world(mut world: DFWorld) -> DFWorld {
    world.world_info = DFWorldInfo {
        ..Default::default()
    };

    world
        .historical_figures
        .add_missing_data(&vec![HistoricalFigure {
            id: 0,
            name: Some("uhos tunnelcobalt".to_owned()),
            race: Some("human".to_owned()),
            race_id: Some("HUMAN".to_owned()),
            caste: Some("male".to_owned()),
            appeared: Some(1),
            entity_link: vec![
                HFEntityLink {
                    hf_id: 0,
                    entity_id: 36,
                    link_type: Some("former member".to_owned()),
                    link_strength: Some(92),
                },
                HFEntityLink {
                    hf_id: 0,
                    entity_id: 33,
                    link_type: Some("former member".to_owned()),
                    link_strength: Some(39),
                },
            ],
            entity_position_link: vec![
                HFEntityPositionLink {
                    id: 0,
                    hf_id: 0,
                    entity_id: 26,
                    position_profile_id: Some(3),
                    start_year: Some(4),
                    end_year: Some(21),
                },
                HFEntityPositionLink {
                    id: 0,
                    hf_id: 0,
                    entity_id: 27,
                    position_profile_id: Some(1),
                    start_year: Some(3),
                    end_year: None,
                },
            ],
            ..Default::default()
        }]);

    world
}
