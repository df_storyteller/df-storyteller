mod common;
mod worlds;

use df_st_core::fillable::{Fillable, Filler};
use df_st_db::{id_filter, string_filter, DBObject};
use pretty_assertions::assert_eq;
use std::fmt::Debug;
use std::path::PathBuf;

use worlds::v47_04::region4 as control_data;

fn setup() {
    common::setup();
}

// -----------------------------
// --------- Unit Tests --------
// -----------------------------

// -----------------------------
// ----- Integration Tests -----
// -----------------------------

#[test]
#[rustfmt::skip]
fn full_import_v47_04_region_4() {
    setup();

    let (config, _temp_dir) = common::setup_default_db();

    let file = PathBuf::from("./tests/worlds/v47_04/region4/");
    let world = 4;

    // Same as `import` subcommand
    df_st_parser::parse_and_store_xml(&file, &config, world);
    df_st_parser::parse_world_map_images(&file, &config, world);
    df_st_parser::parse_site_map_images(&file, &config, world);

    // Open DB connection
    let pool = df_st_db::establish_connection(&config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");

    check_objects::<df_st_db::Artifact, _, _>(&conn, world, control_data::get_artifacts(), 11);
    check_objects::<df_st_db::Creature, _, _>(&conn, world, control_data::get_creatures(), 7);
    check_objects::<df_st_db::DanceForm, _, _>(&conn, world, control_data::get_dance_forms(), 11);
    check_objects::<df_st_db::Entity, _, _>(&conn, world, control_data::get_entities(), 11);
    check_objects::<df_st_db::EntityPopulation, _, _>(&conn, world, control_data::get_entity_populations(), 11);
    check_objects::<df_st_db::HistoricalEra, _, _>(&conn, world, control_data::get_historical_eras(), 11);
    // check_objects::<df_st_db::HistoricalEvent, _, _>(&conn, world, control_data::get_historical_eras(), 11);
    // check_objects::<df_st_db::HistoricalEventCollection, _, _>(&conn, world, control_data::get_historical_eras(), 11);
    // check_objects::<df_st_db::HistoricalFigure, _, _>(&conn, world, control_data::get_historical_eras(), 11);
    check_objects::<df_st_db::Identity, _, _>(&conn, world, control_data::get_identities(), 11);
    check_objects::<df_st_db::Landmass, _, _>(&conn, world, control_data::get_landmasses(), 11);
    check_objects::<df_st_db::MountainPeak, _, _>(&conn, world, control_data::get_mountain_peaks(), 11);
    check_objects::<df_st_db::MusicalForm, _, _>(&conn, world, control_data::get_musical_forms(), 11);
    check_objects::<df_st_db::PoeticForm, _, _>(&conn, world, control_data::get_poetic_forms(), 11);
    // check_objects::<df_st_db::Coordinate, _, _>(&conn, world, control_data::get_poetic_forms(), 11);
    // check_objects::<df_st_db::Rectangle, _, _>(&conn, world, control_data::get_poetic_forms(), 11);
    // check_objects::<df_st_db::Path, _, _>(&conn, world, control_data::get_poetic_forms(), 11);
    check_objects::<df_st_db::Region, _, _>(&conn, world, control_data::get_regions(), 11);
    check_objects::<df_st_db::River, _, _>(&conn, world, control_data::get_rivers(), 11);
    check_objects::<df_st_db::Site, _, _>(&conn, world, control_data::get_sites(), 11);
    // TODO structures and site_properties
    // TODO underground_region, world_construction, world_info, written_content

    // Images
    check_objects_std::<df_st_db::MapImage, _, _>(&conn, world, control_data::get_world_maps(), 18);
    check_objects_std_opt::<df_st_db::SiteMapImage, _, _>(&conn, world, control_data::get_site_maps(), 6);
}

fn check_objects_std_opt<DB, C, D>(
    conn: &df_st_db::DbConnection,
    world_id: i32,
    list: Vec<Option<C>>,
    count: u32,
) where
    DB: DBObject<C, D>,
    C: Fillable + Filler<C, D> + Default + Debug + Clone,
    D: PartialEq<C> + Debug + Clone,
{
    // Get items
    // Compare 1 extra make sure they are both `None`
    for i in 0..=list.len() {
        if let Some(control_item) = list.get(i) {
            let item_search = DB::get_from_db(
                conn,
                id_filter!["id" => i as i32, "world_id" => world_id],
                true,
            )
            .unwrap();
            std::assert_eq!(format!("{:?}", item_search), format!("{:?}", control_item));
        }
    }

    // List items
    // Remove Option wrapper
    let list_no_opt: Vec<C> = list
        .iter()
        .filter(|item| item.is_some())
        .map(|item| item.clone().unwrap())
        .collect();
    let result_list = DB::get_list_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
        None,
        true,
    )
    .unwrap();
    std::assert_eq!(format!("{:?}", result_list), format!("{:?}", list_no_opt));

    // Count items
    let total_count = control_data::get_count(count);
    let count_list = DB::get_count_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
    )
    .unwrap();
    std::assert_eq!(format!("{:?}", count_list), format!("{:?}", total_count));
}

fn check_objects_std<DB, C, D>(
    conn: &df_st_db::DbConnection,
    world_id: i32,
    list: Vec<C>,
    count: u32,
) where
    DB: DBObject<C, D>,
    C: Fillable + Filler<C, D> + Default + Debug + Clone,
    D: PartialEq<C> + Debug + Clone,
{
    // Get items
    // Compare 1 extra make sure they are both `None`
    for i in 0..=list.len() {
        let control_item = list.get(i);
        let item_search = DB::get_from_db(
            conn,
            id_filter!["id" => i as i32, "world_id" => world_id],
            true,
        )
        .unwrap();
        std::assert_eq!(format!("{:?}", item_search), format!("{:?}", control_item));
    }

    // List items
    let result_list = DB::get_list_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
        None,
        true,
    )
    .unwrap();
    std::assert_eq!(format!("{:?}", result_list), format!("{:?}", list));

    // Count items
    let total_count = control_data::get_count(count);
    let count_list = DB::get_count_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
    )
    .unwrap();
    std::assert_eq!(format!("{:?}", count_list), format!("{:?}", total_count));
}

fn check_objects<DB, C, D>(conn: &df_st_db::DbConnection, world_id: i32, list: Vec<C>, count: u32)
where
    DB: DBObject<C, D>,
    C: Fillable + Filler<C, D> + Default + Debug + Clone,
    D: PartialEq<C> + Debug + Clone,
{
    // Get items
    // Compare 1 extra make sure they are both `None`
    for i in 0..=list.len() {
        let control_item = list.get(i);
        let item_search = DB::get_from_db(
            conn,
            id_filter!["id" => i as i32, "world_id" => world_id],
            true,
        )
        .unwrap();
        assert_eq!(format!("{:?}", item_search), format!("{:?}", control_item));
    }

    // List items
    let result_list = DB::get_list_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
        None,
        true,
    )
    .unwrap();
    assert_eq!(format!("{:?}", result_list), format!("{:?}", list));

    // Count items
    let total_count = control_data::get_count(count);
    let count_list = DB::get_count_from_db(
        conn,
        id_filter!["world_id" => world_id],
        string_filter![],
        0,
        20,
        None,
        None,
    )
    .unwrap();
    assert_eq!(format!("{:?}", count_list), format!("{:?}", total_count));
}
