mod common;

use pretty_assertions::assert_eq;
use std::path::PathBuf;

mod worlds;

fn setup() {
    common::setup();
}

// -----------------------------
// --------- Unit Tests --------
// -----------------------------

// -----------------------------
// ----- Integration Tests -----
// -----------------------------

#[test]
fn import_v47_04_region_1() {
    setup();

    let filename = PathBuf::from("./tests/worlds/v47_04/region1-00050-01-01-legends.xml");

    let df_world = df_st_parser::parse_xml_files(&filename);
    // This creates an empty copy but preserves the Hash functions so Id's are the same.
    let df_world_empty = df_world.empty_copy();

    let df_world_expected = worlds::v47_04::region1_00050_01_01_legends::get_world(df_world_empty);

    // Because `PartialEq` is implemented only for item `id` (see `HashAndPartialEqById`).
    // This prevents us from properly comparing them.
    // So covert to strings and comparing the strings is workaround.
    // (not the best, but good enough for tests)
    assert_eq!(
        format!("{:?}", df_world),
        format!("{:?}", df_world_expected),
    );
}

#[test]
fn import_v47_04_region_2() {
    setup();

    let filename = PathBuf::from("./tests/worlds/v47_04/region2-00050-01-01-legends.xml");

    let df_world = df_st_parser::parse_xml_files(&filename);
    // This creates an empty copy but preserves the Hash functions so Id's are the same.
    let df_world_empty = df_world.empty_copy();

    let df_world_expected = worlds::v47_04::region2_00050_01_01_legends::get_world(df_world_empty);

    // Because `PartialEq` is implemented only for item `id` (see `HashAndPartialEqById`).
    // This prevents us from properly comparing them.
    // So covert to strings and comparing the strings is workaround.
    // (not the best, but good enough for tests)
    assert_eq!(
        format!("{:?}", df_world),
        format!("{:?}", df_world_expected),
    );
}

#[test]
fn import_v47_04_region_3() {
    setup();

    let filename = PathBuf::from("./tests/worlds/v47_04/region3-00050-01-01-legends.xml");

    let df_world = df_st_parser::parse_xml_files(&filename);
    // This creates an empty copy but preserves the Hash functions so Id's are the same.
    let df_world_empty = df_world.empty_copy();

    let df_world_expected = worlds::v47_04::region3_00050_01_01_legends::get_world(df_world_empty);

    // Because `PartialEq` is implemented only for item `id` (see `HashAndPartialEqById`).
    // This prevents us from properly comparing them.
    // So covert to strings and comparing the strings is workaround.
    // (not the best, but good enough for tests)
    assert_eq!(
        format!("{:?}", df_world),
        format!("{:?}", df_world_expected),
    );
}

#[test]
fn import_v47_04_region_5() {
    setup();

    let filename = PathBuf::from("./tests/worlds/v47_04/region5-00050-01-01-legends.xml");

    let df_world = df_st_parser::parse_xml_files(&filename);
    // This creates an empty copy but preserves the Hash functions so Id's are the same.
    let df_world_empty = df_world.empty_copy();

    let df_world_expected = worlds::v47_04::region5_00050_01_01_legends::get_world(df_world_empty);

    // Because `PartialEq` is implemented only for item `id` (see `HashAndPartialEqById`).
    // This prevents us from properly comparing them.
    // So covert to strings and comparing the strings is workaround.
    // (not the best, but good enough for tests)
    assert_eq!(
        format!("{:?}", df_world),
        format!("{:?}", df_world_expected),
    );
}
