use df_st_core::{
    ConvertingUtils, DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown,
};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown, HashAndPartialEqById,
)]
pub struct HistoricalFigure {
    pub id: i32,
    pub name: Option<String>,
    pub race: Option<String>,
    pub caste: Option<String>,
    pub appeared: Option<i32>,
    pub birth_year: Option<i32>,
    pub birth_seconds72: Option<i32>,
    pub death_year: Option<i32>,
    pub death_seconds72: Option<i32>,
    pub associated_type: Option<String>,
    pub entity_link: Option<Vec<HFEntityLink>>,
    #[serde(alias = "entity_former_position_link")]
    pub entity_position_link: Option<Vec<HFEntityPositionLink>>,
    pub site_link: Option<Vec<HFSiteLink>>,
    pub sphere: Option<Vec<String>>,
    pub interaction_knowledge: Option<Vec<String>>,
    pub deity: Option<String>,
    pub journey_pet: Option<Vec<String>>,
    pub goal: Option<String>,
    pub relationship_profile_hf_historical: Option<Vec<HFRelationshipProfileHFHistorical>>,
    pub relationship_profile_hf_visual: Option<Vec<HFRelationshipProfileHFVisual>>,
    pub intrigue_actor: Option<Vec<HFIntrigueActor>>,
    pub intrigue_plot: Option<Vec<HFIntriguePlot>>,
    pub ent_pop_id: Option<i32>,
    pub entity_reputation: Option<Vec<HFEntityReputation>>,
    pub vague_relationship: Option<Vec<HFVagueRelationship>>,
    pub active_interaction: Option<String>,
    pub force: Option<()>,
    pub current_identity_id: Option<i32>,
    #[serde(alias = "entity_former_squad_link")]
    pub entity_squad_link: Option<Vec<HFEntitySquadLink>>,
    pub holds_artifact: Option<i32>,
    pub honor_entity: Option<Vec<HFHonorEntity>>,
    pub site_property: Option<Vec<HFSiteProperty>>,
    pub used_identity_id: Option<i32>,
    pub animated: Option<()>,
    pub animated_string: Option<String>,
    pub adventurer: Option<()>,

    pub hf_skill: Option<Vec<HFSkill>>,
    pub hf_link: Option<Vec<HFLink>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct HistoricalFigures {
    pub historical_figure: Option<Vec<HistoricalFigure>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFEntityLink {
    pub entity_id: i32,
    pub link_type: Option<String>,
    pub link_strength: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFEntityPositionLink {
    pub entity_id: i32,
    pub position_profile_id: Option<i32>,
    pub start_year: Option<i32>,
    pub end_year: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFSiteLink {
    pub site_id: i32,
    pub link_type: Option<String>,
    pub entity_id: Option<i32>,
    pub occupation_id: Option<i32>,
    pub sub_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFSkill {
    pub skill: String,
    pub total_ip: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

/// Same as `HFRelationshipProfileHFVisual`, combined later
#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFRelationshipProfileHFHistorical {
    pub hf_id: i32,
    pub meet_count: Option<i32>,
    pub last_meet_year: Option<i32>,
    pub last_meet_seconds72: Option<i32>,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
    pub known_identity_id: Option<i32>,
    pub rep_bonded: Option<i32>,
    pub rep_buddy: Option<i32>,
    pub rep_flatterer: Option<i32>,
    pub rep_friendly: Option<i32>,
    pub rep_grudge: Option<i32>,
    pub rep_information_source: Option<i32>,
    pub rep_killer: Option<i32>,
    pub rep_murderer: Option<i32>,
    pub rep_quarreler: Option<i32>,
    pub rep_violent: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

/// Same as `HFRelationshipProfileHFHistorical`, combined later
#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFRelationshipProfileHFVisual {
    pub hf_id: i32,
    pub meet_count: Option<i32>,
    pub last_meet_year: Option<i32>,
    pub last_meet_seconds72: Option<i32>,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
    pub known_identity_id: Option<i32>,
    pub rep_bonded: Option<i32>,
    pub rep_buddy: Option<i32>,
    pub rep_flatterer: Option<i32>,
    pub rep_friendly: Option<i32>,
    pub rep_grudge: Option<i32>,
    pub rep_information_source: Option<i32>,
    pub rep_killer: Option<i32>,
    pub rep_murderer: Option<i32>,
    pub rep_quarreler: Option<i32>,
    pub rep_violent: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFIntrigueActor {
    pub local_id: i32,
    pub entity_id: Option<i32>,
    pub hfid: Option<i32>,
    pub role: Option<String>,
    pub strategy: Option<String>,
    pub strategy_enid: Option<i32>,
    pub strategy_eppid: Option<i32>,
    pub handle_actor_id: Option<i32>,
    pub promised_actor_immortality: Option<()>,
    pub promised_me_immortality: Option<()>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFIntriguePlot {
    pub local_id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub on_hold: Option<()>,
    pub actor_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub delegated_plot_id: Option<i32>,
    pub delegated_plot_hfid: Option<i32>,
    pub entity_id: Option<i32>,
    pub plot_actor: Option<Vec<HFPlotActor>>,
    pub parent_plot_hfid: Option<i32>,
    pub parent_plot_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFPlotActor {
    pub actor_id: i32,
    pub plot_role: Option<String>,
    pub agreement_id: Option<i32>,
    pub agreement_has_messenger: Option<()>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFEntityReputation {
    pub entity_id: i32,
    pub first_ageless_year: Option<i32>,
    pub first_ageless_season_count: Option<i32>,
    pub unsolved_murders: Option<i32>,
    pub rep_hunter: Option<i32>,
    pub rep_killer: Option<i32>,
    pub rep_knowledge_preserver: Option<i32>,
    pub rep_treasure_hunter: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFVagueRelationship {
    pub hfid: i32,
    pub war_buddy: Option<()>,
    pub artistic_buddy: Option<()>,
    pub atheletic_rival: Option<()>,
    pub athlete_buddy: Option<()>,
    pub business_rival: Option<()>,
    pub childhood_friend: Option<()>,
    pub grudge: Option<()>,
    pub jealous_obsession: Option<()>,
    pub jealous_relationship_grudge: Option<()>,
    pub persecution_grudge: Option<()>,
    pub religious_persecution_grudge: Option<()>,
    pub scholar_buddy: Option<()>,
    pub supernatural_grudge: Option<()>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFEntitySquadLink {
    pub entity_id: i32,
    pub squad_id: i32,
    pub squad_position: Option<i32>,
    pub start_year: Option<i32>,
    pub end_year: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFLink {
    pub hfid: i32,
    pub link_strength: Option<i32>,
    pub link_type: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFHonorEntity {
    pub entity: i32,
    pub battles: Option<i32>,
    pub honor_id: Option<Vec<i32>>,
    pub kills: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HFSiteProperty {
    pub site_id: i32,
    pub property_id: i32,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalFigure, HistoricalFigure> for df_st_core::HistoricalFigure {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalFigure) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.race.add_missing_data(&source.race.to_uniform_name());
        self.race_id.add_missing_data(&source.race);
        self.caste.add_missing_data(&source.caste.to_uniform_name());
        self.appeared.add_missing_data(&source.appeared);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_seconds72.add_missing_data(&source.birth_seconds72);
        self.death_year.add_missing_data(&source.death_year);
        self.death_seconds72.add_missing_data(&source.death_seconds72);
        self.associated_type.add_missing_data(&source.associated_type.to_uniform_name());
        self.entity_link.add_missing_data(&source.entity_link);
        self.entity_position_link.add_missing_data(&source.entity_position_link);
        self.site_link.add_missing_data(&source.site_link);
        self.sphere.add_missing_data(&source.sphere);
        self.interaction_knowledge.add_missing_data(&source.interaction_knowledge);
        self.deity.add_missing_data(&source.deity);
        self.journey_pet.add_missing_data(&source.journey_pet);
        self.goal.add_missing_data(&source.goal);
        self.relationship_profile_hf.add_missing_data(&source.relationship_profile_hf_historical);
        self.relationship_profile_hf.add_missing_data(&source.relationship_profile_hf_visual);
        self.intrigue_actor.add_missing_data(&source.intrigue_actor);
        self.intrigue_plot.add_missing_data(&source.intrigue_plot);
        self.ent_pop_id.add_missing_data(&source.ent_pop_id);
        self.entity_reputation.add_missing_data(&source.entity_reputation);
        self.vague_relationship.add_missing_data(&source.vague_relationship);
        self.active_interaction.add_missing_data(&source.active_interaction);
        self.force.add_missing_data(&source.force);
        self.current_identity_id.add_missing_data(&source.current_identity_id);
        self.entity_squad_link.add_missing_data(&source.entity_squad_link);
        self.holds_artifact.add_missing_data(&source.holds_artifact);
        self.honor_entity.add_missing_data(&source.honor_entity);
        self.site_property.add_missing_data(&source.site_property);
        self.used_identity_id.add_missing_data(&source.used_identity_id);
        self.animated.add_missing_data(&source.animated);
        self.animated_string.add_missing_data(&source.animated_string);
        self.adventurer.add_missing_data(&source.adventurer);
        self.skills.add_missing_data(&source.hf_skill);
        self.links.add_missing_data(&source.hf_link);
    }
}

impl PartialEq<df_st_core::HistoricalFigure> for HistoricalFigure {
    fn eq(&self, other: &df_st_core::HistoricalFigure) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalFigure>, HistoricalFigures>
    for Vec<df_st_core::HistoricalFigure>
{
    fn add_missing_data(&mut self, source: &HistoricalFigures) {
        self.add_missing_data(&source.historical_figure);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalFigure>, HistoricalFigures>
    for IndexMap<u64, df_st_core::HistoricalFigure>
{
    fn add_missing_data(&mut self, source: &HistoricalFigures) {
        self.add_missing_data(&source.historical_figure);
    }
}

impl Filler<df_st_core::HFEntityLink, HFEntityLink> for df_st_core::HFEntityLink {
    fn add_missing_data(&mut self, source: &HFEntityLink) {
        self.entity_id.add_missing_data(&source.entity_id);
        self.link_type.add_missing_data(&source.link_type);
        self.link_strength.add_missing_data(&source.link_strength);
    }
}

impl PartialEq<df_st_core::HFEntityLink> for HFEntityLink {
    fn eq(&self, other: &df_st_core::HFEntityLink) -> bool {
        self.entity_id == other.entity_id && self.link_type == other.link_type
    }
}

impl Filler<df_st_core::HFEntityPositionLink, HFEntityPositionLink>
    for df_st_core::HFEntityPositionLink
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFEntityPositionLink) {
        self.entity_id.add_missing_data(&source.entity_id);
        self.position_profile_id.add_missing_data(&source.position_profile_id);
        self.start_year.add_missing_data(&source.start_year);
        self.end_year.add_missing_data(&source.end_year);
    }
}

impl PartialEq<df_st_core::HFEntityPositionLink> for HFEntityPositionLink {
    fn eq(&self, other: &df_st_core::HFEntityPositionLink) -> bool {
        // Most of these objects are unique but only slightly
        // This means there are a lot of checks.
        self.entity_id == other.entity_id
            && self.start_year == other.start_year
            && self.position_profile_id == other.position_profile_id
    }
}

impl Filler<df_st_core::HFSiteLink, HFSiteLink> for df_st_core::HFSiteLink {
    fn add_missing_data(&mut self, source: &HFSiteLink) {
        self.site_id.add_missing_data(&source.site_id);
        self.link_type.add_missing_data(&source.link_type);
        self.entity_id.add_missing_data(&source.entity_id);
        self.occupation_id.add_missing_data(&source.occupation_id);
        self.sub_id.add_missing_data(&source.sub_id);
    }
}

impl PartialEq<df_st_core::HFSiteLink> for HFSiteLink {
    fn eq(&self, other: &df_st_core::HFSiteLink) -> bool {
        self.site_id == other.site_id
            && self.link_type == other.link_type
            && self.sub_id == other.sub_id
    }
}

impl Filler<df_st_core::HFSkill, HFSkill> for df_st_core::HFSkill {
    fn add_missing_data(&mut self, source: &HFSkill) {
        self.skill.add_missing_data(&source.skill);
        self.total_ip.add_missing_data(&source.total_ip);
    }
}

impl PartialEq<df_st_core::HFSkill> for HFSkill {
    fn eq(&self, other: &df_st_core::HFSkill) -> bool {
        self.skill == other.skill
    }
}

impl Filler<df_st_core::HFRelationshipProfileHF, HFRelationshipProfileHFHistorical>
    for df_st_core::HFRelationshipProfileHF
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFRelationshipProfileHFHistorical) {
        self.hf_id_other.add_missing_data(&source.hf_id);
        self.historical = true;
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72.add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id.add_missing_data(&source.known_identity_id);
        self.rep_bonded.add_missing_data(&source.rep_bonded);
        self.rep_buddy.add_missing_data(&source.rep_buddy);
        self.rep_flatterer.add_missing_data(&source.rep_flatterer);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_grudge.add_missing_data(&source.rep_grudge);
        self.rep_information_source.add_missing_data(&source.rep_information_source);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_murderer.add_missing_data(&source.rep_murderer);
        self.rep_quarreler.add_missing_data(&source.rep_quarreler);
        self.rep_violent.add_missing_data(&source.rep_violent);
    }
}

impl PartialEq<df_st_core::HFRelationshipProfileHF> for HFRelationshipProfileHFHistorical {
    fn eq(&self, other: &df_st_core::HFRelationshipProfileHF) -> bool {
        self.hf_id == other.hf_id_other
    }
}

impl Filler<df_st_core::HFRelationshipProfileHF, HFRelationshipProfileHFVisual>
    for df_st_core::HFRelationshipProfileHF
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFRelationshipProfileHFVisual) {
        self.hf_id_other.add_missing_data(&source.hf_id);
        self.visual = true;
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72.add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id.add_missing_data(&source.known_identity_id);
        self.rep_bonded.add_missing_data(&source.rep_bonded);
        self.rep_buddy.add_missing_data(&source.rep_buddy);
        self.rep_flatterer.add_missing_data(&source.rep_flatterer);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_grudge.add_missing_data(&source.rep_grudge);
        self.rep_information_source.add_missing_data(&source.rep_information_source);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_murderer.add_missing_data(&source.rep_murderer);
        self.rep_quarreler.add_missing_data(&source.rep_quarreler);
        self.rep_violent.add_missing_data(&source.rep_violent);
    }
}

impl PartialEq<df_st_core::HFRelationshipProfileHF> for HFRelationshipProfileHFVisual {
    fn eq(&self, other: &df_st_core::HFRelationshipProfileHF) -> bool {
        self.hf_id == other.hf_id_other
    }
}

impl Filler<df_st_core::HFIntrigueActor, HFIntrigueActor> for df_st_core::HFIntrigueActor {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFIntrigueActor) {
        self.local_id.add_missing_data(&source.local_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.hf_id_other.add_missing_data(&source.hfid);
        self.role.add_missing_data(&source.role);
        self.strategy.add_missing_data(&source.strategy);
        self.strategy_en_id.add_missing_data(&source.strategy_enid);
        self.strategy_epp_id.add_missing_data(&source.strategy_eppid);
        self.handle_actor_id.add_missing_data(&source.handle_actor_id);
        self.promised_actor_immortality.add_missing_data(&source.promised_actor_immortality);
        self.promised_me_immortality.add_missing_data(&source.promised_me_immortality);
    }
}

impl PartialEq<df_st_core::HFIntrigueActor> for HFIntrigueActor {
    fn eq(&self, other: &df_st_core::HFIntrigueActor) -> bool {
        self.local_id == other.local_id
    }
}

impl Filler<df_st_core::HFIntriguePlot, HFIntriguePlot> for df_st_core::HFIntriguePlot {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFIntriguePlot) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.on_hold.add_missing_data(&source.on_hold);
        self.actor_id.add_missing_data(&source.actor_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.delegated_plot_id.add_missing_data(&source.delegated_plot_id);
        self.delegated_plot_hf_id.add_missing_data(&source.delegated_plot_hfid);
        self.entity_id.add_missing_data(&source.entity_id);
        self.plot_actor.add_missing_data(&source.plot_actor);
        self.parent_plot_hf_id.add_missing_data(&source.parent_plot_hfid);
        self.parent_plot_id.add_missing_data(&source.parent_plot_id);
    }
}

impl PartialEq<df_st_core::HFIntriguePlot> for HFIntriguePlot {
    fn eq(&self, other: &df_st_core::HFIntriguePlot) -> bool {
        self.local_id == other.local_id
    }
}

impl Filler<df_st_core::HFPlotActor, HFPlotActor> for df_st_core::HFPlotActor {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFPlotActor) {
        self.actor_id.add_missing_data(&source.actor_id);
        self.plot_role.add_missing_data(&source.plot_role);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.agreement_has_messenger.add_missing_data(&source.agreement_has_messenger);
    }
}

impl PartialEq<df_st_core::HFPlotActor> for HFPlotActor {
    fn eq(&self, other: &df_st_core::HFPlotActor) -> bool {
        self.actor_id == other.actor_id
    }
}

impl Filler<df_st_core::HFEntityReputation, HFEntityReputation> for df_st_core::HFEntityReputation {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFEntityReputation) {
        self.entity_id.add_missing_data(&source.entity_id);
        self.first_ageless_year.add_missing_data(&source.first_ageless_year);
        self.first_ageless_season_count.add_missing_data(&source.first_ageless_season_count);
        self.unsolved_murders.add_missing_data(&source.unsolved_murders);
        self.rep_hunter.add_missing_data(&source.rep_hunter);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_knowledge_preserver.add_missing_data(&source.rep_knowledge_preserver);
        self.rep_treasure_hunter.add_missing_data(&source.rep_treasure_hunter);
    }
}

impl PartialEq<df_st_core::HFEntityReputation> for HFEntityReputation {
    fn eq(&self, other: &df_st_core::HFEntityReputation) -> bool {
        self.entity_id == other.entity_id
    }
}

impl Filler<df_st_core::HFVagueRelationship, HFVagueRelationship>
    for df_st_core::HFVagueRelationship
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFVagueRelationship) {
        self.hf_id_other.add_missing_data(&source.hfid);
        self.war_buddy.add_missing_data(&source.war_buddy);
        self.artistic_buddy.add_missing_data(&source.artistic_buddy);
        self.atheletic_rival.add_missing_data(&source.atheletic_rival);
        self.athlete_buddy.add_missing_data(&source.athlete_buddy);
        self.business_rival.add_missing_data(&source.business_rival);
        self.childhood_friend.add_missing_data(&source.childhood_friend);
        self.grudge.add_missing_data(&source.grudge);
        self.jealous_obsession.add_missing_data(&source.jealous_obsession);
        self.jealous_relationship_grudge.add_missing_data(&source.jealous_relationship_grudge);
        self.persecution_grudge.add_missing_data(&source.persecution_grudge);
        self.religious_persecution_grudge.add_missing_data(&source.religious_persecution_grudge);
        self.scholar_buddy.add_missing_data(&source.scholar_buddy);
        self.supernatural_grudge.add_missing_data(&source.supernatural_grudge);
    }
}

impl PartialEq<df_st_core::HFVagueRelationship> for HFVagueRelationship {
    fn eq(&self, other: &df_st_core::HFVagueRelationship) -> bool {
        self.hfid == other.hf_id_other
    }
}

impl Filler<df_st_core::HFEntitySquadLink, HFEntitySquadLink> for df_st_core::HFEntitySquadLink {
    fn add_missing_data(&mut self, source: &HFEntitySquadLink) {
        self.entity_id.add_missing_data(&source.entity_id);
        self.squad_id.add_missing_data(&source.squad_id);
        self.squad_position.add_missing_data(&source.squad_position);
        self.start_year.add_missing_data(&source.start_year);
        self.end_year.add_missing_data(&source.end_year);
    }
}

impl PartialEq<df_st_core::HFEntitySquadLink> for HFEntitySquadLink {
    fn eq(&self, other: &df_st_core::HFEntitySquadLink) -> bool {
        self.entity_id == other.entity_id && self.squad_id == other.squad_id
    }
}

impl Filler<df_st_core::HFLink, HFLink> for df_st_core::HFLink {
    fn add_missing_data(&mut self, source: &HFLink) {
        self.hf_id_other.add_missing_data(&source.hfid);
        self.link_strength.add_missing_data(&source.link_strength);
        self.link_type.add_missing_data(&source.link_type);
    }
}

// We want to compare `hfid` with `hf_id_other` as `other.hfid` refers to the main hfid.
#[allow(clippy::suspicious_operation_groupings)]
impl PartialEq<df_st_core::HFLink> for HFLink {
    fn eq(&self, other: &df_st_core::HFLink) -> bool {
        self.hfid == other.hf_id_other && self.link_type == other.link_type
    }
}

impl Filler<df_st_core::HFHonorEntity, HFHonorEntity> for df_st_core::HFHonorEntity {
    fn add_missing_data(&mut self, source: &HFHonorEntity) {
        self.entity_id.add_missing_data(&source.entity);
        self.battles.add_missing_data(&source.battles);
        self.honor_id.add_missing_data(&source.honor_id);
        self.kills.add_missing_data(&source.kills);
    }
}

impl PartialEq<df_st_core::HFHonorEntity> for HFHonorEntity {
    fn eq(&self, other: &df_st_core::HFHonorEntity) -> bool {
        self.entity == other.entity_id
    }
}

impl Filler<df_st_core::HFSiteProperty, HFSiteProperty> for df_st_core::HFSiteProperty {
    fn add_missing_data(&mut self, source: &HFSiteProperty) {
        self.site_id.add_missing_data(&source.site_id);
        self.property_id.add_missing_data(&source.property_id);
    }
}

impl PartialEq<df_st_core::HFSiteProperty> for HFSiteProperty {
    fn eq(&self, other: &df_st_core::HFSiteProperty) -> bool {
        self.site_id == other.site_id && self.property_id == other.property_id
    }
}
