use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct EntityPopulation {
    pub id: i32,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct EntityPopulations {
    pub entity_population: Option<Vec<EntityPopulation>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::EntityPopulation, EntityPopulation> for df_st_core::EntityPopulation {
    fn add_missing_data(&mut self, source: &EntityPopulation) {
        self.id.add_missing_data(&source.id);
    }
}

impl PartialEq<df_st_core::EntityPopulation> for EntityPopulation {
    fn eq(&self, other: &df_st_core::EntityPopulation) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::EntityPopulation>, EntityPopulations>
    for Vec<df_st_core::EntityPopulation>
{
    fn add_missing_data(&mut self, source: &EntityPopulations) {
        self.add_missing_data(&source.entity_population);
    }
}

impl Filler<IndexMap<u64, df_st_core::EntityPopulation>, EntityPopulations>
    for IndexMap<u64, df_st_core::EntityPopulation>
{
    fn add_missing_data(&mut self, source: &EntityPopulations) {
        self.add_missing_data(&source.entity_population);
    }
}
