use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct WrittenContent {
    pub id: i32,
    pub title: Option<String>,
    pub author_hfid: Option<i32>,
    pub author_roll: Option<i32>,
    pub form: Option<String>,
    pub form_id: Option<i32>,
    pub style: Option<Vec<String>>, // TODO process further

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct WrittenContents {
    pub written_content: Option<Vec<WrittenContent>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::WrittenContent, WrittenContent> for df_st_core::WrittenContent {
    fn add_missing_data(&mut self, source: &WrittenContent) {
        self.id.add_missing_data(&source.id);
        self.title.add_missing_data(&source.title);
        self.author_hf_id.add_missing_data(&source.author_hfid);
        self.author_roll.add_missing_data(&source.author_roll);
        self.form.add_missing_data(&source.form);
        self.form_id.add_missing_data(&source.form_id);
        if let Some(styles) = &source.style {
            let mut content_styles: Vec<df_st_core::WCStyle> = Vec::new();
            let mut local_id: i32 = 0;
            for style in styles {
                let style_parts: Vec<&str> = style.split(':').collect();
                if style_parts.len() < 2 {
                    continue;
                }
                content_styles.push(df_st_core::WCStyle {
                    written_content_id: source.id,
                    local_id,
                    label: Some(style_parts[0].to_owned()),
                    weight: style_parts[1].parse::<i32>().ok(),
                });
                local_id += 1;
            }
            self.style.add_missing_data(&content_styles);
        }
    }
}

impl PartialEq<df_st_core::WrittenContent> for WrittenContent {
    fn eq(&self, other: &df_st_core::WrittenContent) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::WrittenContent>, WrittenContents> for Vec<df_st_core::WrittenContent> {
    fn add_missing_data(&mut self, source: &WrittenContents) {
        self.add_missing_data(&source.written_content);
    }
}

impl Filler<IndexMap<u64, df_st_core::WrittenContent>, WrittenContents>
    for IndexMap<u64, df_st_core::WrittenContent>
{
    fn add_missing_data(&mut self, source: &WrittenContents) {
        self.add_missing_data(&source.written_content);
    }
}
