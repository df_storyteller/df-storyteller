use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown, HashAndPartialEqById,
)]
pub struct Region {
    pub id: Option<i32>,
    pub name: Option<String>,
    #[serde(alias = "type")]
    pub type_: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct Regions {
    pub region: Option<Vec<Region>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Region, Region> for df_st_core::Region {
    fn add_missing_data(&mut self, source: &Region) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
    }
}

impl PartialEq<df_st_core::Region> for Region {
    fn eq(&self, other: &df_st_core::Region) -> bool {
        match self.id {
            Some(id) => id == other.id,
            None => false,
        }
    }
}

impl Filler<Vec<df_st_core::Region>, Regions> for Vec<df_st_core::Region> {
    fn add_missing_data(&mut self, source: &Regions) {
        self.add_missing_data(&source.region);
    }
}

impl Filler<IndexMap<u64, df_st_core::Region>, Regions> for IndexMap<u64, df_st_core::Region> {
    fn add_missing_data(&mut self, source: &Regions) {
        self.add_missing_data(&source.region);
    }
}
