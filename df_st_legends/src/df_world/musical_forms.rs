use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct MusicalForm {
    pub id: i32,
    pub description: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct MusicalForms {
    pub musical_form: Option<Vec<MusicalForm>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::MusicalForm, MusicalForm> for df_st_core::MusicalForm {
    fn add_missing_data(&mut self, source: &MusicalForm) {
        self.id.add_missing_data(&source.id);
        self.description.add_missing_data(&source.description);
    }
}

impl PartialEq<df_st_core::MusicalForm> for MusicalForm {
    fn eq(&self, other: &df_st_core::MusicalForm) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::MusicalForm>, MusicalForms> for Vec<df_st_core::MusicalForm> {
    fn add_missing_data(&mut self, source: &MusicalForms) {
        self.add_missing_data(&source.musical_form);
    }
}

impl Filler<IndexMap<u64, df_st_core::MusicalForm>, MusicalForms>
    for IndexMap<u64, df_st_core::MusicalForm>
{
    fn add_missing_data(&mut self, source: &MusicalForms) {
        self.add_missing_data(&source.musical_form);
    }
}
