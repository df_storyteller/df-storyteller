use crate::deserializers::{coordinate_deserializer, rectangle_deserializer};
use df_st_core::{
    ConvertingUtils, Coordinate, DeserializeBestEffort, DeserializeBestEffortTypes, Filler,
    HasUnknown, Rectangle,
};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, Default, HasUnknown, HashAndPartialEqById)]
pub struct Site {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub name: Option<String>,
    #[serde(deserialize_with = "coordinate_deserializer")]
    #[serde(default)]
    pub coords: Option<Coordinate>,
    #[serde(deserialize_with = "rectangle_deserializer")]
    #[serde(default)]
    pub rectangle: Option<Rectangle>,
    pub structures: Option<Structures>,
    pub site_properties: Option<SiteProperties>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, HasUnknown)]
pub struct Sites {
    pub site: Option<Vec<Site>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct Structure {
    pub local_id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub subtype: Option<String>,
    pub name: Option<String>,
    pub entity_id: Option<i32>,
    pub worship_hfid: Option<i32>,
    pub copied_artifact_id: Option<Vec<i32>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct Structures {
    pub structure: Option<Vec<Structure>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct SiteProperty {
    /// Local_id
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub structure_id: Option<i32>,
    pub owner_hfid: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct SiteProperties {
    pub site_property: Option<Vec<SiteProperty>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Site, Site> for df_st_core::Site {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Site) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.name.add_missing_data(&source.name);
        self.coord.add_missing_data(&source.coords);
        self.rectangle.add_missing_data(&source.rectangle);
        if let Some(structures) = &source.structures {
            self.structures.add_missing_data(&structures.structure);
        }
        if let Some(site_properties) = &source.site_properties {
            self.site_properties.add_missing_data(&site_properties.site_property);
        }
    }
}

impl Filler<df_st_core::Structure, Structure> for df_st_core::Structure {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Structure) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.subtype.add_missing_data(&source.subtype);
        self.name.add_missing_data(&source.name);
        self.entity_id.add_missing_data(&source.entity_id.negative_to_none());
        self.worship_hf_id.add_missing_data(&source.worship_hfid.negative_to_none());
        self.copied_artifact_ids.add_missing_data(&source.copied_artifact_id.remove_negative());
    }
}

impl Filler<df_st_core::SiteProperty, SiteProperty> for df_st_core::SiteProperty {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &SiteProperty) {
        self.local_id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.structure_id.add_missing_data(&source.structure_id.negative_to_none());
        self.owner_hf_id.add_missing_data(&source.owner_hfid.negative_to_none());
    }
}

impl PartialEq<df_st_core::Site> for Site {
    fn eq(&self, other: &df_st_core::Site) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Structure> for Structure {
    fn eq(&self, other: &df_st_core::Structure) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::SiteProperty> for SiteProperty {
    fn eq(&self, other: &df_st_core::SiteProperty) -> bool {
        self.id == other.local_id
    }
}

impl Filler<Vec<df_st_core::Site>, Sites> for Vec<df_st_core::Site> {
    fn add_missing_data(&mut self, source: &Sites) {
        self.add_missing_data(&source.site);
    }
}

impl Filler<IndexMap<u64, df_st_core::Site>, Sites> for IndexMap<u64, df_st_core::Site> {
    fn add_missing_data(&mut self, source: &Sites) {
        self.add_missing_data(&source.site);
    }
}
