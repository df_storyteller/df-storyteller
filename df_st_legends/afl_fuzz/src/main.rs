#[macro_use]
extern crate afl;
use std::cmp;
use std::io::prelude::*;
use std::io::{self, IoSliceMut};

// Fuzz all the XML files that can be given to the application by the user.
fn main() {
    // fuzz_data();
    fuzz_values();
}

#[allow(dead_code)]
fn fuzz_data() {
    fuzz!(|data: &[u8]| {
        // fuzzed code goes here
        let reader = BufReader::new(data); // CP437 Only
        let parsed_result = &mut quick_xml::de::Deserializer::from_reader(reader);
        let result: Result<df_st_legends::DFWorldLegends, _> =
            serde_path_to_error::deserialize(parsed_result);
        let _parsed_object: df_st_legends::DFWorldLegends = match result {
            Ok(data) => data,
            Err(err) => {
                if err.to_string().contains("Unexpected EOF during reading") {
                    // panic!("Unexpected EOF during importing.");
                    return;
                }
                if err.to_string().contains("Expecting Start event") {
                    // panic!("File is empty.");
                    return;
                }
                //panic!("Error: {} \nIn: {}", err, path);
                return;
            }
        };
    });
}

#[allow(dead_code)]
fn fuzz_values() {
    fuzz!(|data: &[u8]| {
        let mut xml_data: Vec<u8> = "<?xml version=\"1.0\" encoding='CP437'?>
        <df_world>
        <artifacts>
                <artifact>
                        <id>0</id>
                        <name>"
            .as_bytes()
            .to_owned();
        xml_data.append(&mut data.to_owned());
        xml_data.append(
            &mut "</name>
        <site_id>"
                .as_bytes()
                .to_owned(),
        );
        xml_data.append(&mut data.to_owned());
        xml_data.append(
            &mut "</site_id>
            </artifact>
        </artifacts>
        </df_world>"
                .as_bytes()
                .to_owned(),
        );
        // fuzzed code goes here
        let reader = BufReader::new(&*xml_data); // CP437 Only
        let parsed_result = &mut quick_xml::de::Deserializer::from_reader(reader);
        let result: Result<df_st_legends::DFWorldLegends, _> =
            serde_path_to_error::deserialize(parsed_result);
        let _parsed_object: df_st_legends::DFWorldLegends = match result {
            Ok(data) => data,
            Err(err) => {
                if err.to_string().contains("Unexpected EOF during reading") {
                    // panic!("Unexpected EOF during importing.");
                    return;
                }
                if err.to_string().contains("Expecting Start event") {
                    // panic!("File is empty.");
                    return;
                }
                //panic!("Error: {} \nIn: {}", err, path);
                return;
            }
        };
    });
}

//-------------- FROM CP437 ----------------

// Rust default is 8kb, default here at 20kb
const DEFAULT_BUF_SIZE: usize = 20 * 1024;

pub struct BufReader<R> {
    inner: R,
    buf: Box<[u8]>,
    dec_buf: Box<[u8]>,
    pos: usize,
    cap: usize,
}

impl<R: Read> BufReader<R> {
    pub fn new(inner: R) -> BufReader<R> {
        BufReader::with_capacity(DEFAULT_BUF_SIZE, inner)
    }

    pub fn with_capacity(capacity: usize, inner: R) -> BufReader<R> {
        let mut buffer = Vec::with_capacity(capacity);
        let mut decoded_buffer = Vec::with_capacity(capacity * 3);
        buffer.resize(capacity, 0x00);
        decoded_buffer.resize(capacity * 3, 0x00);
        // inner.initializer().initialize(&mut buffer);
        BufReader {
            inner,
            buf: buffer.into_boxed_slice(),
            dec_buf: decoded_buffer.into_boxed_slice(),
            pos: 0,
            cap: 0,
        }
    }
}

impl<R> BufReader<R> {
    pub fn get_ref(&self) -> &R {
        &self.inner
    }

    pub fn get_mut(&mut self) -> &mut R {
        &mut self.inner
    }

    pub fn buffer(&self) -> &[u8] {
        &self.buf[self.pos..self.cap]
    }

    pub fn capacity(&self) -> usize {
        self.buf.len()
    }

    pub fn into_inner(self) -> R {
        self.inner
    }

    /// Invalidates all data in the internal buffer.
    #[inline]
    fn discard_buffer(&mut self) {
        self.pos = 0;
        self.cap = 0;
    }
}

impl<R: Read> Read for BufReader<R> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        // If we don't have any buffered data and we're doing a massive read
        // (larger than our internal buffer), bypass our internal buffer
        // entirely.
        if self.pos == self.cap && buf.len() >= self.buf.len() {
            self.discard_buffer();
            let result = self.inner.read(buf);
            return result;
        }
        let nread = {
            let mut rem = self.fill_buf()?;
            rem.read(buf)?
        };
        self.consume(nread);
        Ok(nread)
    }

    fn read_vectored(&mut self, bufs: &mut [IoSliceMut<'_>]) -> io::Result<usize> {
        let total_len = bufs.iter().map(|b| b.len()).sum::<usize>();
        if self.pos == self.cap && total_len >= self.buf.len() {
            self.discard_buffer();
            let result = self.inner.read_vectored(bufs);
            return result;
        }
        let nread = {
            let mut rem = self.fill_buf()?;
            rem.read_vectored(bufs)?
        };
        self.consume(nread);
        Ok(nread)
    }
}

impl<R: Read> BufRead for BufReader<R> {
    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        // If we've reached the end of our internal buffer then we need to fetch
        // some more data from the underlying reader.
        // Branch using `>=` instead of the more correct `==`
        // to tell the compiler that the pos..cap slice is always valid.
        if self.pos >= self.cap {
            debug_assert!(self.pos == self.cap);
            self.cap = self.inner.read(&mut self.buf)?;
            self.pos = 0;
            let mut dec_pos = 0;
            // Convert all bytes from CP437 to UTF-8
            for i in 0..self.cap {
                let newchar = df_cp437::convert_cp437_byte_to_utf8_bytes(&self.buf[i]);
                if newchar[0] != 0x00 {
                    self.dec_buf[dec_pos] = newchar[0];
                    dec_pos += 1;
                }
                if newchar[1] != 0x00 {
                    self.dec_buf[dec_pos] = newchar[1];
                    dec_pos += 1;
                }
                self.dec_buf[dec_pos] = newchar[2];
                dec_pos += 1;
            }
            // Set new capacity as it can be larger then the value before
            self.cap = dec_pos;
        }
        Ok(&self.dec_buf[self.pos..self.cap])
    }

    fn consume(&mut self, amt: usize) {
        self.pos = cmp::min(self.pos + amt, self.cap);
    }
}
