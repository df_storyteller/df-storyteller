#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![feature(proc_macro_hygiene, decl_macro)]
#![doc(html_root_url = "https://docs.dfstoryteller.com/rust-docs/")]
#![doc(html_favicon_url = "https://docs.dfstoryteller.com/favicon/favicon-16x16.png")]
#![doc(html_logo_url = "https://docs.dfstoryteller.com/logo.svg")]

//! # DF Storyteller - Guide Documentation
//!
//! This crate includes all the documentation about the Guide to get users started.
//!
//!

use colored::*;
use rocket::config::{Config, LogLevel};
use rocket::figment::Figment;
use rocket::http::ContentType;
use rocket::response::content::RawHtml;
use rocket::{get, routes};
use rust_embed::RustEmbed;
use std::borrow::Cow;
use std::ffi::OsStr;
use std::path::PathBuf;

#[derive(RustEmbed)]
#[folder = "./pages/"]
struct StaticAssets;

/// Guide start page
#[get("/")]
async fn index_page<'r>() -> Option<RawHtml<Cow<'static, [u8]>>> {
    let asset = StaticAssets::get("index.html")?;
    Some(RawHtml(asset.data))
}

/// Serve other static file from pages folder.
#[get("/<file..>")]
async fn static_file<'r>(file: PathBuf) -> Option<(ContentType, Cow<'static, [u8]>)> {
    let filename = file
        .display()
        .to_string()
        // replace the Windows `\` with the unix `/` This effect the windows release build.
        .replace('\\', "/");
    let asset = StaticAssets::get(&filename)?;
    let content_type = file
        .extension()
        .and_then(OsStr::to_str)
        .and_then(ContentType::from_extension)
        .unwrap_or(ContentType::Bytes);

    Some((content_type, asset.data))
}

/// Start the Rocket API server that serves the Guide.
pub fn start_guide_server() {
    log::info!("Starting Guide server");

    log::info!(
        "----------------------------------------\n\
        Open following link to view guide: {}\n\
        ----------------------------------------\n",
        "http://localhost:20352/".bright_cyan()
    );

    let launch_result = rocket::execute(
        rocket::custom(set_server_config())
            // Provide static assets for Guide.
            .mount("/", routes![index_page, static_file])
            .launch(),
    );
    match launch_result {
        Ok(_service) => log::info!("Rocket shut down gracefully."),
        Err(err) => log::error!("Rocket error: {}", err),
    };
}

fn set_server_config() -> Figment {
    // Start creating config
    let mut config = Config {
        address: "127.0.0.1".parse().unwrap(),
        port: 20352,
        // Set Workers
        workers: 2,
        // Set Keep Alive duration
        keep_alive: 0,
        ..Default::default()
    };
    // Set logging Level for Rocket (not DF Storyteller)
    if cfg!(debug_assertions) {
        config.log_level = LogLevel::Debug;
    } else {
        config.log_level = LogLevel::Normal;
    }
    // TODO add on error switch to a different port.
    Figment::from(config)
}
