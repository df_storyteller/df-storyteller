#!/bin/sh

# close pgadmin (or disconnect from database so it can be deleted.)
# Reset database (delete database and create it again)
diesel database reset --migration-dir ./df_st_db/migrations/ --database-url "postgres://diesel:<password>@localhost:5432/df_storyteller"

# Create new database
#diesel database setup --migration-dir ./df_st_db/migrations/
