# Change log
In this file you will find all changes to DF Storyteller.
These are changes to the whole application. 
For changes about the API, [see this document](CHANGELOG_API.md).

## Version 0.3.0 (2022-XX-XX)
- Adding `sign` subcommand in debug build to sign verifiers.
- Signing multiple verifiers at once.
- Renaming `type_` to `type` in following places: `Entity`, `EntityLink`, `EntityOccasionSchedule`,
`EntityOccasionScheduleFeature`, `HistoricalEventCollection`, `HFIntriguePlot`, `SiteProperty`, 
`WorldConstruction`, `WrittenContent`, `WCReference`.
- Removing `subregion_id` in `HistoricalEvent` (merged into `region_id`).
- Removing `region_id` parameter from `Coordinate` because filter can be used for this.
- Removing `filter_on_type` parameter from `HistoricalEvent` because filter can be used for this.
- Adding `filter_by` and `filter_value` parameters for most API calls. (issue #109)
- Status response `200 Bad Request` added for list_all and count API calls.
- Error handling of `filter_by`, `group_by` and `order_by` if fields are not found.
- Add `minimal_data` tag to list page API requests (list and item requests).
- Add `minimal_data` field to `ApiPage` response and nested `ApiItem` items.
- Add `_minimal_data` field to `ApiItem` response.
- Renamed `holder_hfid` field to `holder_hf_id` in `Artifact`.
- Renamed `subregion_id` field to `region_id` in `Artifact`.
- Renamed `worship_hfid` field to `worship_hf_id` in `Structure`.
- Renamed `owner_hfid` field to `owner_hf_id` in `SiteProperty`.
- Fixed DB on-conflict replacing of values in `SiteProperty` and `Structure` (Postgres).
- Added `pretty_assertions` for detecting differences in test outputs.
- Fixed `inhabitant` and `copied_artifact_id` always empty in `Structure`.
- Renamed `copied_artifact_id` field to `copied_artifact_ids` in `Structure`.
- Renamed `inhabitant` field to `inhabitant_hf_ids` in `Structure`.
- Added `StructureCopiedArtifactID` and `StructureInhabitantHFID`.
- Fixed `cr_id` not set in `CreatureBiome`.
- Fixed `force_id` always empty in `Region`.
- Renamed `force_id` field to `force_ids` in `Region`.
- Remove `-1` ids in multiple places (not all). Will be removed from list or set to `None` if in an
`Option<>`. They will only be removed if it is a known id field. Regular number field can still
have `-1` values.
- Fixed unable to import multiple `HistoricalEra`.
- Fixed `eater_hf_id` incorrect value in `HistoricalEvent`.
- Fixed `entity_id` never set in `HistoricalEvent`.
- Add support for importing zip archives.
- Changing of license to AGPL-3.0-or-later.
- Adding Windows Control Flow Guard flags for `msvc` builds. (issue #116)
- Config files are saved globally, in user folder, but can locally be overwritten. (issue #111)
- Database used depends on executable (postgres or sqlite) not on config. (issue #111)
- The `--postgres` flag is removed because it was no longer needed.
- Config file can be reset using CLI. (issue #111)
- Config file creates backup when overwritten.
- Database (file) can be reset using CLI. (issue #111)
- Move database file to global user config folder. Exact folder depends on the OS.
This has been changes in default config. (issue #111)
- Removed unused image format support. Only support PNG and BMP.
- Removed blocking when version is out of date. (issue #123)
- Moved minimum glibc support from v2.24 to v2.28.
- Add `GET /link_he_site` and `GET /link_he_site/{site_id}` endpoints.
- Add `GET /link_he_entity` and `GET /link_he_entity/{entity_id}` endpoints.
- Changed `minimal_data` field do not accept `0` and `1` anymore. Just `true` and `false`.
- Disabled `graphql` is currently disabled.

## Version 0.2.2 (2021-03-01)
- Updated dependencies to fix vulnerability RUSTSEC-2021-0003 and RUSTSEC-2021-0020

## Version 0.2.1 (2020-10-29) (yanked/deprecated)
- Updated dependencies to fix vulnerability RUSTSEC-2020-0052

## Version 0.2.0 (2020-08-26) (yanked/deprecated)
- Moved Guide to port 20352, using 20351 as backup port.
- Handling launch service errors.
- `HistoricalEvent` added new fields: `a_hf_ids`, `a_leader_hf_id`, `a_leadership_roll`,
`a_squad_id`, `a_tactician_hf_id`, `a_tactics_roll`, `d_effect`, `d_interaction`, `d_number`,
`d_race`, `d_slain`, `d_squad_id`, `d_tactician_hf_id`, `d_tactics_roll`, `depot_entity_id`,
`ghost`, `leaver_civ_id`, `skill_at_time`, `spotter_hf_id`, `start`, `no_prison_available`,
`abandoned`, `lost_value`, `took_items`, `no_defeat_mention`, `rampage_civ_id`, `searcher_civ_id`,
`result`, `was_raid`, `d_hf_ids`, `contacted_en_id`, `contactor_en_id`, `took_livestock`,
`fled_civ_id`, `hardship`, `new_artifact_id`, `old_artifact_id`, `outcome`, `target_civ_id`,
`unretire`, `art_id`, `art_sub_id`, `building_custom`, `creation_he_id`, `improvement_subtype`,
`item_id`, `maker_hf_id`, `maker_en_id`, `source_hf_id`.
(issue #74, #77, #79, #83, #88, #89, #90, #97, #99, #101, #102, #103, #104)
- Adding the `/link_he_hf` and `/link_he_hf/{hf_id}` api calls.
- Self closing tags (most of them) in the import will no longer show as `Some(false)` but `None`.
- `HistoricalEvent` field changed: `improvement_type` changed from `i32` to `String`. (issue #87)
- Catching failed parser threads. If Legends or Legends_plus parser failed it will recover 
from this error and continue without the effected files. Warning will inform the user. (issue #86)
- Reorder API calls in documentation, does not change functionality.
- `HFEntityReputation` added new fields: `rep_hunter`, `rep_killer`, `rep_knowledge_preserver`,
`rep_treasure_hunter`. (issue #82)
- `HFEntitySquadLink` added new fields: `end_year`. (issue #78)
- Merge `HFRelationshipProfileHFVisual` and `HFRelationshipProfileHFHistorical` together into
`HFRelationshipProfileHF`.
- `HFRelationshipProfileHF` added new fields: `visual`, `historical`, `rep_bonded`, `rep_buddy`,
`rep_flatterer`, `rep_grudge`, `rep_information_source`, `rep_killer`, `rep_murderer`,
`rep_quarreler`, `rep_violent`. (issue #84, #94, #98, #100)
- `HistoricalFigure` added new fields: `adventurer`. (issue #96)
- Add the Timeline example as included visualizer.
- In `HistoricalEvent` renamed `item` to `item_id`.
- Fixing unset `site_id` in `HFSiteLink`.
- Ignore `unk_X` tags in legend files. (issue #105)
- Displays where it creates the `serve-paintings` folder. (issue #80)
- Removed `HERelationshipSupplement` and `HERelationship` from API. 
They are now included in `HistoricalEvent` under the `hf_relationship` type event. (issue #90)
- Internal improvements for allowing filter over `Strings` in database queries. (issue #107)
- Allow `HistoricalEvent` to be filtered in API requests based on `type`. (issue #107)
- Fixed some warnings about mismatched values during merging. (issue #93)

## Version 0.1.1 (2020-08-15) (yanked/deprecated)
- Disable colors and icons for Windows build in CI pipeline.
- `df_st_api` added database features filtering (issue #75).
- Moved "delete database" message for Postgres to be shown at a better moment.
- Database schema: Switching from `Bytea` to `Binary` for images to be database agnostic.
- Guide: added build instructions for Linux.
- Guide: added favicon to main directory.
- Guide: added info for importing folders.
- API: added favicon to main directory.
- Import: Added note for long import time of `Historical Events`.
- Fixed Max OS compiler used for building from source.

## Version 0.1.0 (2020-08-14) (yanked/deprecated)
Initial public release. :D
Only took me 6 months :P

Greetings from the past if someone reads this in the future.
This application was founded during the (start of) COVID-19 (aka Coronavirus) pandemic.
Do you still remember that?