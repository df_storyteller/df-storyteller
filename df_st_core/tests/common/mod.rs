use log::{Level, LevelFilter, Metadata, Record};
use std::path::{Path, PathBuf};
use tempfile::Builder;

pub static LOGGER: Logger = Logger;
pub struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Debug
            && metadata.target() != "serde_xml_rs::de"
            && !metadata.target().starts_with("hyper::")
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR",
                    Level::Warn => "WARN",
                    Level::Info => "INFO",
                    Level::Debug => "DEBUG",
                    Level::Trace => "TRACE",
                },
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}

pub fn setup() {
    log::set_logger(&LOGGER).ok();
    log::set_max_level(LevelFilter::Debug);
}

#[allow(dead_code)]
pub fn set_path(folder: &str) {
    let root = Path::new(folder);
    if !root.exists() {
        std::fs::create_dir(root).unwrap();
    }
    std::env::set_current_dir(&root).unwrap();
}

#[allow(dead_code)]
pub fn get_file(filename: &Path) -> String {
    use std::io::prelude::*;
    let mut file = std::fs::File::open(filename).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    contents
}

#[allow(dead_code)]
pub fn get_temp_dir() -> tempfile::TempDir {
    Builder::new().prefix("df_st_tests-").tempdir().unwrap()
}

#[allow(dead_code)]
pub fn get_fixed_dir(folder: &str) -> PathBuf {
    let root = PathBuf::from(folder);
    if !root.exists() {
        std::fs::create_dir(&root).unwrap();
    }
    root
}
