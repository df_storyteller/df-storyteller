use serde_json::Value;
use std::collections::HashMap;

/// Get all the `unknown: HashMap` values from all the structures and substructures
/// and put them in a list to display them later.
pub trait HasUnknown {
    fn print_unknown(&self, prefix: &str, unknown_map: &mut HashMap<String, u32>);

    fn update_unknown_prefix(&self, prefix: &str, struct_name: &str) -> String {
        format!("{}/{}", prefix, struct_name)
    }

    fn print_unknown_list(
        &self,
        list: &HashMap<String, Value>,
        prefix: &str,
        unknown_map: &mut HashMap<String, u32>,
    ) {
        // update parent
        let parent_key = format!("{}/", prefix);
        let mut value = *unknown_map.get(&parent_key).unwrap_or(&0);
        value += 1;
        unknown_map.insert(parent_key, value);
        // update unknown pairs
        for pair in list {
            let key = format!("{}/{}", prefix, pair.0);
            let mut value = *unknown_map.get(&key).unwrap_or(&0);
            value += 1;
            unknown_map.insert(key, value);
        }
    }

    fn combine_unknown(
        &self,
        prefix: &str,
        unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    );

    fn combine_unknown_list(
        &self,
        list: &HashMap<String, Value>,
        prefix: &str,
        unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
        if !list.is_empty() {
            let parent_key = format!("{}/", prefix);
            let empty_list = Vec::new();
            let vec_list = unknown_combiner.entry(parent_key).or_insert(empty_list);
            for (key, value) in list {
                vec_list.push((key.clone(), value.clone()));
            }
        }
    }
}

impl<T> HasUnknown for Vec<T>
where
    T: HasUnknown,
{
    fn print_unknown(&self, prefix: &str, unknown_map: &mut HashMap<String, u32>) {
        for item in self {
            item.print_unknown(prefix, unknown_map);
        }
    }
    fn combine_unknown(
        &self,
        prefix: &str,
        unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
        for item in self {
            item.combine_unknown(prefix, unknown_combiner);
        }
    }
}

impl<T> HasUnknown for Option<T>
where
    T: HasUnknown,
{
    fn print_unknown(&self, prefix: &str, unknown_map: &mut HashMap<String, u32>) {
        if let Some(x) = self {
            x.print_unknown(prefix, unknown_map);
        }
    }
    fn combine_unknown(
        &self,
        prefix: &str,
        unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
        if let Some(x) = self {
            x.combine_unknown(prefix, unknown_combiner);
        }
    }
}

impl HasUnknown for Vec<()> {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for Option<()> {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for HashMap<String, Value> {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for i32 {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for u32 {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for bool {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for String {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for Value {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for Vec<(String, Value)> {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}
