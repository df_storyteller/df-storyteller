#![forbid(unsafe_code)]
#![deny(clippy::all)]
// This option gives what looks like false warnings on `JsonSchema` derive fields.
#![allow(clippy::field_reassign_with_default)]
// Enable at some point in the future
// #![forbid(missing_docs)]
#![doc(html_root_url = "https://docs.dfstoryteller.com/rust-docs/")]
#![doc(html_favicon_url = "https://docs.dfstoryteller.com/favicon/favicon-16x16.png")]
#![doc(html_logo_url = "https://docs.dfstoryteller.com/logo.svg")]

//! # DF Storyteller - Core objects Documentation
//!
//! In this crate are all the object that are used to convert from and to when the
//! application is running. These are also the objects that are returned from the API.
//!
//! Most of the documentation from the API comes from this crate.
//!

pub mod bufreader;
pub mod config;
pub mod create_new;
pub mod deserialize_best_effort;
pub mod df_st_info;
pub mod df_world;
pub mod fillable;
pub mod git_issue;
pub mod has_unknown;
pub mod item_count;
pub mod positions;
pub mod schema_example;
pub mod site_map_images;
pub mod world_map_images;

pub use deserialize_best_effort::*;
pub use df_st_info::*;
pub use df_world::*;
pub use fillable::*;
pub use git_issue::GitIssue;
pub use has_unknown::*;
pub use positions::*;
pub use schema_example::*;
pub use site_map_images::*;
pub use world_map_images::*;
