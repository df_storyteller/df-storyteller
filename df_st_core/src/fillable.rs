use backtrace::Backtrace;
use indexmap::IndexMap;
use std::cmp::Eq;
use std::collections::HashSet;
use std::fmt::Debug;
use std::hash::{BuildHasher, Hash, Hasher};

pub trait Fillable {}

fn not_same_data<T, R>(dest: &T, src: &R)
where
    T: Debug,
    R: Debug,
{
    log::warn!(
        "Warning: Data not added, but the data is not the same.\n\
        Don't know which value to keep. Default to keep. \n\
        So we keep the first value.\n\
        For more info: {}\n\
        --------------------------------\n\
        \tNot the same: \n{:#?}\n!=\n{:#?}\n\
        --------------------------------",
        super::git_issue::link_to_issue_nr(44),
        dest,
        src
    );
    print_backtrace("Not the same");
}

fn not_same_id<T, R>(dest: &T, src: &R)
where
    T: Debug,
    R: Debug,
{
    log::warn!(
        "Warning: These items do not have the same identifiers.\n\
        But we are trying to merge them in a list. This might result in corrupted data.\n\
        This is probably because of a programming error. Stopped merger of data.\n\
        --------------------------------\n\
        \tDifferent identifiers: \n{:#?}\n!=\n{:#?}\n\
        --------------------------------",
        dest,
        src
    );
    print_backtrace("Different identifiers");
}

/// Displays a shortened backtrace to see where the call came from.
fn print_backtrace(name: &str) {
    let bt = Backtrace::new();
    let frames = bt.frames();
    let mut backtrace_string = String::new();
    let max_line = 11;
    for (frame, line) in frames.iter().zip(0..max_line) {
        let symbol = frame.symbols().get(0).unwrap();
        let mut name = String::new();
        if let Some(name_value) = &symbol.name() {
            let full_name = name_value.to_string();
            let parts: Vec<&str> = full_name.split("::").collect();
            if parts.len() >= 3 {
                name = format!(
                    "{}::{}",
                    parts.get(parts.len() - 3).unwrap_or(&""),
                    parts.get(parts.len() - 2).unwrap_or(&""),
                );
            }
        }
        // Will not be set on some systems.
        // see: https://docs.rs/backtrace/latest/backtrace/struct.Symbol.html
        let mut filepath = String::new();
        if let Some(filename) = &symbol.filename() {
            let line_nr = &symbol.lineno().unwrap_or_default();
            let path = filename.to_str().unwrap();
            let (_, path) = path.split_at(path.find("df_st").unwrap_or(0));
            filepath = format!(" => {}:{}", path, line_nr);
        }
        if line != 0 {
            backtrace_string = format!("{}{}: {}{}\n", backtrace_string, line, name, filepath);
        }
    }
    log::debug!(
        "Backtrace for '{}' (see above): \n{}",
        name,
        backtrace_string
    );
}

pub trait ConvertingUtils {
    /// Convert a the String to lowercase and replace underscore.
    fn to_uniform_name(&self) -> Option<String> {
        None
    }

    /// Negative numbers to None
    /// Use to set `-1` values to `None`
    fn negative_to_none(&self) -> Option<i32> {
        None
    }

    /// Remove negative numbers from the list
    /// Use to remove `-1` values
    fn remove_negative(&self) -> Option<Vec<i32>> {
        None
    }
}

impl ConvertingUtils for Option<String> {
    fn to_uniform_name(&self) -> Option<String> {
        match self {
            Some(string_value) => {
                let mut string_value = string_value.clone();
                string_value = string_value.to_ascii_lowercase();
                string_value = string_value.replace('_', " ");
                Some(string_value)
            }
            None => None,
        }
    }
}

impl ConvertingUtils for Option<i32> {
    fn negative_to_none(&self) -> Option<i32> {
        match self {
            Some(value) => {
                if value < &0 {
                    return None;
                }
                Some(*value)
            }
            None => None,
        }
    }
}

impl ConvertingUtils for Option<Vec<i32>> {
    fn remove_negative(&self) -> Option<Vec<i32>> {
        match self {
            Some(list) => {
                let new_list = list.clone().into_iter().filter(|item| *item >= 0).collect();
                Some(new_list)
            }
            None => None,
        }
    }
}

/// Trait for object that are used for filling `Fillable` structs
pub trait Filler<D, S>
where
    D: Fillable + Debug,
    S: Debug,
{
    /// Add data to self that is set as `None` or appends list items.
    /// Use source object to get values from.
    fn add_missing_data(&mut self, source: &S);

    /// Add data to self from source. Same ways as `add_missing_data`.
    /// This function is used when adding data when looping over lists.
    /// This allows the `index` to be used. For example as an incremental ID.
    /// This function can be overwritten if this ID is wanted.
    fn add_missing_data_indexed(&mut self, source: &S, _index: u64) {
        self.add_missing_data(source);
    }

    /// Add data to self that is set as `None` or appends list items.
    /// If a value is `Some` it will leave the value as is.
    fn never_replace_data(&mut self, source: &S)
    where
        Self: std::fmt::Debug,
    {
        log::warn!("Warning: `never_replace_data` is called for an object where\n\
        this function is not implemented but still called. This function is part of the `Filler` trait.\n\
        Consider calling `add_missing_data` instead.\n\
        --------------------------------\n\
        \tNot implemented for (combination): \n{:#?}\n&\n{:#?}\n\
        --------------------------------",
        self, source);
    }

    /// Add data to self. If already some value present, replace it.
    /// Appends list items.
    /// This is used when there is data that is richer then previous data.
    /// It does not trow the `not_same_data` warning.
    /// `add_missing_data` should be used in most cases instead.
    fn replace_data(&mut self, source: &S)
    where
        Self: std::fmt::Debug,
    {
        log::warn!("Warning: `replace_data` is called for an object where\n\
        this function is not implemented but still called. This function is part of the `Filler` trait.\n\
        Consider calling `add_missing_data` instead.\n\
        --------------------------------\n\
        \tNot implemented for (combination): \n{:#?}\n&\n{:#?}\n\
        --------------------------------",
        self, source);
    }

    fn check_mergeable(&self, source: &S) -> bool
    where
        Self: Debug + Default + PartialEq<Self>,
        S: PartialEq<Self> + Debug,
    {
        // If we are adding thing to a default object we
        // allow the identifiers to match. This is not ideal but
        // this way we do not trip over this all the time and
        // can replace `replace_data` with `add_missing_data` in a lot of cases.
        if self == &(Self::default()) {
            // log::debug!("Warning: not same id, but self is default.");
            return true;
        }
        if source != self {
            log::warn!(
                "Warning: Items are not the same (not same identifiers)\n\
                This will result in corrupted data. Stopping merging data.\n\
                --------------------------------\n\
                \tDifferent Identifiers: \n{:#?}\n!=\n{:#?}\n\
                --------------------------------",
                self,
                source
            );
            return false;
        }
        true
    }
}

// Impl for i32 to i32
impl Fillable for i32 {}
impl Filler<i32, i32> for i32 {
    fn add_missing_data(&mut self, source: &i32) {
        *self = *source;
    }
    fn never_replace_data(&mut self, source: &i32) {
        *self = *source;
    }
    fn replace_data(&mut self, source: &i32) {
        *self = *source;
    }
}

// Impl for Option<i32> to i32
impl Filler<i32, Option<i32>> for i32 {
    fn add_missing_data(&mut self, source: &Option<i32>) {
        if let Some(value) = source {
            *self = *value;
        }
    }
    fn never_replace_data(&mut self, source: &Option<i32>) {
        if let Some(value) = source {
            *self = *value;
        }
    }
    fn replace_data(&mut self, source: &Option<i32>) {
        if let Some(value) = source {
            *self = *value;
        }
    }
}

// Impl for u32 to u32
impl Fillable for u32 {}
impl Filler<u32, u32> for u32 {
    fn add_missing_data(&mut self, source: &u32) {
        *self = *source;
    }
    fn never_replace_data(&mut self, source: &u32) {
        *self = *source;
    }
    fn replace_data(&mut self, source: &u32) {
        *self = *source;
    }
}

// Impl for u8 to u8
impl Fillable for u8 {}
impl Filler<u8, u8> for u8 {
    fn add_missing_data(&mut self, source: &u8) {
        *self = *source;
    }
    fn never_replace_data(&mut self, source: &u8) {
        *self = *source;
    }
    fn replace_data(&mut self, source: &u8) {
        *self = *source;
    }
}

// Impl for bool to bool
impl Fillable for bool {}
impl Filler<bool, bool> for bool {
    fn add_missing_data(&mut self, source: &bool) {
        *self = *source;
    }
    fn never_replace_data(&mut self, source: &bool) {
        *self = *source;
    }
    fn replace_data(&mut self, source: &bool) {
        *self = *source;
    }
}

/// If a value is added the option is always `Some`.
/// The absence of the add_missing_data defines `None`.
impl Filler<Option<bool>, Option<()>> for Option<bool> {
    fn add_missing_data(&mut self, source: &Option<()>) {
        *self = source.as_ref().map(|_| true);
    }
    fn never_replace_data(&mut self, source: &Option<()>) {
        if self.is_some() {
            return;
        }
        *self = source.as_ref().map(|_| true);
    }
    fn replace_data(&mut self, source: &Option<()>) {
        *self = source.as_ref().map(|_| true);
    }
}

/// If a value is "true" or "false" use there respective boolean value.
/// It the tag was a self closing tag `<example/>` then it will still be correct.
/// This is so both (self closing and non-self closing) ways will work.
/// So it works if the type was `Option<bool>` or `Option<()>` but both have to work.
impl Filler<Option<bool>, Option<String>> for Option<bool> {
    fn add_missing_data(&mut self, source: &Option<String>) {
        let result = if let Some(value) = &source {
            match value.as_ref() {
                "true" => true,
                "false" => false,
                "1" => true,
                "0" => false,
                _ => true,
            }
        } else {
            false
        };
        *self = Some(result);
    }
    fn never_replace_data(&mut self, source: &Option<String>) {
        if self.is_some() {
            return;
        }
        let result = if let Some(value) = &source {
            match value.as_ref() {
                "true" => true,
                "false" => false,
                "1" => true,
                "0" => false,
                _ => true,
            }
        } else {
            false
        };
        *self = Some(result);
    }
    fn replace_data(&mut self, source: &Option<String>) {
        let result = if let Some(value) = &source {
            match value.as_ref() {
                "true" => true,
                "false" => false,
                "1" => true,
                "0" => false,
                _ => true,
            }
        } else {
            false
        };
        *self = Some(result);
    }
}

// Impl for String to String
impl Fillable for String {}
impl Filler<String, String> for String {
    fn add_missing_data(&mut self, source: &String) {
        *self = source.clone();
    }
    fn never_replace_data(&mut self, source: &String) {
        *self = source.clone();
    }
    fn replace_data(&mut self, source: &String) {
        *self = source.clone();
    }
}

// Impl for T to Option<T>
// impl<T> Filler<Option<T>, T> for Option<T> where
//     T: Default + PartialEq<T> + ToString + Clone, {
//     fn add_missing_data(&mut self, source: &T){
//         let value =  source.clone();
//         match self {
//             Some(x) => {
//                 if &value != x {
//                     not_same_data((*x).to_string(), (value).to_string());
//                 }
//             },
//             None => {
//                 *self = Some(value);
//             },
//         }
//     }
// }

// Impl for Option<T> to Option<T>
impl<T> Fillable for Option<T> {}
// impl<T> Filler<Option<T>, Option<T>> for Option<T> where
//     T: Default + PartialEq<T> + ToString + Clone, {
//     fn add_missing_data(&mut self, source: &Option<T>){
//         let value = match source {
//             // In case of strings for example we want to
//             // make a copy not just use the reference
//             #[allow(clippy::clone_on_copy)]
//             Some(x) => x.clone(),
//             // if we want to save no value we are done
//             None => return,
//         };
//         match self {
//             Some(x) => {
//                 if &value != x {
//                     not_same_data((*x).to_string(), (value).to_string());
//                 }
//             },
//             None => {
//                 *self = Some(value);
//             },
//         }
//     }
// }

// Impl for Vec<R> to Vec<T>
impl<T> Fillable for Vec<T> {}
impl<T, R> Filler<Vec<T>, Vec<R>> for Vec<T>
where
    T: Fillable + Filler<T, R> + Default + Debug,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug,
{
    /// For each item in `source` list
    ///  * If already in list, `add_missing_data` on both elements
    ///  * Else push item to `self` list
    fn add_missing_data(&mut self, source: &Vec<R>) {
        // check new items
        for (index, item) in source.iter().enumerate() {
            // If item already exists
            // This can be slow for long lists, use IndexMap in those cases.
            let mut found_object_in_self: Option<&mut T> = None;
            for item2 in self.iter_mut() {
                if item == item2 {
                    found_object_in_self = Some(item2);
                    break;
                }
            }
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.add_missing_data_indexed(item, index as u64);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.add_missing_data_indexed(item, index as u64);
                self.push(new_item);
            }
        }
    }

    fn never_replace_data(&mut self, source: &Vec<R>) {
        // check new items
        for item in source {
            // if item already exists
            let mut found_object_in_self: Option<&mut T> = None;
            for item2 in self.iter_mut() {
                if item == item2 {
                    found_object_in_self = Some(item2);
                    break;
                }
            }
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    // merge data, but do not replace
                    item2.never_replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.never_replace_data(item);
                self.push(new_item);
            }
        }
    }

    fn replace_data(&mut self, source: &Vec<R>) {
        // check new items
        for item in source {
            // if item already exists
            let mut found_object_in_self: Option<&mut T> = None;
            for item2 in self.iter_mut() {
                if item == item2 {
                    found_object_in_self = Some(item2);
                    break;
                }
            }
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.replace_data(item);
                self.push(new_item);
            }
        }
    }
}

// Impl for IndexMap<u64,R> to IndexMap<u64,T>
impl<T> Fillable for IndexMap<u64, T> {}
impl<T, R> Filler<IndexMap<u64, T>, IndexMap<u64, R>> for IndexMap<u64, T>
where
    T: Fillable + Filler<T, R> + Default + Debug + Hash,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug + Hash,
{
    /// For each item in `source` list
    ///  * If already in list, `add_missing_data` on both elements
    ///  * Else push item to `self` list
    fn add_missing_data(&mut self, source: &IndexMap<u64, R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for (index, item) in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.add_missing_data_indexed(item, *index);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.add_missing_data_indexed(item, *index);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn never_replace_data(&mut self, source: &IndexMap<u64, R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for (_index, item) in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    // merge data, but do not replace
                    item2.never_replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.never_replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn replace_data(&mut self, source: &IndexMap<u64, R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for (_index, item) in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }
}

// Impl for HashSet<R> to HashSet<T>
impl<T> Fillable for HashSet<T> {}
impl<T, R> Filler<HashSet<T>, HashSet<R>> for HashSet<T>
where
    T: Fillable + Filler<T, R> + Default + Debug + Eq + Hash,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug,
{
    /// For each item in `source` list
    ///  * If already in list, `add_missing_data` on both elements
    ///  * Else push item to `self` list
    fn add_missing_data(&mut self, source: &HashSet<R>) {
        // check new items
        for item in source {
            // Create new item to search the value (so it is T not R)
            let mut new_item = T::default();
            new_item.add_missing_data(item);
            // If item already exists
            let found_object_in_self: Option<T> = self.take(&new_item);
            // add data to the element in the list
            if let Some(mut item2) = found_object_in_self {
                if item == &item2 {
                    item2.add_missing_data(item);
                    // add item back to list
                    self.insert(item2);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                self.insert(new_item);
            }
        }
    }

    fn never_replace_data(&mut self, source: &HashSet<R>) {
        // check new items
        for item in source {
            // Create new item to search the value (so it is T not R)
            let mut new_item = T::default();
            new_item.never_replace_data(item);
            // If item already exists
            let found_object_in_self: Option<T> = self.take(&new_item);
            // add data to the element in the list
            if let Some(mut item2) = found_object_in_self {
                if item == &item2 {
                    item2.never_replace_data(item);
                    // add item back to list
                    self.insert(item2);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                self.insert(new_item);
            }
        }
    }

    fn replace_data(&mut self, source: &HashSet<R>) {
        // check new items
        for item in source {
            // Create new item to search the value (so it is T not R)
            let mut new_item = T::default();
            new_item.replace_data(item);
            // If item already exists
            let found_object_in_self: Option<T> = self.take(&new_item);
            // add data to the element in the list
            if let Some(mut item2) = found_object_in_self {
                if item == &item2 {
                    item2.replace_data(item);
                    // add item back to list
                    self.insert(item2);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                self.insert(new_item);
            }
        }
    }
}

// Impl for Vec<R> to IndexMap<u64,T>
impl<T, R> Filler<IndexMap<u64, T>, Vec<R>> for IndexMap<u64, T>
where
    T: Fillable + Filler<T, R> + Default + Debug + Hash,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug + Hash,
{
    /// For each item in `source` list
    ///  * If already in list, `add_missing_data` on both elements
    ///  * Else push item to `self` list
    fn add_missing_data(&mut self, source: &Vec<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for (index, item) in source.iter().enumerate() {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.add_missing_data_indexed(item, index as u64);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.add_missing_data_indexed(item, index as u64);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn never_replace_data(&mut self, source: &Vec<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for item in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    // merge data, but do not replace
                    item2.never_replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.never_replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn replace_data(&mut self, source: &Vec<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for item in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }
}

// Impl for HashSet<R> to IndexMap<u64, T>
impl<T, R> Filler<IndexMap<u64, T>, HashSet<R>> for IndexMap<u64, T>
where
    T: Fillable + Filler<T, R> + Default + Debug + Hash,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug + Hash,
{
    /// For each item in `source` list
    ///  * If already in list, `add_missing_data` on both elements
    ///  * Else push item to `self` list
    fn add_missing_data(&mut self, source: &HashSet<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for (index, item) in source.iter().enumerate() {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.add_missing_data_indexed(item, index as u64);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.add_missing_data_indexed(item, index as u64);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn never_replace_data(&mut self, source: &HashSet<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for item in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    // merge data, but do not replace
                    item2.never_replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.never_replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }

    fn replace_data(&mut self, source: &HashSet<R>) {
        // Set size (TODO check if implemented in IndexMap)
        // self.reserve(...);
        // check new items
        for item in source {
            // hash item
            let mut hasher = self.hasher().build_hasher();
            item.hash(&mut hasher);
            let hash = hasher.finish();
            // if item already exists
            let found_object_in_self: Option<&mut T> = self.get_mut(&hash);
            // add data to the element in the list
            if let Some(item2) = found_object_in_self {
                if item == item2 {
                    item2.replace_data(item);
                } else {
                    not_same_id(&item, &item2);
                }
            } else {
                // not found in self so add to list
                let mut new_item = T::default();
                new_item.replace_data(item);
                // hash new_item
                let mut hasher = self.hasher().build_hasher();
                new_item.hash(&mut hasher);
                let hash = hasher.finish();

                self.insert(hash, new_item);
            }
        }
    }
}

// Impl for Option<R> to Option<T>
impl<T, R> Filler<Option<T>, Option<R>> for Option<T>
where
    T: Fillable + Filler<T, R> + Default + Debug,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug + Clone,
{
    fn add_missing_data(&mut self, source: &Option<R>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x.clone(),
            // if we want to save no value we are done
            None => return,
        };
        match self {
            Some(x) => {
                // Give warning if data is not the same
                if value != *x {
                    not_same_data(&x, &value);
                } else {
                    x.add_missing_data(&value);
                }
            }
            None => {
                let mut new_value = T::default();
                new_value.add_missing_data(&value);
                *self = Some(new_value);
            }
        }
    }

    fn never_replace_data(&mut self, source: &Option<R>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x.clone(),
            // if we want to save no value we are done
            None => return,
        };
        match self {
            Some(_x) => {
                // Do nothing, do not replace value
            }
            None => {
                let mut new_value = T::default();
                new_value.never_replace_data(&value);
                *self = Some(new_value);
            }
        }
    }

    fn replace_data(&mut self, source: &Option<R>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x.clone(),
            // if we want to save no value we are done
            None => return,
        };
        match self {
            Some(x) => {
                // always replace the data
                x.replace_data(&value);
            }
            None => {
                let mut new_value = T::default();
                new_value.replace_data(&value);
                *self = Some(new_value);
            }
        }
    }
}

// Impl for Option<Vec<R>> to Vec<T>
impl<T, R> Filler<Vec<T>, Option<Vec<R>>> for Vec<T>
where
    T: Fillable + Filler<T, R> + Default + Debug,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug,
{
    fn add_missing_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.add_missing_data(value);
    }

    fn never_replace_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.never_replace_data(value);
    }

    fn replace_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.replace_data(value);
    }
}

// Impl for Option<Vec<R>> to IndexMap<u64,T>
impl<T, R> Filler<IndexMap<u64, T>, Option<Vec<R>>> for IndexMap<u64, T>
where
    T: Fillable + Filler<T, R> + Default + Debug + Hash,
    // Requires <R> == <T> comparisons
    R: PartialEq<T> + Debug + Hash,
{
    fn add_missing_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.add_missing_data(value);
    }

    fn never_replace_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.never_replace_data(value);
    }

    fn replace_data(&mut self, source: &Option<Vec<R>>) {
        let value = match source {
            // In case of strings for example we want to
            // make a copy not just use the reference
            #[allow(clippy::clone_on_copy)]
            Some(x) => x,
            // if we want to save no value we are done
            None => return,
        };
        self.replace_data(value);
    }
}

// Impl for R to Vec<T>
// impl<T, R> Filler<Vec<T>, Option<R>> for Vec<T> where
//     T: Fillable + Filler<Vec<T>,R>,
//     // Requires <R> == <T> comparisons
//     R: {
//     fn add_missing_data(&mut self, source: &Option<R>){
//         let value = match source {
//             // In case of strings for example we want to
//             // make a copy not just use the reference
//             #[allow(clippy::clone_on_copy)]
//             Some(x) => x,
//             // if we want to save no value we are done
//             None => return,
//         };
//         self.add_missing_data(value);
//         // if let Some(value) = source{
//         //     self.add_missing_data(value);
//         // }
//     }
// }
