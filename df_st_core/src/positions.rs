use crate::fillable::{Fillable, Filler};
use crate::HasUnknown;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::fmt;
use std::hash::Hash;

/// A coordinate of a region tile
/// The coordinates are of region tiles (129x129 for medium maps)
/// More info about region tiles can be found
/// [here](https://dwarffortresswiki.org/index.php/DF2014:World_generation#World_size).
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Hash,
    Fillable,
    Filler,
    Default,
    Eq,
    PartialEq,
    JsonSchema,
    GraphQLObject,
)]
pub struct Coordinate {
    pub id: i32,
    pub x: i32,
    pub y: i32,
}

#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Hash,
    Fillable,
    Filler,
    Default,
    Eq,
    PartialEq,
    JsonSchema,
    GraphQLObject,
)]
pub struct Rectangle {
    pub id: i32,
    pub x1: i32,
    pub y1: i32,
    pub x2: i32,
    pub y2: i32,
}

#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Hash,
    Fillable,
    Filler,
    Default,
    Eq,
    PartialEq,
    JsonSchema,
    GraphQLObject,
)]
pub struct Path {
    pub id: i32,
    pub x: i32,
    pub y: i32,
    pub flow: i32,
    pub exit_tile: i32,
    pub elevation: i32,
}

impl HasUnknown for Coordinate {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for Rectangle {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

impl HasUnknown for Path {
    fn print_unknown(&self, _prefix: &str, _unknown_map: &mut HashMap<String, u32>) {}
    fn combine_unknown(
        &self,
        _prefix: &str,
        _unknown_combiner: &mut HashMap<String, Vec<(String, Value)>>,
    ) {
    }
}

/// Force a new format for `Coordinate` to not clutter the screen
impl fmt::Debug for Coordinate {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&format! {"({},{})", self.x, self.y})
    }
}

impl fmt::Display for Coordinate {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {}, {})", self.x1, self.y1, self.x2, self.y2)
    }
}

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "({}, {}, {}, {}, {})",
            self.x, self.y, self.flow, self.exit_tile, self.elevation
        )
    }
}

impl Coordinate {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for Coordinate {
    fn example() -> Self {
        Self::default()
    }
}

impl Rectangle {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for Rectangle {
    fn example() -> Self {
        Self::default()
    }
}

impl Path {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for Path {
    fn example() -> Self {
        Self::default()
    }
}
