/// Trait used to add missing IDs.
/// This trait that is implemented for most objects,
/// as it is required by `add_missing_ids`.
pub trait CreateNew {
    fn new_by_id(id: i32) -> Self;
}
