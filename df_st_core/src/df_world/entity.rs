use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Entity is a collective of something in the world.
/// This is usually in the form of a civilization or government.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Entity {
    /// Identifier for the entity.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub name: Option<String>,
    pub honor: Vec<EntityHonor>,
    pub race: Option<String>,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    // for Religion and MilitaryUnit
    pub worship_id: Vec<i32>,
    // for MilitaryUnit: favorite weapons
    pub weapon: Vec<String>,
    // for Guild
    pub profession: Vec<String>,
    pub entity_link: Vec<EntityLink>,
    pub entity_position: Vec<EntityPosition>,
    /// A list of the current `HistoricalFigure`s assigned to positions.
    /// This list only includes the current positions, not past assignments or positions.
    /// Not all positions listed in `entity_position` are in this list.
    pub entity_position_assignment: Vec<EntityPositionAssignment>,
    /// A list of all `HistoricalFigure`s associated with/part of the Entity
    /// in any point in history.
    pub hf_ids: Vec<i32>,
    pub child_en_ids: Vec<i32>,
    pub claims: Vec<Coordinate>,
    pub occasion: Vec<EntityOccasion>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityHonor {
    pub local_id: i32,
    pub name: Option<String>,
    pub gives_precedence: Option<i32>,
    pub requires_any_melee_or_ranged_skill: Option<bool>,
    pub required_skill_ip_total: Option<i32>,
    pub required_battles: Option<i32>,
    pub required_years: Option<i32>,
    pub required_skill: Option<String>,
    pub required_kills: Option<i32>,
    pub exempt_ep_id: Option<i32>,
    pub exempt_former_ep_id: Option<i32>,
    pub granted_to_everybody: Option<bool>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityLink {
    pub local_id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub target: Option<i32>,
    pub strength: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityPosition {
    pub local_id: i32,
    pub name: Option<String>,
    pub name_male: Option<String>,
    pub name_female: Option<String>,
    pub spouse: Option<String>,
    pub spouse_male: Option<String>,
    pub spouse_female: Option<String>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityPositionAssignment {
    pub local_id: i32,
    pub hf_id: Option<i32>,
    pub position_id: Option<i32>,
    pub squad_id: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityOccasion {
    pub local_id: i32,
    pub name: Option<String>,
    pub event: Option<i32>,
    pub schedule: Vec<EntityOccasionSchedule>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityOccasionSchedule {
    pub local_id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    // for THROWING_COMPETITION
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,

    pub reference: Option<i32>,
    pub reference2: Option<i32>,

    pub feature: Vec<EntityOccasionScheduleFeature>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct EntityOccasionScheduleFeature {
    pub local_id: i32,

    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub reference: Option<i32>,
}

impl CreateNew for Entity {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Entity {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityHonor {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityLink {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityPosition {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityPositionAssignment {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityOccasion {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityOccasionSchedule {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for EntityOccasionScheduleFeature {
    fn example() -> Self {
        Self::default()
    }
}

impl PartialEq for EntityLink {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityHonor {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityHonor {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityPosition {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityPosition {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityPositionAssignment {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityPositionAssignment {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityOccasion {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityOccasion {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityOccasionSchedule {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityOccasionSchedule {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}

impl PartialEq for EntityOccasionScheduleFeature {
    fn eq(&self, other: &Self) -> bool {
        self.local_id == other.local_id
    }
}

impl std::hash::Hash for EntityOccasionScheduleFeature {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.local_id.hash(state);
    }
}
