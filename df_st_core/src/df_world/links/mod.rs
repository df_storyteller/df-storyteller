pub mod link_he_entity;
pub mod link_he_hf;
pub mod link_he_site;

pub use link_he_entity::*;
pub use link_he_hf::*;
pub use link_he_site::*;

use std::collections::HashSet;

#[macro_export]
macro_rules! add_all_from_id_vec(
    { $links:ident, $left_id:ident, $right:ident: [$($right_ids:ident),* $(,)*] } => {
        {
            $(
                Self::add_from_vec(&mut $links, $left_id, &$right.$right_ids);
            )*
        }
     };
);

#[macro_export]
macro_rules! add_all_from_id_option(
    { $links:ident, $left_id:ident, $right:ident: [$($right_id:ident),* $(,)*] } => {
        {
            $(
                Self::add_from_option(&mut $links, $left_id, $right.$right_id);
            )*
        }
     };
);

trait IdLink
where
    Self: std::marker::Sized + std::cmp::Eq + std::hash::Hash + std::fmt::Debug + Clone,
{
    fn new_link(left_id: i32, right_id: i32) -> Self;
    fn get_left_id(&self) -> i32;
    fn get_right_id(&self) -> i32;
    fn get_struct_name() -> &'static str;
    fn get_left_name() -> &'static str;
    fn get_right_name() -> &'static str;

    fn add_from_option(list: &mut HashSet<Self>, left_id: Option<i32>, right_id: Option<i32>) {
        if left_id.is_some() && right_id.is_some() {
            let left_id = left_id.unwrap_or(0);
            let right_id = right_id.unwrap_or(0);
            // Skip values that are -1 or lower
            if left_id < 0 || right_id < 0 {
                return;
            }
            list.insert(Self::new_link(left_id, right_id));
        }
    }

    fn add_from_vec(list: &mut HashSet<Self>, left_id: i32, right_ids: &[i32]) {
        for right_id in right_ids {
            list.insert(Self::new_link(left_id, *right_id));
        }
    }

    /// Remove all links where one of the 2 id's is above the maximum id.
    /// These are id's that are incorrect (uninitialized memory).
    fn remove_incorrect(links: &mut HashSet<Self>, max_left_id: i32, max_right_id: i32) {
        // This is added because of https://github.com/DFHack/dfhack/issues/1629
        let mut remove_list: HashSet<Self> = HashSet::new();
        for item in links.iter() {
            if item.get_left_id() > max_left_id {
                log::warn!(
                    "Found {} where {} is incorrect: {:#?}",
                    Self::get_struct_name(),
                    Self::get_left_name(),
                    item
                );
                remove_list.insert(item.clone());
            }
            if item.get_right_id() > max_right_id {
                log::warn!(
                    "Found {} where {} is incorrect: {:#?}",
                    Self::get_struct_name(),
                    Self::get_right_name(),
                    item
                );
                remove_list.insert(item.clone());
            }
        }
        // Remove all items that where incorrect
        for item in remove_list.iter() {
            links.remove(item);
        }
    }
}
