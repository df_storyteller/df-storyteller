use super::IdLink;
use crate::fillable::{Fillable, Filler};
use crate::{add_all_from_id_option, DFWorld, SchemaExample};
use df_st_derive::{Fillable, Filler};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::Hash;

/// An link between a `HistoricalEvent` and a `Entity`.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
    Hash,
    PartialEq,
    Eq,
)]
pub struct LinkHEEntity {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a Entity.
    pub entity_id: i32,
}

impl IdLink for LinkHEEntity {
    fn new_link(left_id: i32, right_id: i32) -> Self {
        Self {
            he_id: left_id,
            entity_id: right_id,
        }
    }
    fn get_left_id(&self) -> i32 {
        self.he_id
    }
    fn get_right_id(&self) -> i32 {
        self.entity_id
    }
    fn get_struct_name() -> &'static str {
        "LinkHEEntity"
    }
    fn get_left_name() -> &'static str {
        "he_id"
    }
    fn get_right_name() -> &'static str {
        "entity_id"
    }
}

impl LinkHEEntity {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn create_links(world: &DFWorld) -> HashSet<Self> {
        let mut links: HashSet<Self> = HashSet::new();

        for (_, he) in &world.historical_events {
            // let he_id = he.id;
            let he_id_o = Some(he.id);
            // add_all_from_id_vec!(links, he_id, he: [
            //     a_hf_ids,
            // ]);
            add_all_from_id_option!(links, he_id_o, he: [
                civ_entity_id,
                depot_entity_id,
                dest_entity_id,
                entity_id,
                entity_id_1,
                entity_id_2,
                giver_entity_id,
                join_entity_id,
                joined_entity_id,
                joiner_entity_id,
                payer_entity_id,
                receiver_entity_id,
                relevant_entity_id,
                site_entity_id,
                source_entity_id,
                trader_entity_id,
                victim_entity,
            ]);
        }
        // Check if relations correct
        let max_he_id = world.historical_events.len() as i32;
        let max_entity_id = world.entities.len() as i32;
        Self::remove_incorrect(&mut links, max_he_id, max_entity_id);

        links
    }
}

impl SchemaExample for LinkHEEntity {
    fn example() -> Self {
        Self::default()
    }
}
