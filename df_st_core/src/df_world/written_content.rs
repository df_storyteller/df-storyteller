use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A written work that was create at some point in its history in the world.
/// This reference to what is written not the book itself.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct WrittenContent {
    /// Identifier for the written form.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub title: Option<String>,
    pub author_hf_id: Option<i32>,
    pub author_roll: Option<i32>,
    pub form: Option<String>,
    pub form_id: Option<i32>,
    pub page_start: Option<i32>,
    pub page_end: Option<i32>,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub style: Vec<WCStyle>,
    // TODO change to author_id or author_hf_id
    pub author: Option<i32>,
    pub reference: Vec<WCReference>,
}

/// A reference (in a written work) to some other object in the world.
#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct WCReference {
    pub written_content_parent_id: i32,
    /// Assigned after parsing
    pub local_id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub type_id: Option<i32>,

    pub written_content_id: Option<i32>,
    pub he_id: Option<i32>,
    pub site_id: Option<i32>,
    pub poetic_form_id: Option<i32>,
    pub musical_form_id: Option<i32>,
    pub dance_form_id: Option<i32>,
    pub hf_id: Option<i32>,
    pub entity_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub subregion_id: Option<i32>,
}

/// The style of a written work.
#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct WCStyle {
    pub written_content_id: i32,
    /// Assigned after parsing
    pub local_id: i32,
    pub label: Option<String>,
    pub weight: Option<i32>,
}

impl PartialEq for WCReference {
    fn eq(&self, other: &Self) -> bool {
        self.written_content_parent_id == other.written_content_parent_id
            && self.local_id == other.local_id
    }
}

impl std::hash::Hash for WCReference {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.written_content_parent_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq for WCStyle {
    fn eq(&self, other: &Self) -> bool {
        self.written_content_id == other.written_content_id && self.local_id == other.local_id
    }
}

impl std::hash::Hash for WCStyle {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.written_content_id.hash(state);
        self.local_id.hash(state);
    }
}

impl CreateNew for WrittenContent {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for WrittenContent {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for WCReference {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for WCStyle {
    fn example() -> Self {
        Self::default()
    }
}
