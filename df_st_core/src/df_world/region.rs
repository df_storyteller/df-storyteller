use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A Region is a section of map that has specific properties.
/// These regions are usually described by natural features likes forest, oceans or lakes.
/// Every region tile should be in at least one `Region`.
/// [`Sites`](crate::df_world::site::Site) are thus always in at least one `Region`.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Region {
    /// Identifier for the region.
    /// `id` must be unique for the whole world.
    pub id: i32,
    /// Name of the region.
    /// `name` is in all lowercase.
    pub name: Option<String>,
    /// Defines what biome or natural feature the region is.
    /// Example: "Forest", "Lake", "Tundra",...
    #[serde(rename = "type")]
    pub type_: Option<String>,
    /// A list of coordinates for each square that is covered by this region
    /// More info can be found in [`Coordinate`](crate::positions::Coordinate)
    pub coords: Vec<Coordinate>,
    /// Defines if the region is good, neutral or evil.
    pub evilness: Option<String>,
    /// From DFHacK: historical figure IDs of force deities associated
    /// with the region. Number set increases during civ placement.
    pub force_ids: Vec<i32>,
}

impl CreateNew for Region {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Region {
    fn example() -> Self {
        Self {
            id: 5,
            name: Some("the dune of miles".to_owned()),
            type_: Some("Desert".to_owned()),
            coords: vec![
                Coordinate {
                    id: 20,
                    x: 10,
                    y: 20,
                },
                Coordinate {
                    id: 21,
                    x: 10,
                    y: 21,
                },
            ],
            evilness: Some("good".to_owned()),
            force_ids: vec![20, 21],
        }
    }
}
