use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Artifact is valuable or remarkable object in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Artifact {
    /// Identifier for the artifact.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub name: Option<String>,
    pub site_id: Option<i32>,
    pub structure_local_id: Option<i32>,
    pub holder_hf_id: Option<i32>,

    pub abs_tile_x: Option<i32>,
    pub abs_tile_y: Option<i32>,
    pub abs_tile_z: Option<i32>,
    pub region_id: Option<i32>,

    pub item_name: Option<String>,
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,
    pub item_writing: Option<i32>,
    pub item_page_number: Option<i32>,
    pub item_page_written_content_id: Option<i32>,
    pub item_writing_written_content_id: Option<i32>,
    pub item_description: Option<String>,
    pub item_mat: Option<String>,
}

impl CreateNew for Artifact {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Artifact {
    fn example() -> Self {
        Self::default()
    }
}
