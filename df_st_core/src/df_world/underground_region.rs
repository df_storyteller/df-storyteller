use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A Underground Region is a section of map that has specific properties.
/// The underground regions are below the surface of the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct UndergroundRegion {
    /// Identifier for the underground region.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub type_: Option<String>,
    pub depth: Option<i32>,
    /// A list of coordinates for each square that is covered by this underground region
    /// More info can be found in [`Coordinate`](crate::positions::Coordinate)
    pub coords: Vec<Coordinate>,
}

impl CreateNew for UndergroundRegion {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for UndergroundRegion {
    fn example() -> Self {
        Self::default()
    }
}
