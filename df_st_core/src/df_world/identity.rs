use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A (assumed) Identity in the world in the world.
/// This identity is (usually) fake. (TODO: Check)
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Identity {
    /// Identifier for the assumed/fake identity.
    /// `id` must be unique for the whole world.
    pub id: i32,
    /// The name used for this identity
    pub name: Option<String>,
    /// Link to the Historical Figure that uses this identity
    pub hf_id: Option<i32>,
    /// The race of the new identity
    pub race: Option<String>,
    /// The gender of the new identity
    pub caste: Option<String>,
    /// The year this assumed/fake identity was presumably born in
    pub birth_year: Option<i32>, // TODO Date
    /// The month/day this assumed/fake identity was presumably born in
    pub birth_second: Option<i32>,
    /// The profession/job the identity presumably has/is doing
    pub profession: Option<String>,
    /// The entity the identity is presumably part of
    pub entity_id: Option<i32>,
    /// Might be referring to hf_id (TODO)
    pub nemesis_id: Option<i32>,
}

impl CreateNew for Identity {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Identity {
    fn example() -> Self {
        Self::default()
    }
}
