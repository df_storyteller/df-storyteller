use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A form of poetry that was create at some point in its history in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct PoeticForm {
    /// Identifier for the poetic form.
    /// `id` must be unique for the whole world.
    pub id: i32,
    /// The name of the poetic form
    pub name: Option<String>,
    /// The description of what the poem feels like or is about
    pub description: Option<String>,
}

impl CreateNew for PoeticForm {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for PoeticForm {
    fn example() -> Self {
        Self::default()
    }
}
