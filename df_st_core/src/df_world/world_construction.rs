use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Large construction in the world.
/// This is usually in he from of roads, bridges and tunnels.
/// These roads and tunnels connect different sites together.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct WorldConstruction {
    /// Identifier for the world construction.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub name: Option<String>,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub coords: Vec<Coordinate>,
}

impl CreateNew for WorldConstruction {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for WorldConstruction {
    fn example() -> Self {
        Self::default()
    }
}
