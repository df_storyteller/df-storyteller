use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::hash::Hash;

/// A Creature is a species that can exist in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Creature {
    /// Identifier for the creature.
    /// `id` must be unique for the whole world.
    /// This value is not given from files but generated after parsing.
    pub id: i32,

    // always present
    pub creature_id: Option<String>,
    pub name_singular: Option<String>,
    pub name_plural: Option<String>,

    // sometimes present
    /// The Biomes the creature will appear in.
    pub biomes: CreatureBiome,

    pub all_castes_alive: Option<bool>,
    pub artificial_hiveable: Option<bool>,

    pub does_not_exist: Option<bool>,
    pub equipment: Option<bool>,
    pub equipment_wagon: Option<bool>,
    pub evil: Option<bool>,

    pub fanciful: Option<bool>,
    pub generated: Option<bool>,
    pub good: Option<bool>,

    pub has_any_benign: Option<bool>,
    pub has_any_can_swim: Option<bool>,
    pub has_any_cannot_breathe_air: Option<bool>,
    pub has_any_cannot_breathe_water: Option<bool>,
    pub has_any_carnivore: Option<bool>,
    pub has_any_common_domestic: Option<bool>,
    pub has_any_curious_beast: Option<bool>,
    pub has_any_demon: Option<bool>,
    pub has_any_feature_beast: Option<bool>,
    pub has_any_flier: Option<bool>,
    pub has_any_fly_race_gait: Option<bool>,
    pub has_any_grasp: Option<bool>,
    pub has_any_grazer: Option<bool>,
    pub has_any_has_blood: Option<bool>,
    pub has_any_immobile: Option<bool>,
    pub has_any_intelligent_learns: Option<bool>,
    pub has_any_intelligent_speaks: Option<bool>,
    pub has_any_large_predator: Option<bool>,
    pub has_any_local_pops_controllable: Option<bool>,
    pub has_any_local_pops_produce_heroes: Option<bool>,
    pub has_any_megabeast: Option<bool>,
    pub has_any_mischievous: Option<bool>,
    pub has_any_natural_animal: Option<bool>,
    pub has_any_night_creature: Option<bool>,
    pub has_any_night_creature_bogeyman: Option<bool>,
    pub has_any_night_creature_experimenter: Option<bool>,
    pub has_any_night_creature_hunter: Option<bool>,
    pub has_any_night_creature_nightmare: Option<bool>,
    pub has_any_not_fireimmune: Option<bool>,
    pub has_any_not_flier: Option<bool>,
    pub has_any_not_living: Option<bool>,
    pub has_any_outsider_controllable: Option<bool>,
    pub has_any_power: Option<bool>,
    pub has_any_race_gait: Option<bool>,
    pub has_any_semimegabeast: Option<bool>,
    pub has_any_slow_learner: Option<bool>,
    pub has_any_supernatural: Option<bool>,
    pub has_any_titan: Option<bool>,
    pub has_any_unique_demon: Option<bool>,
    pub has_any_utterances: Option<bool>,
    pub has_any_vermin_hateable: Option<bool>,
    pub has_any_vermin_micro: Option<bool>,
    pub has_female: Option<bool>,
    pub has_male: Option<bool>,

    pub large_roaming: Option<bool>,
    pub loose_clusters: Option<bool>,
    pub mates_to_breed: Option<bool>,
    pub mundane: Option<bool>,

    pub occurs_as_entity_race: Option<bool>,
    pub savage: Option<bool>,
    pub small_race: Option<bool>,

    pub two_genders: Option<bool>,
    pub ubiquitous: Option<bool>,

    pub vermin_eater: Option<bool>,
    pub vermin_fish: Option<bool>,
    pub vermin_grounder: Option<bool>,
    pub vermin_rotter: Option<bool>,
    pub vermin_soil: Option<bool>,
    pub vermin_soil_colony: Option<bool>,
}

/// The Biomes the creature will appear in.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Eq,
    PartialEq,
    Hash,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct CreatureBiome {
    pub cr_id: i32,
    pub desert_badland: Option<bool>,
    pub desert_rock: Option<bool>,
    pub desert_sand: Option<bool>,
    pub forest_taiga: Option<bool>,
    pub forest_temperate_broadleaf: Option<bool>,
    pub forest_temperate_conifer: Option<bool>,
    pub forest_tropical_conifer: Option<bool>,
    pub forest_tropical_dry_broadleaf: Option<bool>,
    pub forest_tropical_moist_broadleaf: Option<bool>,
    pub glacier: Option<bool>,
    pub grassland_temperate: Option<bool>,
    pub grassland_tropical: Option<bool>,
    pub lake_temperate_brackishwater: Option<bool>,
    pub lake_temperate_freshwater: Option<bool>,
    pub lake_temperate_saltwater: Option<bool>,
    pub lake_tropical_brackishwater: Option<bool>,
    pub lake_tropical_freshwater: Option<bool>,
    pub lake_tropical_saltwater: Option<bool>,
    pub marsh_temperate_freshwater: Option<bool>,
    pub marsh_temperate_saltwater: Option<bool>,
    pub marsh_tropical_freshwater: Option<bool>,
    pub marsh_tropical_saltwater: Option<bool>,
    pub mountain: Option<bool>,
    pub ocean_arctic: Option<bool>,
    pub ocean_temperate: Option<bool>,
    pub ocean_tropical: Option<bool>,
    pub pool_temperate_brackishwater: Option<bool>,
    pub pool_temperate_freshwater: Option<bool>,
    pub pool_temperate_saltwater: Option<bool>,
    pub pool_tropical_brackishwater: Option<bool>,
    pub pool_tropical_freshwater: Option<bool>,
    pub pool_tropical_saltwater: Option<bool>,
    pub river_temperate_brackishwater: Option<bool>,
    pub river_temperate_freshwater: Option<bool>,
    pub river_temperate_saltwater: Option<bool>,
    pub river_tropical_brackishwater: Option<bool>,
    pub river_tropical_freshwater: Option<bool>,
    pub river_tropical_saltwater: Option<bool>,
    pub savanna_temperate: Option<bool>,
    pub savanna_tropical: Option<bool>,
    pub shrubland_temperate: Option<bool>,
    pub shrubland_tropical: Option<bool>,
    pub subterranean_chasm: Option<bool>,
    pub subterranean_lava: Option<bool>,
    pub subterranean_water: Option<bool>,
    pub swamp_mangrove: Option<bool>,
    pub swamp_temperate_freshwater: Option<bool>,
    pub swamp_temperate_saltwater: Option<bool>,
    pub swamp_tropical_freshwater: Option<bool>,
    pub swamp_tropical_saltwater: Option<bool>,
    pub tundra: Option<bool>,
}

impl CreateNew for Creature {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Creature {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for CreatureBiome {
    fn example() -> Self {
        Self::default()
    }
}
