use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A form of music that was create at some point in its history in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct MusicalForm {
    /// Identifier for the music form.
    /// `id` must be unique for the whole world.
    pub id: i32,
    /// The name of the musical form
    pub name: Option<String>,
    /// The description of what the music sounds like
    pub description: Option<String>,
}

impl CreateNew for MusicalForm {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for MusicalForm {
    fn example() -> Self {
        Self::default()
    }
}
