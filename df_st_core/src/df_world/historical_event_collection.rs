use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A Collection of Historical Events that are related to each other.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct HistoricalEventCollection {
    /// Identifier for the historical event collection.
    /// `id` must be unique for the whole world.
    pub id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,

    pub start_year: Option<i32>, // TODO Date
    pub start_seconds72: Option<i32>,
    pub end_year: Option<i32>,
    pub end_seconds72: Option<i32>,

    // All others are optional
    pub a_support_merc_en_id: Option<i32>,
    pub a_support_merc_hf_id: Vec<i32>,
    pub adjective: Option<String>,
    pub aggressor_ent_id: Option<i32>,
    pub attacking_en_id: Option<i32>,
    pub attacking_hf_id: Vec<i32>,
    pub attacking_merc_enid: Option<i32>,
    pub attacking_squad_animated: Vec<bool>, // Vec<Option<bool>>,
    pub attacking_squad_deaths: Vec<i32>,
    pub attacking_squad_entity_pop: Vec<i32>,
    pub attacking_squad_number: Vec<i32>,
    pub attacking_squad_race: Vec<String>,
    pub attacking_squad_site: Vec<i32>,
    pub civ_id: Option<i32>,
    pub company_merc: Option<bool>,
    pub coords: Option<String>, // TODO parse further

    pub d_support_merc_en_id: Option<i32>,
    pub d_support_merc_hf_id: Vec<i32>,
    pub defender_ent_id: Option<i32>,
    pub defending_en_id: Option<i32>,
    pub defending_hf_id: Vec<i32>,
    pub defending_merc_en_id: Option<i32>,
    pub defending_squad_animated: Vec<bool>, // Vec<Option<bool>>,
    pub defending_squad_deaths: Vec<i32>,
    pub defending_squad_entity_pop: Vec<i32>,
    pub defending_squad_number: Vec<i32>,
    pub defending_squad_race: Vec<String>,
    pub defending_squad_site: Vec<i32>,
    pub he_ids: Vec<i32>,
    pub hec_ids: Vec<i32>,
    pub feature_layer_id: Option<i32>,
    pub individual_merc: Vec<bool>, // Vec<Option<bool>>,
    pub name: Option<String>,
    pub noncom_hf_id: Vec<i32>,
    pub occasion_id: Option<i32>,
    pub ordinal: Option<i32>,
    pub outcome: Vec<String>,
    pub parent_hec_id: Option<i32>,
    pub site_id: Option<i32>,
    pub subregion_id: Option<i32>,
    pub target_entity_id: Option<i32>,
    pub war_hec_id: Option<i32>,
}

impl CreateNew for HistoricalEventCollection {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for HistoricalEventCollection {
    fn example() -> Self {
        Self::default()
    }
}
