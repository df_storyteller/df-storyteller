use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::{Coordinate, Rectangle};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Site {
    /// Identifier for the site.
    /// `id` must be unique for the whole world.
    /// By default the id's of `Sites` start with 1, not 0 as most other objects.
    pub id: i32,
    /// Name of the site.
    /// `name` is in all lowercase.
    pub name: Option<String>,
    /// Defines what type of site it is
    /// Options: cave, hamlet, forest retreat, dark fortress,
    /// town, vault, dark pits, castle, tomb, monastery,
    /// camp, lair, shrine, ...
    #[serde(rename = "type")]
    pub type_: Option<String>,
    /// A coordinate for the region tile the site is in
    /// This means the coordinates are not exact and
    /// can be the same as other sites that are in the same region tile.
    /// More info can be found in [`Coordinate`](crate::positions::Coordinate)
    pub coord: Option<Coordinate>,
    // TODO: Find out what this is values between [0-522]
    pub rectangle: Option<Rectangle>,
    /// A list of all remarkable structures in the site
    pub structures: Vec<Structure>,
    pub site_properties: Vec<SiteProperty>,
    /// The civilization or group the site belongs too.
    /// ID for [`Entity`](crate::Entity)
    pub civ_id: Option<i32>,
    pub cur_owner_id: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct Structure {
    pub site_id: i32,
    pub local_id: i32,
    pub name: Option<String>,
    pub name2: Option<String>,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub subtype: Option<String>,
    pub entity_id: Option<i32>,
    pub worship_hf_id: Option<i32>,
    pub copied_artifact_ids: Vec<i32>,
    // temple
    pub deity_type: Option<i32>,
    pub deity_id: Option<i32>,
    pub religion_id: Option<i32>,
    // dungeon
    pub dungeon_type: Option<i32>,
    pub inhabitant_hf_ids: Vec<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct SiteProperty {
    pub site_id: i32,
    pub local_id: i32,
    #[serde(rename = "type")]
    pub type_: Option<String>,
    pub structure_id: Option<i32>,
    pub owner_hf_id: Option<i32>,
}

impl PartialEq for Structure {
    fn eq(&self, other: &Self) -> bool {
        self.site_id == other.site_id && self.local_id == other.local_id
    }
}

impl std::hash::Hash for Structure {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.site_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq for SiteProperty {
    fn eq(&self, other: &Self) -> bool {
        self.site_id == other.site_id && self.local_id == other.local_id
    }
}

impl std::hash::Hash for SiteProperty {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.site_id.hash(state);
        self.local_id.hash(state);
    }
}

impl CreateNew for Site {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Site {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for Structure {
    fn example() -> Self {
        Self::default()
    }
}

impl SchemaExample for SiteProperty {
    fn example() -> Self {
        Self::default()
    }
}
