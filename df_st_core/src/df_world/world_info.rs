use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// Information about the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct DFWorldInfo {
    /// Identifier for the world.
    /// `id` must be unique among all the worlds.
    pub id: i32,
    pub save_name: Option<String>,
    pub name: Option<String>,
    pub alternative_name: Option<String>,
    pub region_number: Option<i32>,
    pub year: Option<i32>,
    pub month: Option<i32>,
    pub day: Option<i32>,
}

impl CreateNew for DFWorldInfo {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for DFWorldInfo {
    fn example() -> Self {
        Self::default()
    }
}
