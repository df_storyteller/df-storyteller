# Contributing to DF Storyteller

Wonderful, we have been waiting here for you. You are very welcome to help us
make this application even better.

We will first of give you some general information. If you have any questions
you can join our [Discord] here. This document contains guidelines on how to
contribute to DF Storyteller. If you want to create your own painting/application using
DF Storyteller, [look here](docs/paintings.md). These are mostly guidelines,
not rules. Use your best judgment, and feel free to propose changes to this document.

## Code of Conduct

This project and everyone participating in it is governed by the
[Code of Conduct](/docs/code_of_conduct.md). By participating, you are expected
to uphold this code. Please report unacceptable behavior to
[df.storyteller@gmail.com](mailto:df.storyteller@gmail.com) or via [Discord]
and contact a moderator.

## How can I Contribute?

### Reporting Bugs
If you find a bug in our application you can follow these general steps:

* Look in the [Issues](https://gitlab.com/df_storyteller/df-storyteller/-/issues)
  page if someone already reported this issue.
* If you can not find it, please open a new issue. For this you can use the 'bug'
  template or an other template that might fit the problem better.
* Add as much information in there as you seem necessary or relevant.
* Thank you for helping us find all the bugs 🐛
* You might just have to wait a bit now, someone will respond to the issue and
  get it resolved.
  * If nobody response within a month it might be that the project is unmaintained.
    In this case you can always ask on [Discord][Discord] or fork the repository.

### Suggesting Enhancements

You can suggest improvements via [Discord] or by submitting an [issue].

### Your First Code Contribution

Unsure where to begin contributing to DF Storyteller? You can start by looking
through the `Beginner` and `Help Wanted` issues:

- [Beginner issues](https://gitlab.com/df_storyteller/df-storyteller/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Beginner) - issues
  which should only require a few lines of code, and a test or two.
- [Help wanted issues](https://gitlab.com/df_storyteller/df-storyteller/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Help%20Wanted) -
  issues which should be a bit more involved than `Beginner` issues.

If there are no issues with these labels or you want to work on something else
continue reading 😀. When you already have something in mind you want to work on
there are several approaches you can take.

* Open an issue to let us know you are interested in something. We can then use
  this issue give you feedback and help you out if you have any questions.
* Fork the repository. This will give you a copy to work in. You can then create
  a merge/pull request so we can add it back into the main repository.

Also check out the section on the [structure of DF Storyteller](#modules).

The main code is written in the programming language 'Rust', if you don't have 
experience in this language you can [get started here](https://www.rust-lang.org/learn/get-started). 
Write a little program yourself so you know how the basics work. 
This can already be Dwarf Fortress related, maybe you want to do something with the 
[Raw file from DF](https://dwarffortresswiki.org/index.php/DF2014:Raw_file) or use the 
DF Storyteller API [json files](https://crates.io/crates/serde_json) to create a little visualizer. 
Once you know a bit of Rust clone/download the DF Storyteller code and have a look around. 
Suggestion: Take a look in `df-storyteller/df_st_core/src/df_world/` folder.

## Style guides

### Git Commit Messages

* Limit the first line to 72 characters or less
* Reference issues and pull requests liberally after the first line

### Rust Style guide

Lets not reinvent the wheel here. We use the
[Rust Style Guide](https://github.com/rust-dev-tools/fmt-rfcs/blob/master/guide/guide.md).
This means we use [`rustfmt`](https://github.com/rust-lang/rustfmt) for format the code and 
[`clippy`](https://github.com/rust-lang/rust-clippy) for linting and best practices.

Clippy rules can not be ignored unless in very specific cases. 
New contributions should not change the clippy rules.

### Documentation Style guide

Use Markdown in GitLab and Markdown in Rust, simple but effective.

* If you reference the [DF Wiki](https://dwarffortresswiki.org), use the pages
  without the namespace. This improves future compatibility. For example: use
  `https://dwarffortresswiki.org/index.php/Material` instead of
  `https://dwarffortresswiki.org/index.php/DF2014:Material`. (unless it is
  intended to reference an old articles or the masterwork namespace.)

## Structure of DF Storyteller
<a name="modules"></a>

The DF Storyteller code is split into different workspaces. Here is a list of
all the modules and what they include. See the 
[Rust documentation](https://docs.dfstoryteller.com/rust-docs/) for more help.

* **df_st_cli**: This is the start of the CLI (Command-line-interface) application.
This has mostly to do with logging, error handling and the command line interface.
* **df_st_core**: One of the most important workspaces as it includes all the
structures that are returned via the API and the structure that is filled using
the parser. This workspace also includes most of the structure documentation
that is used to create the various documentation. Structures (`structs`) in this
workspace should be changes with care as they are used by almost all other workspaces.
* **df_st_db**: This workspace includes all the code and structures to store and
query data from the database. It also include code to copy all the data from the
database object to the Core structures.
* **df_st_derive**: This is a special workspace it includes code for
`#[derive()]` macros. Working on this workspace is not easy and you should only
touch this if you know how
[procedural macros](https://doc.rust-lang.org/reference/procedural-macros.html)
work. This is basically code that writes code during compile time.
* **df_st_api**: This includes all the RESTfull and GraphQL API code and static
pages. The documentation generation code for the APIs also live here.
* **df_st_legends**: Everything to do with parsing the `...-legends.xml` files.
And changing this data into Core structures.
* **df_st_legends_plus**: Everything to do with parsing the
`...-legends_plus.xml` files from [DFHack](https://github.com/DFHack/dfhack) and
changing this data into Core structures.
* **df_st_world_history**: Everything to do with parsing the
`...-world_history.txt` files. And changing this data into Core structures.
* **df_st_world_sites_and_pops**: Everything to do with parsing the
`...-world_sites_and_pops.txt` files. And changing this data into Core structures.
* **df_st_parser**: Code that interacts with all the parser workspaces
(like `df_st_legends`) and handling of unknown data found in the parsed files.
* **df_st_updater**: Code for checking for updates and deprecating old or unsafe 
versions.
* **df_st_image_maps**: Everything to do with parsing the
world map image files and converting them to png
* **df_st_image_site_maps**: Everything to do with parsing the
site map image files and converting them to png

DF Storyteller also includes some other workspaces but those are mainly there
because public crates are not available and might contain slightly altered code.
The goal should be to remove these from this repo at some point and move them
into other crates.

---

This document is partly based on
[Atoms contribution document](https://github.com/atom/atom/blob/master/CONTRIBUTING.md).

[Discord]: https://discord.gg/aAXt6uu
[issue]: https://gitlab.com/df_storyteller/df-storyteller/-/issues
