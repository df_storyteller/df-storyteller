#![forbid(unsafe_code)]
#![deny(clippy::all)]

// use serde::de::DeserializeOwned;
// use std::fs::File;
// use std::io::Read;
// use anyhow::Error;
//
// #[allow(dead_code)]
// fn read_text_file<C: DeserializeOwned>
// (filename: &str) -> Result<C, Error>{
//     let mut file = File::open(filename)?;
//     let mut data = String::new();
//     file.read_to_string(&mut data)?;
//     // TODO: add own parser
//     let parsed_object: C = serde_xml_rs::from_str(&data)?;
//     Ok(parsed_object)
// }

/// This function is not implemented!
pub fn parse_world_site_and_pops(filename: &str) {
    println!("The World Sites and Pops parser is not created yet.");
    println!("File did not get parsed: {}", filename);
    //read_text_file( filename ).unwrap()
}
