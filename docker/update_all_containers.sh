#!/bin/sh -e

# Clean all old builds
#docker system prune -a

# General

docker login

# Code check

docker build --no-cache -t dfstoryteller/code_check:x86_64-unknown-linux-gnu ./docker/code_check/
docker push dfstoryteller/code_check:x86_64-unknown-linux-gnu

# Linux

docker build --no-cache -t dfstoryteller/builder:x86_64-unknown-linux-gnu ./docker/x86_64-unknown-linux-gnu/
docker push dfstoryteller/builder:x86_64-unknown-linux-gnu

# Windows

docker build --no-cache -t dfstoryteller/builder:x86_64-pc-windows-gnu-0.2.0 ./docker/x86_64-pc-windows-gnu/
docker push dfstoryteller/builder:x86_64-pc-windows-gnu-0.2.0
