# Security in DF Storyteller
We (that is to say me, Ralph Bisschops) take security VERY serious.
This means we will do everything we can to keep out users save.
But mistakes can (and will) always happen. So if you have questions, feel free to ask them in our 
[Discord](https://discord.gg/aAXt6uu).
If you found a vulnerability you can email it to 
[contact.ralph.b@gmail.com](mailto:contact.ralph.b@gmail.com) or 
[df.storyteller@gmail.com](mailto:df.storyteller@gmail.com).
When reporting a vulnerability you can encrypt them using the PGP key(s) below.

PGP Keys:
* Ralph Bisschops: `9141 B8E0 9472 508C ED9D 6998 9EC4 61D9 D716 0787` [Verify with website](https://ralphbisschops.com/contactme.html)
<details><summary markdown="span">PGP Key</summary>

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF9RHQkBEAC+xXYWQmfCOPenYIEZBT9stguWgUgLdZTwYDfWuJ1tzWfbJqQw
YBNGwLGwSY0KUX9in5Pn0ZYjcn2ZjKHMrpjUfzhC4NsxQgJpqoMB6ErPJWv0MIjy
RATn1YHnmSK9kNVWsOzpe/PPjflIkkXFv4K1Cny+LT8JvcljvP5UszcLHI1cGNNA
79fBqoKofqR9fxccLYPGZULpHRQTGRVMRJTAJR9YHHD/eqmLh805/4Zych3mwKYm
ct2mNNPQicPR0PkRRkxSog910MJbxDMQ/cQnDtA7fTP785c6vwXWZqRNxUXNhBaq
dHDgt6fejLpAek2xAglrNiWQ0PZ9fyA7s87H20bgqPf2Mf6GkeeZePuC5Wo2gZTI
2xVdDjBMJ1NewcNEuJHc1BeoZ/tpm1GcrJqu/pXACUxGvGvxBAdas4tIbtGZm/7w
c0dv+doVPYSdie9rm515OI8jldC5zqjJMEsIBDxz/H1sSCdZb93VVFOJKp+bVb0h
V7LD/vGtc6EtpfEI+pF5MJ6o4m7Srvnlf8+U4fXaPtqgb/u4HfyB7lTbvCPC5wlM
458dYRaQPTGeNWdX53sEuyVP276em+mpMGryxaFuXkXsN366yO7v/AkwipRplz2t
Vdpij2KTkKc26rfmJIaRr0Ux0qe4NP+uhRnadwVqhISdOTHA6fVafYtkvQARAQAB
tEJSYWxwaCBCaXNzY2hvcHMgKFB1YmxpYyBDb250YWN0IGVtYWlsKSA8Y29udGFj
dC5yYWxwaC5iQGdtYWlsLmNvbT6JAlQEEwEKAD4WIQSRQbjglHJQjO2daZiexGHZ
1xYHhwUCX1EdCQIbAwUJB4MeFwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRCe
xGHZ1xYHh7GmEAC0zQ+iDCcdJ8Vlxu36tszf06qlSz0iGW/lVWkS/Vvrcu1srOCG
g2QOMZDerPwuIDucQJ1Yz4E3TzibOck0VWqCuVdX+1N/E53Qem1x70uLkQDP4i8i
IznrP7GnRhFeZYuV2O3g0CZeYHBuzozSySMeHEBTb3MlBsFJ5ZgQUG/iWbxjxc+n
qXVYbLvilPcReDek/7twOLyW+PH2unzGkvJ/mpgg16HrdlsMY1/JQQ3Kcl7C2MxO
lfF9Xvzvy61dJQvtsiW8sgALxa8Hgk484e/Yn9vF3t+tQ+Th2iFYBn/u14Rb4tBQ
nuJbdGWFZMkQaTIT9v6WkCYdJxmyH+dAWgU5MDgPRA8BrgMs9RbsiEU/TUdpRaAk
kHF0q7Yji06dIPG5RvKiTgOfRn0LY1NuMb0gg2BNjPc917WK0mVcwByhJvAesHka
RA2mrjuEe8fUDnETIvST4uw1w8+mGpHpV0d7OyvYOT8cOB3y+hKiqvejq/bfZOKa
to4rVTmOA4n+jKkOTcys0nFuw5FPkYTei1AoBJkUR14BxAyLf5K1KX6jiIt53HBn
Gz+8xufJJWF+Kr1yy/E5eoCjFCm4kuSeY/29XVENe6d3RKWefQe0YvMrlmUnTV3Y
z2NZfBvyOsA6jeWcTeZgzVLHeMdkMKA/ckp/6dBlNj0U8odJryJYen9zobkCDQRf
UR0JARAAnn7VqvoroBYTyDbs7xnibQwTBLnUuKPMVtRoEdpRmkDkJ9BJJ5hyMJWp
o1zeUk+qslNV0klB3hW3wJ66AI83Yj++xkmt7nKfJQuHn4kgTLmG28PsCI8SkRCH
Udf5YTAuYUdkppZ3hukSVR2NnfzKI1Jt6qga7WOB+ntiv4HYkSQrswzjVUEGRBhR
J2nKM29wQVJ+OL1YRJSL7ECrqmoW/B2OouwWdQW4cm4XUh7gJg5Mro9HILyJXr2b
Ox4wbXh3zSR3gdkyFCfaMVoHmF/viljsMwItJ73YRHu7bkWLLZh14RT79P5nerrd
e9zsb9KjOI7PnCAC1zC6xmzPci3RAgt4pkvmj21nxyo1gPUp7pRGcFSXqat9S1pC
nOU7BmxTJ7EVlldLDtDSknXnv7u+1s5nLoMm1lPIQIBE7MV6byOLjEBUMzbxU7wj
Jvs3cCb1tCyLxfJvQypsQnwB3i61lLCV/SFYJzOjvayzzXqyJt9Bsuxn+s82bOqN
KApP+DFjh0StUwGYZFIzm12xtnVzdt6i8ZXbd9Bh7usjRWS2b1/4L6CKwBmjKm1I
zp8Tav1iveiX5RXgfy03jHkQNAqSmZuDViT7+9FTgGUV4wi94gMnFA6lChgq0WUU
YvsICR1HqbOZkSc5Ui7noxaJ7Rk1kxhJf6PGo2xgHFOBcoN3+cEAEQEAAYkCPAQY
AQoAJhYhBJFBuOCUclCM7Z1pmJ7EYdnXFgeHBQJfUR0JAhsMBQkHgx4XAAoJEJ7E
YdnXFgeHIzkP/32gyEDByXzirDbcceW/8ZiNNyeUhC5fTInAogh/8vR9GZJ6FIVe
+FKBuuti3rrEZAJv5j1bolqOuSJChrw4GM8s29pfvZIhfsOwWQw6n8/WA8pZjQem
OyujQ+AWk46bzY6ZfLNA2vZOYbFVAKoSjBjVuzB8gu4NQMcWYzJ6NLWFbfnuz02Q
3Vram/FQlgVtlr6QHdeLhy+dq5ZeDGrHZHJ6Juxv44zm6cm7SqibQZ+1j4+7rdtV
IAApfWsRv0Y1u+71KF616y4ZIKvLXJzEeb75S0aPSaAi3kcDCWaPj8Nu3vt8jxYZ
YWL7kVrL38gtbWOUfQBdC/K7HSbBjKcr/V+HYxMtqUgi2WdI2v3YC4OkHnkTEY93
165sVfPk5nbZH+hE/rPZNvO69a1dQuSahlMBNOk/oNjjcN0uA3sbCWs6TpHJUkJD
cSn2iUyvdDnsLs4AWvEDbmZqS81AXJJYTuFtSMcJq4evLUqEvKGSd9KGe9Ar8bex
dxyfJUbKuZzmQulFn+lkGe++NqyhnzhoZFSfjuX6zo3221RConB+VvvJkbjuZiIn
NNGHg9/74HnqRxq5/xKBpsgibUxzlEJSorlNSRcD/sZn4CxXq24C2rZcaKJVC/oG
9/KQIKbpmdhy/xwZ3fBIYneHajID1FFdkmRvW9aybQzh0VcLjgSTLQAQ
=sxe+
-----END PGP PUBLIC KEY BLOCK-----
```

</details>

Because we can only fix problems if we know about them, we will encourage users to test the 
security of our application[^1]. (In the context that it is used.) We do not have a budget for 
a Bug Bounty program. But if you correctly report vulnerabilities to us we will see what we
can do for you.

[^1]: Only test the security of your own instance of the application not on others peoples instances!

## Intended use
The intended use of our application is to be used on a computer that is used behind a NAT router.
This means that the computers ports are not exposed to the public internet.
DF Storyteller was NEVER designed to be used on public servers!

## Known "problems"
There are a few thing that we know that might pose a security risk but because the intended
use of our application there should not be an issue.

If you have a good solution for any of these problems let us know!

### Public internet
If the user is in a shared/public space and/or uses public internet this guaranty is not
enough in most cases. This is why our default settings are set to only allow connections from 
the `localhost`. This setting can be changed by the user to allow all connections.
This setting should not be enabled when on public internet.
We have no way to detect if users is currently on public internet. This means it is up to the
user to keep track of this. (Which is not ideal but the best we can do.)

### HTTPS
We currently do not have HTTPS enabled by default. This is because all traffic is intended for
`localhost` or some other internal IP. You are not allowed to sign a certificate for ip 
addresses. We could do this with a self signed certificate. But in that case the certificate has 
to be create on the clients computer. This comes with a lot of extra things that have to be 
taken into account. This makes the project much bigger and complexer so for the time being we are
not intending to implement this.

All traffic should never pass the NAT boundary and thus should not be able to be monitored.
Unless there is someone malicious on you internal network. But in that case I think you have
bigger problems then someone monitoring your Dwarf Fortress world.

## Technical security implementations

### Update checking
TODO: Add more details
We use a [Curve 25519](https://en.wikipedia.org/wiki/Curve25519) in its 
[ed25519](https://en.wikipedia.org/wiki/EdDSA#Ed25519) form for signing update messages.
Note that we are NOT encrypting the message!
This message and its signature are Base64 encoded in order to store both the message and signature
in a json string.
The signature is a 512 bit signature created using SHA-512.

When the application opens it will download the validation file from one of the sources provided.
If there are multiple sources it will go though them and check until it finds a valid message that
allows the version to be used or explicitly disallows the version.
If an error occurred or it detects that the message has been tampered with it will check the next 
source. If no sources are available it will return an error and close the application.

If the user does not have an internet connection it will permit the execution of the application.
As it does not know if the version is outdated or not.

The messages are in the form:
```json
{
    "version": "0.1.0",
    "status": "latest",
    "message": null,
    "valid_until": "2020-06-30T00:00:00Z"
}
```

The `version` field should match the version of the application exactly.

The `status` field can be 3 options: `"latest"`,`"update_available"` or `"update_required"`. 
If the status is `latest` it will allow the application to start.
If the status is `update_available` it will prompt the user that there is an update and 
start the application.
If the status is `update_required` it will prompt the user that there is an update and
terminate the application. This will disallow old versions for being used.

The `message` field is an optional field. In the case that a message is given it will output this 
message to the user and continue.

The `valid_until` field is a RFC3339 timestamp of the date and time this message is valid.
If this day is passed the message will no longer be accepted. This is compared to the system time 
and the `Date` header field of the response from the server.

The `version` field does not allows tokens to be reused for other versions. The `valid_until` 
field prevents tokens form being infinitely valid. This combined with the signature that 
authenticates the messages comes from a trusted source (developers) should now allow tampering
with the tokens.

Then there should just be a good balance between how long a token is valid and how often we want
to resign the token.

### Used cryptographic mechanisms
This project used [Curve 25519](https://en.wikipedia.org/wiki/Curve25519) in its 
[ed25519](https://en.wikipedia.org/wiki/EdDSA#Ed25519) form, 
[base64](https://en.wikipedia.org/wiki/Base64) (not used any crypto, just for message 
conversion/character escape), 
[SHA-2](https://en.wikipedia.org/wiki/SHA-2) (only Sha256 used for etag generation)

Libraries used: 
- [ed25519-dalek](https://github.com/dalek-cryptography/ed25519-dalek), 
- [base64](https://github.com/marshallpierce/rust-base64), 
- [sha2](https://github.com/RustCrypto/hashes)

All keys meet the [requirement for NIST](https://www.keylength.com/en/compare/) for the next 
18 years (year 2038 for the time or writing in 2020).
Current key size: 
- Hash: 256 bits ( Sha256 )
- Elliptic curve: 256 bits ( Curve 25519, 128 bits of security )
- Key Signing: 256 bits ( Ed25519: Curve 25519, with Sha256 )
