# Setup DF Storyteller on a server

This guide will explain how to setup DF Storyteller on a server. But note that this is not a
full guide. We will skip over smaller steps in order to keep the guide smaller.
Each step will still be described, but might require some searching on the internet for more info
or a guide.

NOTE: You can host DF Storyteller on your computer and do not need a server for this!
This is only needed when you want to have public internet access to your DF Storyteller instance.
For most users this is NOT needed, for normal use see: https://guide.dfstoryteller.com/

This guide assumes you know a bit about linux command or can figure it out along the way.

If you have questions or get stuck somewhere, feel free to ask on [Discord](https://discord.com/invite/aAXt6uu).

## 1. Get a server

Servers are very cheap/free these days and are a great way to learn more about Linux and a lot
of other computer related things.

You can get free trail servers on most cloud hosting platforms. Here are a few examples:
- DigitalOcean ($100 free credit) though https://try.digitalocean.com/freetrialoffer/
- Linode ($100 free credit) though https://www.linode.com/
- Google Cloud Platform (GCP) (free `e2-micro` in some regions) https://cloud.google.com/free/docs/gcp-free-tier/
- Microsoft Azure (free Azure VM for 750h B1s burstable virtual machines for 12 months) https://azure.microsoft.com/en-us/free/
- Amazon (AWS) EC2 (750 h/month for 12 months = 1 year free) https://aws.amazon.com/ec2/
- ... Many more

If you have an educational email you can also apply for even more free credits on some services.

Just pick one of these platforms, most are similar in there offerings.
Note that for DF Storyteller the location of the server is not important the latency is not
going to be a big factor.

We are going to need the following things from the service (all of the platforms have this):
- A Linux virtual machine (VM) or virtual server (are the same thing in most cases).
- I'm assuming you are going to use Debian or Ubuntu as your base OS. This is very common and most
  likely the default. The difference between the 2 for servers is minimal.
- Just pick the cheapest machine they offer. 1 CPU core and 1-2GB RAM is enough.
- Disk space is usually included and depends on the size of the world you are going to import.

At this point just look up how to setup the server on the platform you choose.
Just search `setup debian server on <platform name>` and you will find a guide.
This usually involves the following steps:
- Creating an account at cloud provider. (might involve adding a credit card depending on the service)
- Selecting server you want to spin up:
- - Linux VM
- - OS: Debian or Ubuntu (select latest [LTS version](https://ubuntu.com/blog/what-is-an-ubuntu-lts-release))
- - VM Size: `nano`, `macro` or `small` in most cases. (1 vCPU and 1-2GB RAM/Memory is enough)
- - No additional features or storage is needed in most cases.
- Create SSH key to securely login to server.
- - Windows: use Windows Subsystem for Linux or [Putty](https://www.puttygen.com/download-putty)
- - Linux: build in terminal commands `ssh` and `ssh-keygen`.
- - Max OS: build in terminal commands `ssh` and `ssh-keygen`.
- Connect to server. This will give you a terminal on the server.

## 2. Install software on server.

From here on we will use terminal commands to do things on the server.

First make sure you update the server:
```bash
sudo apt update
sudo apt upgrade
```
Maybe a reboot is required: `sudo reboot` (and log back in after the server has restarted)

### Install and setup Postgres
We will use Postgres because this will be faster and easier on a server.
```bash
sudo apt install postgresql
# Change Postgres password
sudo -u postgres psql postgres
# Replace password. Create something strong as you will not need it often.
ALTER USER postgres WITH password '...password...';
```

### Install and setup DF Storyteller
NOTE: update the server number below to the latest version.
https://gitlab.com/df_storyteller/df-storyteller/-/releases
```bash
# Select the right version
DF_ST_VERSION="0.3.0-development-3"
# Download version from server
wget "https://dfstoryteller.com/versions/${DF_ST_VERSION}/DF_Storyteller_v${DF_ST_VERSION}_Linux_x86_64_Postgres.zip"
# Unpack the zip archive
unzip "DF_Storyteller_v${DF_ST_VERSION}_Linux_x86_64_Postgres.zip"
# Create new folder
sudo mkdir -p "/usr/share/df_storyteller/"
# Copy file to new folder
sudo cp "DF_Storyteller_v${DF_ST_VERSION}_Linux_x86_64_Postgres/df_storyteller" "/usr/share/df_storyteller/df_storyteller"
# Give execute permission
sudo chmod +x "/usr/share/df_storyteller/df_storyteller"
# Create config file and setup database
# This will require you Postgres password from above
/usr/share/df_storyteller/df_storyteller database --drop-db
# The command should not give any errors and finish with: `Done updating database.`
```

Now lets setup a 'service' so that DF Storyteller automatically starts when the server (re)starts
and restarts when it crashes.
```bash
# Download service file
wget "https://dfstoryteller.com/versions/df_storyteller.service"
# If you want to edit the file, now is the moment. By default it will start the service for world `1`.
# vim "df_storyteller.service"
# Copy file
sudo cp df_storyteller.service /etc/systemd/system/
# Change permissions
sudo chown root:root /etc/systemd/system/df_storyteller.service
sudo chmod go-wx /etc/systemd/system/df_storyteller.service
sudo chmod u-x /etc/systemd/system/df_storyteller.service
# Register and enable service
sudo systemctl enable df_storyteller.service
# Start service
sudo service df_storyteller start
```

The default `df_storyteller.service` will look like this:
```ini
[Unit]
Description=DF Storyteller
[Service]
PIDFile=/tmp/DF_Storyteller.pid
User=df_storyteller
Group=df_storyteller
Restart=always
KillSignal=SIGTERM
WorkingDirectory=/usr/share/df_storyteller/
ExecStart=/usr/share/df_storyteller/df_storyteller start -w 1
[Install]
WantedBy=multi-user.target
```

If something is going wrong you can use the following commands to find out what the problem is.
```bash
# View service status
service df_storyteller status
# View logs
journalctl -u sanjoya_cal_sync.service
```

### Install and setup NginX.
We will use this to proxy the DF Storyteller service.
This will allow us to configure TLS (HTTPS).
```bash
sudo apt install nginx
```

Config nginx proxy
```bash
# Create new config file
# You can use the template from below
vim /etc/nginx/sites-available/df_storyteller
# Enable config
sudo ln -s /etc/nginx/sites-available/df_storyteller /etc/nginx/sites-enabled/df_storyteller
# Restart NginX
sudo service nginx restart
```

The default `df_storyteller` NginX config will look like this:
The first section is for hosting you application/visualizer.
The second section is needed for the DF Storyteller proxy.
If you don't have a domain remove the `server_name` lines and you have to move the visualizer to
a separate port (`8080` for example).
```
# DF Storyteller visualizer (optional)
server {
    listen 80;
    listen [::]:80;

    server_name my_df_st_domain.com;
    root /var/www/html/my-app/;

    # For 1 page applications:
    location / {
        try_files $uri $uri/ /index.html;
    }
}

# DF Storyteller proxy
server {
    listen 80;
    listen [::]:80;

    server_name api.my_df_st_domain.com;

    location / {
        proxy_pass http://127.0.0.1:20350;
    }
}
```

### Port forwarding/firewall

Most platforms secure there servers so no ports are exposed to the internet.
So you have to enable some port to pass thought the firewall.
This is usually under the "security" or "firewall" section in the cloud providers platform.

What to allow:
- Port 80  (http) on TCP for all ip's (0.0.0.0/0)
- Port 443 (https) on TCP for all ip's (0.0.0.0/0)

Everything else should be blocked.
You can check the open TCP port using the public ip in an nmap scan:
`sudo nmap -sS xxx.xxx.xxx.xxx`

### Enable HTTPS (TLS)

To allow secure connections you need a domain name.
We are not going over the setup directly.
But in short you should:
- Set your domain settings (via your registrar)
- Install `certbot` to get a Certificate from "Let's Encrypt" (this is free).
- Change the NginX config a bit, certbot can also do this automatically.

Here is a guide that will walk you through it:
https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/
Or
https://adamtheautomator.com/nginx-subdomain/#Setting_Up_HTTPS_on_NGINX_Domain_and_Subdomain

## 3. Import world

Now that everything is set up you can start preparing for importing the world to the server.
Because your server has less RAM then your PC (in most cases) you want to to the parsing on your PC
and export the result from there.

Import the world in your local instance. 
Make sure your local instance saves it to the Postgres database, not SQLite.
It is important that you import the world with the same number as in the `df_storyteller.service` file.
Follow the steps here: https://guide.dfstoryteller.com/import.html

Now export the world using postgres:
```bash
# Export local database (run on your machine or use pgAdmin 4 to backup the database)
pg_dump –u df_storyteller df_storyteller > df_storyteller_database_export.pgsql
# Upload file to server (using filezilla or `scp`)
# Import on server
psql df_storyteller < df_storyteller_database_export.pgsql
# Restart DF Storyteller might be needed
service df_storyteller restart
```

Done. Now you DF Storyteller should work on the domain or IP of your server.
The home page of the server should show a page similar to the [website](https://dfstoryteller.com/).

If you have questions or get stuck somewhere, feel free to ask on [Discord](https://discord.com/invite/aAXt6uu).
