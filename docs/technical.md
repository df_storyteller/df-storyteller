# Technical documentation
This document describes various things that might be useful to better understand the code
or help in special circumstances.


## Features and Deprecation

### Version
The following versioning will be used for API calls and Object fields.
This means all changes to the API should be documented.

The an API call is added in a later versions it will have this specified in the 
documentation as seen below.
```rust
/// Some general info
/// 
/// ( Since = "0.1.0" )
pub fn some_api_call(){}
```

When at a later point there is a breaking change introduced to the API call
it will be marked as:
```rust
/// Some general info
/// 
/// ( Since = "0.1.0", Breaking_Change = "0.2.0" )
pub fn some_api_call(){}
```
When multiple breaking changes happened the version number will be updated to the
latest version where the breaking change happened. The old version number should
be documented in the changelog.


### Features
When a function uses a particular feature it will have this specified in the 
documentation as seen below.
```rust
/// Some general info
/// 
/// ( Feature = "some_feature", Since = "0.1.0" )
pub fn some_api_call(){}
```

### Deprecation
When code is no longer supported and will be or is deprecated should be 
specified with:
```rust
#[deprecated(since="0.5.0", note="please use `new_method` instead")]
```
Then an API Call is being deprecated it should (if possible) still work in the
next version of DF Storyteller. A warning should be added whenever the deprecated
function is called in order to notify the user and developer that it should be changed.