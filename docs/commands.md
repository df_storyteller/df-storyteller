# Command your storyteller
DF Storyteller is designed as a command line tool. This allows us to provide
different functionalities depending on what the user wants. The commands can
also be used by other applications to open the application in different ways.

## Help

The commands below might change depending on the version you are using.
The help flag will provide you with a list of all the commands and flags
that your version implements. We will only cover a few commands here.
You can also use the help command in subcommand to get more information
about the subcommand.

```bash
# Get the general help from the app
./df_storyteller --help
# Get the help pages for a subcommand (`import` in this case)
./df_storyteller import --help
```

## Guide

This command will guide you through some steps that most people will use.
The guide will give you the desired commands to start use this application.
The guide will include the following steps. The steps might change depending
on selected options.

* Install, Setup and Configure DF Storyteller and its database.
* Export a world from Dwarf Fortress (optionally using DFHack).
* Import a world into DF Storyteller.
* Start the DF Storyteller API server.

To start this guide by running:

```bash
./df_storyteller guide
```

You can also find the [guide on our website](https://guide.dfstoryteller.com).

## Import

This command lets you import a new world into the database. It reads the exported
files and parses them and insert them in the database. 
You only have to provide the path to one file or folder and it
will automatically search that folder for all other files and include them in the save.

The program is written to get as much data as possible from the provided files. If there is data
missing it should still complete without any problems and just accept unknown values.
(if this is not the case please open an
[issue](https://gitlab.com/df_storyteller/df-storyteller/-/issues).)

```bash
# Import a new world from the folder `world`. It will search for more info by itself.
# The world will be saved in the database under world id `1`.
./df_storyteller import -w 1 ./world/region1-00250-01-01-legends.xml
# or import using the folder
./df_storyteller import -w 1 ./world/
```

Once all the data is parsed it will insert it into the database.
The application will then close itself. You can now start the API server by
using the `start` subcommand, see next section for more info.

World id `0` is used as a fallback. Because of this it is not recommended to use world id 0.

If the world id you give if already found it will update/merge/overwrite the existing 
items in the database for that world id. This can be used to add more info after you 
already imported the world.
Because of upstream libraries this is only supported for Postgres, not SQLite. 
SQLite will stop the application as soon as it finds a database conflict. 
As soon as [Diesel](https://github.com/diesel-rs/diesel) creates its next feature release 
this should be supported.
This can also be used to import worlds that are to big to import at once. 
(Note that foreign key constraints might give errors).

## Start

This subcommand will start one of the imported worlds and allows you to view it
using the API (or via a painter). You can also use this to view the API
documentation.

```bash
# Start the API server with world id=`1`
./df_storyteller start -w 1
```

## List

This subcommand will show a list of all the worlds found in the database.
This way you know which world to start.

```bash
# View a list of all the imported worlds and there ids
./df_storyteller list
# Example output:
# List of worlds in database:
#   1: Moÿoecamo (The Mythical Universe), Year: 50 Region: 6
#   2: Zavazoram (The Momentous World), Year: 50 Region: 14
```

## Export

(Not implemented yet)
Save one of the imported worlds as an `df_storyteller` file. This can be an
json or xml file. This file includes all the data in a combined form.
These files can be large so watch out with opening them.

```bash
# Export the world with id=`1` to the `./df_storyteller.json` file
./df_storyteller export -w 1 ./
# Export the world with id=`2` to the `./my-personal-export.xml` file
./df_storyteller export -w 2 ./my-personal-export.xml
```

## Docs
<a name="docs"></a>

This will export all the documentation to local file. So API server does not
have to be up in order to view them. It will also export an `openapi.json`
file that can be used in other documentation viewers that you want.
(Currently only available for the RESTfull API, GraphQL implementation
does not have good documentation builders.)

You can also view the documentation when using the `start` subcommand.

```bash
# Export documentation to the `./docs/`` folder
./df_storyteller docs ./docs/
```

## Config

This subcommand will create a valid config file. It will read an already existing file and write it back.
If no file is found it will create the config file using the default configuration.
```bash
# Create/overwrite the config file `df_storyteller-config.json`.
./df_storyteller config
```

## Database

This subcommand sets up the database. It can be used for both SQLite and Postgres.
```bash
# Run all migration, aka setup the database using existing configurations.
./df_storyteller database
```

The following steps are only for Postgres.
To setup Postgres we created this subcommand to help you with this.
```bash
# Start configuring the Postgres database.
./df_storyteller database --postgres
```
The command above will ask you for your Postgres password.
This password is used to login to the database once, 
create a user called `df_storyteller` and then log back out.
It will then login as `df_storyteller` and create the database `df_storyteller`
(and delete if it already exist if the `--drop-db` is set)
It will then create the `df_storyteller-config.json` file with the correct info in it.

More flags are available use `df_storyteller database --help` to see them all.