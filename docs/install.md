# Install your own storyteller!
<a name="install"></a>
DF Storyteller is written to be used by other applications so it might
be included in the visualizer you want to use. Look here for a
[list of visualizers (painters)](docs/paintings.md).
To get started, download the executable from the
[release page](https://gitlab.com/df_storyteller/df-storyteller/-/releases).

---

There are more detailed steps on how to configure DF Storyteller included in
the `guide` subcommand. You can also find these steps here:

## Use the [Online Guide](https://guide.dfstoryteller.com/)

---

## Windows

[You can find a video with instructions here.](https://www.youtube.com/watch?v=8PAYa5lkP24)

Pre-requirements (PostgreSQL or SQLite):
* PostgreSQL (minimum version unknown at the moment.) (https://www.postgresql.org/download/windows/)
  * Check on selected components (at least): 'PostgreSQL Server' and 'Command Line Tools'
  * 'pgAdmin' is also good to have to make things easier.
  * Write down the postgres password you use.
  * If you change the port, write this down to. Default is `5432`.
* SQLite (minimum version 3.6.19) (https://sqlite.org/download.html)
  * TODO

You might need to set the path to the PostgreSQL dll files. See the "Compile from source" section below.

For Windows users we HIGHLY command using [Windows Terminal](https://aka.ms/terminal)!
Open a terminal/PowerShell/CMD/Command Prompt and use the `cd .\Documents\df_storyteller\` 
(change the folder) to go to the right directory and run it using `.\df_storyteller.exe --help`. 
Use the command in the next section for more info.

Problems?:

* If you get dll errors. See the section on PostgreSQL.
* Double-clicking the .exe does not currently work and will open a command prompt and close it immediately.
* If you get other error messages. The error should say what is wrong. 
If you don't know what the error means, look through the issues and report it if you can not find it.


## Linux

Note: Some things need to be worked out for these steps to work. There is some
addition work that has to be done. See:
[issue 17](https://gitlab.com/df_storyteller/df-storyteller/-/issues/17)

* DF storyteller currently depends on [PostgreSQL](https://www.postgresql.org/) or [SQLite](https://sqlite.org).
* PostgreSQL:
  * This might already be installed on your system. <!---TODO: How to check? -->
  * You can install this using the instructions [here](https://www.postgresql.org/download/).
* SQLite:
  * This might already be installed on your system. <!---TODO: How to check? -->
  * You can install this using the instructions [here](#sqlite).
* Download the executable [here](https://gitlab.com/df_storyteller/df-storyteller/-/releases).
* Move the file to a location you want.
* Open a terminal where the download is located.
* Make sure the file is set as executable.
  * If this is not the case you can use `$ sudo chmod +x df_storyteller`
* You can now use the application for example `$ ./df_storyteller --help`
For more example on how to use it [look here](commands.md).

## MacOS
TODO

## Compile from source
To compile from source make sure you have the following installed:
* **Rust** (rustup + cargo) ([Linux](https://www.rust-lang.org/tools/install), [Windows](https://www.rust-lang.org/tools/install?platform_override=win)) 
  * `rustup toolchain install nightly`
* **PostgreSQL** + development libraries. (minimum version unknown at the moment.) (https://www.postgresql.org/download/)
  * If you do not have PostgreSQL installed in the default directory you might
  have to edit the Cargo.toml file if you get a `libpq.lib` error.
* **SQLite** + development libraries. (minimum version 3.6.19) (https://sqlite.org/download.html)
* (Windows only) Visual C++ (or have Visual Studio installed with Visual C++ option enabled.) (https://visualstudio.microsoft.com/visual-cpp-build-tools/)
  * Check 'C++ build tools' and click 'Install'.
  * Note it might be possible to build with GNU instead. For this rustc target has to be changed (TODO).
--------
**Windows only section**
Because windows makes things difficult you have to take a few extra steps.
(Some steps might be different depending on the language and version of your system.)
Following steps are tested on Windows 10.

* Open File Explorer
* Right-click on "This PC" (in the left bar) and select "Properties"
* In the new window click on "Advanced system settings"
* In the new window click "Environment Variables..." (in the "Advanced" tab)
* In the new window again, in the top section "User variables for \<username\>"
Double-click on "Path"
* In that window click "New" and paste or type `C:\Program Files\PostgreSQL\12\bin`
and hit `Enter`.
* Click "New" again and paste or type `C:\Program Files\PostgreSQL\12\lib`
and hit `Enter`.
* Now click "OK" on the last 3 windows and you can close the others.

Now windows knows where to find all the `.dll` files it needs to communicate with
PostgreSQL.

-------

Download the code from the repo. Move into the folder.
Open a terminal (or PowerShell/CMD/Command Prompt on Windows). Use
`cd ./path/to/the/right/folder` to make sure you are in the folder
where the `README.md` file is located.

In this folder we are going to build our application. For this we use `cargo`.
**Note**: The following steps will use most of your CPU, so make sure to close
applications that you do not use at the moment.
Run the command below. It should not show any red.
```bash
rustup toolchain install nightly
rustup override set nightly
rustup update
cargo build --release
```
If you cancel the build process at any moment it should resume where it was canceled.
This might take a few minutes. It should end with (or something similar):

```
Finished release [optimized] target(s) in 9m 08s
```

Once this is compiled setup the `df_storyteller-config.json` file and database user (in postgres and add this to config).
The created binary can be found in `/target/release/df_storyteller`. This file can be move without any problems to a different directory.

### Help with errors
`LINK : fatal error LNK1181: cannot open input file 'libpq.lib'`
`libpq.lib` is part of PostgreSQL. So this error means that it can not find the
postgres files. On Linux install `apt-get install libpq-dev`.

## Install SQLite
<a name="sqlite"></a>

### Linux
The `libsqlite3-dev` and `sqlite3-devel` are only needed when you build from source:
```sh
# on Ubuntu/Debian
sudo apt-get install libsqlite3-dev sqlite3

# on OpenSUSE
sudo zypper install sqlite3-devel libsqlite3-0 sqlite3

# on Fedora
sudo dnf install libsqlite3x-devel sqlite3x

# on macOS (using homebrew)
brew install sqlite3
```

If you want to inspect your SQLite database you can install `sqlitebrowser`.
```
# Ubuntu/Debian
sudo apt-get install sqlitebrowser
```

When you are compiling from source and you get the error: 
`/usr/bin/ld: cannot find -lsqlite3`
You did not install the development libraries needed. Do `sudo apt-get install libsqlite3-dev` to fix this.
For other distorts look in the section above.

We require SQLite version 3.6.19 or later because of the introduction of foreign key constraints.

## Install PostgreSQL
<a name="postgres"></a>

It is best to follow the steps in the official guide [here](https://www.postgresql.org/download/linux/).
Here is just a brief version with a little bit of specific info about DF Storyteller.

### Linux

```sh
# Ubuntu variances (including Linux Mint)
sudo apt-get install postgresql libpq-dev
# (optional) For debugging in Postgres
sudo apt-get install pgadmin4
```