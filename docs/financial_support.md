# Financial Support

We would like to keep the DF Storyteller project alive so the whole Dwarf Fortress community can
benefit from this. Because of this we are starting to ask for financial support. This is so the
project is maintained is kept alive.

## Why ask for support?

There are a few reasons. Because this project, like many others, started as a hobby projects it 
can be created quickly but also fall by the wayside quickly. It would be sad so to see the projects
that was once full of love fall into neglect. We are thus hoping that even small contributions keep
the project alive and keep it growing.

## Why do you need money?

We all got to live our lives and if we can only spend our weekends and free time on the project it
might be hard to give it any priority. As the attention of the developers and maintainers switches
to something else it might not get maintained over the long run. We have seen this with many other
projects (in the Dwarf Fortress community).

The same is true for other related projects. Like the projects DF Storyteller was build on and the
projects build using DF Storyteller. This is why we are also trying to keep those alive, with
[monetary support and development time](#dependencies).

## How can I support you?

First off, we are just happy if you use our application. But because you asked, there are 2 ways:

- [![PayPal](https://img.shields.io/badge/Support-PayPal-00457C?logo=PayPal)(Donate or Subscribe)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NCHQJ6CXLH7UN&source=url)
- [![Patreon](https://img.shields.io/badge/Support-Patreon-F96854?logo=Patreon)](https://www.patreon.com/DFStoryteller)
(higher transaction costs then Paypal, but might be easier to use)

### What do I get in return?

You are added to the `supporters` Discord channel where you can vote on poles and help decide what
the future of DF Stoyteller will look like.

## Do I need to pay?

No! The project is free and will always be free to use. It is licensed under the
[AGPLv3 or later license](https://choosealicense.com/licenses/agpl-3.0/), which in short will ensure
it can always be used for free.

## How is the support used?

We want the money to be used to support development of DF Storyteller, but not be limited to this
project.
[Free and open-source software (FOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software)
has always had difficulty with finding financial support. We might not change this, but we can
always try to make a difference.

This is why we are dividing the money (after processing fees) into different pools. Each pool will
be used for its purpose.[^1]:

- **35%**[^3]: Paying for maintenance and development of DF Storyteller. [More info](#development_df_st)
- **15%**[^2]: Features/focus voted on by people that support the project. [More info](#voted_improvements)
- **15%**[^2]: Redistributed to fund for the development of FOSS projects DF Storyteller depends on. [More info](#dependencies)
- **15%**[^2]: Support for the development of FOSS projects created using DF Storyteller.
This include libraries and [paintings](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/paintings.md). [More info](#use_of_df_st)
- **10%**[^2]: Creation of bigger features. [More info](#bigger_features)
- **5%**[^2]: For main project maintainer(s)/developer(s).
This is for dealing with general upkeep and the creation of releases. [More info](#main_developer)
- **5%**[^2]: Service costs and server costs. [More info](#upkeep_costs)

Note: These pools might change if the needs change, you never know what the future brings.

[^1]: There might be some money being combined to accomplish particular goals or fund features.
[^2]: Rounded down to the nearest €1 to keep bookkeeping manageable.
[^3]: Including overflow from roundings.

### Development of DF Storyteller

<a name="development_df_st"></a>

The money in this pool will go to the people/developers/maintainers of DF Storyteller. This money
will be divided monthly and proportional to the contributions made by that person[^4]. We will not
take into account the first contribution (unless it is it is a big contribution)[^5]. The
contributor has to be at least semi-active inside the DF ST community (Discord, GitLab,...). And
has to contribute on an somewhat regular bases. The is both to improve general maintenance, but is
defined loosely. This means that even a person making a few changes in the code over a few months,
and non after or before, can still receive a cut of the pie.

[^4]: The money might not be transfered every month but instead transferred in bulk to keep transaction cost low.
[^5]: This is so we don't have to deal with minor (one time) contributions.

### Voted improvements by supporters

<a name="voted_improvements"></a>

Features/improvements/focus voted on by the supporters. Because we want the people that support us
some influence over the future of the projects we are offering them a vote. These votes are used to
fund the development of features or improvements requested by them. There will be poles going out
to these supporters in order to collect there opinion. The features/improvements will be depending
on the size of this pool. If the vote aligns with other pools the money can be combined to realize
bigger features or changes.

Not all features suggested will be available in poles as we have to keep the goal of the project
in mind and [prevent feature creep](https://en.wikipedia.org/wiki/Feature_creep). But development
of side-projects will not be excluded. The suggestion for these features are not limited by the
supporters.

### FOSS projects DF Storyteller depends on

<a name="dependencies"></a>

DF Storyteller could not exist without other developers that made libraries, tools, scripts,
applications, ... that we and other depend on. This does not only include our own dependencies but
also related decencies. Some of these projects are vital to keep everything working. Thus providing
some funds for these projects might maintain them and keep them going or fix problems. Some examples
for direct dependencies are [DF Hack](https://github.com/DFHack/dfhack) (exporting of legends),
[Rocker](https://rocket.rs/) (api service), [Diesel](https://diesel.rs/) (database interactions),
[RapiDoc](https://mrin9.github.io/RapiDoc/) (documentation),
[okapi](https://github.com/GREsau/okapi) (documentation generation),
[quick-xml](https://github.com/tafia/quick-xml) (xml parsing), and
[many many more](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/LICENSES_of_used_crates.md).
Each of these projects was in turn build using other dependencies.

Because there are turtles all the way down we can not fund everything or divide our funds over all
of them. So to keep the transactions meaningful we are going to work only in multiples of €1. The
DF ST project developers will pick one or more project(s) and divide the money according to some
guidelines. The projects also have to meet some other criteria. The money can be given conditionally
(taken other factors below into account), like the development of features, fixing of bugs,...

Criteria:

- The project must be open-source (under a permissive license
[approved by the FSF or OSI](https://spdx.org/licenses/)).
- The project is not created or directly supported/influenced by other big corporations.
- The project must be used directly (or indirectly, to a limited degree) by DF Storyteller or the
development/use of it.
- The accepted money must be used for the development of the project or used to pay for time spend
on developing.

Division guidelines:

- The money is proportional to the use in DF Storyteller.
- Previous transactions are taken into account.
- Taken into account the need of money for the project and the good it can provide.

Some of these guidelines can sound vague but it is difficult to write perfect guidelines for this.
The spirit of these criteria and guidelines are that the money will be spend on the right projects
and fall into the right hands.

If for some reason the project was abendoned or we could not transfer funds to the developers.
We instead have the option to get the project back of the ground with our time spend on fixing
issues or setting up support for it.

### FOSS projects created using DF Storyteller
<a name="use_of_df_st"></a>

We also want to promote projects in DF Storyteller and keep the projects maintained and updated.
This is where this pool comes in. This pool is simply to help the development and maintenance of
libraries, paintings and others that use DF Storyteller to build on. These projects must follow
[the requirements listed here](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/paintings.md#requirements-for-this-list).
With fundings divided proportional to there use, impact and novelty. Taken into account previous
transactions. The money does not have to be spend all on just a few projects just because there
are the only once in the list. In this case money will be saved for future projects or fund
features. The money can be given conditionally (taken other factors below into account), like the
development of features, fixing of bugs,...

### Bigger features

<a name="bigger_features"></a>

When bigger features are wanted to be added it might not be enough to ever create these in
combination with updating and maintaining the code. Because of this we created this pool. The money
is saved over time and can be used to fund the development of bigger projects later on. This can be
in combination of other pools if they align. The features the money is spend on will be picked on a
combination of: 

- Interest of the community.
- Interest of the developers.
- Needs/wants for paintings.
- The size of the pool.

### Main project maintainer(s)/developer(s) 

<a name="main_developer"></a>

This pool is for the lead/main maintainer(s)/developer(s) of the project. This person has to keep
track of the bookkeeping, creating of releases, available to help/manage other developers, manage
platforms (like Discord), update servers, look out for problems in the projects,...

So this person has to keep the ship afloat and makes sure he is there when things get
[fun](https://dwarffortresswiki.org/index.php?title=Fun). So for this person/these people is a
separate pool, but this comes with some additional responsibilities.

### Service cost and Server upkeep

<a name="upkeep_costs"></a>

Most of the project is build using free and open-source software. But some things do cost money,
like some services or the hosting costs for the server, buying of domain names,... To cover these
costs there is this pool. At the end of the year the money left over in this pool (after taking
into account future expenses) will go to the [Bigger features](#bigger_features) and
[Developement](#development_df_st) pool at a 50-50% ratio.

## General rules and guidelines

### Who manages the money?
The [project maintainers/developers](#main_developer) they take care of the bookkeeping and all
related things.

### Who can view the finances/bookkeeping?
All supporters can view the bookkeeping. All incomes are grouped together in 2 groups (paypal
and patreon) to anonymize the supporters.

### Have questions?
If you have questions feel free to ask them in Discord or via [email](df.storyteller@gmail.com). 