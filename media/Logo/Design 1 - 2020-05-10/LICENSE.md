# DF Storyteller Logo Version 1 - 2020/05/10

[DF Storyteller Logo](https://gitlab.com/df_storyteller/df-storyteller) by 
Ralph Bisschops is licensed under 
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=ccchooser).

