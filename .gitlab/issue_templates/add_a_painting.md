# Add a Painting
<!-- Fill in the blanks -->
I made this wonderful painting! And I want it to be added to [the list(s)](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/paintings.md)!

## First
Make sure you follow the following [requirements](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/paintings.md#requirements-for-this-list).

## My Painting
<!-- My painting has many features, please share it with us! -->


## Information
<!-- Now lets get down to business -->
<!-- If there is information that you don't have right now, you can always add it later -->
**Name**: (Name of the painting)
**Logo**: (Link to the logo, if you have any)*
<!-- *: Note you need to have created the image yourself or
provide proof that you have the right to use the image. -->
**Description**: (A short, one line, description of your application, make it distinct from other apps)
**Links**: (Add one or more links to where people can find more info, website, forums, git repo, ...)
**The app works on**: (Windows, Linux, OSX)
**DF Storyteller version**: (the version you know it works on or a minimum version.)


<!-- Leave the information below untouched! -->
/label ~"Add a Painting" ~"Waiting on Review"
