# Bug Report
<!-- Thanks for reporting a bug, you can change the format below,
but make sure we have all the information-->

## Summary
<!-- Write a summary of the bug, this can be used a a quick reminder.-->

## Steps to reproduce
<!-- What steps did you take before the bug happened
Or what steps we can take so we can recreate the bug.-->
1. ...
1. ...
1. ...

How often does this bug happen?:
<!-- (Always, sometimes, almost never,...) -->

## What is the current bug behavior?
<!-- The bug will result in: ... -->

## What is the expected behavior?
<!-- I wanted this to happen: ... -->

## Possible Fix
<!-- Do you know how we might fix this? -->

## Additional information
<!-- Include additional information we can use -->
<!-- If the bug happened while importing, upload the files somewhere like
Google Drive, Microsoft OneDrive, DropBox, pCloud, ...
(make sure the file does not disappear before the issue is solved)
Please upload the file(s) as a zip (or other archive) this will reduce the size A LOT.-->

<details><summary markdown="span">Terminal output</summary>

```
$ df_storyteller .......
```

</details>

### DF Storyteller version
- Run `df_storyteller --version`:
- Dwarf Fortress version:
- DFHack version:
- OS: (Windows, Linux, Mac)
- OS Version:


<!-- Leave the information below untouched! -->
/label ~Bug ~"Need Confirmation"
