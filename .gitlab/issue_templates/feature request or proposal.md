# Feature request of Proposal

## Summary
<!-- Describe shortly what you want -->

<!-- scrap what does not fit -->
This feature should be part of the sub command: (start, import, export, guide, docs, ...)

## Feature explanation
<!-- Go nuts! Please keep it structured. -->


## What would the impact of the be?
<!-- Do you think this changes anything already implemented? -->
<!-- What benefit would this have for the users? -->


<!-- Leave the information below untouched! -->
/label ~"Feature Request/Proposal" ~Discussion
