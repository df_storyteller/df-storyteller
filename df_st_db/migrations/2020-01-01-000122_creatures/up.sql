
CREATE TABLE creatures (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  creature_id VARCHAR,
  name_singular VARCHAR,
  name_plural VARCHAR,

  PRIMARY KEY (id, world_id)
);

CREATE TABLE creatures_a_g (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  all_castes_alive BOOLEAN,
  artificial_hiveable BOOLEAN,

  does_not_exist BOOLEAN,
  equipment BOOLEAN,
  equipment_wagon BOOLEAN,
  evil BOOLEAN,

  fanciful BOOLEAN,
  generated BOOLEAN,
  good BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);

CREATE TABLE creatures_h_h_1 (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  has_any_benign BOOLEAN,
  has_any_can_swim BOOLEAN,
  has_any_cannot_breathe_air BOOLEAN,
  has_any_cannot_breathe_water BOOLEAN,
  has_any_carnivore BOOLEAN,
  has_any_common_domestic BOOLEAN,
  has_any_curious_beast BOOLEAN,
  has_any_demon BOOLEAN,
  has_any_feature_beast BOOLEAN,
  has_any_flier BOOLEAN,
  has_any_fly_race_gait BOOLEAN,
  has_any_grasp BOOLEAN,
  has_any_grazer BOOLEAN,
  has_any_has_blood BOOLEAN,
  has_any_immobile BOOLEAN,
  has_any_intelligent_learns BOOLEAN,
  has_any_intelligent_speaks BOOLEAN,
  has_any_large_predator BOOLEAN,
  has_any_local_pops_controllable BOOLEAN,
  has_any_local_pops_produce_heroes BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);

CREATE TABLE creatures_h_h_2 (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  has_any_megabeast BOOLEAN,
  has_any_mischievous BOOLEAN,
  has_any_natural_animal BOOLEAN,
  has_any_night_creature BOOLEAN,
  has_any_night_creature_bogeyman BOOLEAN,
  has_any_night_creature_experimenter BOOLEAN,
  has_any_night_creature_hunter BOOLEAN,
  has_any_night_creature_nightmare BOOLEAN,
  has_any_not_fireimmune BOOLEAN,
  has_any_not_flier BOOLEAN,
  has_any_not_living BOOLEAN,
  has_any_outsider_controllable BOOLEAN,
  has_any_power BOOLEAN,
  has_any_race_gait BOOLEAN,
  has_any_semimegabeast BOOLEAN,
  has_any_slow_learner BOOLEAN,
  has_any_supernatural BOOLEAN,
  has_any_titan BOOLEAN,
  has_any_unique_demon BOOLEAN,
  has_any_utterances BOOLEAN,
  has_any_vermin_hateable BOOLEAN,
  has_any_vermin_micro BOOLEAN,
  has_female BOOLEAN,
  has_male BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);

CREATE TABLE creatures_l_z (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  large_roaming BOOLEAN,
  loose_clusters BOOLEAN,
  mates_to_breed BOOLEAN,
  mundane BOOLEAN,

  occurs_as_entity_race BOOLEAN,
  savage BOOLEAN,
  small_race BOOLEAN,

  two_genders BOOLEAN,
  ubiquitous BOOLEAN,

  vermin_eater BOOLEAN,
  vermin_fish BOOLEAN,
  vermin_grounder BOOLEAN,
  vermin_rotter BOOLEAN,
  vermin_soil BOOLEAN,
  vermin_soil_colony BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);

CREATE TABLE creature_biomes_1 (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  desert_badland BOOLEAN,
  desert_rock BOOLEAN,
  desert_sand BOOLEAN,
  forest_taiga BOOLEAN,
  forest_temperate_broadleaf BOOLEAN,
  forest_temperate_conifer BOOLEAN,
  forest_tropical_conifer BOOLEAN,
  forest_tropical_dry_broadleaf BOOLEAN,
  forest_tropical_moist_broadleaf BOOLEAN,
  glacier BOOLEAN,
  grassland_temperate BOOLEAN,
  grassland_tropical BOOLEAN,
  lake_temperate_brackishwater BOOLEAN,
  lake_temperate_freshwater BOOLEAN,
  lake_temperate_saltwater BOOLEAN,
  lake_tropical_brackishwater BOOLEAN,
  lake_tropical_freshwater BOOLEAN,
  lake_tropical_saltwater BOOLEAN,
  marsh_temperate_freshwater BOOLEAN,
  marsh_temperate_saltwater BOOLEAN,
  marsh_tropical_freshwater BOOLEAN,
  marsh_tropical_saltwater BOOLEAN,
  mountain BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);

CREATE TABLE creature_biomes_2 (
  cr_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  ocean_arctic BOOLEAN,
  ocean_temperate BOOLEAN,
  ocean_tropical BOOLEAN,
  pool_temperate_brackishwater BOOLEAN,
  pool_temperate_freshwater BOOLEAN,
  pool_temperate_saltwater BOOLEAN,
  pool_tropical_brackishwater BOOLEAN,
  pool_tropical_freshwater BOOLEAN,
  pool_tropical_saltwater BOOLEAN,
  river_temperate_brackishwater BOOLEAN,
  river_temperate_freshwater BOOLEAN,
  river_temperate_saltwater BOOLEAN,
  river_tropical_brackishwater BOOLEAN,
  river_tropical_freshwater BOOLEAN,
  river_tropical_saltwater BOOLEAN,
  savanna_temperate BOOLEAN,
  savanna_tropical BOOLEAN,
  shrubland_temperate BOOLEAN,
  shrubland_tropical BOOLEAN,
  subterranean_chasm BOOLEAN,
  subterranean_lava BOOLEAN,
  subterranean_water BOOLEAN,
  swamp_mangrove BOOLEAN,
  swamp_temperate_freshwater BOOLEAN,
  swamp_temperate_saltwater BOOLEAN,
  swamp_tropical_freshwater BOOLEAN,
  swamp_tropical_saltwater BOOLEAN,
  tundra BOOLEAN,

  PRIMARY KEY (cr_id, world_id),
  FOREIGN KEY (cr_id, world_id) REFERENCES creatures (id, world_id)
);