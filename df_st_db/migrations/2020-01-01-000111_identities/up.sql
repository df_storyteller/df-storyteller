
CREATE TABLE identities (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  hf_id INTEGER,
  race VARCHAR,
  caste VARCHAR,
  birth_year INTEGER,
  birth_second INTEGER,
  profession VARCHAR,
  entity_id INTEGER,
  nemesis_id INTEGER,
  
  PRIMARY KEY (id, world_id)
);
