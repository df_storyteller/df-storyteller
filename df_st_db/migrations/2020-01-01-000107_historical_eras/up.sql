
CREATE TABLE historical_eras (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  start_year INTEGER,
  
  PRIMARY KEY (id, world_id)
);