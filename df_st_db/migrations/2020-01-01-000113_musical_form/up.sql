
CREATE TABLE musical_forms (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  name VARCHAR,
  description VARCHAR,

  PRIMARY KEY (id, world_id)
);
