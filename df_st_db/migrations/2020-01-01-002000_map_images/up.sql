
CREATE TABLE map_images (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  name VARCHAR NOT NULL,
  data BYTEA NOT NULL,
  format VARCHAR NOT NULL,

  PRIMARY KEY (id, world_id)
);
