
CREATE TABLE landmasses (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  coord_1_id INTEGER,
  coord_2_id INTEGER,

  PRIMARY KEY (id, world_id),
  FOREIGN KEY (coord_1_id, world_id) REFERENCES coordinates (id, world_id),
  FOREIGN KEY (coord_2_id, world_id) REFERENCES coordinates (id, world_id)
);
