
DROP TABLE historical_figures;
DROP TABLE hf_entity_links;
DROP TABLE hf_entity_position_links;
DROP TABLE hf_site_links;
DROP TABLE hf_skills;
DROP TABLE hf_relationship_profile_hf;
DROP TABLE hf_intrigue_actors;
DROP TABLE hf_intrigue_plots;
DROP TABLE hf_plot_actors;
DROP TABLE hf_entity_reputations;
DROP TABLE hf_vague_relationships;
DROP TABLE hf_entity_squad_links;
DROP TABLE hf_links;
DROP TABLE hf_honor_entities;
DROP TABLE hf_site_properties;
DROP TABLE hf_spheres;
DROP TABLE hf_interaction_knowledges;
DROP TABLE hf_journey_pets;
DROP TABLE hf_honor_ids;
