
CREATE TABLE paths (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  flow INTEGER NOT NULL,
  exit_tile INTEGER NOT NULL,
  elevation INTEGER NOT NULL,
  river_id INTEGER,
  
  PRIMARY KEY (id, world_id)
)
