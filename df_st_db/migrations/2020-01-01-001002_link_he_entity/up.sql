
CREATE TABLE link_he_entity (
  he_id INTEGER NOT NULL,
  entity_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, entity_id, world_id),
  -- FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id),
  FOREIGN KEY (entity_id, world_id) REFERENCES historical_figures (id, world_id)
);
