
CREATE TABLE entity_populations (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  civ_id INTEGER,
  PRIMARY KEY (id, world_id)
  -- FOREIGN KEY (civ_id) REFERENCES civilization/entity (id)
);

CREATE TABLE entity_population_races (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  entity_population_id INTEGER NOT NULL,
  race VARCHAR,
  amount INTEGER,

  PRIMARY KEY (id, world_id),
  FOREIGN KEY (entity_population_id, world_id) 
    REFERENCES entity_populations (id, world_id)
);