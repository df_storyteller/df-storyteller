
CREATE TABLE rivers (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  end_pos_id INTEGER,

  PRIMARY KEY (id, world_id),
  FOREIGN KEY (end_pos_id, world_id) REFERENCES coordinates (id, world_id)
);

-- ALTER TABLE paths
-- ADD CONSTRAINT rivers_paths
-- FOREIGN KEY (river_id, world_id) REFERENCES rivers (id, world_id);
