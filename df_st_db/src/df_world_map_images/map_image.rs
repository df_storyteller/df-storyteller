use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::map_images;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "map_images"]
pub struct MapImage {
    pub id: i32,
    pub world_id: i32,
    pub name: String,
    pub data: Vec<u8>,
    pub format: String,
}

impl MapImage {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::MapImage, MapImage> for MapImage {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, map_images: &[MapImage]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(map_images::table)
            .values(map_images)
            .on_conflict((map_images::id, map_images::world_id))
            .do_update()
            .set((
                map_images::name.eq(excluded(map_images::name)),
                map_images::data.eq(excluded(map_images::data)),
                map_images::format.eq(excluded(map_images::format)),
            ))
            .execute(conn)
            .expect("Error saving map_images");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, map_images: &[MapImage]) {
        diesel::insert_into(map_images::table)
            .values(map_images)
            .execute(conn)
            .expect("Error saving map_images");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<MapImage>, Error> {
        use crate::schema::map_images::dsl::*;
        let query = map_images;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<MapImage>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<MapImage>, Error> {
        use crate::schema::map_images::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = map_images.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "format" => format,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "data" => data,
                "format" => format,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id"],
            _ => vec!["id", "name", "data", "format"],
        }
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[MapImage],
        core_list: Vec<df_st_core::MapImage>,
    ) -> Result<Vec<df_st_core::MapImage>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::map_images::dsl::*;
        let query = map_images.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "format" => format,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: String},
                "data" => {data: Vec<u8>},
                "format" => {format: String},
            };},
        };
    }
}

/// From Core to DB
impl Filler<MapImage, df_st_core::MapImage> for MapImage {
    fn add_missing_data(&mut self, source: &df_st_core::MapImage) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.data = source.data.clone();
        self.format.add_missing_data(&source.format);
    }
}

/// From DB to Core
impl Filler<df_st_core::MapImage, MapImage> for df_st_core::MapImage {
    fn add_missing_data(&mut self, source: &MapImage) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.data = source.data.clone();
        self.format.add_missing_data(&source.format);
    }
}

impl PartialEq<MapImage> for df_st_core::MapImage {
    fn eq(&self, other: &MapImage) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::MapImage> for MapImage {
    fn eq(&self, other: &df_st_core::MapImage) -> bool {
        self.id == other.id
    }
}
