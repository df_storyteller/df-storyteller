#![forbid(unsafe_code)]
#![deny(clippy::all)]
// Temporary, until https://github.com/diesel-rs/diesel/issues/3182 is in release.
#![allow(clippy::extra_unused_lifetimes)]

#[macro_use]
extern crate diesel;

#[macro_use]
mod db_object;

#[macro_use]
extern crate diesel_migrations;

mod df_site_map_images;
mod df_world;
mod df_world_map_images;
pub mod schema;

pub use db_object::{DBObject, MatchBy, OrderTypes};
pub use df_site_map_images::*;
pub use df_world::*;
pub use df_world_map_images::*;
pub use schema::*;
use std::convert::TryFrom;

use ::r2d2::Error;
use df_st_core::config::{get_database_url, get_db_system_url, RootConfig};
use diesel::r2d2::{self, ConnectionManager};
use diesel::{Connection, RunQueryDsl};
use diesel_migrations::embed_migrations;

// PostgreSQL
#[cfg(feature = "postgres")]
pub type DbConnection = diesel::pg::PgConnection;
// SQLite
#[cfg(feature = "sqlite")]
pub type DbConnection = diesel::sqlite::SqliteConnection;

// Common
pub type DbManager = ConnectionManager<DbConnection>;
pub type DbPool = r2d2::Pool<DbManager>;
pub type DbPooledConnection = r2d2::PooledConnection<DbManager>;
pub type DbError = Error;

#[cfg(feature = "postgres")]
pub static DB_SERVICE: df_st_core::config::DBService = df_st_core::config::DBService::Postgres;
#[cfg(feature = "sqlite")]
pub static DB_SERVICE: df_st_core::config::DBService = df_st_core::config::DBService::SQLite;

// Embed all migrations in binary
embed_migrations!("./migrations");

fn generate_random_password() -> String {
    use rand::{Rng, SeedableRng};
    // Password settings
    let password_length = 30;
    let characters: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                            abcdefghijklmnopqrstuvwxyz\
                            0123456789";
    // Set random generator.
    // ChaCha is a cryptographically secure random number generator.
    // We are using 20 rounds, from_entropy will provide secure entropy.
    // This will panic if this is unable to provide secure entropy.
    let mut rng = rand_chacha::ChaCha20Rng::from_entropy();

    let password: String = (0..password_length)
        .map(|_| {
            let idx = rng.gen_range(0..characters.len());
            characters[idx] as char
        })
        .collect();
    password
}

/// Only used for Postgres database
pub fn create_user(privileged_config: &RootConfig) {
    if !cfg!(feature = "postgres") {
        unreachable!("Using Postgres function without Postgres feature enabled.");
    }

    let database_url = get_db_system_url(privileged_config, &DB_SERVICE);
    if let Some(database_url) = database_url {
        let user = "df_storyteller".to_string();
        // Create a random password
        let password = generate_random_password();
        println!("generated Password for {}: {}", user, password);

        // Do not allow password shorter then 6 characters.
        if password.len() < 6 {
            panic!(
                "The password set for the database needs \
                to be at least 6 characters long."
            );
        }

        let conn = match DbConnection::establish(&database_url) {
            Ok(value) => value,
            Err(err) => {
                log::error!("Could not connect to database: {}", database_url);
                log::error!("{}", err.to_string());
                panic!("Error connecting to database.");
            }
        };

        // Use `$1` for postgres and `?` in other cases.
        // TODO bind did not seem to work.
        match diesel::sql_query(format!(
            "CREATE ROLE {} WITH \
            LOGIN \
            NOSUPERUSER \
            CREATEDB \
            NOCREATEROLE \
            INHERIT \
            NOREPLICATION \
            CONNECTION LIMIT -1 \
            PASSWORD '{}';",
            user, password
        ))
        // TODO prepare statement
        // .bind::<Text, _>(password.clone())
        .execute(&conn)
        {
            Ok(_) => {}
            Err(err) => match err {
                diesel::result::Error::DatabaseError(_, info) => {
                    if info.message().ends_with("already exists") {
                        log::warn!("User already exists, changing password.");
                        change_user_password(privileged_config, user.clone(), password.clone());
                    } else {
                        println!("Error: {}", info.message());
                    }
                }
                _ => panic!("Database error: {:?}", err),
            },
        }
        // Save password in config
        log::info!("Saving new config to file.");

        let port = privileged_config.database.config.port.unwrap_or(5432);
        let mut new_config = privileged_config.clone();
        new_config.database.config = df_st_core::config::DBURLConfig {
            db_path: Some(df_st_core::config::get_default_store_sqlite_db_path()),
            user: Some(user),
            password,
            host: Some("localhost".to_owned()),
            port: Some(port),
            database: Some("df_storyteller".to_owned()),
            ..Default::default()
        };
        df_st_core::config::store_config_to_file(new_config, None);
    }
}

/// Only used for Postgres database
fn change_user_password(privileged_config: &RootConfig, user: String, password: String) {
    if !cfg!(feature = "postgres") {
        unreachable!("Using Postgres function without Postgres feature enabled.");
    }

    let database_url = get_db_system_url(privileged_config, &DB_SERVICE);
    if let Some(database_url) = database_url {
        // Do not allow password shorter then 6 characters.
        if password.len() < 6 {
            panic!(
                "The password set for the database needs \
                to be at least 6 characters long."
            );
        }

        let conn = match DbConnection::establish(&database_url) {
            Ok(value) => value,
            Err(err) => {
                log::error!("Could not connect to database: {}", database_url);
                log::error!("{}", err.to_string());
                panic!("Error connecting to database.");
            }
        };

        // Use `$1` for postgres and `?` in other cases.
        diesel::sql_query(format!(
            "ALTER ROLE {} WITH \
            LOGIN \
            NOSUPERUSER \
            CREATEDB \
            NOCREATEROLE \
            INHERIT \
            NOREPLICATION \
            CONNECTION LIMIT -1 \
            PASSWORD '{}';",
            user, password
        ))
        // TODO prepare statement
        // .bind::<Text, _>(password)
        .execute(&conn)
        .unwrap();
        log::info!("Password for {} updated.", user);
    }
}

/// Only used for Postgres database
pub fn recreate_database(config: &RootConfig, drop_db: bool) {
    if !cfg!(feature = "postgres") {
        unreachable!("Using Postgres function without Postgres feature enabled.");
    }

    let database_url = get_db_system_url(config, &DB_SERVICE);
    let database_name = match &config.database.config.database {
        Some(db_name) => db_name.clone(),
        None => "df_storyteller".to_owned(),
    };

    if let Some(database_url) = database_url {
        let conn = match DbConnection::establish(&database_url) {
            Ok(value) => value,
            Err(err) => {
                log::error!("Could not connect to database: {}", database_url);
                log::error!("{}", err.to_string());
                panic!("Error connecting to database.");
            }
        };
        if drop_db {
            // Drop database if already exists
            // TODO backup old db to file for if someone did not read the warnings.
            match diesel::sql_query(format!("DROP DATABASE IF EXISTS {};", database_name))
                .execute(&conn)
            {
                Ok(_) => {}
                Err(err) => match err {
                    diesel::result::Error::DatabaseError(_, info) => {
                        if info.message().starts_with("must be owner of database") {
                            panic!("Must be owner of database to drop database.");
                        } else {
                            panic!("Database error: {}", info.message());
                        }
                    }
                    _ => panic!("Database error: {:?}", err),
                },
            };
            log::info!("Database `{}` deleted.", database_name);
        }
        // Create a new database. This will fail is already exists.
        match diesel::sql_query(format!("CREATE DATABASE {};", database_name)).execute(&conn) {
            Ok(_) => {}
            Err(err) => match err {
                diesel::result::Error::DatabaseError(_, info) => {
                    log::info!(
                        "Database not deleted. If the database already exists \
                        add the flag `--drop-db` to drop the existing database first. \
                        WARNING this will delete all stored in the `{}` database!",
                        database_name
                    );
                    if info.message().ends_with("already exists") {
                        panic!("Database already exists.");
                    } else {
                        panic!("Database error: {}", info.message());
                    }
                }
                _ => panic!("Database error: {:?}", err),
            },
        };
        log::info!("Database `{}` created.", database_name);
    }
}

/// Delete a SQLite database file
pub fn delete_sqlite_db(config: &RootConfig) {
    if !cfg!(feature = "sqlite") {
        unreachable!("Using SQLite function without SQLite feature enabled.");
    }
    let db_config = config.database.config.clone();
    let db_path = match db_config.db_path {
        Some(db_path) => db_path,
        None => df_st_core::config::get_default_store_sqlite_db_path(),
    };
    let db_path_string = db_path.to_str().unwrap_or("<invalid filename>");
    log::info!("Removing SQLite DB: `{}`", db_path_string);
    std::fs::remove_file(&db_path)
        .unwrap_or_else(|_| panic!("Could not remove SQLite DB file: `{}`", db_path_string));
}

pub fn run_migrations(config: &RootConfig) {
    log::info!("Updating database...");
    let pool = establish_connection(config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");
    embedded_migrations::run_with_output(&conn, &mut std::io::stdout()).unwrap();
    // embedded_migrations::run(&conn).unwrap();
    log::info!("Done updating database.");
}

pub fn establish_connection(config: &RootConfig) -> Result<DbPool, Error> {
    let database_url = get_database_url(config, &DB_SERVICE);
    if database_url.is_none() {
        log::error!(
            "No database url found, update in config file required.\n\
            Set database.config or database.url"
        );
        panic!("No database url found in config.");
    }
    let manager = DbManager::new(database_url.unwrap());
    let mut builder = r2d2::Pool::builder();
    if let Some(pool_size) = &config.database.pool_size {
        builder = builder.max_size(u32::try_from(*pool_size).unwrap());
    }
    builder.build(manager)
}

pub fn check_db_has_tables(config: &RootConfig) -> bool {
    let pool = match establish_connection(config) {
        Ok(pool) => pool,
        Err(err) => {
            log::error!("Could not connect to database.");
            log::error!("{}", err.to_string());
            panic!("Error connecting to database.");
        }
    };
    let conn = pool.get().expect("Couldn't get db connection from pool.");
    DFWorldInfo::get_list_from_db(
        &*conn,
        std::collections::HashMap::new(),
        std::collections::HashMap::new(),
        0,
        1,
        None,
        None,
        None,
        true,
    )
    .is_ok()
}

/// Returns the name of the database service (`"postgres"` or `"sqlite"`)
pub fn get_db_service_name() -> String {
    if cfg!(feature = "postgres") {
        "postgres".to_owned()
    } else if cfg!(feature = "sqlite") {
        "sqlite".to_owned()
    } else {
        unreachable!("No DB feature selected");
    }
}
