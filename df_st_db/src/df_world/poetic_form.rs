use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::poetic_forms;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "poetic_forms"]
pub struct PoeticForm {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub description: Option<String>,
}

impl PoeticForm {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::PoeticForm, PoeticForm> for PoeticForm {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, poetic_forms: &[PoeticForm]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(poetic_forms::table)
            .values(poetic_forms)
            .on_conflict((poetic_forms::id, poetic_forms::world_id))
            .do_update()
            .set((
                poetic_forms::name.eq(excluded(poetic_forms::name)),
                poetic_forms::description.eq(excluded(poetic_forms::description)),
            ))
            .execute(conn)
            .expect("Error saving poetic_forms");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, poetic_forms: &[PoeticForm]) {
        diesel::insert_into(poetic_forms::table)
            .values(poetic_forms)
            .execute(conn)
            .expect("Error saving poetic_forms");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<PoeticForm>, Error> {
        use crate::schema::poetic_forms::dsl::*;
        let query = poetic_forms;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<PoeticForm>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<PoeticForm>, Error> {
        use crate::schema::poetic_forms::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = poetic_forms.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "description" => description,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "description" => description,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id"],
            _ => vec!["id", "name", "description"],
        }
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[PoeticForm],
        core_list: Vec<df_st_core::PoeticForm>,
    ) -> Result<Vec<df_st_core::PoeticForm>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::poetic_forms::dsl::*;
        let query = poetic_forms.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "description" => description,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "description" => {description: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<PoeticForm, df_st_core::PoeticForm> for PoeticForm {
    fn add_missing_data(&mut self, source: &df_st_core::PoeticForm) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.description.add_missing_data(&source.description);
    }
}

/// From DB to Core
impl Filler<df_st_core::PoeticForm, PoeticForm> for df_st_core::PoeticForm {
    fn add_missing_data(&mut self, source: &PoeticForm) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.description.add_missing_data(&source.description);
    }
}

impl PartialEq<PoeticForm> for df_st_core::PoeticForm {
    fn eq(&self, other: &PoeticForm) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::PoeticForm> for PoeticForm {
    fn eq(&self, other: &df_st_core::PoeticForm) -> bool {
        self.id == other.id
    }
}
