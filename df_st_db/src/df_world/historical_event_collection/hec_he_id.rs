use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_he_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

/// HEC_HE_ID: HistoricalEventCollection HistoricalEvent ID
#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_he_ids"]
#[primary_key(hec_id, he_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
#[allow(clippy::upper_case_acronyms)]
pub struct HECHEID {
    pub hec_id: i32,
    pub he_id: i32,
    pub world_id: i32,
}

impl HECHEID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECHEID, HECHEID> for HECHEID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_he_ids: &[HECHEID]) {
        diesel::insert_into(hec_he_ids::table)
            .values(hec_he_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_he_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_he_ids: &[HECHEID]) {
        diesel::insert_into(hec_he_ids::table)
            .values(hec_he_ids)
            .execute(conn)
            .expect("Error saving hec_he_ids");
    }

    /// Get a list of HECHEID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HECHEID>, Error> {
        use crate::schema::hec_he_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_he_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "he_id" => he_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECHEID>, Error> {
        use crate::schema::hec_he_ids::dsl::*;
        let query = hec_he_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        Ok(query.first::<HECHEID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hec_id", "he_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECHEID],
        core_list: Vec<HECHEID>,
    ) -> Result<Vec<HECHEID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_he_ids::dsl::*;
        let query = hec_he_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "he_id" => {he_id: i32},
            };},
        };
    }
}

impl PartialEq for HECHEID {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.he_id == other.he_id
    }
}

impl Hash for HECHEID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.he_id.hash(state);
    }
}
