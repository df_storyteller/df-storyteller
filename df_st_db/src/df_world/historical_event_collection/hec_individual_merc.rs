use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_individual_mercs;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_individual_mercs"]
#[primary_key(hec_id, individual_merc)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HECIndividualMerc {
    pub hec_id: i32,
    pub individual_merc: bool,
    pub world_id: i32,
}

impl HECIndividualMerc {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECIndividualMerc, HECIndividualMerc> for HECIndividualMerc {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_individual_mercs: &[HECIndividualMerc]) {
        diesel::insert_into(hec_individual_mercs::table)
            .values(hec_individual_mercs)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_individual_mercs");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_individual_mercs: &[HECIndividualMerc]) {
        diesel::insert_into(hec_individual_mercs::table)
            .values(hec_individual_mercs)
            .execute(conn)
            .expect("Error saving hec_individual_mercs");
    }

    /// Get a list of HECIndividualMerc from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HECIndividualMerc>, Error> {
        use crate::schema::hec_individual_mercs::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_individual_mercs.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "individual_merc" => individual_merc,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECIndividualMerc>, Error> {
        use crate::schema::hec_individual_mercs::dsl::*;
        let query = hec_individual_mercs;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        Ok(query.first::<HECIndividualMerc>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hec_id", "individual_merc"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECIndividualMerc],
        core_list: Vec<HECIndividualMerc>,
    ) -> Result<Vec<HECIndividualMerc>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_individual_mercs::dsl::*;
        let query = hec_individual_mercs
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "individual_merc" => {individual_merc: bool},
            };},
        };
    }
}

impl PartialEq for HECIndividualMerc {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.individual_merc == other.individual_merc
    }
}

impl Hash for HECIndividualMerc {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.individual_merc.hash(state);
    }
}
