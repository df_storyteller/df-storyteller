use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_d_support_merc_hf_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_d_support_merc_hf_ids"]
#[primary_key(hec_id, d_support_merc_hf_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HECDSupportMercHFID {
    pub hec_id: i32,
    pub d_support_merc_hf_id: i32,
    pub world_id: i32,
}

impl HECDSupportMercHFID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECDSupportMercHFID, HECDSupportMercHFID> for HECDSupportMercHFID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_d_support_merc_hf_ids: &[HECDSupportMercHFID]) {
        diesel::insert_into(hec_d_support_merc_hf_ids::table)
            .values(hec_d_support_merc_hf_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_d_support_merc_hf_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_d_support_merc_hf_ids: &[HECDSupportMercHFID]) {
        diesel::insert_into(hec_d_support_merc_hf_ids::table)
            .values(hec_d_support_merc_hf_ids)
            .execute(conn)
            .expect("Error saving hec_d_support_merc_hf_ids");
    }

    /// Get a list of HECDSupportMercHFID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HECDSupportMercHFID>, Error> {
        use crate::schema::hec_d_support_merc_hf_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_d_support_merc_hf_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "d_support_merc_hf_id" => d_support_merc_hf_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECDSupportMercHFID>, Error> {
        use crate::schema::hec_d_support_merc_hf_ids::dsl::*;
        let query = hec_d_support_merc_hf_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        Ok(query.first::<HECDSupportMercHFID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hec_id", "d_support_merc_hf_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECDSupportMercHFID],
        core_list: Vec<HECDSupportMercHFID>,
    ) -> Result<Vec<HECDSupportMercHFID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_d_support_merc_hf_ids::dsl::*;
        let query = hec_d_support_merc_hf_ids
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "d_support_merc_hf_id" => {d_support_merc_hf_id: i32},
            };},
        };
    }
}

impl PartialEq for HECDSupportMercHFID {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.d_support_merc_hf_id == other.d_support_merc_hf_id
    }
}

impl Hash for HECDSupportMercHFID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.d_support_merc_hf_id.hash(state);
    }
}
