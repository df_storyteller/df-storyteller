use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_honor_entities;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_honor_entities"]
#[primary_key(hf_id, entity_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFHonorEntity {
    pub hf_id: i32,
    pub entity_id: i32,
    pub world_id: i32,
    pub battles: Option<i32>,
    pub kills: Option<i32>,
}

impl HFHonorEntity {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFHonorEntity, HFHonorEntity> for HFHonorEntity {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_honor_entities: &[HFHonorEntity]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_honor_entities::table)
            .values(hf_honor_entities)
            .on_conflict((
                hf_honor_entities::hf_id,
                hf_honor_entities::entity_id,
                hf_honor_entities::world_id,
            ))
            .do_update()
            .set((
                hf_honor_entities::battles.eq(excluded(hf_honor_entities::battles)),
                hf_honor_entities::kills.eq(excluded(hf_honor_entities::kills)),
            ))
            .execute(conn)
            .expect("Error saving hf_honor_entities");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_honor_entities: &[HFHonorEntity]) {
        diesel::insert_into(hf_honor_entities::table)
            .values(hf_honor_entities)
            .execute(conn)
            .expect("Error saving hf_honor_entities");
    }

    /// Get a list of HFHonorEntity from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFHonorEntity>, Error> {
        use crate::schema::hf_honor_entities::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_honor_entities.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "battles" => battles,
                "kills" => kills,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFHonorEntity>, Error> {
        use crate::schema::hf_honor_entities::dsl::*;
        let query = hf_honor_entities;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(entity_id.eq(id_filter.get("entity_id").unwrap_or(&0)));
        Ok(query.first::<HFHonorEntity>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hf_id", "entity_id", "battles", "kills"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFHonorEntity],
        core_list: Vec<df_st_core::HFHonorEntity>,
    ) -> Result<Vec<df_st_core::HFHonorEntity>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_honor_entities::dsl::*;
        let query = hf_honor_entities.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "entity_id" => {entity_id: i32},
                "battles" => {battles: Option<i32>},
                "kills" => {kills: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFHonorEntity, df_st_core::HFHonorEntity> for HFHonorEntity {
    fn add_missing_data(&mut self, source: &df_st_core::HFHonorEntity) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.battles.add_missing_data(&source.battles);
        self.kills.add_missing_data(&source.kills);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFHonorEntity, HFHonorEntity> for df_st_core::HFHonorEntity {
    fn add_missing_data(&mut self, source: &HFHonorEntity) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.battles.add_missing_data(&source.battles);
        self.kills.add_missing_data(&source.kills);
    }
}

impl PartialEq<df_st_core::HFHonorEntity> for HFHonorEntity {
    fn eq(&self, other: &df_st_core::HFHonorEntity) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl PartialEq<HFHonorEntity> for df_st_core::HFHonorEntity {
    fn eq(&self, other: &HFHonorEntity) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl PartialEq for HFHonorEntity {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl Hash for HFHonorEntity {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}
