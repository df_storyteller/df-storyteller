use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_entity_reputations;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_entity_reputations"]
#[primary_key(hf_id, entity_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFEntityReputation {
    pub hf_id: i32,
    pub entity_id: i32,
    pub world_id: i32,
    pub first_ageless_year: Option<i32>,
    pub first_ageless_season_count: Option<i32>,
    pub unsolved_murders: Option<i32>,
    pub rep_hunter: Option<i32>,
    pub rep_killer: Option<i32>,
    pub rep_knowledge_preserver: Option<i32>,
    pub rep_treasure_hunter: Option<i32>,
}

impl HFEntityReputation {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFEntityReputation, HFEntityReputation> for HFEntityReputation {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    #[rustfmt::skip]
    fn insert_into_db(conn: &DbConnection, hf_entity_reputations: &[HFEntityReputation]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_entity_reputations::table)
            .values(hf_entity_reputations)
            .on_conflict((
                hf_entity_reputations::hf_id,
                hf_entity_reputations::entity_id,
                hf_entity_reputations::world_id,
            ))
            .do_update()
            .set((
                hf_entity_reputations::first_ageless_year.eq(excluded(hf_entity_reputations::first_ageless_year)),
                hf_entity_reputations::first_ageless_season_count.eq(excluded(hf_entity_reputations::first_ageless_season_count)),
                hf_entity_reputations::unsolved_murders.eq(excluded(hf_entity_reputations::unsolved_murders)),
                hf_entity_reputations::rep_hunter.eq(excluded(hf_entity_reputations::rep_hunter)),
                hf_entity_reputations::rep_killer.eq(excluded(hf_entity_reputations::rep_killer)),
                hf_entity_reputations::rep_knowledge_preserver.eq(excluded(hf_entity_reputations::rep_knowledge_preserver)),
                hf_entity_reputations::rep_treasure_hunter.eq(excluded(hf_entity_reputations::rep_treasure_hunter)),
            ))
            .execute(conn)
            .expect("Error saving hf_entity_reputations");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_entity_reputations: &[HFEntityReputation]) {
        diesel::insert_into(hf_entity_reputations::table)
            .values(hf_entity_reputations)
            .execute(conn)
            .expect("Error saving hf_entity_reputations");
    }

    /// Get a list of HFEntityReputation from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFEntityReputation>, Error> {
        use crate::schema::hf_entity_reputations::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_entity_reputations.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "first_ageless_year" => first_ageless_year,
                "first_ageless_season_count" => first_ageless_season_count,
                "unsolved_murders" => unsolved_murders,
                "rep_hunter" => rep_hunter,
                "rep_killer" => rep_killer,
                "rep_knowledge_preserver" => rep_knowledge_preserver,
                "rep_treasure_hunter" => rep_treasure_hunter,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFEntityReputation>, Error> {
        use crate::schema::hf_entity_reputations::dsl::*;
        let query = hf_entity_reputations;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(entity_id.eq(id_filter.get("entity_id").unwrap_or(&0)));
        Ok(query.first::<HFEntityReputation>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "hf_id",
            "entity_id",
            "first_ageless_year",
            "first_ageless_season_count",
            "unsolved_murders",
            "rep_hunter",
            "rep_killer",
            "rep_knowledge_preserver",
            "rep_treasure_hunter",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFEntityReputation],
        core_list: Vec<df_st_core::HFEntityReputation>,
    ) -> Result<Vec<df_st_core::HFEntityReputation>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_entity_reputations::dsl::*;
        let query = hf_entity_reputations
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "entity_id" => {entity_id: i32},
                "first_ageless_year" => {first_ageless_year: Option<i32>},
                "first_ageless_season_count" => {first_ageless_season_count: Option<i32>},
                "unsolved_murders" => {unsolved_murders: Option<i32>},
                "rep_hunter" => {rep_hunter: Option<i32>},
                "rep_killer" => {rep_killer: Option<i32>},
                "rep_knowledge_preserver" => {rep_knowledge_preserver: Option<i32>},
                "rep_treasure_hunter" => {rep_treasure_hunter: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFEntityReputation, df_st_core::HFEntityReputation> for HFEntityReputation {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HFEntityReputation) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.first_ageless_year.add_missing_data(&source.first_ageless_year);
        self.first_ageless_season_count.add_missing_data(&source.first_ageless_season_count);
        self.unsolved_murders.add_missing_data(&source.unsolved_murders);
        self.rep_hunter.add_missing_data(&source.rep_hunter);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_knowledge_preserver.add_missing_data(&source.rep_knowledge_preserver);
        self.rep_treasure_hunter.add_missing_data(&source.rep_treasure_hunter);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFEntityReputation, HFEntityReputation> for df_st_core::HFEntityReputation {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFEntityReputation) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.first_ageless_year.add_missing_data(&source.first_ageless_year);
        self.first_ageless_season_count.add_missing_data(&source.first_ageless_season_count);
        self.unsolved_murders.add_missing_data(&source.unsolved_murders);
        self.rep_hunter.add_missing_data(&source.rep_hunter);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_knowledge_preserver.add_missing_data(&source.rep_knowledge_preserver);
        self.rep_treasure_hunter.add_missing_data(&source.rep_treasure_hunter);
    }
}

impl PartialEq<df_st_core::HFEntityReputation> for HFEntityReputation {
    fn eq(&self, other: &df_st_core::HFEntityReputation) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl PartialEq<HFEntityReputation> for df_st_core::HFEntityReputation {
    fn eq(&self, other: &HFEntityReputation) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl PartialEq for HFEntityReputation {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl Hash for HFEntityReputation {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}
