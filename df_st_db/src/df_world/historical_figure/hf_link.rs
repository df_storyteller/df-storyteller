use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_links;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_links"]
#[primary_key(hf_id, hf_id_other, link_type)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFLink {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub link_type: String,
    pub world_id: i32,
    pub link_strength: Option<i32>,
}

impl HFLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFLink, HFLink> for HFLink {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_links: &[HFLink]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_links::table)
            .values(hf_links)
            .on_conflict((
                hf_links::hf_id,
                hf_links::hf_id_other,
                hf_links::link_type,
                hf_links::world_id,
            ))
            .do_update()
            .set((hf_links::link_strength.eq(excluded(hf_links::link_strength)),))
            .execute(conn)
            .expect("Error saving hf_links");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_links: &[HFLink]) {
        diesel::insert_into(hf_links::table)
            .values(hf_links)
            .execute(conn)
            .expect("Error saving hf_links");
    }

    /// Get a list of HFLink from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFLink>, Error> {
        use crate::schema::hf_links::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_links.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
                "link_type" => link_type,
                "link_strength" => link_strength,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFLink>, Error> {
        use crate::schema::hf_links::dsl::*;
        let query = hf_links;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(hf_id_other.eq(id_filter.get("hf_id_other").unwrap_or(&0)));
        Ok(query.first::<HFLink>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hf_id", "hf_id_other", "link_type", "link_strength"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFLink],
        core_list: Vec<df_st_core::HFLink>,
    ) -> Result<Vec<df_st_core::HFLink>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_links::dsl::*;
        let query = hf_links.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "hf_id_other" => {hf_id_other: i32},
                "link_type" => {link_type: String},
                "link_strength" => {link_strength: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFLink, df_st_core::HFLink> for HFLink {
    fn add_missing_data(&mut self, source: &df_st_core::HFLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        if let Some(link_type) = &source.link_type {
            self.link_type.add_missing_data(&link_type.clone());
        }
        self.link_strength.add_missing_data(&source.link_strength);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFLink, HFLink> for df_st_core::HFLink {
    fn add_missing_data(&mut self, source: &HFLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        if !source.link_type.is_empty() {
            self.link_type
                .add_missing_data(&Some(source.link_type.clone()));
        }
        self.link_strength.add_missing_data(&source.link_strength);
    }
}

impl PartialEq<df_st_core::HFLink> for HFLink {
    fn eq(&self, other: &df_st_core::HFLink) -> bool {
        self.hf_id == other.hf_id
            && self.hf_id_other == other.hf_id_other
            && match &other.link_type {
                Some(value) => &self.link_type == value,
                None => self.link_type == *"",
            }
    }
}

impl PartialEq<HFLink> for df_st_core::HFLink {
    fn eq(&self, other: &HFLink) -> bool {
        self.hf_id == other.hf_id
            && self.hf_id_other == other.hf_id_other
            && match &self.link_type {
                Some(value) => value == &other.link_type,
                None => *"" == other.link_type,
            }
    }
}

impl PartialEq for HFLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.hf_id_other == other.hf_id_other
            && self.link_type == other.link_type
    }
}

impl Hash for HFLink {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
        self.link_type.hash(state);
    }
}
