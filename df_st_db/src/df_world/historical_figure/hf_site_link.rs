use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_site_links;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_site_links"]
#[primary_key(hf_id, site_id, link_type)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFSiteLink {
    pub hf_id: i32,
    pub site_id: i32,
    pub link_type: String,
    pub world_id: i32,
    pub entity_id: Option<i32>,
    pub occupation_id: Option<i32>,
    pub sub_id: Option<i32>,
}

impl HFSiteLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFSiteLink, HFSiteLink> for HFSiteLink {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_site_links: &[HFSiteLink]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_site_links::table)
            .values(hf_site_links)
            .on_conflict((
                hf_site_links::hf_id,
                hf_site_links::site_id,
                hf_site_links::link_type,
                hf_site_links::world_id,
            ))
            .do_update()
            .set((
                hf_site_links::entity_id.eq(excluded(hf_site_links::entity_id)),
                hf_site_links::occupation_id.eq(excluded(hf_site_links::occupation_id)),
                hf_site_links::sub_id.eq(excluded(hf_site_links::sub_id)),
            ))
            .execute(conn)
            .expect("Error saving hf_site_links");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_site_links: &[HFSiteLink]) {
        diesel::insert_into(hf_site_links::table)
            .values(hf_site_links)
            .execute(conn)
            .expect("Error saving hf_site_links");
    }

    /// Get a list of HFSiteLink from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFSiteLink>, Error> {
        use crate::schema::hf_site_links::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_site_links.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "site_id" => site_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "site_id" => site_id,
                "link_type" => link_type,
                "entity_id" => entity_id,
                "occupation_id" => occupation_id,
                "sub_id" => sub_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFSiteLink>, Error> {
        use crate::schema::hf_site_links::dsl::*;
        let query = hf_site_links;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(site_id.eq(id_filter.get("site_id").unwrap_or(&0)));
        Ok(query.first::<HFSiteLink>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "hf_id",
            "site_id",
            "link_type",
            "entity_id",
            "occupation_id",
            "sub_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFSiteLink],
        core_list: Vec<df_st_core::HFSiteLink>,
    ) -> Result<Vec<df_st_core::HFSiteLink>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_site_links::dsl::*;
        let query = hf_site_links.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "site_id" => site_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "site_id" => {site_id: i32},
                "link_type" => {link_type: String},
                "entity_id" => {entity_id: Option<i32>},
                "occupation_id" => {occupation_id: Option<i32>},
                "sub_id" => {sub_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFSiteLink, df_st_core::HFSiteLink> for HFSiteLink {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HFSiteLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.site_id.add_missing_data(&source.site_id);
        if let Some(link_type) = &source.link_type {
            self.link_type.add_missing_data(&link_type.clone());
        }
        self.entity_id.add_missing_data(&source.entity_id);
        self.occupation_id.add_missing_data(&source.occupation_id);
        self.sub_id.add_missing_data(&source.sub_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFSiteLink, HFSiteLink> for df_st_core::HFSiteLink {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFSiteLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.site_id.add_missing_data(&source.site_id);
        if !source.link_type.is_empty() {
            self.link_type.add_missing_data(&Some(source.link_type.clone()));
        }
        self.entity_id.add_missing_data(&source.entity_id);
        self.occupation_id.add_missing_data(&source.occupation_id);
        self.sub_id.add_missing_data(&source.sub_id);
    }
}

impl PartialEq<df_st_core::HFSiteLink> for HFSiteLink {
    fn eq(&self, other: &df_st_core::HFSiteLink) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && match &other.link_type {
                Some(value) => &self.link_type == value,
                None => self.link_type == *"",
            }
    }
}

impl PartialEq<HFSiteLink> for df_st_core::HFSiteLink {
    fn eq(&self, other: &HFSiteLink) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && match &self.link_type {
                Some(value) => value == &other.link_type,
                None => *"" == other.link_type,
            }
    }
}

impl PartialEq for HFSiteLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.link_type == other.link_type
    }
}

impl Hash for HFSiteLink {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
        self.link_type.hash(state);
    }
}
