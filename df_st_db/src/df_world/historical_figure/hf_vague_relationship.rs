use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_vague_relationships;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_vague_relationships"]
#[primary_key(hf_id, hf_id_other)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFVagueRelationship {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub world_id: i32,
    pub war_buddy: Option<bool>,
    pub artistic_buddy: Option<bool>,
    pub atheletic_rival: Option<bool>,
    pub athlete_buddy: Option<bool>,
    pub business_rival: Option<bool>,
    pub childhood_friend: Option<bool>,
    pub grudge: Option<bool>,
    pub jealous_obsession: Option<bool>,
    pub jealous_relationship_grudge: Option<bool>,
    pub persecution_grudge: Option<bool>,
    pub religious_persecution_grudge: Option<bool>,
    pub scholar_buddy: Option<bool>,
    pub supernatural_grudge: Option<bool>,
}

impl HFVagueRelationship {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFVagueRelationship, HFVagueRelationship> for HFVagueRelationship {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_vague_relationships: &[HFVagueRelationship]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_vague_relationships::table)
            .values(hf_vague_relationships)
            .on_conflict((
                hf_vague_relationships::hf_id,
                hf_vague_relationships::hf_id_other,
                hf_vague_relationships::world_id,
            ))
            .do_update()
            .set((
                hf_vague_relationships::war_buddy.eq(excluded(hf_vague_relationships::war_buddy)),
                hf_vague_relationships::artistic_buddy
                    .eq(excluded(hf_vague_relationships::artistic_buddy)),
                hf_vague_relationships::atheletic_rival
                    .eq(excluded(hf_vague_relationships::atheletic_rival)),
                hf_vague_relationships::athlete_buddy
                    .eq(excluded(hf_vague_relationships::athlete_buddy)),
                hf_vague_relationships::business_rival
                    .eq(excluded(hf_vague_relationships::business_rival)),
                hf_vague_relationships::childhood_friend
                    .eq(excluded(hf_vague_relationships::childhood_friend)),
                hf_vague_relationships::grudge.eq(excluded(hf_vague_relationships::grudge)),
                hf_vague_relationships::jealous_obsession
                    .eq(excluded(hf_vague_relationships::jealous_obsession)),
                hf_vague_relationships::jealous_relationship_grudge.eq(excluded(
                    hf_vague_relationships::jealous_relationship_grudge,
                )),
                hf_vague_relationships::persecution_grudge
                    .eq(excluded(hf_vague_relationships::persecution_grudge)),
                hf_vague_relationships::religious_persecution_grudge.eq(excluded(
                    hf_vague_relationships::religious_persecution_grudge,
                )),
                hf_vague_relationships::scholar_buddy
                    .eq(excluded(hf_vague_relationships::scholar_buddy)),
                hf_vague_relationships::supernatural_grudge
                    .eq(excluded(hf_vague_relationships::supernatural_grudge)),
            ))
            .execute(conn)
            .expect("Error saving hf_vague_relationships");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_vague_relationships: &[HFVagueRelationship]) {
        diesel::insert_into(hf_vague_relationships::table)
            .values(hf_vague_relationships)
            .execute(conn)
            .expect("Error saving hf_vague_relationships");
    }

    /// Get a list of HFVagueRelationship from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFVagueRelationship>, Error> {
        use crate::schema::hf_vague_relationships::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_vague_relationships.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
                "war_buddy" => war_buddy,
                "artistic_buddy" => artistic_buddy,
                "atheletic_rival" => atheletic_rival,
                "athlete_buddy" => athlete_buddy,
                "business_rival" => business_rival,
                "childhood_friend" => childhood_friend,
                "grudge" => grudge,
                "jealous_obsession" => jealous_obsession,
                "jealous_relationship_grudge" => jealous_relationship_grudge,
                "persecution_grudge" => persecution_grudge,
                "religious_persecution_grudge" => religious_persecution_grudge,
                "scholar_buddy" => scholar_buddy,
                "supernatural_grudge" => supernatural_grudge,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFVagueRelationship>, Error> {
        use crate::schema::hf_vague_relationships::dsl::*;
        let query = hf_vague_relationships;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(hf_id_other.eq(id_filter.get("hf_id_other").unwrap_or(&0)));
        Ok(query.first::<HFVagueRelationship>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "hf_id",
            "hf_id_other",
            "war_buddy",
            "artistic_buddy",
            "atheletic_rival",
            "athlete_buddy",
            "business_rival",
            "childhood_friend",
            "grudge",
            "jealous_obsession",
            "jealous_relationship_grudge",
            "persecution_grudge",
            "religious_persecution_grudge",
            "scholar_buddy",
            "supernatural_grudge",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFVagueRelationship],
        core_list: Vec<df_st_core::HFVagueRelationship>,
    ) -> Result<Vec<df_st_core::HFVagueRelationship>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_vague_relationships::dsl::*;
        let query = hf_vague_relationships
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "hf_id_other" => {hf_id_other: i32},
                "war_buddy" => {war_buddy: Option<bool>},
                "artistic_buddy" => {artistic_buddy: Option<bool>},
                "atheletic_rival" => {atheletic_rival: Option<bool>},
                "athlete_buddy" => {athlete_buddy: Option<bool>},
                "business_rival" => {business_rival: Option<bool>},
                "childhood_friend" => {childhood_friend: Option<bool>},
                "grudge" => {grudge: Option<bool>},
                "jealous_obsession" => {jealous_obsession: Option<bool>},
                "jealous_relationship_grudge" => {jealous_relationship_grudge: Option<bool>},
                "persecution_grudge" => {persecution_grudge: Option<bool>},
                "religious_persecution_grudge" => {religious_persecution_grudge: Option<bool>},
                "scholar_buddy" => {scholar_buddy: Option<bool>},
                "supernatural_grudge" => {supernatural_grudge: Option<bool>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFVagueRelationship, df_st_core::HFVagueRelationship> for HFVagueRelationship {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HFVagueRelationship) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.war_buddy.add_missing_data(&source.war_buddy);
        self.artistic_buddy.add_missing_data(&source.artistic_buddy);
        self.atheletic_rival.add_missing_data(&source.atheletic_rival);
        self.athlete_buddy.add_missing_data(&source.athlete_buddy);
        self.business_rival.add_missing_data(&source.business_rival);
        self.childhood_friend.add_missing_data(&source.childhood_friend);
        self.grudge.add_missing_data(&source.grudge);
        self.jealous_obsession.add_missing_data(&source.jealous_obsession);
        self.jealous_relationship_grudge.add_missing_data(&source.jealous_relationship_grudge);
        self.persecution_grudge.add_missing_data(&source.persecution_grudge);
        self.religious_persecution_grudge.add_missing_data(&source.religious_persecution_grudge);
        self.scholar_buddy.add_missing_data(&source.scholar_buddy);
        self.supernatural_grudge.add_missing_data(&source.supernatural_grudge);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFVagueRelationship, HFVagueRelationship>
    for df_st_core::HFVagueRelationship
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFVagueRelationship) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.war_buddy.add_missing_data(&source.war_buddy);
        self.artistic_buddy.add_missing_data(&source.artistic_buddy);
        self.atheletic_rival.add_missing_data(&source.atheletic_rival);
        self.athlete_buddy.add_missing_data(&source.athlete_buddy);
        self.business_rival.add_missing_data(&source.business_rival);
        self.childhood_friend.add_missing_data(&source.childhood_friend);
        self.grudge.add_missing_data(&source.grudge);
        self.jealous_obsession.add_missing_data(&source.jealous_obsession);
        self.jealous_relationship_grudge.add_missing_data(&source.jealous_relationship_grudge);
        self.persecution_grudge.add_missing_data(&source.persecution_grudge);
        self.religious_persecution_grudge.add_missing_data(&source.religious_persecution_grudge);
        self.scholar_buddy.add_missing_data(&source.scholar_buddy);
        self.supernatural_grudge.add_missing_data(&source.supernatural_grudge);
    }
}

impl PartialEq<df_st_core::HFVagueRelationship> for HFVagueRelationship {
    fn eq(&self, other: &df_st_core::HFVagueRelationship) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq<HFVagueRelationship> for df_st_core::HFVagueRelationship {
    fn eq(&self, other: &HFVagueRelationship) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq for HFVagueRelationship {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl Hash for HFVagueRelationship {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}
