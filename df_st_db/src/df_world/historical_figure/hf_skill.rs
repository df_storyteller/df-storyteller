use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_skills;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_skills"]
#[primary_key(hf_id, skill)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFSkill {
    pub hf_id: i32,
    pub skill: String,
    pub world_id: i32,
    pub total_ip: Option<i32>,
}

impl HFSkill {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFSkill, HFSkill> for HFSkill {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_skills: &[HFSkill]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_skills::table)
            .values(hf_skills)
            .on_conflict((hf_skills::hf_id, hf_skills::skill, hf_skills::world_id))
            .do_update()
            .set((hf_skills::total_ip.eq(excluded(hf_skills::total_ip)),))
            .execute(conn)
            .expect("Error saving hf_skills");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_skills: &[HFSkill]) {
        diesel::insert_into(hf_skills::table)
            .values(hf_skills)
            .execute(conn)
            .expect("Error saving hf_skills");
    }

    /// Get a list of HFSkill from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFSkill>, Error> {
        use crate::schema::hf_skills::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_skills.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "skill" => skill,
                "total_ip" => total_ip,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFSkill>, Error> {
        use crate::schema::hf_skills::dsl::*;
        let query = hf_skills;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        Ok(query.first::<HFSkill>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["hf_id", "skill", "total_ip"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFSkill],
        core_list: Vec<df_st_core::HFSkill>,
    ) -> Result<Vec<df_st_core::HFSkill>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_skills::dsl::*;
        let query = hf_skills.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "skill" => {skill: String},
                "total_ip" => {total_ip: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFSkill, df_st_core::HFSkill> for HFSkill {
    fn add_missing_data(&mut self, source: &df_st_core::HFSkill) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.skill.add_missing_data(&source.skill);
        self.total_ip.add_missing_data(&source.total_ip);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFSkill, HFSkill> for df_st_core::HFSkill {
    fn add_missing_data(&mut self, source: &HFSkill) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.skill.add_missing_data(&source.skill);
        self.total_ip.add_missing_data(&source.total_ip);
    }
}

impl PartialEq<df_st_core::HFSkill> for HFSkill {
    fn eq(&self, other: &df_st_core::HFSkill) -> bool {
        self.hf_id == other.hf_id && self.skill == other.skill
    }
}

impl PartialEq<HFSkill> for df_st_core::HFSkill {
    fn eq(&self, other: &HFSkill) -> bool {
        self.hf_id == other.hf_id && self.skill == other.skill
    }
}

impl PartialEq for HFSkill {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.skill == other.skill
    }
}

impl Hash for HFSkill {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.skill.hash(state);
    }
}
