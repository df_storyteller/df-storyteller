use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_relationship_profile_hf;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_relationship_profile_hf"]
#[primary_key(hf_id, hf_id_other)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFRelationshipProfileHF {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub world_id: i32,
    pub visual: bool,
    pub historical: bool,

    pub meet_count: Option<i32>,
    pub last_meet_year: Option<i32>,
    pub last_meet_seconds72: Option<i32>,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
    pub known_identity_id: Option<i32>,
    pub rep_bonded: Option<i32>,
    pub rep_buddy: Option<i32>,
    pub rep_flatterer: Option<i32>,
    pub rep_friendly: Option<i32>,
    pub rep_grudge: Option<i32>,
    pub rep_information_source: Option<i32>,
    pub rep_killer: Option<i32>,
    pub rep_murderer: Option<i32>,
    pub rep_quarreler: Option<i32>,
    pub rep_violent: Option<i32>,
}

impl HFRelationshipProfileHF {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFRelationshipProfileHF, HFRelationshipProfileHF>
    for HFRelationshipProfileHF
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    #[rustfmt::skip]
    fn insert_into_db(
        conn: &DbConnection,
        hf_relationship_profile_hf: &[HFRelationshipProfileHF],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_relationship_profile_hf::table)
            .values(hf_relationship_profile_hf)
            .on_conflict((
                hf_relationship_profile_hf::hf_id,
                hf_relationship_profile_hf::hf_id_other,
                hf_relationship_profile_hf::world_id,
            ))
            .do_update()
            .set((
                hf_relationship_profile_hf::visual.eq(excluded(hf_relationship_profile_hf::visual)),
                hf_relationship_profile_hf::historical.eq(excluded(hf_relationship_profile_hf::historical)),
                hf_relationship_profile_hf::meet_count.eq(excluded(hf_relationship_profile_hf::meet_count)),
                hf_relationship_profile_hf::last_meet_year.eq(excluded(hf_relationship_profile_hf::last_meet_year)),
                hf_relationship_profile_hf::last_meet_seconds72.eq(excluded(hf_relationship_profile_hf::last_meet_seconds72)),
                hf_relationship_profile_hf::love.eq(excluded(hf_relationship_profile_hf::love)),
                hf_relationship_profile_hf::respect.eq(excluded(hf_relationship_profile_hf::respect)),
                hf_relationship_profile_hf::trust.eq(excluded(hf_relationship_profile_hf::trust)),
                hf_relationship_profile_hf::loyalty.eq(excluded(hf_relationship_profile_hf::loyalty)),
                hf_relationship_profile_hf::fear.eq(excluded(hf_relationship_profile_hf::fear)),
                hf_relationship_profile_hf::known_identity_id.eq(excluded(hf_relationship_profile_hf::known_identity_id)),
                hf_relationship_profile_hf::rep_bonded.eq(excluded(hf_relationship_profile_hf::rep_bonded)),
                hf_relationship_profile_hf::rep_buddy.eq(excluded(hf_relationship_profile_hf::rep_buddy)),
                hf_relationship_profile_hf::rep_flatterer.eq(excluded(hf_relationship_profile_hf::rep_flatterer)),
                hf_relationship_profile_hf::rep_friendly.eq(excluded(hf_relationship_profile_hf::rep_friendly)),
                hf_relationship_profile_hf::rep_grudge.eq(excluded(hf_relationship_profile_hf::rep_grudge)),
                hf_relationship_profile_hf::rep_information_source.eq(excluded(hf_relationship_profile_hf::rep_information_source)),
                hf_relationship_profile_hf::rep_killer.eq(excluded(hf_relationship_profile_hf::rep_killer)),
                hf_relationship_profile_hf::rep_murderer.eq(excluded(hf_relationship_profile_hf::rep_murderer)),
                hf_relationship_profile_hf::rep_quarreler.eq(excluded(hf_relationship_profile_hf::rep_quarreler)),
                hf_relationship_profile_hf::rep_violent.eq(excluded(hf_relationship_profile_hf::rep_violent)),
            ))
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_relationship_profile_hf: &[HFRelationshipProfileHF]) {
        diesel::insert_into(hf_relationship_profile_hf::table)
            .values(hf_relationship_profile_hf)
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf");
    }

    /// Get a list of HFRelationshipProfileHF from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HFRelationshipProfileHF>, Error> {
        use crate::schema::hf_relationship_profile_hf::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_relationship_profile_hf.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
                "visual" => visual,
                "historical" => historical,
                "meet_count" => meet_count,
                "last_meet_year" => last_meet_year,
                "last_meet_seconds72" => last_meet_seconds72,
                "love" => love,
                "respect" => respect,
                "trust" => trust,
                "loyalty" => loyalty,
                "fear" => fear,
                "known_identity_id" => known_identity_id,
                "rep_bonded" => rep_bonded,
                "rep_buddy" => rep_buddy,
                "rep_flatterer" => rep_flatterer,
                "rep_friendly" => rep_friendly,
                "rep_grudge" => rep_grudge,
                "rep_information_source" => rep_information_source,
                "rep_killer" => rep_killer,
                "rep_murderer" => rep_murderer,
                "rep_quarreler" => rep_quarreler,
                "rep_violent" => rep_violent,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFRelationshipProfileHF>, Error> {
        use crate::schema::hf_relationship_profile_hf::dsl::*;
        let query = hf_relationship_profile_hf;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        let query = query.filter(hf_id_other.eq(id_filter.get("hf_id_other").unwrap_or(&0)));
        Ok(query.first::<HFRelationshipProfileHF>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "hf_id",
            "hf_id_other",
            "visual",
            "historical",
            "meet_count",
            "last_meet_year",
            "last_meet_seconds72",
            "love",
            "respect",
            "trust",
            "loyalty",
            "fear",
            "known_identity_id",
            "rep_bonded",
            "rep_buddy",
            "rep_flatterer",
            "rep_friendly",
            "rep_grudge",
            "rep_information_source",
            "rep_killer",
            "rep_murderer",
            "rep_quarreler",
            "rep_violent",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFRelationshipProfileHF],
        core_list: Vec<df_st_core::HFRelationshipProfileHF>,
    ) -> Result<Vec<df_st_core::HFRelationshipProfileHF>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_relationship_profile_hf::dsl::*;
        let query = hf_relationship_profile_hf
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "hf_id_other" => {hf_id_other: i32},
                "visual" => {visual: bool},
                "historical" => {historical: bool},
                "meet_count" => {meet_count: Option<i32>},
                "last_meet_year" => {last_meet_year: Option<i32>},
                "last_meet_seconds72" => {last_meet_seconds72: Option<i32>},
                "love" => {love: Option<i32>},
                "respect" => {respect: Option<i32>},
                "trust" => {trust: Option<i32>},
                "loyalty" => {loyalty: Option<i32>},
                "fear" => {fear: Option<i32>},
                "known_identity_id" => {known_identity_id: Option<i32>},
                "rep_bonded" => {rep_bonded: Option<i32>},
                "rep_buddy" => {rep_buddy: Option<i32>},
                "rep_flatterer" => {rep_flatterer: Option<i32>},
                "rep_friendly" => {rep_friendly: Option<i32>},
                "rep_grudge" => {rep_grudge: Option<i32>},
                "rep_information_source" => {rep_information_source: Option<i32>},
                "rep_killer" => {rep_killer: Option<i32>},
                "rep_murderer" => {rep_murderer: Option<i32>},
                "rep_quarreler" => {rep_quarreler: Option<i32>},
                "rep_violent" => {rep_violent: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFRelationshipProfileHF, df_st_core::HFRelationshipProfileHF>
    for HFRelationshipProfileHF
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HFRelationshipProfileHF) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.visual.add_missing_data(&source.visual);
        self.historical.add_missing_data(&source.historical);
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72.add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id.add_missing_data(&source.known_identity_id);
        self.rep_bonded.add_missing_data(&source.rep_bonded);
        self.rep_buddy.add_missing_data(&source.rep_buddy);
        self.rep_flatterer.add_missing_data(&source.rep_flatterer);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_grudge.add_missing_data(&source.rep_grudge);
        self.rep_information_source.add_missing_data(&source.rep_information_source);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_murderer.add_missing_data(&source.rep_murderer);
        self.rep_quarreler.add_missing_data(&source.rep_quarreler);
        self.rep_violent.add_missing_data(&source.rep_violent);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFRelationshipProfileHF, HFRelationshipProfileHF>
    for df_st_core::HFRelationshipProfileHF
{
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HFRelationshipProfileHF) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.visual.add_missing_data(&source.visual);
        self.historical.add_missing_data(&source.historical);
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72.add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id.add_missing_data(&source.known_identity_id);
        self.rep_bonded.add_missing_data(&source.rep_bonded);
        self.rep_buddy.add_missing_data(&source.rep_buddy);
        self.rep_flatterer.add_missing_data(&source.rep_flatterer);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_grudge.add_missing_data(&source.rep_grudge);
        self.rep_information_source.add_missing_data(&source.rep_information_source);
        self.rep_killer.add_missing_data(&source.rep_killer);
        self.rep_murderer.add_missing_data(&source.rep_murderer);
        self.rep_quarreler.add_missing_data(&source.rep_quarreler);
        self.rep_violent.add_missing_data(&source.rep_violent);
    }
}

impl PartialEq<df_st_core::HFRelationshipProfileHF> for HFRelationshipProfileHF {
    fn eq(&self, other: &df_st_core::HFRelationshipProfileHF) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq<HFRelationshipProfileHF> for df_st_core::HFRelationshipProfileHF {
    fn eq(&self, other: &HFRelationshipProfileHF) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq for HFRelationshipProfileHF {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl Hash for HFRelationshipProfileHF {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}
