use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::link_he_site;
use crate::{add_link_db_object_impl, DbConnection};
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Queryable, Insertable, Fillable, Default, Hash, PartialEq, Eq,
)]
#[table_name = "link_he_site"]
pub struct LinkHESite {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a HistoricalFigure.
    pub site_id: i32,
    pub world_id: i32,
}

impl LinkHESite {
    pub fn new() -> Self {
        Self::default()
    }
}

add_link_db_object_impl!(
    df_st_core::LinkHESite,
    LinkHESite,
    link_he_site,
    "link_he_site",
    he_id,
    "he_id",
    site_id,
    "site_id"
);

/// From Core to DB
impl Filler<LinkHESite, df_st_core::LinkHESite> for LinkHESite {
    fn add_missing_data(&mut self, source: &df_st_core::LinkHESite) {
        self.he_id.add_missing_data(&source.he_id);
        self.site_id.add_missing_data(&source.site_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::LinkHESite, LinkHESite> for df_st_core::LinkHESite {
    fn add_missing_data(&mut self, source: &LinkHESite) {
        self.he_id.add_missing_data(&source.he_id);
        self.site_id.add_missing_data(&source.site_id);
    }
}

impl PartialEq<LinkHESite> for df_st_core::LinkHESite {
    fn eq(&self, other: &LinkHESite) -> bool {
        self.he_id == other.he_id && self.site_id == other.site_id
    }
}

impl PartialEq<df_st_core::LinkHESite> for LinkHESite {
    fn eq(&self, other: &df_st_core::LinkHESite) -> bool {
        self.he_id == other.he_id && self.site_id == other.site_id
    }
}
