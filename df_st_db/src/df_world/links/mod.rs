pub mod link_he_entity;
pub mod link_he_hf;
pub mod link_he_site;

pub use link_he_entity::*;
pub use link_he_hf::*;
pub use link_he_site::*;

#[macro_export]
macro_rules! add_link_db_object_impl(
    { $core_link:ty, $db_link:ty, $db_table:ident, $db_table_str:literal,
      $left:ident, $left_str:literal,
      $right:ident, $right_str:literal} => {

impl DBObject<$core_link, $db_link> for $db_link {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {}

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, values: &[Self]) {
        diesel::insert_into($db_table::table)
            .values(values)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect(&format!("Error saving {}", $db_table_str));
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, values: &[Self]) {
        diesel::insert_into($db_table::table)
            .values(values)
            .execute(conn)
            .expect(&format!("Error saving {}", $db_table_str));
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Self>, Error> {
        use $crate::schema::$db_table::dsl::*;
        let query = $db_table;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter($left.eq(id_filter.get($left_str).unwrap_or(&0)));
        let query = query.filter($right.eq(id_filter.get($right_str).unwrap_or(&0)));
        Ok(query.first::<Self>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<Self>, Error> {
        use $crate::schema::$db_table::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = $db_table.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                $left_str => $left,
                $right_str => $right,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                $left_str => $left,
                $right_str => $right,
            })},
        }
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![$left_str, $right_str]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[Self],
        core_list: Vec<$core_link>,
    ) -> Result<Vec<$core_link>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use $crate::schema::$db_table::dsl::*;
        let query = $db_table.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                $left_str => $left,
                $right_str => $right,
            ],
            {group_by!{
                group_by_opt, query, conn,
                $left_str => {$left: i32},
                $right_str => {$right: i32},
            };},
        };
    }
}

     };
);
