use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::link_he_entity;
use crate::{add_link_db_object_impl, DbConnection};
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Queryable, Insertable, Fillable, Default, Hash, PartialEq, Eq,
)]
#[table_name = "link_he_entity"]
pub struct LinkHEEntity {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a Entity.
    pub entity_id: i32,
    pub world_id: i32,
}

impl LinkHEEntity {
    pub fn new() -> Self {
        Self::default()
    }
}

add_link_db_object_impl!(
    df_st_core::LinkHEEntity,
    LinkHEEntity,
    link_he_entity,
    "link_he_entity",
    he_id,
    "he_id",
    entity_id,
    "entity_id"
);

/// From Core to DB
impl Filler<LinkHEEntity, df_st_core::LinkHEEntity> for LinkHEEntity {
    fn add_missing_data(&mut self, source: &df_st_core::LinkHEEntity) {
        self.he_id.add_missing_data(&source.he_id);
        self.entity_id.add_missing_data(&source.entity_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::LinkHEEntity, LinkHEEntity> for df_st_core::LinkHEEntity {
    fn add_missing_data(&mut self, source: &LinkHEEntity) {
        self.he_id.add_missing_data(&source.he_id);
        self.entity_id.add_missing_data(&source.entity_id);
    }
}

impl PartialEq<LinkHEEntity> for df_st_core::LinkHEEntity {
    fn eq(&self, other: &LinkHEEntity) -> bool {
        self.he_id == other.he_id && self.entity_id == other.entity_id
    }
}

impl PartialEq<df_st_core::LinkHEEntity> for LinkHEEntity {
    fn eq(&self, other: &df_st_core::LinkHEEntity) -> bool {
        self.he_id == other.he_id && self.entity_id == other.entity_id
    }
}
