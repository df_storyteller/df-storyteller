use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::link_he_hf;
use crate::{add_link_db_object_impl, DbConnection};
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Queryable, Insertable, Fillable, Default, Hash, PartialEq, Eq,
)]
#[table_name = "link_he_hf"]
pub struct LinkHEHF {
    /// A reference to a HistoricalEvent.
    pub he_id: i32,
    /// A reference to a HistoricalFigure.
    pub hf_id: i32,
    pub world_id: i32,
}

impl LinkHEHF {
    pub fn new() -> Self {
        Self::default()
    }
}

add_link_db_object_impl!(
    df_st_core::LinkHEHF,
    LinkHEHF,
    link_he_hf,
    "link_he_hf",
    he_id,
    "he_id",
    hf_id,
    "hf_id"
);

/// From Core to DB
impl Filler<LinkHEHF, df_st_core::LinkHEHF> for LinkHEHF {
    fn add_missing_data(&mut self, source: &df_st_core::LinkHEHF) {
        self.he_id.add_missing_data(&source.he_id);
        self.hf_id.add_missing_data(&source.hf_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::LinkHEHF, LinkHEHF> for df_st_core::LinkHEHF {
    fn add_missing_data(&mut self, source: &LinkHEHF) {
        self.he_id.add_missing_data(&source.he_id);
        self.hf_id.add_missing_data(&source.hf_id);
    }
}

impl PartialEq<LinkHEHF> for df_st_core::LinkHEHF {
    fn eq(&self, other: &LinkHEHF) -> bool {
        self.he_id == other.he_id && self.hf_id == other.hf_id
    }
}

impl PartialEq<df_st_core::LinkHEHF> for LinkHEHF {
    fn eq(&self, other: &df_st_core::LinkHEHF) -> bool {
        self.he_id == other.he_id && self.hf_id == other.hf_id
    }
}
