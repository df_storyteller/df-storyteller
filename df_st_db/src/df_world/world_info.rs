use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::worlds;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "worlds"]
pub struct DFWorldInfo {
    pub id: i32,
    pub save_name: Option<String>,
    pub name: Option<String>,
    pub alternative_name: Option<String>,
    pub region_number: Option<i32>,
    pub year: Option<i32>,
    pub month: Option<i32>,
    pub day: Option<i32>,
}

impl DFWorldInfo {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::DFWorldInfo, DFWorldInfo> for DFWorldInfo {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, worlds: &[DFWorldInfo]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(worlds::table)
            .values(worlds)
            .on_conflict(worlds::id)
            .do_update()
            .set((
                worlds::save_name.eq(excluded(worlds::save_name)),
                worlds::name.eq(excluded(worlds::name)),
                worlds::alternative_name.eq(excluded(worlds::alternative_name)),
                worlds::region_number.eq(excluded(worlds::region_number)),
                worlds::year.eq(excluded(worlds::year)),
                worlds::month.eq(excluded(worlds::month)),
                worlds::day.eq(excluded(worlds::day)),
            ))
            .execute(conn)
            .expect("Error saving worlds");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, worlds: &[DFWorldInfo]) {
        diesel::insert_into(worlds::table)
            .values(worlds)
            .execute(conn)
            .expect("Error saving worlds");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<DFWorldInfo>, Error> {
        use crate::schema::worlds::dsl::*;
        let query = worlds;
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<DFWorldInfo>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<DFWorldInfo>, Error> {
        use crate::schema::worlds::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = worlds.limit(limit).offset(offset);
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "region_number" => region_number,
                "year" => year,
                "month" => month,
                "day" => day,
            ],
            string_filter,
            [
                "save_name" => save_name,
                "name" => name,
                "alternative_name" => alternative_name,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "save_name" => save_name,
                "name" => name,
                "alternative_name" => alternative_name,
                "region_number" => region_number,
                "year" => year,
                "month" => month,
                "day" => day,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id", "region_number", "year", "month", "day"],
            _ => vec![
                "id",
                "save_name",
                "name",
                "alternative_name",
                "region_number",
                "year",
                "month",
                "day",
            ],
        }
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[DFWorldInfo],
        core_list: Vec<df_st_core::DFWorldInfo>,
    ) -> Result<Vec<df_st_core::DFWorldInfo>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::worlds::dsl::*;
        let query = worlds.limit(limit as i64).offset(offset as i64);
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "region_number" => region_number,
                "year" => year,
                "month" => month,
                "day" => day,
            ],
            string_filter,
            [
                "save_name" => save_name,
                "name" => name,
                "alternative_name" => alternative_name,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "save_name" => {save_name: Option<String>},
                "name" => {name: Option<String>},
                "alternative_name" => {alternative_name: Option<String>},
                "region_number" => {region_number: Option<i32>},
                "year" => {year: Option<i32>},
                "month" => {month: Option<i32>},
                "day" => {day: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<DFWorldInfo, df_st_core::DFWorldInfo> for DFWorldInfo {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::DFWorldInfo) {
        self.id.add_missing_data(&source.id);
        self.save_name.add_missing_data(&source.save_name);
        self.name.add_missing_data(&source.name);
        self.alternative_name.add_missing_data(&source.alternative_name);
        self.region_number.add_missing_data(&source.region_number);
        self.year.add_missing_data(&source.year);
        self.month.add_missing_data(&source.month);
        self.day.add_missing_data(&source.day);
    }
}

/// From DB to Core
impl Filler<df_st_core::DFWorldInfo, DFWorldInfo> for df_st_core::DFWorldInfo {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &DFWorldInfo) {
        self.id.add_missing_data(&source.id);
        self.save_name.add_missing_data(&source.save_name);
        self.name.add_missing_data(&source.name);
        self.alternative_name.add_missing_data(&source.alternative_name);
        self.region_number.add_missing_data(&source.region_number);
        self.year.add_missing_data(&source.year);
        self.month.add_missing_data(&source.month);
        self.day.add_missing_data(&source.day);
    }
}

impl PartialEq<DFWorldInfo> for df_st_core::DFWorldInfo {
    fn eq(&self, other: &DFWorldInfo) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::DFWorldInfo> for DFWorldInfo {
    fn eq(&self, other: &df_st_core::DFWorldInfo) -> bool {
        self.id == other.id
    }
}
