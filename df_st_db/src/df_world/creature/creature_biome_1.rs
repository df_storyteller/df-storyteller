use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creature_biomes_1;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creature_biomes_1"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureBiome1 {
    pub cr_id: i32,
    pub world_id: i32,

    pub desert_badland: Option<bool>,
    pub desert_rock: Option<bool>,
    pub desert_sand: Option<bool>,
    pub forest_taiga: Option<bool>,
    pub forest_temperate_broadleaf: Option<bool>,
    pub forest_temperate_conifer: Option<bool>,
    pub forest_tropical_conifer: Option<bool>,
    pub forest_tropical_dry_broadleaf: Option<bool>,
    pub forest_tropical_moist_broadleaf: Option<bool>,
    pub glacier: Option<bool>,
    pub grassland_temperate: Option<bool>,
    pub grassland_tropical: Option<bool>,
    pub lake_temperate_brackishwater: Option<bool>,
    pub lake_temperate_freshwater: Option<bool>,
    pub lake_temperate_saltwater: Option<bool>,
    pub lake_tropical_brackishwater: Option<bool>,
    pub lake_tropical_freshwater: Option<bool>,
    pub lake_tropical_saltwater: Option<bool>,
    pub marsh_temperate_freshwater: Option<bool>,
    pub marsh_temperate_saltwater: Option<bool>,
    pub marsh_tropical_freshwater: Option<bool>,
    pub marsh_tropical_saltwater: Option<bool>,
    pub mountain: Option<bool>,
}

impl CreatureBiome1 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::CreatureBiome, CreatureBiome1> for CreatureBiome1 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creature_biomes_1: &[CreatureBiome1]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creature_biomes_1::table)
            .values(creature_biomes_1)
            .on_conflict((creature_biomes_1::cr_id, creature_biomes_1::world_id))
            .do_update()
            .set((
                creature_biomes_1::desert_badland.eq(excluded(creature_biomes_1::desert_badland)),
                creature_biomes_1::desert_rock.eq(excluded(creature_biomes_1::desert_rock)),
                creature_biomes_1::desert_sand.eq(excluded(creature_biomes_1::desert_sand)),
                creature_biomes_1::forest_taiga.eq(excluded(creature_biomes_1::forest_taiga)),
                creature_biomes_1::forest_temperate_broadleaf
                    .eq(excluded(creature_biomes_1::forest_temperate_broadleaf)),
                creature_biomes_1::forest_temperate_conifer
                    .eq(excluded(creature_biomes_1::forest_temperate_conifer)),
                creature_biomes_1::forest_tropical_conifer
                    .eq(excluded(creature_biomes_1::forest_tropical_conifer)),
                creature_biomes_1::forest_tropical_dry_broadleaf
                    .eq(excluded(creature_biomes_1::forest_tropical_dry_broadleaf)),
                creature_biomes_1::forest_tropical_moist_broadleaf
                    .eq(excluded(creature_biomes_1::forest_tropical_moist_broadleaf)),
                creature_biomes_1::glacier.eq(excluded(creature_biomes_1::glacier)),
                creature_biomes_1::grassland_temperate
                    .eq(excluded(creature_biomes_1::grassland_temperate)),
                creature_biomes_1::grassland_tropical
                    .eq(excluded(creature_biomes_1::grassland_tropical)),
                creature_biomes_1::lake_temperate_brackishwater
                    .eq(excluded(creature_biomes_1::lake_temperate_brackishwater)),
                creature_biomes_1::lake_temperate_freshwater
                    .eq(excluded(creature_biomes_1::lake_temperate_freshwater)),
                creature_biomes_1::lake_temperate_saltwater
                    .eq(excluded(creature_biomes_1::lake_temperate_saltwater)),
                creature_biomes_1::lake_tropical_brackishwater
                    .eq(excluded(creature_biomes_1::lake_tropical_brackishwater)),
                creature_biomes_1::lake_tropical_freshwater
                    .eq(excluded(creature_biomes_1::lake_tropical_freshwater)),
                creature_biomes_1::lake_tropical_saltwater
                    .eq(excluded(creature_biomes_1::lake_tropical_saltwater)),
                creature_biomes_1::marsh_temperate_freshwater
                    .eq(excluded(creature_biomes_1::marsh_temperate_freshwater)),
                creature_biomes_1::marsh_temperate_saltwater
                    .eq(excluded(creature_biomes_1::marsh_temperate_saltwater)),
                creature_biomes_1::marsh_tropical_freshwater
                    .eq(excluded(creature_biomes_1::marsh_tropical_freshwater)),
                creature_biomes_1::marsh_tropical_saltwater
                    .eq(excluded(creature_biomes_1::marsh_tropical_saltwater)),
                creature_biomes_1::mountain.eq(excluded(creature_biomes_1::mountain)),
            ))
            .execute(conn)
            .expect("Error saving creature_biomes_1");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creature_biomes_1: &[CreatureBiome1]) {
        diesel::insert_into(creature_biomes_1::table)
            .values(creature_biomes_1)
            .execute(conn)
            .expect("Error saving creature_biomes_1");
    }

    /// Get a list of CreatureBiome1 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<CreatureBiome1>, Error> {
        /*
        use crate::schema::creature_biomes_1::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creature_biomes_1.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "desert_badland" => desert_badland,
                "desert_rock" => desert_rock,
                "desert_sand" => desert_sand,
                "forest_taiga" => forest_taiga,
                "forest_temperate_broadleaf" => forest_temperate_broadleaf,
                "forest_temperate_conifer" => forest_temperate_conifer,
                "forest_tropical_conifer" => forest_tropical_conifer,
                "forest_tropical_dry_broadleaf" => forest_tropical_dry_broadleaf,
                "forest_tropical_moist_broadleaf" => forest_tropical_moist_broadleaf,
                "glacier" => glacier,
                "grassland_temperate" => grassland_temperate,
                "grassland_tropical" => grassland_tropical,
                "lake_temperate_brackishwater" => lake_temperate_brackishwater,
                "lake_temperate_freshwater" => lake_temperate_freshwater,
                "lake_temperate_saltwater" => lake_temperate_saltwater,
                "lake_tropical_brackishwater" => lake_tropical_brackishwater,
                "lake_tropical_freshwater" => lake_tropical_freshwater,
                "lake_tropical_saltwater" => lake_tropical_saltwater,
                "marsh_temperate_freshwater" => marsh_temperate_freshwater,
                "marsh_temperate_saltwater" => marsh_temperate_saltwater,
                "marsh_tropical_freshwater" => marsh_tropical_freshwater,
                "marsh_tropical_saltwater" => marsh_tropical_saltwater,
                "mountain" => mountain,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureBiome1>, Error> {
        use crate::schema::creature_biomes_1::dsl::*;
        let query = creature_biomes_1;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        Ok(query.first::<CreatureBiome1>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "cr_id",
            "desert_badland",
            "desert_rock",
            "desert_sand",
            "forest_taiga",
            "forest_temperate_broadleaf",
            "forest_temperate_conifer",
            "forest_tropical_conifer",
            "forest_tropical_dry_broadleaf",
            "forest_tropical_moist_broadleaf",
            "glacier",
            "grassland_temperate",
            "grassland_tropical",
            "lake_temperate_brackishwater",
            "lake_temperate_freshwater",
            "lake_temperate_saltwater",
            "lake_tropical_brackishwater",
            "lake_tropical_freshwater",
            "lake_tropical_saltwater",
            "marsh_temperate_freshwater",
            "marsh_temperate_saltwater",
            "marsh_tropical_freshwater",
            "marsh_tropical_saltwater",
            "mountain",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureBiome1],
        core_list: Vec<df_st_core::CreatureBiome>,
    ) -> Result<Vec<df_st_core::CreatureBiome>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creature_biomes_1::dsl::*;
        let query = creature_biomes_1.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "desert_badland" => {desert_badland: Option<bool>},
                "desert_rock" => {desert_rock: Option<bool>},
                "desert_sand" => {desert_sand: Option<bool>},
                "forest_taiga" => {forest_taiga: Option<bool>},
                "forest_temperate_broadleaf" => {forest_temperate_broadleaf: Option<bool>},
                "forest_temperate_conifer" => {forest_temperate_conifer: Option<bool>},
                "forest_tropical_conifer" => {forest_tropical_conifer: Option<bool>},
                "forest_tropical_dry_broadleaf" => {forest_tropical_dry_broadleaf: Option<bool>},
                "forest_tropical_moist_broadleaf" => {forest_tropical_moist_broadleaf: Option<bool>},
                "glacier" => {glacier: Option<bool>},
                "grassland_temperate" => {grassland_temperate: Option<bool>},
                "grassland_tropical" => {grassland_tropical: Option<bool>},
                "lake_temperate_brackishwater" => {lake_temperate_brackishwater: Option<bool>},
                "lake_temperate_freshwater" => {lake_temperate_freshwater: Option<bool>},
                "lake_temperate_saltwater" => {lake_temperate_saltwater: Option<bool>},
                "lake_tropical_brackishwater" => {lake_tropical_brackishwater: Option<bool>},
                "lake_tropical_freshwater" => {lake_tropical_freshwater: Option<bool>},
                "lake_tropical_saltwater" => {lake_tropical_saltwater: Option<bool>},
                "marsh_temperate_freshwater" => {marsh_temperate_freshwater: Option<bool>},
                "marsh_temperate_saltwater" => {marsh_temperate_saltwater: Option<bool>},
                "marsh_tropical_freshwater" => {marsh_tropical_freshwater: Option<bool>},
                "marsh_tropical_saltwater" => {marsh_tropical_saltwater: Option<bool>},
                "mountain" => {mountain: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureBiome1, df_st_core::CreatureBiome> for CreatureBiome1 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::CreatureBiome) {
        self.cr_id.add_missing_data(&source.cr_id);
        self.desert_badland.add_missing_data(&source.desert_badland);
        self.desert_rock.add_missing_data(&source.desert_rock);
        self.desert_sand.add_missing_data(&source.desert_sand);
        self.forest_taiga.add_missing_data(&source.forest_taiga);
        self.forest_temperate_broadleaf.add_missing_data(&source.forest_temperate_broadleaf);
        self.forest_temperate_conifer.add_missing_data(&source.forest_temperate_conifer);
        self.forest_tropical_conifer.add_missing_data(&source.forest_tropical_conifer);
        self.forest_tropical_dry_broadleaf.add_missing_data(&source.forest_tropical_dry_broadleaf);
        self.forest_tropical_moist_broadleaf.add_missing_data(&source.forest_tropical_moist_broadleaf);
        self.glacier.add_missing_data(&source.glacier);
        self.grassland_temperate.add_missing_data(&source.grassland_temperate);
        self.grassland_tropical.add_missing_data(&source.grassland_tropical);
        self.lake_temperate_brackishwater.add_missing_data(&source.lake_temperate_brackishwater);
        self.lake_temperate_freshwater.add_missing_data(&source.lake_temperate_freshwater);
        self.lake_temperate_saltwater.add_missing_data(&source.lake_temperate_saltwater);
        self.lake_tropical_brackishwater.add_missing_data(&source.lake_tropical_brackishwater);
        self.lake_tropical_freshwater.add_missing_data(&source.lake_tropical_freshwater);
        self.lake_tropical_saltwater.add_missing_data(&source.lake_tropical_saltwater);
        self.marsh_temperate_freshwater.add_missing_data(&source.marsh_temperate_freshwater);
        self.marsh_temperate_saltwater.add_missing_data(&source.marsh_temperate_saltwater);
        self.marsh_tropical_freshwater.add_missing_data(&source.marsh_tropical_freshwater);
        self.marsh_tropical_saltwater.add_missing_data(&source.marsh_tropical_saltwater);
        self.mountain.add_missing_data(&source.mountain);
    }
}

/// From DB to Core
impl Filler<df_st_core::CreatureBiome, CreatureBiome1> for df_st_core::CreatureBiome {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &CreatureBiome1) {
        self.cr_id.add_missing_data(&source.cr_id);
        self.desert_badland.add_missing_data(&source.desert_badland);
        self.desert_rock.add_missing_data(&source.desert_rock);
        self.desert_sand.add_missing_data(&source.desert_sand);
        self.forest_taiga.add_missing_data(&source.forest_taiga);
        self.forest_temperate_broadleaf.add_missing_data(&source.forest_temperate_broadleaf);
        self.forest_temperate_conifer.add_missing_data(&source.forest_temperate_conifer);
        self.forest_tropical_conifer.add_missing_data(&source.forest_tropical_conifer);
        self.forest_tropical_dry_broadleaf.add_missing_data(&source.forest_tropical_dry_broadleaf);
        self.forest_tropical_moist_broadleaf.add_missing_data(&source.forest_tropical_moist_broadleaf);
        self.glacier.add_missing_data(&source.glacier);
        self.grassland_temperate.add_missing_data(&source.grassland_temperate);
        self.grassland_tropical.add_missing_data(&source.grassland_tropical);
        self.lake_temperate_brackishwater.add_missing_data(&source.lake_temperate_brackishwater);
        self.lake_temperate_freshwater.add_missing_data(&source.lake_temperate_freshwater);
        self.lake_temperate_saltwater.add_missing_data(&source.lake_temperate_saltwater);
        self.lake_tropical_brackishwater.add_missing_data(&source.lake_tropical_brackishwater);
        self.lake_tropical_freshwater.add_missing_data(&source.lake_tropical_freshwater);
        self.lake_tropical_saltwater.add_missing_data(&source.lake_tropical_saltwater);
        self.marsh_temperate_freshwater.add_missing_data(&source.marsh_temperate_freshwater);
        self.marsh_temperate_saltwater.add_missing_data(&source.marsh_temperate_saltwater);
        self.marsh_tropical_freshwater.add_missing_data(&source.marsh_tropical_freshwater);
        self.marsh_tropical_saltwater.add_missing_data(&source.marsh_tropical_saltwater);
        self.mountain.add_missing_data(&source.mountain);
    }
}

impl PartialEq for CreatureBiome1 {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureBiome1 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureBiome1> for df_st_core::CreatureBiome {
    fn eq(&self, other: &CreatureBiome1) -> bool {
        self.cr_id == other.cr_id
    }
}

impl PartialEq<df_st_core::CreatureBiome> for CreatureBiome1 {
    fn eq(&self, other: &df_st_core::CreatureBiome) -> bool {
        self.cr_id == other.cr_id
    }
}
