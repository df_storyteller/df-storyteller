use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creatures_l_z;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creatures_l_z"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureLZ {
    pub cr_id: i32,
    pub world_id: i32,

    pub large_roaming: Option<bool>,
    pub loose_clusters: Option<bool>,
    pub mates_to_breed: Option<bool>,
    pub mundane: Option<bool>,

    pub occurs_as_entity_race: Option<bool>,
    pub savage: Option<bool>,
    pub small_race: Option<bool>,

    pub two_genders: Option<bool>,
    pub ubiquitous: Option<bool>,

    pub vermin_eater: Option<bool>,
    pub vermin_fish: Option<bool>,
    pub vermin_grounder: Option<bool>,
    pub vermin_rotter: Option<bool>,
    pub vermin_soil: Option<bool>,
    pub vermin_soil_colony: Option<bool>,
}

impl CreatureLZ {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Creature, CreatureLZ> for CreatureLZ {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creatures_l_z: &[CreatureLZ]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creatures_l_z::table)
            .values(creatures_l_z)
            .on_conflict((creatures_l_z::cr_id, creatures_l_z::world_id))
            .do_update()
            .set((
                creatures_l_z::large_roaming.eq(excluded(creatures_l_z::large_roaming)),
                creatures_l_z::loose_clusters.eq(excluded(creatures_l_z::loose_clusters)),
                creatures_l_z::mates_to_breed.eq(excluded(creatures_l_z::mates_to_breed)),
                creatures_l_z::mundane.eq(excluded(creatures_l_z::mundane)),
                creatures_l_z::occurs_as_entity_race
                    .eq(excluded(creatures_l_z::occurs_as_entity_race)),
                creatures_l_z::savage.eq(excluded(creatures_l_z::savage)),
                creatures_l_z::small_race.eq(excluded(creatures_l_z::small_race)),
                creatures_l_z::two_genders.eq(excluded(creatures_l_z::two_genders)),
                creatures_l_z::ubiquitous.eq(excluded(creatures_l_z::ubiquitous)),
                creatures_l_z::vermin_eater.eq(excluded(creatures_l_z::vermin_eater)),
                creatures_l_z::vermin_fish.eq(excluded(creatures_l_z::vermin_fish)),
                creatures_l_z::vermin_grounder.eq(excluded(creatures_l_z::vermin_grounder)),
                creatures_l_z::vermin_rotter.eq(excluded(creatures_l_z::vermin_rotter)),
                creatures_l_z::vermin_soil.eq(excluded(creatures_l_z::vermin_soil)),
                creatures_l_z::vermin_soil_colony.eq(excluded(creatures_l_z::vermin_soil_colony)),
            ))
            .execute(conn)
            .expect("Error saving creatures_l_z");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creatures_l_z: &[CreatureLZ]) {
        diesel::insert_into(creatures_l_z::table)
            .values(creatures_l_z)
            .execute(conn)
            .expect("Error saving creatures_l_z");
    }

    /// Get a list of CreatureLZ from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<CreatureLZ>, Error> {
        /*
        use crate::schema::creatures_l_z::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creatures_l_z.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "large_roaming" => large_roaming,
                "loose_clusters" => loose_clusters,
                "mates_to_breed" => mates_to_breed,
                "mundane" => mundane,
                "occurs_as_entity_race" => occurs_as_entity_race,
                "savage" => savage,
                "small_race" => small_race,
                "two_genders" => two_genders,
                "ubiquitous" => ubiquitous,
                "vermin_eater" => vermin_eater,
                "vermin_fish" => vermin_fish,
                "vermin_grounder" => vermin_grounder,
                "vermin_rotter" => vermin_rotter,
                "vermin_soil" => vermin_soil,
                "vermin_soil_colony" => vermin_soil_colony,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureLZ>, Error> {
        use crate::schema::creatures_l_z::dsl::*;
        let query = creatures_l_z;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        Ok(query.first::<CreatureLZ>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "cr_id",
            "large_roaming",
            "loose_clusters",
            "mates_to_breed",
            "mundane",
            "occurs_as_entity_race",
            "savage",
            "small_race",
            "two_genders",
            "ubiquitous",
            "vermin_eater",
            "vermin_fish",
            "vermin_grounder",
            "vermin_rotter",
            "vermin_soil",
            "vermin_soil_colony",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureLZ],
        core_list: Vec<df_st_core::Creature>,
    ) -> Result<Vec<df_st_core::Creature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creatures_l_z::dsl::*;
        let query = creatures_l_z.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "large_roaming" => {large_roaming: Option<bool>},
                "loose_clusters" => {loose_clusters: Option<bool>},
                "mates_to_breed" => {mates_to_breed: Option<bool>},
                "mundane" => {mundane: Option<bool>},
                "occurs_as_entity_race" => {occurs_as_entity_race: Option<bool>},
                "savage" => {savage: Option<bool>},
                "small_race" => {small_race: Option<bool>},
                "two_genders" => {two_genders: Option<bool>},
                "ubiquitous" => {ubiquitous: Option<bool>},
                "vermin_eater" => {vermin_eater: Option<bool>},
                "vermin_fish" => {vermin_fish: Option<bool>},
                "vermin_grounder" => {vermin_grounder: Option<bool>},
                "vermin_rotter" => {vermin_rotter: Option<bool>},
                "vermin_soil" => {vermin_soil: Option<bool>},
                "vermin_soil_colony" => {vermin_soil_colony: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureLZ, df_st_core::Creature> for CreatureLZ {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::Creature) {
        self.cr_id.add_missing_data(&source.id);
        self.large_roaming.add_missing_data(&source.large_roaming);
        self.loose_clusters.add_missing_data(&source.loose_clusters);
        self.mates_to_breed.add_missing_data(&source.mates_to_breed);
        self.mundane.add_missing_data(&source.mundane);
        self.occurs_as_entity_race.add_missing_data(&source.occurs_as_entity_race);
        self.savage.add_missing_data(&source.savage);
        self.small_race.add_missing_data(&source.small_race);
        self.two_genders.add_missing_data(&source.two_genders);
        self.ubiquitous.add_missing_data(&source.ubiquitous);
        self.vermin_eater.add_missing_data(&source.vermin_eater);
        self.vermin_fish.add_missing_data(&source.vermin_fish);
        self.vermin_grounder.add_missing_data(&source.vermin_grounder);
        self.vermin_rotter.add_missing_data(&source.vermin_rotter);
        self.vermin_soil.add_missing_data(&source.vermin_soil);
        self.vermin_soil_colony.add_missing_data(&source.vermin_soil_colony);
    }
}

/// From DB to Core
impl Filler<df_st_core::Creature, CreatureLZ> for df_st_core::Creature {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &CreatureLZ) {
        self.id.add_missing_data(&source.cr_id);
        self.large_roaming.add_missing_data(&source.large_roaming);
        self.loose_clusters.add_missing_data(&source.loose_clusters);
        self.mates_to_breed.add_missing_data(&source.mates_to_breed);
        self.mundane.add_missing_data(&source.mundane);
        self.occurs_as_entity_race.add_missing_data(&source.occurs_as_entity_race);
        self.savage.add_missing_data(&source.savage);
        self.small_race.add_missing_data(&source.small_race);
        self.two_genders.add_missing_data(&source.two_genders);
        self.ubiquitous.add_missing_data(&source.ubiquitous);
        self.vermin_eater.add_missing_data(&source.vermin_eater);
        self.vermin_fish.add_missing_data(&source.vermin_fish);
        self.vermin_grounder.add_missing_data(&source.vermin_grounder);
        self.vermin_rotter.add_missing_data(&source.vermin_rotter);
        self.vermin_soil.add_missing_data(&source.vermin_soil);
        self.vermin_soil_colony.add_missing_data(&source.vermin_soil_colony);
    }
}

impl PartialEq for CreatureLZ {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureLZ {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureLZ> for df_st_core::Creature {
    fn eq(&self, other: &CreatureLZ) -> bool {
        self.id == other.cr_id
    }
}

impl PartialEq<df_st_core::Creature> for CreatureLZ {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.cr_id == other.id
    }
}
