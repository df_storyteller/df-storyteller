use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creature_biomes_2;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creature_biomes_2"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureBiome2 {
    pub cr_id: i32,
    pub world_id: i32,

    pub ocean_arctic: Option<bool>,
    pub ocean_temperate: Option<bool>,
    pub ocean_tropical: Option<bool>,
    pub pool_temperate_brackishwater: Option<bool>,
    pub pool_temperate_freshwater: Option<bool>,
    pub pool_temperate_saltwater: Option<bool>,
    pub pool_tropical_brackishwater: Option<bool>,
    pub pool_tropical_freshwater: Option<bool>,
    pub pool_tropical_saltwater: Option<bool>,
    pub river_temperate_brackishwater: Option<bool>,
    pub river_temperate_freshwater: Option<bool>,
    pub river_temperate_saltwater: Option<bool>,
    pub river_tropical_brackishwater: Option<bool>,
    pub river_tropical_freshwater: Option<bool>,
    pub river_tropical_saltwater: Option<bool>,
    pub savanna_temperate: Option<bool>,
    pub savanna_tropical: Option<bool>,
    pub shrubland_temperate: Option<bool>,
    pub shrubland_tropical: Option<bool>,
    pub subterranean_chasm: Option<bool>,
    pub subterranean_lava: Option<bool>,
    pub subterranean_water: Option<bool>,
    pub swamp_mangrove: Option<bool>,
    pub swamp_temperate_freshwater: Option<bool>,
    pub swamp_temperate_saltwater: Option<bool>,
    pub swamp_tropical_freshwater: Option<bool>,
    pub swamp_tropical_saltwater: Option<bool>,
    pub tundra: Option<bool>,
}

impl CreatureBiome2 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::CreatureBiome, CreatureBiome2> for CreatureBiome2 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creature_biomes_2: &[CreatureBiome2]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creature_biomes_2::table)
            .values(creature_biomes_2)
            .on_conflict((creature_biomes_2::cr_id, creature_biomes_2::world_id))
            .do_update()
            .set((
                creature_biomes_2::ocean_arctic.eq(excluded(creature_biomes_2::ocean_arctic)),
                creature_biomes_2::ocean_temperate.eq(excluded(creature_biomes_2::ocean_temperate)),
                creature_biomes_2::ocean_tropical.eq(excluded(creature_biomes_2::ocean_tropical)),
                creature_biomes_2::pool_temperate_brackishwater
                    .eq(excluded(creature_biomes_2::pool_temperate_brackishwater)),
                creature_biomes_2::pool_temperate_freshwater
                    .eq(excluded(creature_biomes_2::pool_temperate_freshwater)),
                creature_biomes_2::pool_temperate_saltwater
                    .eq(excluded(creature_biomes_2::pool_temperate_saltwater)),
                creature_biomes_2::pool_tropical_brackishwater
                    .eq(excluded(creature_biomes_2::pool_tropical_brackishwater)),
                creature_biomes_2::pool_tropical_freshwater
                    .eq(excluded(creature_biomes_2::pool_tropical_freshwater)),
                creature_biomes_2::pool_tropical_saltwater
                    .eq(excluded(creature_biomes_2::pool_tropical_saltwater)),
                creature_biomes_2::river_temperate_brackishwater
                    .eq(excluded(creature_biomes_2::river_temperate_brackishwater)),
                creature_biomes_2::river_temperate_freshwater
                    .eq(excluded(creature_biomes_2::river_temperate_freshwater)),
                creature_biomes_2::river_temperate_saltwater
                    .eq(excluded(creature_biomes_2::river_temperate_saltwater)),
                creature_biomes_2::river_tropical_brackishwater
                    .eq(excluded(creature_biomes_2::river_tropical_brackishwater)),
                creature_biomes_2::river_tropical_freshwater
                    .eq(excluded(creature_biomes_2::river_tropical_freshwater)),
                creature_biomes_2::river_tropical_saltwater
                    .eq(excluded(creature_biomes_2::river_tropical_saltwater)),
                creature_biomes_2::savanna_temperate
                    .eq(excluded(creature_biomes_2::savanna_temperate)),
                creature_biomes_2::savanna_tropical
                    .eq(excluded(creature_biomes_2::savanna_tropical)),
                creature_biomes_2::shrubland_temperate
                    .eq(excluded(creature_biomes_2::shrubland_temperate)),
                creature_biomes_2::shrubland_tropical
                    .eq(excluded(creature_biomes_2::shrubland_tropical)),
                creature_biomes_2::subterranean_chasm
                    .eq(excluded(creature_biomes_2::subterranean_chasm)),
                creature_biomes_2::subterranean_lava
                    .eq(excluded(creature_biomes_2::subterranean_lava)),
                creature_biomes_2::subterranean_water
                    .eq(excluded(creature_biomes_2::subterranean_water)),
                creature_biomes_2::swamp_mangrove.eq(excluded(creature_biomes_2::swamp_mangrove)),
                creature_biomes_2::swamp_temperate_freshwater
                    .eq(excluded(creature_biomes_2::swamp_temperate_freshwater)),
                creature_biomes_2::swamp_temperate_saltwater
                    .eq(excluded(creature_biomes_2::swamp_temperate_saltwater)),
                creature_biomes_2::swamp_tropical_freshwater
                    .eq(excluded(creature_biomes_2::swamp_tropical_freshwater)),
                creature_biomes_2::swamp_tropical_saltwater
                    .eq(excluded(creature_biomes_2::swamp_tropical_saltwater)),
                creature_biomes_2::tundra.eq(excluded(creature_biomes_2::tundra)),
            ))
            .execute(conn)
            .expect("Error saving creature_biomes_2");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creature_biomes_2: &[CreatureBiome2]) {
        diesel::insert_into(creature_biomes_2::table)
            .values(creature_biomes_2)
            .execute(conn)
            .expect("Error saving creature_biomes_2");
    }

    /// Get a list of CreatureBiome2 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<CreatureBiome2>, Error> {
        /*
        use crate::schema::creature_biomes_2::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creature_biomes_2.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "ocean_arctic" => ocean_arctic,
                "ocean_temperate" => ocean_temperate,
                "ocean_tropical" => ocean_tropical,
                "pool_temperate_brackishwater" => pool_temperate_brackishwater,
                "pool_temperate_freshwater" => pool_temperate_freshwater,
                "pool_temperate_saltwater" => pool_temperate_saltwater,
                "pool_tropical_brackishwater" => pool_tropical_brackishwater,
                "pool_tropical_freshwater" => pool_tropical_freshwater,
                "pool_tropical_saltwater" => pool_tropical_saltwater,
                "river_temperate_brackishwater" => river_temperate_brackishwater,
                "river_temperate_freshwater" => river_temperate_freshwater,
                "river_temperate_saltwater" => river_temperate_saltwater,
                "river_tropical_brackishwater" => river_tropical_brackishwater,
                "river_tropical_freshwater" => river_tropical_freshwater,
                "river_tropical_saltwater" => river_tropical_saltwater,
                "savanna_temperate" => savanna_temperate,
                "savanna_tropical" => savanna_tropical,
                "shrubland_temperate" => shrubland_temperate,
                "shrubland_tropical" => shrubland_tropical,
                "subterranean_chasm" => subterranean_chasm,
                "subterranean_lava" => subterranean_lava,
                "subterranean_water" => subterranean_water,
                "swamp_mangrove" => swamp_mangrove,
                "swamp_temperate_freshwater" => swamp_temperate_freshwater,
                "swamp_temperate_saltwater" => swamp_temperate_saltwater,
                "swamp_tropical_freshwater" => swamp_tropical_freshwater,
                "swamp_tropical_saltwater" => swamp_tropical_saltwater,
                "tundra" => tundra,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureBiome2>, Error> {
        use crate::schema::creature_biomes_2::dsl::*;
        let query = creature_biomes_2;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        Ok(query.first::<CreatureBiome2>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "cr_id",
            "ocean_arctic",
            "ocean_temperate",
            "ocean_tropical",
            "pool_temperate_brackishwater",
            "pool_temperate_freshwater",
            "pool_temperate_saltwater",
            "pool_tropical_brackishwater",
            "pool_tropical_freshwater",
            "pool_tropical_saltwater",
            "river_temperate_brackishwater",
            "river_temperate_freshwater",
            "river_temperate_saltwater",
            "river_tropical_brackishwater",
            "river_tropical_freshwater",
            "river_tropical_saltwater",
            "savanna_temperate",
            "savanna_tropical",
            "shrubland_temperate",
            "shrubland_tropical",
            "subterranean_chasm",
            "subterranean_lava",
            "subterranean_water",
            "swamp_mangrove",
            "swamp_temperate_freshwater",
            "swamp_temperate_saltwater",
            "swamp_tropical_freshwater",
            "swamp_tropical_saltwater",
            "tundra",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureBiome2],
        core_list: Vec<df_st_core::CreatureBiome>,
    ) -> Result<Vec<df_st_core::CreatureBiome>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creature_biomes_2::dsl::*;
        let query = creature_biomes_2.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "ocean_arctic" => {ocean_arctic: Option<bool>},
                "ocean_temperate" => {ocean_temperate: Option<bool>},
                "ocean_tropical" => {ocean_tropical: Option<bool>},
                "pool_temperate_brackishwater" => {pool_temperate_brackishwater: Option<bool>},
                "pool_temperate_freshwater" => {pool_temperate_freshwater: Option<bool>},
                "pool_temperate_saltwater" => {pool_temperate_saltwater: Option<bool>},
                "pool_tropical_brackishwater" => {pool_tropical_brackishwater: Option<bool>},
                "pool_tropical_freshwater" => {pool_tropical_freshwater: Option<bool>},
                "pool_tropical_saltwater" => {pool_tropical_saltwater: Option<bool>},
                "river_temperate_brackishwater" => {river_temperate_brackishwater: Option<bool>},
                "river_temperate_freshwater" => {river_temperate_freshwater: Option<bool>},
                "river_temperate_saltwater" => {river_temperate_saltwater: Option<bool>},
                "river_tropical_brackishwater" => {river_tropical_brackishwater: Option<bool>},
                "river_tropical_freshwater" => {river_tropical_freshwater: Option<bool>},
                "river_tropical_saltwater" => {river_tropical_saltwater: Option<bool>},
                "savanna_temperate" => {savanna_temperate: Option<bool>},
                "savanna_tropical" => {savanna_tropical: Option<bool>},
                "shrubland_temperate" => {shrubland_temperate: Option<bool>},
                "shrubland_tropical" => {shrubland_tropical: Option<bool>},
                "subterranean_chasm" => {subterranean_chasm: Option<bool>},
                "subterranean_lava" => {subterranean_lava: Option<bool>},
                "subterranean_water" => {subterranean_water: Option<bool>},
                "swamp_mangrove" => {swamp_mangrove: Option<bool>},
                "swamp_temperate_freshwater" => {swamp_temperate_freshwater: Option<bool>},
                "swamp_temperate_saltwater" => {swamp_temperate_saltwater: Option<bool>},
                "swamp_tropical_freshwater" => {swamp_tropical_freshwater: Option<bool>},
                "swamp_tropical_saltwater" => {swamp_tropical_saltwater: Option<bool>},
                "tundra" => {tundra: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureBiome2, df_st_core::CreatureBiome> for CreatureBiome2 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::CreatureBiome) {
        self.cr_id.add_missing_data(&source.cr_id);
        self.ocean_arctic.add_missing_data(&source.ocean_arctic);
        self.ocean_temperate.add_missing_data(&source.ocean_temperate);
        self.ocean_tropical.add_missing_data(&source.ocean_tropical);
        self.pool_temperate_brackishwater.add_missing_data(&source.pool_temperate_brackishwater);
        self.pool_temperate_freshwater.add_missing_data(&source.pool_temperate_freshwater);
        self.pool_temperate_saltwater.add_missing_data(&source.pool_temperate_saltwater);
        self.pool_tropical_brackishwater.add_missing_data(&source.pool_tropical_brackishwater);
        self.pool_tropical_freshwater.add_missing_data(&source.pool_tropical_freshwater);
        self.pool_tropical_saltwater.add_missing_data(&source.pool_tropical_saltwater);
        self.river_temperate_brackishwater.add_missing_data(&source.river_temperate_brackishwater);
        self.river_temperate_freshwater.add_missing_data(&source.river_temperate_freshwater);
        self.river_temperate_saltwater.add_missing_data(&source.river_temperate_saltwater);
        self.river_tropical_brackishwater.add_missing_data(&source.river_tropical_brackishwater);
        self.river_tropical_freshwater.add_missing_data(&source.river_tropical_freshwater);
        self.river_tropical_saltwater.add_missing_data(&source.river_tropical_saltwater);
        self.savanna_temperate.add_missing_data(&source.savanna_temperate);
        self.savanna_tropical.add_missing_data(&source.savanna_tropical);
        self.shrubland_temperate.add_missing_data(&source.shrubland_temperate);
        self.shrubland_tropical.add_missing_data(&source.shrubland_tropical);
        self.subterranean_chasm.add_missing_data(&source.subterranean_chasm);
        self.subterranean_lava.add_missing_data(&source.subterranean_lava);
        self.subterranean_water.add_missing_data(&source.subterranean_water);
        self.swamp_mangrove.add_missing_data(&source.swamp_mangrove);
        self.swamp_temperate_freshwater.add_missing_data(&source.swamp_temperate_freshwater);
        self.swamp_temperate_saltwater.add_missing_data(&source.swamp_temperate_saltwater);
        self.swamp_tropical_freshwater.add_missing_data(&source.swamp_tropical_freshwater);
        self.swamp_tropical_saltwater.add_missing_data(&source.swamp_tropical_saltwater);
        self.tundra.add_missing_data(&source.tundra);
    }
}

/// From DB to Core
impl Filler<df_st_core::CreatureBiome, CreatureBiome2> for df_st_core::CreatureBiome {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &CreatureBiome2) {
        self.cr_id.add_missing_data(&source.cr_id);
        self.ocean_arctic.add_missing_data(&source.ocean_arctic);
        self.ocean_temperate.add_missing_data(&source.ocean_temperate);
        self.ocean_tropical.add_missing_data(&source.ocean_tropical);
        self.pool_temperate_brackishwater.add_missing_data(&source.pool_temperate_brackishwater);
        self.pool_temperate_freshwater.add_missing_data(&source.pool_temperate_freshwater);
        self.pool_temperate_saltwater.add_missing_data(&source.pool_temperate_saltwater);
        self.pool_tropical_brackishwater.add_missing_data(&source.pool_tropical_brackishwater);
        self.pool_tropical_freshwater.add_missing_data(&source.pool_tropical_freshwater);
        self.pool_tropical_saltwater.add_missing_data(&source.pool_tropical_saltwater);
        self.river_temperate_brackishwater.add_missing_data(&source.river_temperate_brackishwater);
        self.river_temperate_freshwater.add_missing_data(&source.river_temperate_freshwater);
        self.river_temperate_saltwater.add_missing_data(&source.river_temperate_saltwater);
        self.river_tropical_brackishwater.add_missing_data(&source.river_tropical_brackishwater);
        self.river_tropical_freshwater.add_missing_data(&source.river_tropical_freshwater);
        self.river_tropical_saltwater.add_missing_data(&source.river_tropical_saltwater);
        self.savanna_temperate.add_missing_data(&source.savanna_temperate);
        self.savanna_tropical.add_missing_data(&source.savanna_tropical);
        self.shrubland_temperate.add_missing_data(&source.shrubland_temperate);
        self.shrubland_tropical.add_missing_data(&source.shrubland_tropical);
        self.subterranean_chasm.add_missing_data(&source.subterranean_chasm);
        self.subterranean_lava.add_missing_data(&source.subterranean_lava);
        self.subterranean_water.add_missing_data(&source.subterranean_water);
        self.swamp_mangrove.add_missing_data(&source.swamp_mangrove);
        self.swamp_temperate_freshwater.add_missing_data(&source.swamp_temperate_freshwater);
        self.swamp_temperate_saltwater.add_missing_data(&source.swamp_temperate_saltwater);
        self.swamp_tropical_freshwater.add_missing_data(&source.swamp_tropical_freshwater);
        self.swamp_tropical_saltwater.add_missing_data(&source.swamp_tropical_saltwater);
        self.tundra.add_missing_data(&source.tundra);
    }
}

impl PartialEq for CreatureBiome2 {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureBiome2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureBiome2> for df_st_core::CreatureBiome {
    fn eq(&self, other: &CreatureBiome2) -> bool {
        self.cr_id == other.cr_id
    }
}

impl PartialEq<df_st_core::CreatureBiome> for CreatureBiome2 {
    fn eq(&self, other: &df_st_core::CreatureBiome) -> bool {
        self.cr_id == other.cr_id
    }
}
