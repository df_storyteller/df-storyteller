use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creatures_h_h_1;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creatures_h_h_1"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureHH1 {
    pub cr_id: i32,
    pub world_id: i32,

    pub has_any_benign: Option<bool>,
    pub has_any_can_swim: Option<bool>,
    pub has_any_cannot_breathe_air: Option<bool>,
    pub has_any_cannot_breathe_water: Option<bool>,
    pub has_any_carnivore: Option<bool>,
    pub has_any_common_domestic: Option<bool>,
    pub has_any_curious_beast: Option<bool>,
    pub has_any_demon: Option<bool>,
    pub has_any_feature_beast: Option<bool>,
    pub has_any_flier: Option<bool>,
    pub has_any_fly_race_gait: Option<bool>,
    pub has_any_grasp: Option<bool>,
    pub has_any_grazer: Option<bool>,
    pub has_any_has_blood: Option<bool>,
    pub has_any_immobile: Option<bool>,
    pub has_any_intelligent_learns: Option<bool>,
    pub has_any_intelligent_speaks: Option<bool>,
    pub has_any_large_predator: Option<bool>,
    pub has_any_local_pops_controllable: Option<bool>,
    pub has_any_local_pops_produce_heroes: Option<bool>,
}

impl CreatureHH1 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Creature, CreatureHH1> for CreatureHH1 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creatures_h_h_1: &[CreatureHH1]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creatures_h_h_1::table)
            .values(creatures_h_h_1)
            .on_conflict((creatures_h_h_1::cr_id, creatures_h_h_1::world_id))
            .do_update()
            .set((
                creatures_h_h_1::has_any_benign.eq(excluded(creatures_h_h_1::has_any_benign)),
                creatures_h_h_1::has_any_can_swim.eq(excluded(creatures_h_h_1::has_any_can_swim)),
                creatures_h_h_1::has_any_cannot_breathe_air
                    .eq(excluded(creatures_h_h_1::has_any_cannot_breathe_air)),
                creatures_h_h_1::has_any_cannot_breathe_water
                    .eq(excluded(creatures_h_h_1::has_any_cannot_breathe_water)),
                creatures_h_h_1::has_any_carnivore.eq(excluded(creatures_h_h_1::has_any_carnivore)),
                creatures_h_h_1::has_any_common_domestic
                    .eq(excluded(creatures_h_h_1::has_any_common_domestic)),
                creatures_h_h_1::has_any_curious_beast
                    .eq(excluded(creatures_h_h_1::has_any_curious_beast)),
                creatures_h_h_1::has_any_demon.eq(excluded(creatures_h_h_1::has_any_demon)),
                creatures_h_h_1::has_any_feature_beast
                    .eq(excluded(creatures_h_h_1::has_any_feature_beast)),
                creatures_h_h_1::has_any_flier.eq(excluded(creatures_h_h_1::has_any_flier)),
                creatures_h_h_1::has_any_fly_race_gait
                    .eq(excluded(creatures_h_h_1::has_any_fly_race_gait)),
                creatures_h_h_1::has_any_grasp.eq(excluded(creatures_h_h_1::has_any_grasp)),
                creatures_h_h_1::has_any_grazer.eq(excluded(creatures_h_h_1::has_any_grazer)),
                creatures_h_h_1::has_any_has_blood.eq(excluded(creatures_h_h_1::has_any_has_blood)),
                creatures_h_h_1::has_any_immobile.eq(excluded(creatures_h_h_1::has_any_immobile)),
                creatures_h_h_1::has_any_intelligent_learns
                    .eq(excluded(creatures_h_h_1::has_any_intelligent_learns)),
                creatures_h_h_1::has_any_intelligent_speaks
                    .eq(excluded(creatures_h_h_1::has_any_intelligent_speaks)),
                creatures_h_h_1::has_any_large_predator
                    .eq(excluded(creatures_h_h_1::has_any_large_predator)),
                creatures_h_h_1::has_any_local_pops_controllable
                    .eq(excluded(creatures_h_h_1::has_any_local_pops_controllable)),
                creatures_h_h_1::has_any_local_pops_produce_heroes
                    .eq(excluded(creatures_h_h_1::has_any_local_pops_produce_heroes)),
            ))
            .execute(conn)
            .expect("Error saving creatures_h_h_1");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creatures_h_h_1: &[CreatureHH1]) {
        diesel::insert_into(creatures_h_h_1::table)
            .values(creatures_h_h_1)
            .execute(conn)
            .expect("Error saving creatures_h_h_1");
    }

    /// Get a list of CreatureHH1 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<CreatureHH1>, Error> {
        /*
        use crate::schema::creatures_h_h_1::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creatures_h_h_1.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "has_any_benign" => has_any_benign,
                "has_any_can_swim" => has_any_can_swim,
                "has_any_cannot_breathe_air" => has_any_cannot_breathe_air,
                "has_any_cannot_breathe_water" => has_any_cannot_breathe_water,
                "has_any_carnivore" => has_any_carnivore,
                "has_any_common_domestic" => has_any_common_domestic,
                "has_any_curious_beast" => has_any_curious_beast,
                "has_any_demon" => has_any_demon,
                "has_any_feature_beast" => has_any_feature_beast,
                "has_any_flier" => has_any_flier,
                "has_any_fly_race_gait" => has_any_fly_race_gait,
                "has_any_grasp" => has_any_grasp,
                "has_any_grazer" => has_any_grazer,
                "has_any_has_blood" => has_any_has_blood,
                "has_any_immobile" => has_any_immobile,
                "has_any_intelligent_learns" => has_any_intelligent_learns,
                "has_any_intelligent_speaks" => has_any_intelligent_speaks,
                "has_any_large_predator" => has_any_large_predator,
                "has_any_local_pops_controllable" => has_any_local_pops_controllable,
                "has_any_local_pops_produce_heroes" => has_any_local_pops_produce_heroes,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureHH1>, Error> {
        use crate::schema::creatures_h_h_1::dsl::*;
        let query = creatures_h_h_1;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        Ok(query.first::<CreatureHH1>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "cr_id",
            "has_any_benign",
            "has_any_can_swim",
            "has_any_cannot_breathe_air",
            "has_any_cannot_breathe_water",
            "has_any_carnivore",
            "has_any_common_domestic",
            "has_any_curious_beast",
            "has_any_demon",
            "has_any_feature_beast",
            "has_any_flier",
            "has_any_fly_race_gait",
            "has_any_grasp",
            "has_any_grazer",
            "has_any_has_blood",
            "has_any_immobile",
            "has_any_intelligent_learns",
            "has_any_intelligent_speaks",
            "has_any_large_predator",
            "has_any_local_pops_controllable",
            "has_any_local_pops_produce_heroes",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureHH1],
        core_list: Vec<df_st_core::Creature>,
    ) -> Result<Vec<df_st_core::Creature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creatures_h_h_1::dsl::*;
        let query = creatures_h_h_1.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "has_any_benign" => {has_any_benign: Option<bool>},
                "has_any_can_swim" => {has_any_can_swim: Option<bool>},
                "has_any_cannot_breathe_air" => {has_any_cannot_breathe_air: Option<bool>},
                "has_any_cannot_breathe_water" => {has_any_cannot_breathe_water: Option<bool>},
                "has_any_carnivore" => {has_any_carnivore: Option<bool>},
                "has_any_common_domestic" => {has_any_common_domestic: Option<bool>},
                "has_any_curious_beast" => {has_any_curious_beast: Option<bool>},
                "has_any_demon" => {has_any_demon: Option<bool>},
                "has_any_feature_beast" => {has_any_feature_beast: Option<bool>},
                "has_any_flier" => {has_any_flier: Option<bool>},
                "has_any_fly_race_gait" => {has_any_fly_race_gait: Option<bool>},
                "has_any_grasp" => {has_any_grasp: Option<bool>},
                "has_any_grazer" => {has_any_grazer: Option<bool>},
                "has_any_has_blood" => {has_any_has_blood: Option<bool>},
                "has_any_immobile" => {has_any_immobile: Option<bool>},
                "has_any_intelligent_learns" => {has_any_intelligent_learns: Option<bool>},
                "has_any_intelligent_speaks" => {has_any_intelligent_speaks: Option<bool>},
                "has_any_large_predator" => {has_any_large_predator: Option<bool>},
                "has_any_local_pops_controllable" => {has_any_local_pops_controllable: Option<bool>},
                "has_any_local_pops_produce_heroes" => {has_any_local_pops_produce_heroes: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureHH1, df_st_core::Creature> for CreatureHH1 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::Creature) {
        self.cr_id.add_missing_data(&source.id);
        self.has_any_benign.add_missing_data(&source.has_any_benign);
        self.has_any_can_swim.add_missing_data(&source.has_any_can_swim);
        self.has_any_cannot_breathe_air.add_missing_data(&source.has_any_cannot_breathe_air);
        self.has_any_cannot_breathe_water.add_missing_data(&source.has_any_cannot_breathe_water);
        self.has_any_carnivore.add_missing_data(&source.has_any_carnivore);
        self.has_any_common_domestic.add_missing_data(&source.has_any_common_domestic);
        self.has_any_curious_beast.add_missing_data(&source.has_any_curious_beast);
        self.has_any_demon.add_missing_data(&source.has_any_demon);
        self.has_any_feature_beast.add_missing_data(&source.has_any_feature_beast);
        self.has_any_flier.add_missing_data(&source.has_any_flier);
        self.has_any_fly_race_gait.add_missing_data(&source.has_any_fly_race_gait);
        self.has_any_grasp.add_missing_data(&source.has_any_grasp);
        self.has_any_grazer.add_missing_data(&source.has_any_grazer);
        self.has_any_has_blood.add_missing_data(&source.has_any_has_blood);
        self.has_any_immobile.add_missing_data(&source.has_any_immobile);
        self.has_any_intelligent_learns.add_missing_data(&source.has_any_intelligent_learns);
        self.has_any_intelligent_speaks.add_missing_data(&source.has_any_intelligent_speaks);
        self.has_any_large_predator.add_missing_data(&source.has_any_large_predator);
        self.has_any_local_pops_controllable.add_missing_data(&source.has_any_local_pops_controllable);
        self.has_any_local_pops_produce_heroes.add_missing_data(&source.has_any_local_pops_produce_heroes);
    }
}

/// From DB to Core
impl Filler<df_st_core::Creature, CreatureHH1> for df_st_core::Creature {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &CreatureHH1) {
        self.id.add_missing_data(&source.cr_id);
        self.has_any_benign.add_missing_data(&source.has_any_benign);
        self.has_any_can_swim.add_missing_data(&source.has_any_can_swim);
        self.has_any_cannot_breathe_air.add_missing_data(&source.has_any_cannot_breathe_air);
        self.has_any_cannot_breathe_water.add_missing_data(&source.has_any_cannot_breathe_water);
        self.has_any_carnivore.add_missing_data(&source.has_any_carnivore);
        self.has_any_common_domestic.add_missing_data(&source.has_any_common_domestic);
        self.has_any_curious_beast.add_missing_data(&source.has_any_curious_beast);
        self.has_any_demon.add_missing_data(&source.has_any_demon);
        self.has_any_feature_beast.add_missing_data(&source.has_any_feature_beast);
        self.has_any_flier.add_missing_data(&source.has_any_flier);
        self.has_any_fly_race_gait.add_missing_data(&source.has_any_fly_race_gait);
        self.has_any_grasp.add_missing_data(&source.has_any_grasp);
        self.has_any_grazer.add_missing_data(&source.has_any_grazer);
        self.has_any_has_blood.add_missing_data(&source.has_any_has_blood);
        self.has_any_immobile.add_missing_data(&source.has_any_immobile);
        self.has_any_intelligent_learns.add_missing_data(&source.has_any_intelligent_learns);
        self.has_any_intelligent_speaks.add_missing_data(&source.has_any_intelligent_speaks);
        self.has_any_large_predator.add_missing_data(&source.has_any_large_predator);
        self.has_any_local_pops_controllable.add_missing_data(&source.has_any_local_pops_controllable);
        self.has_any_local_pops_produce_heroes.add_missing_data(&source.has_any_local_pops_produce_heroes);
    }
}

impl PartialEq for CreatureHH1 {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureHH1 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureHH1> for df_st_core::Creature {
    fn eq(&self, other: &CreatureHH1) -> bool {
        self.id == other.cr_id
    }
}

impl PartialEq<df_st_core::Creature> for CreatureHH1 {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.cr_id == other.id
    }
}
