use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Coordinate, DBDFWorld};
use crate::schema::underground_regions;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;
use std::convert::TryInto;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "underground_regions"]
pub struct UndergroundRegion {
    pub id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub depth: Option<i32>,
}

impl UndergroundRegion {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::UndergroundRegion, UndergroundRegion> for UndergroundRegion {
    /// Add Coordinates to the world that are part of UndergroundRegion
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for ug_region in core_world.underground_regions.values() {
            for coord in &ug_region.coords {
                let new_id: i32 = world.coordinates.len().try_into().unwrap();
                world.coordinates.push(Coordinate {
                    id: new_id,
                    x: coord.x,
                    y: coord.y,
                    underground_region_id: Some(ug_region.id),
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, ug_regions: &[UndergroundRegion]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(underground_regions::table)
            .values(ug_regions)
            .on_conflict((underground_regions::id, underground_regions::world_id))
            .do_update()
            .set((
                underground_regions::type_.eq(excluded(underground_regions::type_)),
                underground_regions::depth.eq(excluded(underground_regions::depth)),
            ))
            .execute(conn)
            .expect("Error saving underground_regions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, ug_regions: &[UndergroundRegion]) {
        diesel::insert_into(underground_regions::table)
            .values(ug_regions)
            .execute(conn)
            .expect("Error saving underground_regions");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<UndergroundRegion>, Error> {
        use crate::schema::underground_regions::dsl::*;
        let query = underground_regions;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<UndergroundRegion>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<UndergroundRegion>, Error> {
        use crate::schema::underground_regions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = underground_regions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "depth" => depth,
            ],
            string_filter,
            [
                "type" => type_,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "type" => type_,
                "depth" => depth,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id", "depth"],
            _ => vec!["id", "type", "depth"],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[UndergroundRegion],
        _core_list: Vec<df_st_core::UndergroundRegion>,
    ) -> Result<Vec<df_st_core::UndergroundRegion>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add Coordinates
        let coord_list = Coordinate::belonging_to(db_list)
            .filter(crate::schema::coordinates::world_id.eq(world_id))
            .load::<Coordinate>(conn)?
            .grouped_by(db_list);
        let core_list2 = db_list
            .iter()
            .zip(coord_list)
            .map(|(ug_region, coord)| {
                let mut core_ug_region = df_st_core::UndergroundRegion::default();
                core_ug_region.add_missing_data(ug_region);
                let mut core_coords: Vec<df_st_core::Coordinate> = Vec::new();
                core_coords.add_missing_data(&coord);
                core_ug_region.add_missing_data(&df_st_core::UndergroundRegion {
                    id: core_ug_region.id,
                    coords: core_coords,
                    ..Default::default()
                });
                core_ug_region
            })
            .collect();
        // There is currently no data created before this
        // function that is not in this object.
        // So just swapping it saves a bit of time.
        // core_list.add_missing_data(&core_list2);
        Ok(core_list2)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::underground_regions::dsl::*;
        let query = underground_regions
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "depth" => depth,
            ],
            string_filter,
            [
                "type" => type_,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "type" => {type_: Option<String>},
                "depth" => {depth: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<UndergroundRegion, df_st_core::UndergroundRegion> for UndergroundRegion {
    fn add_missing_data(&mut self, source: &df_st_core::UndergroundRegion) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.depth.add_missing_data(&source.depth);
    }
}

/// From DB to Core
impl Filler<df_st_core::UndergroundRegion, UndergroundRegion> for df_st_core::UndergroundRegion {
    fn add_missing_data(&mut self, source: &UndergroundRegion) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.depth.add_missing_data(&source.depth);
    }
}

impl PartialEq<UndergroundRegion> for df_st_core::UndergroundRegion {
    fn eq(&self, other: &UndergroundRegion) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::UndergroundRegion> for UndergroundRegion {
    fn eq(&self, other: &df_st_core::UndergroundRegion) -> bool {
        self.id == other.id
    }
}
