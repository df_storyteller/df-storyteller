use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_s_s1;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_s_s1"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventSS1 {
    pub he_id: i32,
    pub world_id: i32,

    pub saboteur_hf_id: Option<i32>,
    pub sanctify_hf_id: Option<i32>,
    pub schedule_id: Option<i32>,
    pub searcher_civ_id: Option<i32>,
    pub season: Option<String>,
    pub secret_goal: Option<String>,
    pub seeker_hf_id: Option<i32>,
    pub seller_hf_id: Option<i32>,
    pub shrine_amount_destroyed: Option<i32>,
    pub site_civ_id: Option<i32>,
    pub site_entity_id: Option<i32>,
    pub site_hf_id: Option<i32>,
    pub site_id: Option<i32>,
    pub site_id_1: Option<i32>,
    pub site_id_2: Option<i32>,
    pub site_property_id: Option<i32>,
    pub situation: Option<String>,
    pub skill_at_time: Option<i32>,
    pub slayer_caste: Option<i32>,
    pub slayer_hf_id: Option<i32>,
    pub slayer_item_id: Option<i32>,
    pub slayer_race: Option<String>,
    pub slayer_shooter_item_id: Option<i32>,
    pub snatcher_hf_id: Option<i32>,
    pub source_entity_id: Option<i32>,
    pub source_hf_id: Option<i32>,
    pub source_site_id: Option<i32>,
    pub source_structure_id: Option<i32>,
    pub stash_site_id: Option<i32>,
    pub speaker_hf_id: Option<i32>,
}

impl HistoricalEventSS1 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventSS1> for HistoricalEventSS1 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_s_s1: &[HistoricalEventSS1]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_s_s1::table)
            .values(historical_events_s_s1)
            .on_conflict((
                historical_events_s_s1::he_id,
                historical_events_s_s1::world_id,
            ))
            .do_update()
            .set((
                historical_events_s_s1::saboteur_hf_id.eq(excluded(historical_events_s_s1::saboteur_hf_id)),
                historical_events_s_s1::sanctify_hf_id.eq(excluded(historical_events_s_s1::sanctify_hf_id)),
                historical_events_s_s1::schedule_id.eq(excluded(historical_events_s_s1::schedule_id)),
                historical_events_s_s1::searcher_civ_id.eq(excluded(historical_events_s_s1::searcher_civ_id)),
                historical_events_s_s1::season.eq(excluded(historical_events_s_s1::season)),
                historical_events_s_s1::secret_goal.eq(excluded(historical_events_s_s1::secret_goal)),
                historical_events_s_s1::seeker_hf_id.eq(excluded(historical_events_s_s1::seeker_hf_id)),
                historical_events_s_s1::seller_hf_id.eq(excluded(historical_events_s_s1::seller_hf_id)),
                historical_events_s_s1::shrine_amount_destroyed.eq(excluded(historical_events_s_s1::shrine_amount_destroyed)),
                historical_events_s_s1::site_civ_id.eq(excluded(historical_events_s_s1::site_civ_id)),
                historical_events_s_s1::site_entity_id.eq(excluded(historical_events_s_s1::site_entity_id)),
                historical_events_s_s1::site_hf_id.eq(excluded(historical_events_s_s1::site_hf_id)),
                historical_events_s_s1::site_id.eq(excluded(historical_events_s_s1::site_id)),
                historical_events_s_s1::site_id_1.eq(excluded(historical_events_s_s1::site_id_1)),
                historical_events_s_s1::site_id_2.eq(excluded(historical_events_s_s1::site_id_2)),
                historical_events_s_s1::site_property_id.eq(excluded(historical_events_s_s1::site_property_id)),
                historical_events_s_s1::situation.eq(excluded(historical_events_s_s1::situation)),
                historical_events_s_s1::skill_at_time.eq(excluded(historical_events_s_s1::skill_at_time)),
                historical_events_s_s1::slayer_caste.eq(excluded(historical_events_s_s1::slayer_caste)),
                historical_events_s_s1::slayer_hf_id.eq(excluded(historical_events_s_s1::slayer_hf_id)),
                historical_events_s_s1::slayer_item_id.eq(excluded(historical_events_s_s1::slayer_item_id)),
                historical_events_s_s1::slayer_race.eq(excluded(historical_events_s_s1::slayer_race)),
                historical_events_s_s1::slayer_shooter_item_id.eq(excluded(historical_events_s_s1::slayer_shooter_item_id)),
                historical_events_s_s1::snatcher_hf_id.eq(excluded(historical_events_s_s1::snatcher_hf_id)),
                historical_events_s_s1::source_entity_id.eq(excluded(historical_events_s_s1::source_entity_id)),
                historical_events_s_s1::source_hf_id.eq(excluded(historical_events_s_s1::source_hf_id)),
                historical_events_s_s1::source_site_id.eq(excluded(historical_events_s_s1::source_site_id)),
                historical_events_s_s1::source_structure_id.eq(excluded(historical_events_s_s1::source_structure_id)),
                historical_events_s_s1::stash_site_id.eq(excluded(historical_events_s_s1::stash_site_id)),
                historical_events_s_s1::speaker_hf_id.eq(excluded(historical_events_s_s1::speaker_hf_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_s_s1");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_s_s1: &[HistoricalEventSS1]) {
        diesel::insert_into(historical_events_s_s1::table)
            .values(historical_events_s_s1)
            .execute(conn)
            .expect("Error saving historical_events_s_s1");
    }

    /// Get a list of HistoricalEventSS1 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventSS1>, Error> {
        /*
        use crate::schema::historical_events_s_s1::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_s_s1.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "saboteur_hf_id" => saboteur_hf_id,
                "sanctify_hf_id" => sanctify_hf_id,
                "schedule_id" => schedule_id,
                "searcher_civ_id" => searcher_civ_id,
                "season" => season,
                "secret_goal" => secret_goal,
                "seeker_hf_id" => seeker_hf_id,
                "seller_hf_id" => seller_hf_id,
                "shrine_amount_destroyed" => shrine_amount_destroyed,
                "site_civ_id" => site_civ_id,
                "site_entity_id" => site_entity_id,
                "site_hf_id" => site_hf_id,
                "site_id" => site_id,
                "site_id_1" => site_id_1,
                "site_id_2" => site_id_2,
                "site_property_id" => site_property_id,
                "situation" => situation,
                "skill_at_time" => skill_at_time,
                "slayer_caste" => slayer_caste,
                "slayer_hf_id" => slayer_hf_id,
                "slayer_item_id" => slayer_item_id,
                "slayer_race" => slayer_race,
                "slayer_shooter_item_id" => slayer_shooter_item_id,
                "snatcher_hf_id" => snatcher_hf_id,
                "source_entity_id" => source_entity_id,
                "source_hf_id" => source_hf_id,
                "source_site_id" => source_site_id,
                "source_structure_id" => source_structure_id,
                "stash_site_id" => stash_site_id,
                "speaker_hf_id" => speaker_hf_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventSS1>, Error> {
        use crate::schema::historical_events_s_s1::dsl::*;
        let query = historical_events_s_s1;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventSS1>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "saboteur_hf_id",
            "sanctify_hf_id",
            "schedule_id",
            "searcher_civ_id",
            "season",
            "secret_goal",
            "seeker_hf_id",
            "seller_hf_id",
            "shrine_amount_destroyed",
            "site_civ_id",
            "site_entity_id",
            "site_hf_id",
            "site_id",
            "site_id_1",
            "site_id_2",
            "site_property_id",
            "situation",
            "skill_at_time",
            "slayer_caste",
            "slayer_hf_id",
            "slayer_item_id",
            "slayer_race",
            "slayer_shooter_item_id",
            "snatcher_hf_id",
            "source_entity_id",
            "source_hf_id",
            "source_site_id",
            "source_structure_id",
            "stash_site_id",
            "speaker_hf_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventSS1],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_s_s1::dsl::*;
        let query = historical_events_s_s1.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "saboteur_hf_id" => {saboteur_hf_id: Option<i32>},
                "sanctify_hf_id" => {sanctify_hf_id: Option<i32>},
                "schedule_id" => {schedule_id: Option<i32>},
                "searcher_civ_id" => {searcher_civ_id: Option<i32>},
                "season" => {season: Option<String>},
                "secret_goal" => {secret_goal: Option<String>},
                "seeker_hf_id" => {seeker_hf_id: Option<i32>},
                "seller_hf_id" => {seller_hf_id: Option<i32>},
                "shrine_amount_destroyed" => {shrine_amount_destroyed: Option<i32>},
                "site_civ_id" => {site_civ_id: Option<i32>},
                "site_entity_id" => {site_entity_id: Option<i32>},
                "site_hf_id" => {site_hf_id: Option<i32>},
                "site_id" => {site_id: Option<i32>},
                "site_id_1" => {site_id_1: Option<i32>},
                "site_id_2" => {site_id_2: Option<i32>},
                "site_property_id" => {site_property_id: Option<i32>},
                "situation" => {situation: Option<String>},
                "skill_at_time" => {skill_at_time: Option<i32>},
                "slayer_caste" => {slayer_caste: Option<i32>},
                "slayer_hf_id" => {slayer_hf_id: Option<i32>},
                "slayer_item_id" => {slayer_item_id: Option<i32>},
                "slayer_race" => {slayer_race: Option<String>},
                "slayer_shooter_item_id" => {slayer_shooter_item_id: Option<i32>},
                "snatcher_hf_id" => {snatcher_hf_id: Option<i32>},
                "source_entity_id" => {source_entity_id: Option<i32>},
                "source_hf_id" => {source_hf_id: Option<i32>},
                "source_site_id" => {source_site_id: Option<i32>},
                "source_structure_id" => {source_structure_id: Option<i32>},
                "stash_site_id" => {stash_site_id: Option<i32>},
                "speaker_hf_id" => {speaker_hf_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventSS1, df_st_core::HistoricalEvent> for HistoricalEventSS1 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.saboteur_hf_id.add_missing_data(&source.saboteur_hf_id);
        self.sanctify_hf_id.add_missing_data(&source.sanctify_hf_id);
        self.schedule_id.add_missing_data(&source.schedule_id);
        self.searcher_civ_id.add_missing_data(&source.searcher_civ_id);
        self.season.add_missing_data(&source.season);
        self.secret_goal.add_missing_data(&source.secret_goal);
        self.seeker_hf_id.add_missing_data(&source.seeker_hf_id);
        self.seller_hf_id.add_missing_data(&source.seller_hf_id);
        self.shrine_amount_destroyed.add_missing_data(&source.shrine_amount_destroyed);
        self.site_civ_id.add_missing_data(&source.site_civ_id);
        self.site_entity_id.add_missing_data(&source.site_entity_id);
        self.site_hf_id.add_missing_data(&source.site_hf_id);
        self.site_id.add_missing_data(&source.site_id);
        self.site_id_1.add_missing_data(&source.site_id_1);
        self.site_id_2.add_missing_data(&source.site_id_2);
        self.site_property_id.add_missing_data(&source.site_property_id);
        self.situation.add_missing_data(&source.situation);
        self.skill_at_time.add_missing_data(&source.skill_at_time);
        self.slayer_caste.add_missing_data(&source.slayer_caste);
        self.slayer_hf_id.add_missing_data(&source.slayer_hf_id);
        self.slayer_item_id.add_missing_data(&source.slayer_item_id);
        self.slayer_race.add_missing_data(&source.slayer_race);
        self.slayer_shooter_item_id.add_missing_data(&source.slayer_shooter_item_id);
        self.snatcher_hf_id.add_missing_data(&source.snatcher_hf_id);
        self.source_entity_id.add_missing_data(&source.source_entity_id);
        self.source_hf_id.add_missing_data(&source.source_hf_id);
        self.source_site_id.add_missing_data(&source.source_site_id);
        self.source_structure_id.add_missing_data(&source.source_structure_id);
        self.stash_site_id.add_missing_data(&source.stash_site_id);
        self.speaker_hf_id.add_missing_data(&source.speaker_hf_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventSS1> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventSS1) {
        self.id.add_missing_data(&source.he_id);
        self.saboteur_hf_id.add_missing_data(&source.saboteur_hf_id);
        self.sanctify_hf_id.add_missing_data(&source.sanctify_hf_id);
        self.schedule_id.add_missing_data(&source.schedule_id);
        self.searcher_civ_id.add_missing_data(&source.searcher_civ_id);
        self.season.add_missing_data(&source.season);
        self.secret_goal.add_missing_data(&source.secret_goal);
        self.seeker_hf_id.add_missing_data(&source.seeker_hf_id);
        self.seller_hf_id.add_missing_data(&source.seller_hf_id);
        self.shrine_amount_destroyed.add_missing_data(&source.shrine_amount_destroyed);
        self.site_civ_id.add_missing_data(&source.site_civ_id);
        self.site_entity_id.add_missing_data(&source.site_entity_id);
        self.site_hf_id.add_missing_data(&source.site_hf_id);
        self.site_id.add_missing_data(&source.site_id);
        self.site_id_1.add_missing_data(&source.site_id_1);
        self.site_id_2.add_missing_data(&source.site_id_2);
        self.site_property_id.add_missing_data(&source.site_property_id);
        self.situation.add_missing_data(&source.situation);
        self.skill_at_time.add_missing_data(&source.skill_at_time);
        self.slayer_caste.add_missing_data(&source.slayer_caste);
        self.slayer_hf_id.add_missing_data(&source.slayer_hf_id);
        self.slayer_item_id.add_missing_data(&source.slayer_item_id);
        self.slayer_race.add_missing_data(&source.slayer_race);
        self.slayer_shooter_item_id.add_missing_data(&source.slayer_shooter_item_id);
        self.snatcher_hf_id.add_missing_data(&source.snatcher_hf_id);
        self.source_entity_id.add_missing_data(&source.source_entity_id);
        self.source_hf_id.add_missing_data(&source.source_hf_id);
        self.source_site_id.add_missing_data(&source.source_site_id);
        self.source_structure_id.add_missing_data(&source.source_structure_id);
        self.stash_site_id.add_missing_data(&source.stash_site_id);
        self.speaker_hf_id.add_missing_data(&source.speaker_hf_id);
    }
}

impl PartialEq for HistoricalEventSS1 {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventSS1 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventSS1> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventSS1) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventSS1 {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
