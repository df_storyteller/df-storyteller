use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_circumstances;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_circumstances"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HECircumstance {
    pub he_id: i32,
    pub world_id: i32,

    pub type_: Option<String>,
    pub death: Option<i32>,
    pub prayer: Option<i32>,
    pub dream_about: Option<i32>,
    pub defeated: Option<i32>,
    pub murdered: Option<i32>,
    pub hec_id: Option<i32>,
}

impl HECircumstance {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HECircumstance, HECircumstance> for HECircumstance {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_circumstances: &[HECircumstance]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(he_circumstances::table)
            .values(he_circumstances)
            .on_conflict((he_circumstances::he_id, he_circumstances::world_id))
            .do_update()
            .set((
                he_circumstances::type_.eq(excluded(he_circumstances::type_)),
                he_circumstances::death.eq(excluded(he_circumstances::death)),
                he_circumstances::prayer.eq(excluded(he_circumstances::prayer)),
                he_circumstances::dream_about.eq(excluded(he_circumstances::dream_about)),
                he_circumstances::defeated.eq(excluded(he_circumstances::defeated)),
                he_circumstances::murdered.eq(excluded(he_circumstances::murdered)),
                he_circumstances::hec_id.eq(excluded(he_circumstances::hec_id)),
            ))
            .execute(conn)
            .expect("Error saving he_circumstances");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_circumstances: &[HECircumstance]) {
        diesel::insert_into(he_circumstances::table)
            .values(he_circumstances)
            .execute(conn)
            .expect("Error saving he_circumstances");
    }

    /// Get a list of HECircumstance from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HECircumstance>, Error> {
        use crate::schema::he_circumstances::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_circumstances.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "type" => type_,
                "death" => death,
                "prayer" => prayer,
                "dream_about" => dream_about,
                "defeated" => defeated,
                "murdered" => murdered,
                "hec_id" => hec_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECircumstance>, Error> {
        use crate::schema::he_circumstances::dsl::*;
        let query = he_circumstances;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HECircumstance>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "type",
            "death",
            "prayer",
            "dream_about",
            "defeated",
            "murdered",
            "hec_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECircumstance],
        core_list: Vec<df_st_core::HECircumstance>,
    ) -> Result<Vec<df_st_core::HECircumstance>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_circumstances::dsl::*;
        let query = he_circumstances.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "type" => {type_: Option<String>},
                "death" => {death: Option<i32>},
                "prayer" => {prayer: Option<i32>},
                "dream_about" => {dream_about: Option<i32>},
                "defeated" => {defeated: Option<i32>},
                "murdered" => {murdered: Option<i32>},
                "hec_id" => {hec_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HECircumstance, df_st_core::HECircumstance> for HECircumstance {
    fn add_missing_data(&mut self, source: &df_st_core::HECircumstance) {
        self.type_.add_missing_data(&source.type_);
        self.death.add_missing_data(&source.death);
        self.prayer.add_missing_data(&source.prayer);
        self.dream_about.add_missing_data(&source.dream_about);
        self.defeated.add_missing_data(&source.defeated);
        self.murdered.add_missing_data(&source.murdered);
        self.hec_id.add_missing_data(&source.hec_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HECircumstance, HECircumstance> for df_st_core::HECircumstance {
    fn add_missing_data(&mut self, source: &HECircumstance) {
        self.type_.add_missing_data(&source.type_);
        self.death.add_missing_data(&source.death);
        self.prayer.add_missing_data(&source.prayer);
        self.dream_about.add_missing_data(&source.dream_about);
        self.defeated.add_missing_data(&source.defeated);
        self.murdered.add_missing_data(&source.murdered);
        self.hec_id.add_missing_data(&source.hec_id);
    }
}

impl Filler<df_st_core::HistoricalEvent, HECircumstance> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HECircumstance) {
        self.circumstance_obj.add_missing_data(&Some(source.clone()));
    }
}

impl PartialEq for HECircumstance {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HECircumstance {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HECircumstance> for df_st_core::HECircumstance {
    fn eq(&self, other: &HECircumstance) -> bool {
        self.type_ == other.type_
    }
}

impl PartialEq<df_st_core::HECircumstance> for HECircumstance {
    fn eq(&self, other: &df_st_core::HECircumstance) -> bool {
        self.type_ == other.type_
    }
}
