use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_j_m;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_j_m"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventJM {
    pub he_id: i32,
    pub world_id: i32,

    pub join_entity_id: Option<i32>,
    pub joined_entity_id: Option<i32>,
    pub joiner_entity_id: Option<i32>,

    pub knowledge: Option<String>,
    pub last_owner_hf_id: Option<i32>,
    pub law_add: Option<String>,
    pub law_remove: Option<String>,
    pub leader_hf_id: Option<i32>,
    pub leaver_civ_id: Option<i32>,
    pub link: Option<String>,
    pub lost_value: Option<bool>,
    pub lure_hf_id: Option<i32>,
    pub link_type: Option<String>,

    pub master_wc_id: Option<i32>,
    pub method: Option<String>,
    pub modification: Option<String>,
    pub modifier_hf_id: Option<i32>,
    pub mood: Option<String>,
    pub moved_to_site_id: Option<i32>,
    pub maker_hf_id: Option<i32>,
    pub maker_en_id: Option<i32>,
    pub mat: Option<String>,
    pub mat_type: Option<i32>,
    pub mat_index: Option<i32>,
}

impl HistoricalEventJM {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventJM> for HistoricalEventJM {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_j_m: &[HistoricalEventJM]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_j_m::table)
            .values(historical_events_j_m)
            .on_conflict((
                historical_events_j_m::he_id,
                historical_events_j_m::world_id,
            ))
            .do_update()
            .set((
                historical_events_j_m::join_entity_id.eq(excluded(historical_events_j_m::join_entity_id)),
                historical_events_j_m::joined_entity_id.eq(excluded(historical_events_j_m::joined_entity_id)),
                historical_events_j_m::joiner_entity_id.eq(excluded(historical_events_j_m::joiner_entity_id)),
                historical_events_j_m::knowledge.eq(excluded(historical_events_j_m::knowledge)),
                historical_events_j_m::last_owner_hf_id.eq(excluded(historical_events_j_m::last_owner_hf_id)),
                historical_events_j_m::law_add.eq(excluded(historical_events_j_m::law_add)),
                historical_events_j_m::law_remove.eq(excluded(historical_events_j_m::law_remove)),
                historical_events_j_m::leader_hf_id.eq(excluded(historical_events_j_m::leader_hf_id)),
                historical_events_j_m::leaver_civ_id.eq(excluded(historical_events_j_m::leaver_civ_id)),
                historical_events_j_m::link.eq(excluded(historical_events_j_m::link)),
                historical_events_j_m::lost_value.eq(excluded(historical_events_j_m::lost_value)),
                historical_events_j_m::lure_hf_id.eq(excluded(historical_events_j_m::lure_hf_id)),
                historical_events_j_m::link_type.eq(excluded(historical_events_j_m::link_type)),
                historical_events_j_m::master_wc_id.eq(excluded(historical_events_j_m::master_wc_id)),
                historical_events_j_m::method.eq(excluded(historical_events_j_m::method)),
                historical_events_j_m::modification.eq(excluded(historical_events_j_m::modification)),
                historical_events_j_m::modifier_hf_id.eq(excluded(historical_events_j_m::modifier_hf_id)),
                historical_events_j_m::mood.eq(excluded(historical_events_j_m::mood)),
                historical_events_j_m::moved_to_site_id.eq(excluded(historical_events_j_m::moved_to_site_id)),
                historical_events_j_m::maker_hf_id.eq(excluded(historical_events_j_m::maker_hf_id)),
                historical_events_j_m::maker_en_id.eq(excluded(historical_events_j_m::maker_en_id)),
                historical_events_j_m::mat.eq(excluded(historical_events_j_m::mat)),
                historical_events_j_m::mat_type.eq(excluded(historical_events_j_m::mat_type)),
                historical_events_j_m::mat_index.eq(excluded(historical_events_j_m::mat_index)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_j_m");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_j_m: &[HistoricalEventJM]) {
        diesel::insert_into(historical_events_j_m::table)
            .values(historical_events_j_m)
            .execute(conn)
            .expect("Error saving historical_events_j_m");
    }

    /// Get a list of HistoricalEventJM from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventJM>, Error> {
        /*
        use crate::schema::historical_events_j_m::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_j_m.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "join_entity_id" => join_entity_id,
                "joined_entity_id" => joined_entity_id,
                "joiner_entity_id" => joiner_entity_id,
                "knowledge" => knowledge,
                "last_owner_hf_id" => last_owner_hf_id,
                "law_add" => law_add,
                "law_remove" => law_remove,
                "leader_hf_id" => leader_hf_id,
                "leaver_civ_id" => leaver_civ_id,
                "link" => link,
                "lost_value" => lost_value,
                "lure_hf_id" => lure_hf_id,
                "link_type" => link_type,
                "master_wc_id" => master_wc_id,
                "method" => method,
                "modification" => modification,
                "modifier_hf_id" => modifier_hf_id,
                "mood" => mood,
                "moved_to_site_id" => moved_to_site_id,
                "maker_hf_id" => maker_hf_id,
                "maker_en_id" => maker_en_id,
                "mat" => mat,
                "mat_type" => mat_type,
                "mat_index" => mat_index,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventJM>, Error> {
        use crate::schema::historical_events_j_m::dsl::*;
        let query = historical_events_j_m;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventJM>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "join_entity_id",
            "joined_entity_id",
            "joiner_entity_id",
            "knowledge",
            "last_owner_hf_id",
            "law_add",
            "law_remove",
            "leader_hf_id",
            "leaver_civ_id",
            "link",
            "lost_value",
            "lure_hf_id",
            "link_type",
            "master_wc_id",
            "method",
            "modification",
            "modifier_hf_id",
            "mood",
            "moved_to_site_id",
            "maker_hf_id",
            "maker_en_id",
            "mat",
            "mat_type",
            "mat_index",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventJM],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_j_m::dsl::*;
        let query = historical_events_j_m.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "join_entity_id" => {join_entity_id: Option<i32>},
                "joined_entity_id" => {joined_entity_id: Option<i32>},
                "joiner_entity_id" => {joiner_entity_id: Option<i32>},
                "knowledge" => {knowledge: Option<String>},
                "last_owner_hf_id" => {last_owner_hf_id: Option<i32>},
                "law_add" => {law_add: Option<String>},
                "law_remove" => {law_remove: Option<String>},
                "leader_hf_id" => {leader_hf_id: Option<i32>},
                "leaver_civ_id" => {leaver_civ_id: Option<i32>},
                "link" => {link: Option<String>},
                "lost_value" => {lost_value: Option<bool>},
                "lure_hf_id" => {lure_hf_id: Option<i32>},
                "link_type" => {link_type: Option<String>},
                "master_wc_id" => {master_wc_id: Option<i32>},
                "method" => {method: Option<String>},
                "modification" => {modification: Option<String>},
                "modifier_hf_id" => {modifier_hf_id: Option<i32>},
                "mood" => {mood: Option<String>},
                "moved_to_site_id" => {moved_to_site_id: Option<i32>},
                "maker_hf_id" => {maker_hf_id: Option<i32>},
                "maker_en_id" => {maker_en_id: Option<i32>},
                "mat" => {mat: Option<String>},
                "mat_type" => {mat_type: Option<i32>},
                "mat_index" => {mat_index: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventJM, df_st_core::HistoricalEvent> for HistoricalEventJM {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.join_entity_id.add_missing_data(&source.join_entity_id);
        self.joined_entity_id.add_missing_data(&source.joined_entity_id);
        self.joiner_entity_id.add_missing_data(&source.joiner_entity_id);
        self.knowledge.add_missing_data(&source.knowledge);
        self.last_owner_hf_id.add_missing_data(&source.last_owner_hf_id);
        self.law_add.add_missing_data(&source.law_add);
        self.law_remove.add_missing_data(&source.law_remove);
        self.leader_hf_id.add_missing_data(&source.leader_hf_id);
        self.leaver_civ_id.add_missing_data(&source.leaver_civ_id);
        self.link.add_missing_data(&source.link);
        self.lost_value.add_missing_data(&source.lost_value);
        self.lure_hf_id.add_missing_data(&source.lure_hf_id);
        self.link_type.add_missing_data(&source.link_type);
        self.master_wc_id.add_missing_data(&source.master_wc_id);
        self.method.add_missing_data(&source.method);
        self.modification.add_missing_data(&source.modification);
        self.modifier_hf_id.add_missing_data(&source.modifier_hf_id);
        self.mood.add_missing_data(&source.mood);
        self.moved_to_site_id.add_missing_data(&source.moved_to_site_id);
        self.maker_hf_id.add_missing_data(&source.maker_hf_id);
        self.maker_en_id.add_missing_data(&source.maker_en_id);
        self.mat.add_missing_data(&source.mat);
        self.mat_type.add_missing_data(&source.mat_type);
        self.mat_index.add_missing_data(&source.mat_index);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventJM> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventJM) {
        self.id.add_missing_data(&source.he_id);
        self.join_entity_id.add_missing_data(&source.join_entity_id);
        self.joined_entity_id.add_missing_data(&source.joined_entity_id);
        self.joiner_entity_id.add_missing_data(&source.joiner_entity_id);
        self.knowledge.add_missing_data(&source.knowledge);
        self.last_owner_hf_id.add_missing_data(&source.last_owner_hf_id);
        self.law_add.add_missing_data(&source.law_add);
        self.law_remove.add_missing_data(&source.law_remove);
        self.leader_hf_id.add_missing_data(&source.leader_hf_id);
        self.leaver_civ_id.add_missing_data(&source.leaver_civ_id);
        self.link.add_missing_data(&source.link);
        self.lost_value.add_missing_data(&source.lost_value);
        self.lure_hf_id.add_missing_data(&source.lure_hf_id);
        self.link_type.add_missing_data(&source.link_type);
        self.master_wc_id.add_missing_data(&source.master_wc_id);
        self.method.add_missing_data(&source.method);
        self.modification.add_missing_data(&source.modification);
        self.modifier_hf_id.add_missing_data(&source.modifier_hf_id);
        self.mood.add_missing_data(&source.mood);
        self.moved_to_site_id.add_missing_data(&source.moved_to_site_id);
        self.maker_hf_id.add_missing_data(&source.maker_hf_id);
        self.maker_en_id.add_missing_data(&source.maker_en_id);
        self.mat.add_missing_data(&source.mat);
        self.mat_type.add_missing_data(&source.mat_type);
        self.mat_index.add_missing_data(&source.mat_index);
    }
}

impl PartialEq for HistoricalEventJM {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventJM {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventJM> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventJM) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventJM {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
