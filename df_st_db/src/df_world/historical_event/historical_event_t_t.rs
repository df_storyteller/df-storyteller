use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_t_t;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_t_t"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventTT {
    pub he_id: i32,
    pub world_id: i32,

    pub target_civ_id: Option<i32>,
    pub target_en_id: Option<i32>,
    pub target_hf_id: Option<i32>,
    pub target_identity: Option<i32>,
    pub target_seen_as: Option<String>,
    pub teacher_hf_id: Option<i32>,
    pub theft_method: Option<String>,
    pub took_items: Option<bool>,
    pub took_livestock: Option<bool>,
    pub top_facet: Option<String>,
    pub top_facet_modifier: Option<i32>,
    pub top_facet_rating: Option<i32>,
    pub top_relationship_factor: Option<String>,
    pub top_relationship_modifier: Option<i32>,
    pub top_relationship_rating: Option<i32>,
    pub top_value: Option<String>,
    pub top_value_modifier: Option<i32>,
    pub top_value_rating: Option<i32>,
    pub topic: Option<String>,
    pub trader_entity_id: Option<i32>,
    pub trader_hf_id: Option<i32>,
    pub trickster_hf_id: Option<i32>,
    pub tree: Option<i32>,
    pub trickster: Option<i32>,
}

impl HistoricalEventTT {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventTT> for HistoricalEventTT {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_t_t: &[HistoricalEventTT]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_t_t::table)
            .values(historical_events_t_t)
            .on_conflict((
                historical_events_t_t::he_id,
                historical_events_t_t::world_id,
            ))
            .do_update()
            .set((
                historical_events_t_t::target_civ_id.eq(excluded(historical_events_t_t::target_civ_id)),
                historical_events_t_t::target_en_id.eq(excluded(historical_events_t_t::target_en_id)),
                historical_events_t_t::target_hf_id.eq(excluded(historical_events_t_t::target_hf_id)),
                historical_events_t_t::target_identity.eq(excluded(historical_events_t_t::target_identity)),
                historical_events_t_t::target_seen_as.eq(excluded(historical_events_t_t::target_seen_as)),
                historical_events_t_t::teacher_hf_id.eq(excluded(historical_events_t_t::teacher_hf_id)),
                historical_events_t_t::theft_method.eq(excluded(historical_events_t_t::theft_method)),
                historical_events_t_t::took_items.eq(excluded(historical_events_t_t::took_items)),
                historical_events_t_t::took_livestock.eq(excluded(historical_events_t_t::took_livestock)),
                historical_events_t_t::top_facet.eq(excluded(historical_events_t_t::top_facet)),
                historical_events_t_t::top_facet_modifier.eq(excluded(historical_events_t_t::top_facet_modifier)),
                historical_events_t_t::top_facet_rating.eq(excluded(historical_events_t_t::top_facet_rating)),
                historical_events_t_t::top_relationship_factor.eq(excluded(historical_events_t_t::top_relationship_factor)),
                historical_events_t_t::top_relationship_modifier.eq(excluded(historical_events_t_t::top_relationship_modifier)),
                historical_events_t_t::top_relationship_rating.eq(excluded(historical_events_t_t::top_relationship_rating)),
                historical_events_t_t::top_value.eq(excluded(historical_events_t_t::top_value)),
                historical_events_t_t::top_value_modifier.eq(excluded(historical_events_t_t::top_value_modifier)),
                historical_events_t_t::top_value_rating.eq(excluded(historical_events_t_t::top_value_rating)),
                historical_events_t_t::topic.eq(excluded(historical_events_t_t::topic)),
                historical_events_t_t::trader_entity_id.eq(excluded(historical_events_t_t::trader_entity_id)),
                historical_events_t_t::trader_hf_id.eq(excluded(historical_events_t_t::trader_hf_id)),
                historical_events_t_t::trickster_hf_id.eq(excluded(historical_events_t_t::trickster_hf_id)),
                historical_events_t_t::tree.eq(excluded(historical_events_t_t::tree)),
                historical_events_t_t::trickster.eq(excluded(historical_events_t_t::trickster)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_t_t");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_t_t: &[HistoricalEventTT]) {
        diesel::insert_into(historical_events_t_t::table)
            .values(historical_events_t_t)
            .execute(conn)
            .expect("Error saving historical_events_t_t");
    }

    /// Get a list of HistoricalEventTT from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventTT>, Error> {
        /*
        use crate::schema::historical_events_t_t::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_t_t.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "target_civ_id" => target_civ_id,
                "target_en_id" => target_en_id,
                "target_hf_id" => target_hf_id,
                "target_identity" => target_identity,
                "target_seen_as" => target_seen_as,
                "teacher_hf_id" => teacher_hf_id,
                "theft_method" => theft_method,
                "took_items" => took_items,
                "took_livestock" => took_livestock,
                "top_facet" => top_facet,
                "top_facet_modifier" => top_facet_modifier,
                "top_facet_rating" => top_facet_rating,
                "top_relationship_factor" => top_relationship_factor,
                "top_relationship_modifier" => top_relationship_modifier,
                "top_relationship_rating" => top_relationship_rating,
                "top_value" => top_value,
                "top_value_modifier" => top_value_modifier,
                "top_value_rating" => top_value_rating,
                "topic" => topic,
                "trader_entity_id" => trader_entity_id,
                "trader_hf_id" => trader_hf_id,
                "trickster_hf_id" => trickster_hf_id,
                "tree" => tree,
                "trickster" => trickster,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventTT>, Error> {
        use crate::schema::historical_events_t_t::dsl::*;
        let query = historical_events_t_t;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventTT>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "target_civ_id",
            "target_en_id",
            "target_hf_id",
            "target_identity",
            "target_seen_as",
            "teacher_hf_id",
            "theft_method",
            "took_items",
            "took_livestock",
            "top_facet",
            "top_facet_modifier",
            "top_facet_rating",
            "top_relationship_factor",
            "top_relationship_modifier",
            "top_relationship_rating",
            "top_value",
            "top_value_modifier",
            "top_value_rating",
            "topic",
            "trader_entity_id",
            "trader_hf_id",
            "trickster_hf_id",
            "tree",
            "trickster",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventTT],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_t_t::dsl::*;
        let query = historical_events_t_t.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "target_civ_id" => {target_civ_id: Option<i32>},
                "target_en_id" => {target_en_id: Option<i32>},
                "target_hf_id" => {target_hf_id: Option<i32>},
                "target_identity" => {target_identity: Option<i32>},
                "target_seen_as" => {target_seen_as: Option<String>},
                "teacher_hf_id" => {teacher_hf_id: Option<i32>},
                "theft_method" => {theft_method: Option<String>},
                "took_items" => {took_items: Option<bool>},
                "took_livestock" => {took_livestock: Option<bool>},
                "top_facet" => {top_facet: Option<String>},
                "top_facet_modifier" => {top_facet_modifier: Option<i32>},
                "top_facet_rating" => {top_facet_rating: Option<i32>},
                "top_relationship_factor" => {top_relationship_factor: Option<String>},
                "top_relationship_modifier" => {top_relationship_modifier: Option<i32>},
                "top_relationship_rating" => {top_relationship_rating: Option<i32>},
                "top_value" => {top_value: Option<String>},
                "top_value_modifier" => {top_value_modifier: Option<i32>},
                "top_value_rating" => {top_value_rating: Option<i32>},
                "topic" => {topic: Option<String>},
                "trader_entity_id" => {trader_entity_id: Option<i32>},
                "trader_hf_id" => {trader_hf_id: Option<i32>},
                "trickster_hf_id" => {trickster_hf_id: Option<i32>},
                "tree" => {tree: Option<i32>},
                "trickster" => {trickster: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventTT, df_st_core::HistoricalEvent> for HistoricalEventTT {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.target_civ_id.add_missing_data(&source.target_civ_id);
        self.target_en_id.add_missing_data(&source.target_en_id);
        self.target_hf_id.add_missing_data(&source.target_hf_id);
        self.target_identity.add_missing_data(&source.target_identity);
        self.target_seen_as.add_missing_data(&source.target_seen_as);
        self.teacher_hf_id.add_missing_data(&source.teacher_hf_id);
        self.theft_method.add_missing_data(&source.theft_method);
        self.took_items.add_missing_data(&source.took_items);
        self.took_livestock.add_missing_data(&source.took_livestock);
        self.top_facet.add_missing_data(&source.top_facet);
        self.top_facet_modifier.add_missing_data(&source.top_facet_modifier);
        self.top_facet_rating.add_missing_data(&source.top_facet_rating);
        self.top_relationship_factor.add_missing_data(&source.top_relationship_factor);
        self.top_relationship_modifier.add_missing_data(&source.top_relationship_modifier);
        self.top_relationship_rating.add_missing_data(&source.top_relationship_rating);
        self.top_value.add_missing_data(&source.top_value);
        self.top_value_modifier.add_missing_data(&source.top_value_modifier);
        self.top_value_rating.add_missing_data(&source.top_value_rating);
        self.topic.add_missing_data(&source.topic);
        self.trader_entity_id.add_missing_data(&source.trader_entity_id);
        self.trader_hf_id.add_missing_data(&source.trader_hf_id);
        self.trickster_hf_id.add_missing_data(&source.trickster_hf_id);
        self.tree.add_missing_data(&source.tree);
        self.trickster.add_missing_data(&source.trickster);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventTT> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventTT) {
        self.id.add_missing_data(&source.he_id);
        self.target_civ_id.add_missing_data(&source.target_civ_id);
        self.target_en_id.add_missing_data(&source.target_en_id);
        self.target_hf_id.add_missing_data(&source.target_hf_id);
        self.target_identity.add_missing_data(&source.target_identity);
        self.target_seen_as.add_missing_data(&source.target_seen_as);
        self.teacher_hf_id.add_missing_data(&source.teacher_hf_id);
        self.theft_method.add_missing_data(&source.theft_method);
        self.took_items.add_missing_data(&source.took_items);
        self.took_livestock.add_missing_data(&source.took_livestock);
        self.top_facet.add_missing_data(&source.top_facet);
        self.top_facet_modifier.add_missing_data(&source.top_facet_modifier);
        self.top_facet_rating.add_missing_data(&source.top_facet_rating);
        self.top_relationship_factor.add_missing_data(&source.top_relationship_factor);
        self.top_relationship_modifier.add_missing_data(&source.top_relationship_modifier);
        self.top_relationship_rating.add_missing_data(&source.top_relationship_rating);
        self.top_value.add_missing_data(&source.top_value);
        self.top_value_modifier.add_missing_data(&source.top_value_modifier);
        self.top_value_rating.add_missing_data(&source.top_value_rating);
        self.topic.add_missing_data(&source.topic);
        self.trader_entity_id.add_missing_data(&source.trader_entity_id);
        self.trader_hf_id.add_missing_data(&source.trader_hf_id);
        self.trickster_hf_id.add_missing_data(&source.trickster_hf_id);
        self.tree.add_missing_data(&source.tree);
        self.trickster.add_missing_data(&source.trickster);
    }
}

impl PartialEq for HistoricalEventTT {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventTT {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventTT> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventTT) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventTT {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
