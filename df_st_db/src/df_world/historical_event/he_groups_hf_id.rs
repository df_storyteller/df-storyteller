use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_groups_hf_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_groups_hf_ids"]
#[primary_key(he_id, hf_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HEGroupsHFID {
    pub he_id: i32,
    pub hf_id: i32,
    pub world_id: i32,
}

impl HEGroupsHFID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HEGroupsHFID, HEGroupsHFID> for HEGroupsHFID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_groups_hf_ids: &[HEGroupsHFID]) {
        diesel::insert_into(he_groups_hf_ids::table)
            .values(he_groups_hf_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving he_groups_hf_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_groups_hf_ids: &[HEGroupsHFID]) {
        diesel::insert_into(he_groups_hf_ids::table)
            .values(he_groups_hf_ids)
            .execute(conn)
            .expect("Error saving he_groups_hf_ids");
    }

    /// Get a list of HEGroupsHFID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HEGroupsHFID>, Error> {
        use crate::schema::he_groups_hf_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_groups_hf_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "hf_id" => hf_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "hf_id" => hf_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HEGroupsHFID>, Error> {
        use crate::schema::he_groups_hf_ids::dsl::*;
        let query = he_groups_hf_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        Ok(query.first::<HEGroupsHFID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["he_id", "hf_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HEGroupsHFID],
        core_list: Vec<HEGroupsHFID>,
    ) -> Result<Vec<HEGroupsHFID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_groups_hf_ids::dsl::*;
        let query = he_groups_hf_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "hf_id" => hf_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "hf_id" => {hf_id: i32},
            };},
        };
    }
}

impl PartialEq for HEGroupsHFID {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id && self.hf_id == other.hf_id
    }
}

impl Hash for HEGroupsHFID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
        self.hf_id.hash(state);
    }
}
