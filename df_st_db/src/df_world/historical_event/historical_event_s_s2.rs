use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_s_s2;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_s_s2"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventSS2 {
    pub he_id: i32,
    pub world_id: i32,

    pub spotter_hf_id: Option<i32>,
    pub start: Option<bool>,
    pub state: Option<String>,
    pub structure_id: Option<i32>,
    pub student_hf_id: Option<i32>,
    pub subtype: Option<String>,
    pub successful: Option<bool>,
    pub surveiled_coconspirator: Option<bool>,
    pub surveiled_contact: Option<bool>,
    pub surveiled_convicted: Option<bool>,
    pub surveiled_target: Option<bool>,
    pub secret_text: Option<String>,
    pub shooter_artifact_id: Option<i32>,
    pub shooter_item: Option<String>,
    pub shooter_item_subtype: Option<String>,
    pub shooter_item_type: Option<String>,
    pub shooter_mat: Option<String>,
    pub source: Option<i32>,
}

impl HistoricalEventSS2 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventSS2> for HistoricalEventSS2 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_s_s2: &[HistoricalEventSS2]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_s_s2::table)
            .values(historical_events_s_s2)
            .on_conflict((
                historical_events_s_s2::he_id,
                historical_events_s_s2::world_id,
            ))
            .do_update()
            .set((
                historical_events_s_s2::spotter_hf_id.eq(excluded(historical_events_s_s2::spotter_hf_id)),
                historical_events_s_s2::start.eq(excluded(historical_events_s_s2::start)),
                historical_events_s_s2::state.eq(excluded(historical_events_s_s2::state)),
                historical_events_s_s2::structure_id.eq(excluded(historical_events_s_s2::structure_id)),
                historical_events_s_s2::student_hf_id.eq(excluded(historical_events_s_s2::student_hf_id)),
                historical_events_s_s2::subtype.eq(excluded(historical_events_s_s2::subtype)),
                historical_events_s_s2::successful.eq(excluded(historical_events_s_s2::successful)),
                historical_events_s_s2::surveiled_coconspirator.eq(excluded(historical_events_s_s2::surveiled_coconspirator)),
                historical_events_s_s2::surveiled_contact.eq(excluded(historical_events_s_s2::surveiled_contact)),
                historical_events_s_s2::surveiled_convicted.eq(excluded(historical_events_s_s2::surveiled_convicted)),
                historical_events_s_s2::surveiled_target.eq(excluded(historical_events_s_s2::surveiled_target)),
                historical_events_s_s2::secret_text.eq(excluded(historical_events_s_s2::secret_text)),
                historical_events_s_s2::shooter_artifact_id.eq(excluded(historical_events_s_s2::shooter_artifact_id)),
                historical_events_s_s2::shooter_item.eq(excluded(historical_events_s_s2::shooter_item)),
                historical_events_s_s2::shooter_item_subtype.eq(excluded(historical_events_s_s2::shooter_item_subtype)),
                historical_events_s_s2::shooter_item_type.eq(excluded(historical_events_s_s2::shooter_item_type)),
                historical_events_s_s2::shooter_mat.eq(excluded(historical_events_s_s2::shooter_mat)),
                historical_events_s_s2::source.eq(excluded(historical_events_s_s2::source)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_s_s2");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_s_s2: &[HistoricalEventSS2]) {
        diesel::insert_into(historical_events_s_s2::table)
            .values(historical_events_s_s2)
            .execute(conn)
            .expect("Error saving historical_events_s_s2");
    }

    /// Get a list of HistoricalEventSS2 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventSS2>, Error> {
        /*
        use crate::schema::historical_events_s_s2::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_s_s2.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "spotter_hf_id" => spotter_hf_id,
                "start" => start,
                "state" => state,
                "structure_id" => structure_id,
                "student_hf_id" => student_hf_id,
                "subtype" => subtype,
                "successful" => successful,
                "surveiled_coconspirator" => surveiled_coconspirator,
                "surveiled_contact" => surveiled_contact,
                "surveiled_convicted" => surveiled_convicted,
                "surveiled_target" => surveiled_target,
                "secret_text" => secret_text,
                "shooter_artifact_id" => shooter_artifact_id,
                "shooter_item" => shooter_item,
                "shooter_item_subtype" => shooter_item_subtype,
                "shooter_item_type" => shooter_item_type,
                "shooter_mat" => shooter_mat,
                "source" => source,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventSS2>, Error> {
        use crate::schema::historical_events_s_s2::dsl::*;
        let query = historical_events_s_s2;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventSS2>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "spotter_hf_id",
            "start",
            "state",
            "structure_id",
            "student_hf_id",
            "subtype",
            "successful",
            "surveiled_coconspirator",
            "surveiled_contact",
            "surveiled_convicted",
            "surveiled_target",
            "secret_text",
            "shooter_artifact_id",
            "shooter_item",
            "shooter_item_subtype",
            "shooter_item_type",
            "shooter_mat",
            "source",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventSS2],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_s_s2::dsl::*;
        let query = historical_events_s_s2.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "spotter_hf_id" => {spotter_hf_id: Option<i32>},
                "start" => {start: Option<bool>},
                "state" => {state: Option<String>},
                "structure_id" => {structure_id: Option<i32>},
                "student_hf_id" => {student_hf_id: Option<i32>},
                "subtype" => {subtype: Option<String>},
                "successful" => {successful: Option<bool>},
                "surveiled_coconspirator" => {surveiled_coconspirator: Option<bool>},
                "surveiled_contact" => {surveiled_contact: Option<bool>},
                "surveiled_convicted" => {surveiled_convicted: Option<bool>},
                "surveiled_target" => {surveiled_target: Option<bool>},
                "secret_text" => {secret_text: Option<String>},
                "shooter_artifact_id" => {shooter_artifact_id: Option<i32>},
                "shooter_item" => {shooter_item: Option<String>},
                "shooter_item_subtype" => {shooter_item_subtype: Option<String>},
                "shooter_item_type" => {shooter_item_type: Option<String>},
                "shooter_mat" => {shooter_mat: Option<String>},
                "source" => {source: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventSS2, df_st_core::HistoricalEvent> for HistoricalEventSS2 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.spotter_hf_id.add_missing_data(&source.spotter_hf_id);
        self.start.add_missing_data(&source.start);
        self.state.add_missing_data(&source.state);
        self.structure_id.add_missing_data(&source.structure_id);
        self.student_hf_id.add_missing_data(&source.student_hf_id);
        self.subtype.add_missing_data(&source.subtype);
        self.successful.add_missing_data(&source.successful);
        self.surveiled_coconspirator.add_missing_data(&source.surveiled_coconspirator);
        self.surveiled_contact.add_missing_data(&source.surveiled_contact);
        self.surveiled_convicted.add_missing_data(&source.surveiled_convicted);
        self.surveiled_target.add_missing_data(&source.surveiled_target);
        self.secret_text.add_missing_data(&source.secret_text);
        self.shooter_artifact_id.add_missing_data(&source.shooter_artifact_id);
        self.shooter_item.add_missing_data(&source.shooter_item);
        self.shooter_item_subtype.add_missing_data(&source.shooter_item_subtype);
        self.shooter_item_type.add_missing_data(&source.shooter_item_type);
        self.shooter_mat.add_missing_data(&source.shooter_mat);
        self.source.add_missing_data(&source.source);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventSS2> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventSS2) {
        self.id.add_missing_data(&source.he_id);
        self.spotter_hf_id.add_missing_data(&source.spotter_hf_id);
        self.start.add_missing_data(&source.start);
        self.state.add_missing_data(&source.state);
        self.structure_id.add_missing_data(&source.structure_id);
        self.student_hf_id.add_missing_data(&source.student_hf_id);
        self.subtype.add_missing_data(&source.subtype);
        self.successful.add_missing_data(&source.successful);
        self.surveiled_coconspirator.add_missing_data(&source.surveiled_coconspirator);
        self.surveiled_contact.add_missing_data(&source.surveiled_contact);
        self.surveiled_convicted.add_missing_data(&source.surveiled_convicted);
        self.surveiled_target.add_missing_data(&source.surveiled_target);
        self.secret_text.add_missing_data(&source.secret_text);
        self.shooter_artifact_id.add_missing_data(&source.shooter_artifact_id);
        self.shooter_item.add_missing_data(&source.shooter_item);
        self.shooter_item_subtype.add_missing_data(&source.shooter_item_subtype);
        self.shooter_item_type.add_missing_data(&source.shooter_item_type);
        self.shooter_mat.add_missing_data(&source.shooter_mat);
        self.source.add_missing_data(&source.source);
    }
}

impl PartialEq for HistoricalEventSS2 {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventSS2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventSS2> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventSS2) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventSS2 {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
