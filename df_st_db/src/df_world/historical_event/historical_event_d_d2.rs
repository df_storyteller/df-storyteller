use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_d_d2;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_d_d2"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventDD2 {
    pub he_id: i32,
    pub world_id: i32,

    pub dest_entity_id: Option<i32>,
    pub dest_site_id: Option<i32>,
    pub dest_structure_id: Option<i32>,
    pub destroyed_structure_id: Option<i32>,
    pub destroyer_en_id: Option<i32>,
    pub detected: Option<bool>,
    pub did_not_reveal_all_in_interrogation: Option<bool>,
    pub disturbance: Option<bool>,
    pub dispute: Option<String>,
    pub doer_hf_id: Option<i32>,
    pub death_cause: Option<String>,
    pub destination: Option<i32>,
    pub doer: Option<i32>,
    pub dye_mat: Option<String>,
    pub dye_mat_index: Option<i32>,
    pub dye_mat_type: Option<i32>,
}

impl HistoricalEventDD2 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventDD2> for HistoricalEventDD2 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d2: &[HistoricalEventDD2]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_d_d2::table)
            .values(historical_events_d_d2)
            .on_conflict((
                historical_events_d_d2::he_id,
                historical_events_d_d2::world_id,
            ))
            .do_update()
            .set((
                historical_events_d_d2::dest_entity_id.eq(excluded(historical_events_d_d2::dest_entity_id)),
                historical_events_d_d2::dest_site_id.eq(excluded(historical_events_d_d2::dest_site_id)),
                historical_events_d_d2::dest_structure_id.eq(excluded(historical_events_d_d2::dest_structure_id)),
                historical_events_d_d2::destroyed_structure_id.eq(excluded(historical_events_d_d2::destroyed_structure_id)),
                historical_events_d_d2::destroyer_en_id.eq(excluded(historical_events_d_d2::destroyer_en_id)),
                historical_events_d_d2::detected.eq(excluded(historical_events_d_d2::detected)),
                historical_events_d_d2::did_not_reveal_all_in_interrogation.eq(excluded(historical_events_d_d2::did_not_reveal_all_in_interrogation)),
                historical_events_d_d2::disturbance.eq(excluded(historical_events_d_d2::disturbance)),
                historical_events_d_d2::dispute.eq(excluded(historical_events_d_d2::dispute)),
                historical_events_d_d2::doer_hf_id.eq(excluded(historical_events_d_d2::doer_hf_id)),
                historical_events_d_d2::death_cause.eq(excluded(historical_events_d_d2::death_cause)),
                historical_events_d_d2::destination.eq(excluded(historical_events_d_d2::destination)),
                historical_events_d_d2::doer.eq(excluded(historical_events_d_d2::doer)),
                historical_events_d_d2::dye_mat.eq(excluded(historical_events_d_d2::dye_mat)),
                historical_events_d_d2::dye_mat_index.eq(excluded(historical_events_d_d2::dye_mat_index)),
                historical_events_d_d2::dye_mat_type.eq(excluded(historical_events_d_d2::dye_mat_type)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_d_d2");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d2: &[HistoricalEventDD2]) {
        diesel::insert_into(historical_events_d_d2::table)
            .values(historical_events_d_d2)
            .execute(conn)
            .expect("Error saving historical_events_d_d2");
    }

    /// Get a list of HistoricalEventDD2 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventDD2>, Error> {
        /*
        use crate::schema::historical_events_d_d2::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_d_d2.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "dest_entity_id" => dest_entity_id,
                "dest_site_id" => dest_site_id,
                "dest_structure_id" => dest_structure_id,
                "destroyed_structure_id" => destroyed_structure_id,
                "destroyer_en_id" => destroyer_en_id,
                "detected" => detected,
                "did_not_reveal_all_in_interrogation" => did_not_reveal_all_in_interrogation,
                "disturbance" => disturbance,
                "dispute" => dispute,
                "doer_hf_id" => doer_hf_id,
                "death_cause" => death_cause,
                "destination" => destination,
                "doer" => doer,
                "dye_mat" => dye_mat,
                "dye_mat_index" => dye_mat_index,
                "dye_mat_type" => dye_mat_type,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventDD2>, Error> {
        use crate::schema::historical_events_d_d2::dsl::*;
        let query = historical_events_d_d2;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventDD2>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "dest_entity_id",
            "dest_site_id",
            "dest_structure_id",
            "destroyed_structure_id",
            "destroyer_en_id",
            "detected",
            "did_not_reveal_all_in_interrogation",
            "disturbance",
            "dispute",
            "doer_hf_id",
            "death_cause",
            "destination",
            "doer",
            "dye_mat",
            "dye_mat_index",
            "dye_mat_type",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventDD2],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_d_d2::dsl::*;
        let query = historical_events_d_d2.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "dest_entity_id" => {dest_entity_id: Option<i32>},
                "dest_site_id" => {dest_site_id: Option<i32>},
                "dest_structure_id" => {dest_structure_id: Option<i32>},
                "destroyed_structure_id" => {destroyed_structure_id: Option<i32>},
                "destroyer_en_id" => {destroyer_en_id: Option<i32>},
                "detected" => {detected: Option<bool>},
                "did_not_reveal_all_in_interrogation" => {did_not_reveal_all_in_interrogation: Option<bool>},
                "disturbance" => {disturbance: Option<bool>},
                "dispute" => {dispute: Option<String>},
                "doer_hf_id" => {doer_hf_id: Option<i32>},
                "death_cause" => {death_cause: Option<String>},
                "destination" => {destination: Option<i32>},
                "doer" => {doer: Option<i32>},
                "dye_mat" => {dye_mat: Option<String>},
                "dye_mat_index" => {dye_mat_index: Option<i32>},
                "dye_mat_type" => {dye_mat_type: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventDD2, df_st_core::HistoricalEvent> for HistoricalEventDD2 {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.dest_entity_id.add_missing_data(&source.dest_entity_id);
        self.dest_site_id.add_missing_data(&source.dest_site_id);
        self.dest_structure_id.add_missing_data(&source.dest_structure_id);
        self.destroyed_structure_id.add_missing_data(&source.destroyed_structure_id);
        self.destroyer_en_id.add_missing_data(&source.destroyer_en_id);
        self.detected.add_missing_data(&source.detected);
        self.did_not_reveal_all_in_interrogation.add_missing_data(&source.did_not_reveal_all_in_interrogation);
        self.disturbance.add_missing_data(&source.disturbance);
        self.dispute.add_missing_data(&source.dispute);
        self.doer_hf_id.add_missing_data(&source.doer_hf_id);
        self.death_cause.add_missing_data(&source.death_cause);
        self.destination.add_missing_data(&source.destination);
        self.doer.add_missing_data(&source.doer);
        self.dye_mat.add_missing_data(&source.dye_mat);
        self.dye_mat_index.add_missing_data(&source.dye_mat_index);
        self.dye_mat_type.add_missing_data(&source.dye_mat_type);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventDD2> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventDD2) {
        self.id.add_missing_data(&source.he_id);
        self.dest_entity_id.add_missing_data(&source.dest_entity_id);
        self.dest_site_id.add_missing_data(&source.dest_site_id);
        self.dest_structure_id.add_missing_data(&source.dest_structure_id);
        self.destroyed_structure_id.add_missing_data(&source.destroyed_structure_id);
        self.destroyer_en_id.add_missing_data(&source.destroyer_en_id);
        self.detected.add_missing_data(&source.detected);
        self.did_not_reveal_all_in_interrogation.add_missing_data(&source.did_not_reveal_all_in_interrogation);
        self.disturbance.add_missing_data(&source.disturbance);
        self.dispute.add_missing_data(&source.dispute);
        self.doer_hf_id.add_missing_data(&source.doer_hf_id);
        self.death_cause.add_missing_data(&source.death_cause);
        self.destination.add_missing_data(&source.destination);
        self.doer.add_missing_data(&source.doer);
        self.dye_mat.add_missing_data(&source.dye_mat);
        self.dye_mat_index.add_missing_data(&source.dye_mat_index);
        self.dye_mat_type.add_missing_data(&source.dye_mat_type);
    }
}

impl PartialEq for HistoricalEventDD2 {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventDD2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventDD2> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventDD2) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventDD2 {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
