use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_q_r;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_q_r"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventQR {
    pub he_id: i32,
    pub world_id: i32,

    pub quality: Option<i32>,
    pub rampage_civ_id: Option<i32>,
    pub ransomed_hf_id: Option<i32>,
    pub ransomer_hf_id: Option<i32>,
    pub reason: Option<i32>,
    pub reason_id: Option<i32>,
    pub rebuilt_ruined: Option<bool>,
    pub receiver_entity_id: Option<i32>,
    pub receiver_hf_id: Option<i32>,
    pub relationship: Option<String>,
    pub relevant_entity_id: Option<i32>,
    pub relevant_id_for_method: Option<i32>,
    pub relevant_position_profile_id: Option<i32>,
    pub religion_id: Option<i32>,
    pub resident_civ_id: Option<i32>,
    pub result: Option<String>,
    pub return_: Option<bool>,
    pub race_id: Option<String>,
    pub rebuild: Option<bool>,
    pub region_id: Option<i32>,
}

impl HistoricalEventQR {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventQR> for HistoricalEventQR {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[rustfmt::skip]
    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_q_r: &[HistoricalEventQR]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_q_r::table)
            .values(historical_events_q_r)
            .on_conflict((
                historical_events_q_r::he_id,
                historical_events_q_r::world_id,
            ))
            .do_update()
            .set((
                historical_events_q_r::quality.eq(excluded(historical_events_q_r::quality)),
                historical_events_q_r::rampage_civ_id.eq(excluded(historical_events_q_r::rampage_civ_id)),
                historical_events_q_r::ransomed_hf_id.eq(excluded(historical_events_q_r::ransomed_hf_id)),
                historical_events_q_r::ransomer_hf_id.eq(excluded(historical_events_q_r::ransomer_hf_id)),
                historical_events_q_r::reason.eq(excluded(historical_events_q_r::reason)),
                historical_events_q_r::reason_id.eq(excluded(historical_events_q_r::reason_id)),
                historical_events_q_r::rebuilt_ruined.eq(excluded(historical_events_q_r::rebuilt_ruined)),
                historical_events_q_r::receiver_entity_id.eq(excluded(historical_events_q_r::receiver_entity_id)),
                historical_events_q_r::receiver_hf_id.eq(excluded(historical_events_q_r::receiver_hf_id)),
                historical_events_q_r::relationship.eq(excluded(historical_events_q_r::relationship)),
                historical_events_q_r::relevant_entity_id.eq(excluded(historical_events_q_r::relevant_entity_id)),
                historical_events_q_r::relevant_id_for_method.eq(excluded(historical_events_q_r::relevant_id_for_method)),
                historical_events_q_r::relevant_position_profile_id.eq(excluded(historical_events_q_r::relevant_position_profile_id,)),
                historical_events_q_r::religion_id.eq(excluded(historical_events_q_r::religion_id)),
                historical_events_q_r::resident_civ_id.eq(excluded(historical_events_q_r::resident_civ_id)),
                historical_events_q_r::result.eq(excluded(historical_events_q_r::result)),
                historical_events_q_r::return_.eq(excluded(historical_events_q_r::return_)),
                historical_events_q_r::race_id.eq(excluded(historical_events_q_r::race_id)),
                historical_events_q_r::rebuild.eq(excluded(historical_events_q_r::rebuild)),
                historical_events_q_r::region_id.eq(excluded(historical_events_q_r::region_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_q_r");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_q_r: &[HistoricalEventQR]) {
        diesel::insert_into(historical_events_q_r::table)
            .values(historical_events_q_r)
            .execute(conn)
            .expect("Error saving historical_events_q_r");
    }

    /// Get a list of HistoricalEventQR from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventQR>, Error> {
        /*
        use crate::schema::historical_events_q_r::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_q_r.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "quality" => quality,
                "rampage_civ_id" => rampage_civ_id,
                "ransomed_hf_id" => ransomed_hf_id,
                "ransomer_hf_id" => ransomer_hf_id,
                "reason" => reason,
                "reason_id" => reason_id,
                "rebuilt_ruined" => rebuilt_ruined,
                "receiver_entity_id" => receiver_entity_id,
                "receiver_hf_id" => receiver_hf_id,
                "relationship" => relationship,
                "relevant_entity_id" => relevant_entity_id,
                "relevant_id_for_method" => relevant_id_for_method,
                "relevant_position_profile_id" => relevant_position_profile_id,
                "religion_id" => religion_id,
                "resident_civ_id" => resident_civ_id,
                "result" => result,
                "return" => return_,
                "race_id" => race_id,
                "rebuild" => rebuild,
                "region_id" => region_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventQR>, Error> {
        use crate::schema::historical_events_q_r::dsl::*;
        let query = historical_events_q_r;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventQR>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "quality",
            "rampage_civ_id",
            "ransomed_hf_id",
            "ransomer_hf_id",
            "reason",
            "reason_id",
            "rebuilt_ruined",
            "receiver_entity_id",
            "receiver_hf_id",
            "relationship",
            "relevant_entity_id",
            "relevant_id_for_method",
            "relevant_position_profile_id",
            "religion_id",
            "resident_civ_id",
            "result",
            "return",
            "race_id",
            "rebuild",
            "region_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventQR],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_q_r::dsl::*;
        let query = historical_events_q_r.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "quality" => {quality: Option<i32>},
                "rampage_civ_id" => {rampage_civ_id: Option<i32>},
                "ransomed_hf_id" => {ransomed_hf_id: Option<i32>},
                "ransomer_hf_id" => {ransomer_hf_id: Option<i32>},
                "reason" => {reason: Option<i32>},
                "reason_id" => {reason_id: Option<i32>},
                "rebuilt_ruined" => {rebuilt_ruined: Option<bool>},
                "receiver_entity_id" => {receiver_entity_id: Option<i32>},
                "receiver_hf_id" => {receiver_hf_id: Option<i32>},
                "relationship" => {relationship: Option<String>},
                "relevant_entity_id" => {relevant_entity_id: Option<i32>},
                "relevant_id_for_method" => {relevant_id_for_method: Option<i32>},
                "relevant_position_profile_id" => {relevant_position_profile_id: Option<i32>},
                "religion_id" => {religion_id: Option<i32>},
                "resident_civ_id" => {resident_civ_id: Option<i32>},
                "result" => {result: Option<String>},
                "return" => {return_: Option<bool>},
                "race_id" => {race_id: Option<String>},
                "rebuild" => {rebuild: Option<bool>},
                "region_id" => {region_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventQR, df_st_core::HistoricalEvent> for HistoricalEventQR {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.quality.add_missing_data(&source.quality);
        self.rampage_civ_id.add_missing_data(&source.rampage_civ_id);
        self.ransomed_hf_id.add_missing_data(&source.ransomed_hf_id);
        self.ransomer_hf_id.add_missing_data(&source.ransomer_hf_id);
        self.reason.add_missing_data(&source.reason);
        self.reason_id.add_missing_data(&source.reason_id);
        self.rebuilt_ruined.add_missing_data(&source.rebuilt_ruined);
        self.receiver_entity_id.add_missing_data(&source.receiver_entity_id);
        self.receiver_hf_id.add_missing_data(&source.receiver_hf_id);
        self.relationship.add_missing_data(&source.relationship);
        self.relevant_entity_id.add_missing_data(&source.relevant_entity_id);
        self.relevant_id_for_method.add_missing_data(&source.relevant_id_for_method);
        self.relevant_position_profile_id.add_missing_data(&source.relevant_position_profile_id);
        self.religion_id.add_missing_data(&source.religion_id);
        self.resident_civ_id.add_missing_data(&source.resident_civ_id);
        self.result.add_missing_data(&source.result);
        self.return_.add_missing_data(&source.return_);
        self.race_id.add_missing_data(&source.race_id);
        self.rebuild.add_missing_data(&source.rebuild);
        self.region_id.add_missing_data(&source.region_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventQR> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventQR) {
        self.id.add_missing_data(&source.he_id);
        self.quality.add_missing_data(&source.quality);
        self.rampage_civ_id.add_missing_data(&source.rampage_civ_id);
        self.ransomed_hf_id.add_missing_data(&source.ransomed_hf_id);
        self.ransomer_hf_id.add_missing_data(&source.ransomer_hf_id);
        self.reason.add_missing_data(&source.reason);
        self.reason_id.add_missing_data(&source.reason_id);
        self.rebuilt_ruined.add_missing_data(&source.rebuilt_ruined);
        self.receiver_entity_id.add_missing_data(&source.receiver_entity_id);
        self.receiver_hf_id.add_missing_data(&source.receiver_hf_id);
        self.relationship.add_missing_data(&source.relationship);
        self.relevant_entity_id.add_missing_data(&source.relevant_entity_id);
        self.relevant_id_for_method.add_missing_data(&source.relevant_id_for_method);
        self.relevant_position_profile_id.add_missing_data(&source.relevant_position_profile_id);
        self.religion_id.add_missing_data(&source.religion_id);
        self.resident_civ_id.add_missing_data(&source.resident_civ_id);
        self.result.add_missing_data(&source.result);
        self.return_.add_missing_data(&source.return_);
        self.race_id.add_missing_data(&source.race_id);
        self.rebuild.add_missing_data(&source.rebuild);
        self.region_id.add_missing_data(&source.region_id);
    }
}

impl PartialEq for HistoricalEventQR {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventQR {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventQR> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventQR) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventQR {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
