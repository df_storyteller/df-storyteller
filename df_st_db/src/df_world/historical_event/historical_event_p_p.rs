use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_p_p;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_p_p"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventPP {
    pub he_id: i32,
    pub world_id: i32,

    pub partial_incorporation: Option<bool>,
    pub payer_entity_id: Option<i32>,
    pub payer_hf_id: Option<i32>,
    pub persecutor_en_id: Option<i32>,
    pub persecutor_hf_id: Option<i32>,
    pub plotter_hf_id: Option<i32>,
    pub pop_fl_id: Option<i32>,
    pub pop_number_moved: Option<i32>,
    pub pop_race: Option<i32>,
    pub pop_sr_id: Option<i32>,
    pub pos_taker_hf_id: Option<i32>,
    pub position_id: Option<i32>,
    pub position_profile_id: Option<i32>,
    pub prison_months: Option<i32>,
    pub production_zone_id: Option<i32>,
    pub promise_to_hf_id: Option<i32>,
    pub property_confiscated_from_hf_id: Option<i32>,
    pub purchased_unowned: Option<bool>,
    pub part_lost: Option<bool>,
    pub pile_type: Option<String>,
    pub position: Option<String>,
    pub props_item_mat: Option<String>,
    pub props_item_mat_index: Option<i32>,
    pub props_item_mat_type: Option<i32>,
    pub props_item_subtype: Option<String>,
    pub props_item_type: Option<String>,
    pub props_pile_type: Option<i32>,
}

impl HistoricalEventPP {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventPP> for HistoricalEventPP {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_p_p: &[HistoricalEventPP]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_p_p::table)
            .values(historical_events_p_p)
            .on_conflict((
                historical_events_p_p::he_id,
                historical_events_p_p::world_id,
            ))
            .do_update()
            .set((
                historical_events_p_p::partial_incorporation
                    .eq(excluded(historical_events_p_p::partial_incorporation)),
                historical_events_p_p::payer_entity_id
                    .eq(excluded(historical_events_p_p::payer_entity_id)),
                historical_events_p_p::payer_hf_id.eq(excluded(historical_events_p_p::payer_hf_id)),
                historical_events_p_p::persecutor_en_id
                    .eq(excluded(historical_events_p_p::persecutor_en_id)),
                historical_events_p_p::persecutor_hf_id
                    .eq(excluded(historical_events_p_p::persecutor_hf_id)),
                historical_events_p_p::plotter_hf_id
                    .eq(excluded(historical_events_p_p::plotter_hf_id)),
                historical_events_p_p::pop_fl_id.eq(excluded(historical_events_p_p::pop_fl_id)),
                historical_events_p_p::pop_number_moved
                    .eq(excluded(historical_events_p_p::pop_number_moved)),
                historical_events_p_p::pop_race.eq(excluded(historical_events_p_p::pop_race)),
                historical_events_p_p::pop_sr_id.eq(excluded(historical_events_p_p::pop_sr_id)),
                historical_events_p_p::pos_taker_hf_id
                    .eq(excluded(historical_events_p_p::pos_taker_hf_id)),
                historical_events_p_p::position_id.eq(excluded(historical_events_p_p::position_id)),
                historical_events_p_p::position_profile_id
                    .eq(excluded(historical_events_p_p::position_profile_id)),
                historical_events_p_p::prison_months
                    .eq(excluded(historical_events_p_p::prison_months)),
                historical_events_p_p::production_zone_id
                    .eq(excluded(historical_events_p_p::production_zone_id)),
                historical_events_p_p::promise_to_hf_id
                    .eq(excluded(historical_events_p_p::promise_to_hf_id)),
                historical_events_p_p::property_confiscated_from_hf_id.eq(excluded(
                    historical_events_p_p::property_confiscated_from_hf_id,
                )),
                historical_events_p_p::purchased_unowned
                    .eq(excluded(historical_events_p_p::purchased_unowned)),
                historical_events_p_p::part_lost.eq(excluded(historical_events_p_p::part_lost)),
                historical_events_p_p::pile_type.eq(excluded(historical_events_p_p::pile_type)),
                historical_events_p_p::position.eq(excluded(historical_events_p_p::position)),
                historical_events_p_p::props_item_mat
                    .eq(excluded(historical_events_p_p::props_item_mat)),
                historical_events_p_p::props_item_mat_index
                    .eq(excluded(historical_events_p_p::props_item_mat_index)),
                historical_events_p_p::props_item_mat_type
                    .eq(excluded(historical_events_p_p::props_item_mat_type)),
                historical_events_p_p::props_item_subtype
                    .eq(excluded(historical_events_p_p::props_item_subtype)),
                historical_events_p_p::props_item_type
                    .eq(excluded(historical_events_p_p::props_item_type)),
                historical_events_p_p::props_pile_type
                    .eq(excluded(historical_events_p_p::props_pile_type)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_p_p");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_p_p: &[HistoricalEventPP]) {
        diesel::insert_into(historical_events_p_p::table)
            .values(historical_events_p_p)
            .execute(conn)
            .expect("Error saving historical_events_p_p");
    }

    /// Get a list of HistoricalEventPP from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventPP>, Error> {
        /*
        use crate::schema::historical_events_p_p::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_p_p.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "partial_incorporation" => partial_incorporation,
                "payer_entity_id" => payer_entity_id,
                "payer_hf_id" => payer_hf_id,
                "persecutor_en_id" => persecutor_en_id,
                "persecutor_hf_id" => persecutor_hf_id,
                "plotter_hf_id" => plotter_hf_id,
                "pop_fl_id" => pop_fl_id,
                "pop_number_moved" => pop_number_moved,
                "pop_race" => pop_race,
                "pop_sr_id" => pop_sr_id,
                "pos_taker_hf_id" => pos_taker_hf_id,
                "position_id" => position_id,
                "position_profile_id" => position_profile_id,
                "prison_months" => prison_months,
                "production_zone_id" => production_zone_id,
                "promise_to_hf_id" => promise_to_hf_id,
                "property_confiscated_from_hf_id" => property_confiscated_from_hf_id,
                "purchased_unowned" => purchased_unowned,
                "part_lost" => part_lost,
                "pile_type" => pile_type,
                "position" => position,
                "props_item_mat" => props_item_mat,
                "props_item_mat_index" => props_item_mat_index,
                "props_item_mat_type" => props_item_mat_type,
                "props_item_subtype" => props_item_subtype,
                "props_item_type" => props_item_type,
                "props_pile_type" => props_pile_type,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventPP>, Error> {
        use crate::schema::historical_events_p_p::dsl::*;
        let query = historical_events_p_p;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventPP>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "partial_incorporation",
            "payer_entity_id",
            "payer_hf_id",
            "persecutor_en_id",
            "persecutor_hf_id",
            "plotter_hf_id",
            "pop_fl_id",
            "pop_number_moved",
            "pop_race",
            "pop_sr_id",
            "pos_taker_hf_id",
            "position_id",
            "position_profile_id",
            "prison_months",
            "production_zone_id",
            "promise_to_hf_id",
            "property_confiscated_from_hf_id",
            "purchased_unowned",
            "part_lost",
            "pile_type",
            "position",
            "props_item_mat",
            "props_item_mat_index",
            "props_item_mat_type",
            "props_item_subtype",
            "props_item_type",
            "props_pile_type",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventPP],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_p_p::dsl::*;
        let query = historical_events_p_p.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "partial_incorporation" => {partial_incorporation: Option<bool>},
                "payer_entity_id" => {payer_entity_id: Option<i32>},
                "payer_hf_id" => {payer_hf_id: Option<i32>},
                "persecutor_en_id" => {persecutor_en_id: Option<i32>},
                "persecutor_hf_id" => {persecutor_hf_id: Option<i32>},
                "plotter_hf_id" => {plotter_hf_id: Option<i32>},
                "pop_fl_id" => {pop_fl_id: Option<i32>},
                "pop_number_moved" => {pop_number_moved: Option<i32>},
                "pop_race" => {pop_race: Option<i32>},
                "pop_sr_id" => {pop_sr_id: Option<i32>},
                "pos_taker_hf_id" => {pos_taker_hf_id: Option<i32>},
                "position_id" => {position_id: Option<i32>},
                "position_profile_id" => {position_profile_id: Option<i32>},
                "prison_months" => {prison_months: Option<i32>},
                "production_zone_id" => {production_zone_id: Option<i32>},
                "promise_to_hf_id" => {promise_to_hf_id: Option<i32>},
                "property_confiscated_from_hf_id" => {property_confiscated_from_hf_id: Option<i32>},
                "purchased_unowned" => {purchased_unowned: Option<bool>},
                "part_lost" => {part_lost: Option<bool>},
                "pile_type" => {pile_type: Option<String>},
                "position" => {position: Option<String>},
                "props_item_mat" => {props_item_mat: Option<String>},
                "props_item_mat_index" => {props_item_mat_index: Option<i32>},
                "props_item_mat_type" => {props_item_mat_type: Option<i32>},
                "props_item_subtype" => {props_item_subtype: Option<String>},
                "props_item_type" => {props_item_type: Option<String>},
                "props_pile_type" => {props_pile_type: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventPP, df_st_core::HistoricalEvent> for HistoricalEventPP {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.partial_incorporation.add_missing_data(&source.partial_incorporation);
        self.payer_entity_id.add_missing_data(&source.payer_entity_id);
        self.payer_hf_id.add_missing_data(&source.payer_hf_id);
        self.persecutor_en_id.add_missing_data(&source.persecutor_en_id);
        self.persecutor_hf_id.add_missing_data(&source.persecutor_hf_id);
        self.plotter_hf_id.add_missing_data(&source.plotter_hf_id);
        self.pop_fl_id.add_missing_data(&source.pop_fl_id);
        self.pop_number_moved.add_missing_data(&source.pop_number_moved);
        self.pop_race.add_missing_data(&source.pop_race);
        self.pop_sr_id.add_missing_data(&source.pop_sr_id);
        self.pos_taker_hf_id.add_missing_data(&source.pos_taker_hf_id);
        self.position_id.add_missing_data(&source.position_id);
        self.position_profile_id.add_missing_data(&source.position_profile_id);
        self.prison_months.add_missing_data(&source.prison_months);
        self.production_zone_id.add_missing_data(&source.production_zone_id);
        self.promise_to_hf_id.add_missing_data(&source.promise_to_hf_id);
        self.property_confiscated_from_hf_id.add_missing_data(&source.property_confiscated_from_hf_id);
        self.purchased_unowned.add_missing_data(&source.purchased_unowned);
        self.part_lost.add_missing_data(&source.part_lost);
        self.pile_type.add_missing_data(&source.pile_type);
        self.position.add_missing_data(&source.position);
        self.props_item_mat.add_missing_data(&source.props_item_mat);
        self.props_item_mat_index.add_missing_data(&source.props_item_mat_index);
        self.props_item_mat_type.add_missing_data(&source.props_item_mat_type);
        self.props_item_subtype.add_missing_data(&source.props_item_subtype);
        self.props_item_type.add_missing_data(&source.props_item_type);
        self.props_pile_type.add_missing_data(&source.props_pile_type);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventPP> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventPP) {
        self.id.add_missing_data(&source.he_id);
        self.partial_incorporation.add_missing_data(&source.partial_incorporation);
        self.payer_entity_id.add_missing_data(&source.payer_entity_id);
        self.payer_hf_id.add_missing_data(&source.payer_hf_id);
        self.persecutor_en_id.add_missing_data(&source.persecutor_en_id);
        self.persecutor_hf_id.add_missing_data(&source.persecutor_hf_id);
        self.plotter_hf_id.add_missing_data(&source.plotter_hf_id);
        self.pop_fl_id.add_missing_data(&source.pop_fl_id);
        self.pop_number_moved.add_missing_data(&source.pop_number_moved);
        self.pop_race.add_missing_data(&source.pop_race);
        self.pop_sr_id.add_missing_data(&source.pop_sr_id);
        self.pos_taker_hf_id.add_missing_data(&source.pos_taker_hf_id);
        self.position_id.add_missing_data(&source.position_id);
        self.position_profile_id.add_missing_data(&source.position_profile_id);
        self.prison_months.add_missing_data(&source.prison_months);
        self.production_zone_id.add_missing_data(&source.production_zone_id);
        self.promise_to_hf_id.add_missing_data(&source.promise_to_hf_id);
        self.property_confiscated_from_hf_id.add_missing_data(&source.property_confiscated_from_hf_id);
        self.purchased_unowned.add_missing_data(&source.purchased_unowned);
        self.part_lost.add_missing_data(&source.part_lost);
        self.pile_type.add_missing_data(&source.pile_type);
        self.position.add_missing_data(&source.position);
        self.props_item_mat.add_missing_data(&source.props_item_mat);
        self.props_item_mat_index.add_missing_data(&source.props_item_mat_index);
        self.props_item_mat_type.add_missing_data(&source.props_item_mat_type);
        self.props_item_subtype.add_missing_data(&source.props_item_subtype);
        self.props_item_type.add_missing_data(&source.props_item_type);
        self.props_pile_type.add_missing_data(&source.props_pile_type);
    }
}

impl PartialEq for HistoricalEventPP {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventPP {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventPP> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventPP) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventPP {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
