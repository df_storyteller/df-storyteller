use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_expelled_creatures;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_expelled_creatures"]
#[primary_key(he_id, creature_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HEExpelledCreature {
    pub he_id: i32,
    pub creature_id: i32,
    pub world_id: i32,
}

impl HEExpelledCreature {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HEExpelledCreature, HEExpelledCreature> for HEExpelledCreature {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_expelled_creatures: &[HEExpelledCreature]) {
        diesel::insert_into(he_expelled_creatures::table)
            .values(he_expelled_creatures)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving he_expelled_creatures");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_expelled_creatures: &[HEExpelledCreature]) {
        diesel::insert_into(he_expelled_creatures::table)
            .values(he_expelled_creatures)
            .execute(conn)
            .expect("Error saving he_expelled_creatures");
    }

    /// Get a list of HEExpelledCreature from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HEExpelledCreature>, Error> {
        use crate::schema::he_expelled_creatures::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_expelled_creatures.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "creature_id" => creature_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "creature_id" => creature_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HEExpelledCreature>, Error> {
        use crate::schema::he_expelled_creatures::dsl::*;
        let query = he_expelled_creatures;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        let query = query.filter(creature_id.eq(id_filter.get("creature_id").unwrap_or(&0)));
        Ok(query.first::<HEExpelledCreature>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["he_id", "creature_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HEExpelledCreature],
        core_list: Vec<HEExpelledCreature>,
    ) -> Result<Vec<HEExpelledCreature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_expelled_creatures::dsl::*;
        let query = he_expelled_creatures
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "creature_id" => creature_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "creature_id" => {creature_id: i32},
            };},
        };
    }
}

impl PartialEq for HEExpelledCreature {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id && self.creature_id == other.creature_id
    }
}

impl Hash for HEExpelledCreature {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
        self.creature_id.hash(state);
    }
}
