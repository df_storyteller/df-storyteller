use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_i_i;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_i_i"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventII {
    pub he_id: i32,
    pub world_id: i32,

    pub identity_id: Option<i32>,
    pub identity_id1: Option<i32>,
    pub identity_id2: Option<i32>,
    pub inherited: Option<bool>,
    pub initiating_en_id: Option<i32>,
    pub instigator_hf_id: Option<i32>,
    pub interaction: Option<i32>,
    pub interrogator_hf_id: Option<i32>,
    pub identity_caste: Option<String>,
    pub identity_hf_id: Option<i32>,
    pub identity_name: Option<String>,
    pub identity_nemesis_id: Option<i32>,
    pub identity_race: Option<String>,
    pub imp_mat: Option<String>,
    pub imp_mat_index: Option<i32>,
    pub imp_mat_type: Option<i32>,
    pub improvement_subtype: Option<String>,
    pub improvement_type: Option<String>,
    pub injury_type: Option<String>,
    // pub interaction_id: Option<i32>,
    pub interaction_action: Option<String>,
    pub interaction_string: Option<String>,
    pub item_id: Option<i32>,
    pub item_mat: Option<String>,
    pub item_subtype: Option<String>,
    pub item_type: Option<String>,
}

impl HistoricalEventII {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventII> for HistoricalEventII {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    #[rustfmt::skip]
    fn insert_into_db(conn: &DbConnection, historical_events_i_i: &[HistoricalEventII]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_i_i::table)
            .values(historical_events_i_i)
            .on_conflict((
                historical_events_i_i::he_id,
                historical_events_i_i::world_id,
            ))
            .do_update()
            .set((
                historical_events_i_i::identity_id.eq(excluded(historical_events_i_i::identity_id)),
                historical_events_i_i::identity_id1.eq(excluded(historical_events_i_i::identity_id1)),
                historical_events_i_i::identity_id2.eq(excluded(historical_events_i_i::identity_id2)),
                historical_events_i_i::inherited.eq(excluded(historical_events_i_i::inherited)),
                historical_events_i_i::initiating_en_id.eq(excluded(historical_events_i_i::initiating_en_id)),
                historical_events_i_i::instigator_hf_id.eq(excluded(historical_events_i_i::instigator_hf_id)),
                historical_events_i_i::interaction.eq(excluded(historical_events_i_i::interaction)),
                historical_events_i_i::interrogator_hf_id.eq(excluded(historical_events_i_i::interrogator_hf_id)),
                historical_events_i_i::identity_caste.eq(excluded(historical_events_i_i::identity_caste)),
                historical_events_i_i::identity_hf_id.eq(excluded(historical_events_i_i::identity_hf_id)),
                historical_events_i_i::identity_name.eq(excluded(historical_events_i_i::identity_name)),
                historical_events_i_i::identity_nemesis_id.eq(excluded(historical_events_i_i::identity_nemesis_id)),
                historical_events_i_i::identity_race.eq(excluded(historical_events_i_i::identity_race)),
                historical_events_i_i::imp_mat.eq(excluded(historical_events_i_i::imp_mat)),
                historical_events_i_i::imp_mat_index.eq(excluded(historical_events_i_i::imp_mat_index)),
                historical_events_i_i::imp_mat_type.eq(excluded(historical_events_i_i::imp_mat_type)),
                historical_events_i_i::improvement_subtype.eq(excluded(historical_events_i_i::improvement_subtype)),
                historical_events_i_i::improvement_type.eq(excluded(historical_events_i_i::improvement_type)),
                historical_events_i_i::injury_type.eq(excluded(historical_events_i_i::injury_type)),
                // historical_events_i_i::interaction_id.eq(excluded(historical_events_i_i::interaction_id)),
                historical_events_i_i::interaction_action.eq(excluded(historical_events_i_i::interaction_action)),
                historical_events_i_i::interaction_string.eq(excluded(historical_events_i_i::interaction_string)),
                historical_events_i_i::item_id.eq(excluded(historical_events_i_i::item_id)),
                historical_events_i_i::item_mat.eq(excluded(historical_events_i_i::item_mat)),
                historical_events_i_i::item_subtype.eq(excluded(historical_events_i_i::item_subtype)),
                historical_events_i_i::item_type.eq(excluded(historical_events_i_i::item_type)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_i_i");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_i_i: &[HistoricalEventII]) {
        diesel::insert_into(historical_events_i_i::table)
            .values(historical_events_i_i)
            .execute(conn)
            .expect("Error saving historical_events_i_i");
    }

    /// Get a list of HistoricalEventII from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEventII>, Error> {
        /*
        use crate::schema::historical_events_i_i::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_i_i.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "identity_id" => identity_id,
                "identity_id1" => identity_id1,
                "identity_id2" => identity_id2,
                "inherited" => inherited,
                "initiating_en_id" => initiating_en_id,
                "instigator_hf_id" => instigator_hf_id,
                "interaction" => interaction,
                "interrogator_hf_id" => interrogator_hf_id,
                "identity_caste" => identity_caste,
                "identity_hf_id" => identity_hf_id,
                "identity_name" => identity_name,
                "identity_nemesis_id" => identity_nemesis_id,
                "identity_race" => identity_race,
                "imp_mat" => imp_mat,
                "imp_mat_index" => imp_mat_index,
                "imp_mat_type" => imp_mat_type,
                "improvement_subtype" => improvement_subtype,
                "improvement_type" => improvement_type,
                "injury_type" => injury_type,
                // "interaction_id" => interaction_id,
                "interaction_action" => interaction_action,
                "interaction_string" => interaction_string,
                "item_id" => item_id,
                "item_mat" => item_mat,
                "item_subtype" => item_subtype,
                "item_type" => item_type,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventII>, Error> {
        use crate::schema::historical_events_i_i::dsl::*;
        let query = historical_events_i_i;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEventII>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "he_id",
            "identity_id",
            "identity_id1",
            "identity_id2",
            "inherited",
            "initiating_en_id",
            "instigator_hf_id",
            "interaction",
            "interrogator_hf_id",
            "identity_caste",
            "identity_hf_id",
            "identity_name",
            "identity_nemesis_id",
            "identity_race",
            "imp_mat",
            "imp_mat_index",
            "imp_mat_type",
            "improvement_subtype",
            "improvement_type",
            "injury_type",
            // "interaction_id",
            "interaction_action",
            "interaction_string",
            "item_id",
            "item_mat",
            "item_subtype",
            "item_type",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventII],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_i_i::dsl::*;
        let query = historical_events_i_i.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "identity_id" => {identity_id: Option<i32>},
                "identity_id1" => {identity_id1: Option<i32>},
                "identity_id2" => {identity_id2: Option<i32>},
                "inherited" => {inherited: Option<bool>},
                "initiating_en_id" => {initiating_en_id: Option<i32>},
                "instigator_hf_id" => {instigator_hf_id: Option<i32>},
                "interaction" => {interaction: Option<i32>},
                "interrogator_hf_id" => {interrogator_hf_id: Option<i32>},
                "identity_caste" => {identity_caste: Option<String>},
                "identity_hf_id" => {identity_hf_id: Option<i32>},
                "identity_name" => {identity_name: Option<String>},
                "identity_nemesis_id" => {identity_nemesis_id: Option<i32>},
                "identity_race" => {identity_race: Option<String>},
                "imp_mat" => {imp_mat: Option<String>},
                "imp_mat_index" => {imp_mat_index: Option<i32>},
                "imp_mat_type" => {imp_mat_type: Option<i32>},
                "improvement_subtype" => {improvement_subtype: Option<String>},
                "improvement_type" => {improvement_type: Option<String>},
                "injury_type" => {injury_type: Option<String>},
                // "interaction_id" => {interaction_id: Option<i32>},
                "interaction_action" => {interaction_action: Option<String>},
                "interaction_string" => {interaction_string: Option<String>},
                "item_id" => {item_id: Option<i32>},
                "item_mat" => {item_mat: Option<String>},
                "item_subtype" => {item_subtype: Option<String>},
                "item_type" => {item_type: Option<String>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventII, df_st_core::HistoricalEvent> for HistoricalEventII {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.identity_id.add_missing_data(&source.identity_id);
        self.identity_id1.add_missing_data(&source.identity_id1);
        self.identity_id2.add_missing_data(&source.identity_id2);
        self.inherited.add_missing_data(&source.inherited);
        self.initiating_en_id.add_missing_data(&source.initiating_en_id);
        self.instigator_hf_id.add_missing_data(&source.instigator_hf_id);
        self.interaction.add_missing_data(&source.interaction);
        self.interrogator_hf_id.add_missing_data(&source.interrogator_hf_id);
        self.identity_caste.add_missing_data(&source.identity_caste);
        self.identity_hf_id.add_missing_data(&source.identity_hf_id);
        self.identity_name.add_missing_data(&source.identity_name);
        self.identity_nemesis_id.add_missing_data(&source.identity_nemesis_id);
        self.identity_race.add_missing_data(&source.identity_race);
        self.imp_mat.add_missing_data(&source.imp_mat);
        self.imp_mat_index.add_missing_data(&source.imp_mat_index);
        self.imp_mat_type.add_missing_data(&source.imp_mat_type);
        self.improvement_subtype.add_missing_data(&source.improvement_subtype);
        self.improvement_type.add_missing_data(&source.improvement_type);
        self.injury_type.add_missing_data(&source.injury_type);
        // self.interaction_id.add_missing_data(&source.interaction_id);
        self.interaction_action.add_missing_data(&source.interaction_action);
        self.interaction_string.add_missing_data(&source.interaction_string);
        self.item_id.add_missing_data(&source.item_id);
        self.item_mat.add_missing_data(&source.item_mat);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_type.add_missing_data(&source.item_type);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventII> for df_st_core::HistoricalEvent {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalEventII) {
        self.id.add_missing_data(&source.he_id);
        self.identity_id.add_missing_data(&source.identity_id);
        self.identity_id1.add_missing_data(&source.identity_id1);
        self.identity_id2.add_missing_data(&source.identity_id2);
        self.inherited.add_missing_data(&source.inherited);
        self.initiating_en_id.add_missing_data(&source.initiating_en_id);
        self.instigator_hf_id.add_missing_data(&source.instigator_hf_id);
        self.interaction.add_missing_data(&source.interaction);
        self.interrogator_hf_id.add_missing_data(&source.interrogator_hf_id);
        self.identity_caste.add_missing_data(&source.identity_caste);
        self.identity_hf_id.add_missing_data(&source.identity_hf_id);
        self.identity_name.add_missing_data(&source.identity_name);
        self.identity_nemesis_id.add_missing_data(&source.identity_nemesis_id);
        self.identity_race.add_missing_data(&source.identity_race);
        self.imp_mat.add_missing_data(&source.imp_mat);
        self.imp_mat_index.add_missing_data(&source.imp_mat_index);
        self.imp_mat_type.add_missing_data(&source.imp_mat_type);
        self.improvement_subtype.add_missing_data(&source.improvement_subtype);
        self.improvement_type.add_missing_data(&source.improvement_type);
        self.injury_type.add_missing_data(&source.injury_type);
        // self.interaction_id.add_missing_data(&source.interaction_id);
        self.interaction_action.add_missing_data(&source.interaction_action);
        self.interaction_string.add_missing_data(&source.interaction_string);
        self.item_id.add_missing_data(&source.item_id);
        self.item_mat.add_missing_data(&source.item_mat);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_type.add_missing_data(&source.item_type);
    }
}

impl PartialEq for HistoricalEventII {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventII {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventII> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventII) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventII {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
