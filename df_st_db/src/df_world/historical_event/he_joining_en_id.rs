use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_joining_en_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_joining_en_ids"]
#[primary_key(he_id, en_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HEJoiningENID {
    pub he_id: i32,
    pub en_id: i32,
    pub world_id: i32,
}

impl HEJoiningENID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HEJoiningENID, HEJoiningENID> for HEJoiningENID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_joining_en_ids: &[HEJoiningENID]) {
        diesel::insert_into(he_joining_en_ids::table)
            .values(he_joining_en_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving he_joining_en_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_joining_en_ids: &[HEJoiningENID]) {
        diesel::insert_into(he_joining_en_ids::table)
            .values(he_joining_en_ids)
            .execute(conn)
            .expect("Error saving he_joining_en_ids");
    }

    /// Get a list of HEJoiningENID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HEJoiningENID>, Error> {
        use crate::schema::he_joining_en_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_joining_en_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "en_id" => en_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "en_id" => en_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HEJoiningENID>, Error> {
        use crate::schema::he_joining_en_ids::dsl::*;
        let query = he_joining_en_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        Ok(query.first::<HEJoiningENID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["he_id", "en_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HEJoiningENID],
        core_list: Vec<HEJoiningENID>,
    ) -> Result<Vec<HEJoiningENID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_joining_en_ids::dsl::*;
        let query = he_joining_en_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "en_id" => {en_id: i32},
            };},
        };
    }
}

impl PartialEq for HEJoiningENID {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id && self.en_id == other.en_id
    }
}

impl Hash for HEJoiningENID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
        self.en_id.hash(state);
    }
}
