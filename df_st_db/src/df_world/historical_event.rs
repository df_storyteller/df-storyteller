use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::historical_events;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

mod he_a_hf_id;
mod he_bodies_hf_id;
mod he_circumstance;
mod he_competitor_hf_id;
mod he_conspirator_hf_id;
mod he_d_hf_id;
mod he_expelled_creature;
mod he_expelled_hf_id;
mod he_expelled_number;
mod he_expelled_pop_id;
mod he_groups_hf_id;
mod he_implicated_hf_id;
mod he_joining_en_id;
mod he_pet;
mod he_reason;
mod historical_event_a_a;
mod historical_event_b_b;
mod historical_event_c_c;
mod historical_event_d_d1;
mod historical_event_d_d2;
mod historical_event_e_g;
mod historical_event_h_h;
mod historical_event_i_i;
mod historical_event_j_m;
mod historical_event_n_o;
mod historical_event_p_p;
mod historical_event_q_r;
mod historical_event_s_s1;
mod historical_event_s_s2;
mod historical_event_t_t;
mod historical_event_u_w;
pub use he_a_hf_id::HEAHFID;
pub use he_bodies_hf_id::HEBodiesHFID;
pub use he_circumstance::HECircumstance;
pub use he_competitor_hf_id::HECompetitorHFID;
pub use he_conspirator_hf_id::HEConspiratorHFID;
pub use he_d_hf_id::HEDHFID;
pub use he_expelled_creature::HEExpelledCreature;
pub use he_expelled_hf_id::HEExpelledHFID;
pub use he_expelled_number::HEExpelledNumber;
pub use he_expelled_pop_id::HEExpelledPopID;
pub use he_groups_hf_id::HEGroupsHFID;
pub use he_implicated_hf_id::HEImplicatedHFID;
pub use he_joining_en_id::HEJoiningENID;
pub use he_pet::HEPet;
pub use he_reason::HEReason;
pub use historical_event_a_a::HistoricalEventAA;
pub use historical_event_b_b::HistoricalEventBB;
pub use historical_event_c_c::HistoricalEventCC;
pub use historical_event_d_d1::HistoricalEventDD1;
pub use historical_event_d_d2::HistoricalEventDD2;
pub use historical_event_e_g::HistoricalEventEG;
pub use historical_event_h_h::HistoricalEventHH;
pub use historical_event_i_i::HistoricalEventII;
pub use historical_event_j_m::HistoricalEventJM;
pub use historical_event_n_o::HistoricalEventNO;
pub use historical_event_p_p::HistoricalEventPP;
pub use historical_event_q_r::HistoricalEventQR;
pub use historical_event_s_s1::HistoricalEventSS1;
pub use historical_event_s_s2::HistoricalEventSS2;
pub use historical_event_t_t::HistoricalEventTT;
pub use historical_event_u_w::HistoricalEventUW;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "historical_events"]
pub struct HistoricalEvent {
    pub id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub year: Option<i32>,
    pub seconds72: Option<i32>,
}

impl HistoricalEvent {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEvent> for HistoricalEvent {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for historical_event in core_world.historical_events.values() {
            // Add circumstance_obj list
            if let Some(circumstance) = &historical_event.circumstance_obj {
                let mut db_circumstance = HECircumstance::new();
                db_circumstance.he_id = historical_event.id;
                db_circumstance.add_missing_data(circumstance);
                world.he_circumstances.push(db_circumstance);
            }
            // Add reason_obj list
            if let Some(reason) = &historical_event.reason_obj {
                let mut db_reason = HEReason::new();
                db_reason.he_id = historical_event.id;
                db_reason.add_missing_data(reason);
                // Don't add id everything is `None`
                if !(db_reason.type_.is_none()
                    && db_reason.glorify_hf_id.is_none()
                    && db_reason.artifact_is_heirloom_of_family_hf_id.is_none()
                    && db_reason.artifact_is_symbol_of_entity_position.is_none())
                {
                    world.he_reasons.push(db_reason);
                }
            }
            // Add bodies_hf_id list
            for hf_id in &historical_event.bodies_hf_id {
                world.he_bodies_hf_ids.push(HEBodiesHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_competitor_hf_id list
            for hf_id in &historical_event.competitor_hf_id {
                world.he_competitor_hf_ids.push(HECompetitorHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_conspirator_hf_id list
            for hf_id in &historical_event.conspirator_hf_id {
                world.he_conspirator_hf_ids.push(HEConspiratorHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_expelled_creature list
            for creature_id in &historical_event.expelled_creature {
                world.he_expelled_creatures.push(HEExpelledCreature {
                    he_id: historical_event.id,
                    creature_id: *creature_id,
                    ..Default::default()
                });
            }
            // Add he_expelled_hf_id list
            for hf_id in &historical_event.expelled_hf_id {
                world.he_expelled_hf_ids.push(HEExpelledHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_expelled_number list
            for number in &historical_event.expelled_number {
                world.he_expelled_numbers.push(HEExpelledNumber {
                    he_id: historical_event.id,
                    number: *number,
                    ..Default::default()
                });
            }
            // Add he_expelled_pop_id list
            for pop_id in &historical_event.expelled_pop_id {
                world.he_expelled_pop_ids.push(HEExpelledPopID {
                    he_id: historical_event.id,
                    pop_id: *pop_id,
                    ..Default::default()
                });
            }
            // Add he_groups_hf_id list
            for hf_id in &historical_event.groups_hf_id {
                world.he_groups_hf_ids.push(HEGroupsHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_implicated_hf_id list
            for hf_id in &historical_event.implicated_hf_id {
                world.he_implicated_hf_ids.push(HEImplicatedHFID {
                    he_id: historical_event.id,
                    hf_id: *hf_id,
                    ..Default::default()
                });
            }
            // Add he_joining_en_id list
            for en_id in &historical_event.joining_en_id {
                world.he_joining_en_ids.push(HEJoiningENID {
                    he_id: historical_event.id,
                    en_id: *en_id,
                    ..Default::default()
                });
            }
            // Add he_pet list
            for pet in &historical_event.pets {
                world.he_pets.push(HEPet {
                    he_id: historical_event.id,
                    pet: pet.clone(),
                    ..Default::default()
                });
            }
            // Add he_a_hf_id list
            for a_hf_id in &historical_event.a_hf_ids {
                world.he_a_hf_ids.push(HEAHFID {
                    he_id: historical_event.id,
                    hf_id: *a_hf_id,
                    ..Default::default()
                });
            }
            // Add he_d_hf_id list
            for d_hf_id in &historical_event.d_hf_ids {
                world.he_d_hf_ids.push(HEDHFID {
                    he_id: historical_event.id,
                    hf_id: *d_hf_id,
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events: &[HistoricalEvent]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events::table)
            .values(historical_events)
            .on_conflict((historical_events::id, historical_events::world_id))
            .do_update()
            .set((
                historical_events::type_.eq(excluded(historical_events::type_)),
                historical_events::year.eq(excluded(historical_events::year)),
                historical_events::seconds72.eq(excluded(historical_events::seconds72)),
            ))
            .execute(conn)
            .expect("Error saving historical_events");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events: &[HistoricalEvent]) {
        diesel::insert_into(historical_events::table)
            .values(historical_events)
            .execute(conn)
            .expect("Error saving historical_events");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEvent>, Error> {
        use crate::schema::historical_events::dsl::*;
        let query = historical_events;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<HistoricalEvent>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalEvent>, Error> {
        use crate::schema::historical_events::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = historical_events.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "year" => year,
                "seconds72" => seconds72,
            ],
            string_filter,
            [
                "type" => type_,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "type" => type_,
                "year" => year,
                "seconds72" => seconds72,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id", "year", "seconds72"],
            _ => vec!["id", "type", "year", "seconds72"],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[HistoricalEvent],
        _core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add HistoricalEventAA
        let he_a_a_list = HistoricalEventAA::belonging_to(db_list)
            .filter(crate::schema::historical_events_a_a::world_id.eq(world_id))
            .load::<HistoricalEventAA>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventBB
        let he_b_b_list = HistoricalEventBB::belonging_to(db_list)
            .filter(crate::schema::historical_events_b_b::world_id.eq(world_id))
            .load::<HistoricalEventBB>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventCC
        let he_c_c_list = HistoricalEventCC::belonging_to(db_list)
            .filter(crate::schema::historical_events_c_c::world_id.eq(world_id))
            .load::<HistoricalEventCC>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventDD1
        let he_d_d1_list = HistoricalEventDD1::belonging_to(db_list)
            .filter(crate::schema::historical_events_d_d1::world_id.eq(world_id))
            .load::<HistoricalEventDD1>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventDD2
        let he_d_d2_list = HistoricalEventDD2::belonging_to(db_list)
            .filter(crate::schema::historical_events_d_d2::world_id.eq(world_id))
            .load::<HistoricalEventDD2>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventEG
        let he_e_g_list = HistoricalEventEG::belonging_to(db_list)
            .filter(crate::schema::historical_events_e_g::world_id.eq(world_id))
            .load::<HistoricalEventEG>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventHH
        let he_h_h_list = HistoricalEventHH::belonging_to(db_list)
            .filter(crate::schema::historical_events_h_h::world_id.eq(world_id))
            .load::<HistoricalEventHH>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventII
        let he_i_i_list = HistoricalEventII::belonging_to(db_list)
            .filter(crate::schema::historical_events_i_i::world_id.eq(world_id))
            .load::<HistoricalEventII>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventJM
        let he_j_m_list = HistoricalEventJM::belonging_to(db_list)
            .filter(crate::schema::historical_events_j_m::world_id.eq(world_id))
            .load::<HistoricalEventJM>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventNO
        let he_n_o_list = HistoricalEventNO::belonging_to(db_list)
            .filter(crate::schema::historical_events_n_o::world_id.eq(world_id))
            .load::<HistoricalEventNO>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventPP
        let he_p_p_list = HistoricalEventPP::belonging_to(db_list)
            .filter(crate::schema::historical_events_p_p::world_id.eq(world_id))
            .load::<HistoricalEventPP>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventQR
        let he_q_r_list = HistoricalEventQR::belonging_to(db_list)
            .filter(crate::schema::historical_events_q_r::world_id.eq(world_id))
            .load::<HistoricalEventQR>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventSS1
        let he_s_s1_list = HistoricalEventSS1::belonging_to(db_list)
            .filter(crate::schema::historical_events_s_s1::world_id.eq(world_id))
            .load::<HistoricalEventSS1>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventSS2
        let he_s_s2_list = HistoricalEventSS2::belonging_to(db_list)
            .filter(crate::schema::historical_events_s_s2::world_id.eq(world_id))
            .load::<HistoricalEventSS2>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventTT
        let he_t_t_list = HistoricalEventTT::belonging_to(db_list)
            .filter(crate::schema::historical_events_t_t::world_id.eq(world_id))
            .load::<HistoricalEventTT>(conn)?
            .grouped_by(db_list);
        // Add HistoricalEventUW
        let he_u_w_list = HistoricalEventUW::belonging_to(db_list)
            .filter(crate::schema::historical_events_u_w::world_id.eq(world_id))
            .load::<HistoricalEventUW>(conn)?
            .grouped_by(db_list);
        // Add HECircumstance
        let circumstances_list = HECircumstance::belonging_to(db_list)
            .filter(crate::schema::he_circumstances::world_id.eq(world_id))
            .load::<HECircumstance>(conn)?
            .grouped_by(db_list);
        // Add HEReason
        let reason_list = HEReason::belonging_to(db_list)
            .filter(crate::schema::he_reasons::world_id.eq(world_id))
            .load::<HEReason>(conn)?
            .grouped_by(db_list);
        // Add HEBodiesHFID
        let bodies_list = HEBodiesHFID::belonging_to(db_list)
            .filter(crate::schema::he_bodies_hf_ids::world_id.eq(world_id))
            .load::<HEBodiesHFID>(conn)?
            .grouped_by(db_list);
        // Add HECompetitorHFID
        let competitors_list = HECompetitorHFID::belonging_to(db_list)
            .filter(crate::schema::he_competitor_hf_ids::world_id.eq(world_id))
            .load::<HECompetitorHFID>(conn)?
            .grouped_by(db_list);
        // Add HEConspiratorHFID
        let conspirators_list = HEConspiratorHFID::belonging_to(db_list)
            .filter(crate::schema::he_conspirator_hf_ids::world_id.eq(world_id))
            .load::<HEConspiratorHFID>(conn)?
            .grouped_by(db_list);
        // Add HEExpelledCreature
        let expelled_creatures_list = HEExpelledCreature::belonging_to(db_list)
            .filter(crate::schema::he_expelled_creatures::world_id.eq(world_id))
            .load::<HEExpelledCreature>(conn)?
            .grouped_by(db_list);
        // Add HEExpelledHFID
        let expelled_hfs_list = HEExpelledHFID::belonging_to(db_list)
            .filter(crate::schema::he_expelled_hf_ids::world_id.eq(world_id))
            .load::<HEExpelledHFID>(conn)?
            .grouped_by(db_list);
        // Add HEExpelledNumber
        let expelled_numbers_list = HEExpelledNumber::belonging_to(db_list)
            .filter(crate::schema::he_expelled_numbers::world_id.eq(world_id))
            .load::<HEExpelledNumber>(conn)?
            .grouped_by(db_list);
        // Add HEExpelledPopID
        let expelled_pops_list = HEExpelledPopID::belonging_to(db_list)
            .filter(crate::schema::he_expelled_pop_ids::world_id.eq(world_id))
            .load::<HEExpelledPopID>(conn)?
            .grouped_by(db_list);
        // Add HEGroupsHFID
        let groups_hfs_list = HEGroupsHFID::belonging_to(db_list)
            .filter(crate::schema::he_groups_hf_ids::world_id.eq(world_id))
            .load::<HEGroupsHFID>(conn)?
            .grouped_by(db_list);
        // Add HEImplicatedHFID
        let implicated_hfs_list = HEImplicatedHFID::belonging_to(db_list)
            .filter(crate::schema::he_implicated_hf_ids::world_id.eq(world_id))
            .load::<HEImplicatedHFID>(conn)?
            .grouped_by(db_list);
        // Add HEJoiningENID
        let joining_ens_list = HEJoiningENID::belonging_to(db_list)
            .filter(crate::schema::he_joining_en_ids::world_id.eq(world_id))
            .load::<HEJoiningENID>(conn)?
            .grouped_by(db_list);
        // Add HEPets
        let pets_list = HEPet::belonging_to(db_list)
            .filter(crate::schema::he_pets::world_id.eq(world_id))
            .load::<HEPet>(conn)?
            .grouped_by(db_list);
        // Add HEAHFIDs
        let he_a_hf_id_list = HEAHFID::belonging_to(db_list)
            .filter(crate::schema::he_a_hf_ids::world_id.eq(world_id))
            .load::<HEAHFID>(conn)?
            .grouped_by(db_list);
        // Add HEDHFIDs
        let he_d_hf_id_list = HEDHFID::belonging_to(db_list)
            .filter(crate::schema::he_d_hf_ids::world_id.eq(world_id))
            .load::<HEDHFID>(conn)?
            .grouped_by(db_list);

        // Merge all
        let mut core_list: Vec<df_st_core::HistoricalEvent> = Vec::new();
        for (index, he) in db_list.iter().enumerate() {
            let mut core_he = df_st_core::HistoricalEvent::default();
            core_he.add_missing_data(he);
            let he_a_a = he_a_a_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_a_a);
            let he_b_b = he_b_b_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_b_b);
            let he_c_c = he_c_c_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_c_c);
            let he_d_d1 = he_d_d1_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_d_d1);
            let he_d_d2 = he_d_d2_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_d_d2);
            let he_e_g = he_e_g_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_e_g);
            let he_h_h = he_h_h_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_h_h);
            let he_i_i = he_i_i_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_i_i);
            let he_j_m = he_j_m_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_j_m);
            let he_n_o = he_n_o_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_n_o);
            let he_p_p = he_p_p_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_p_p);
            let he_q_r = he_q_r_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_q_r);
            let he_s_s1 = he_s_s1_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_s_s1);
            let he_s_s2 = he_s_s2_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_s_s2);
            let he_t_t = he_t_t_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_t_t);
            let he_u_w = he_u_w_list.get(index).unwrap().get(0).unwrap();
            core_he.add_missing_data(he_u_w);

            let circumstance = circumstances_list.get(index).unwrap().get(0);
            if let Some(circumstance) = circumstance {
                core_he.add_missing_data(circumstance);
            }

            let reason = reason_list.get(index).unwrap().get(0);
            if let Some(reason) = reason {
                core_he.add_missing_data(reason);
            }

            for body in bodies_list.get(index).unwrap() {
                core_he.bodies_hf_id.push(body.hf_id);
            }
            for competitor in competitors_list.get(index).unwrap() {
                core_he.competitor_hf_id.push(competitor.hf_id);
            }
            for conspirator in conspirators_list.get(index).unwrap() {
                core_he.conspirator_hf_id.push(conspirator.hf_id);
            }
            for expelled_creature in expelled_creatures_list.get(index).unwrap() {
                core_he
                    .expelled_creature
                    .push(expelled_creature.creature_id);
            }
            for expelled_hf in expelled_hfs_list.get(index).unwrap() {
                core_he.expelled_hf_id.push(expelled_hf.hf_id);
            }
            for expelled_number in expelled_numbers_list.get(index).unwrap() {
                core_he.expelled_number.push(expelled_number.number);
            }
            for expelled_pop in expelled_pops_list.get(index).unwrap() {
                core_he.expelled_pop_id.push(expelled_pop.pop_id);
            }
            for groups_hf in groups_hfs_list.get(index).unwrap() {
                core_he.groups_hf_id.push(groups_hf.hf_id);
            }
            for implicated_hf in implicated_hfs_list.get(index).unwrap() {
                core_he.implicated_hf_id.push(implicated_hf.hf_id);
            }
            for joining_en in joining_ens_list.get(index).unwrap() {
                core_he.joining_en_id.push(joining_en.en_id);
            }
            for pet in pets_list.get(index).unwrap() {
                core_he.pets.push(pet.pet.clone());
            }
            for a_hf_id in he_a_hf_id_list.get(index).unwrap() {
                core_he.a_hf_ids.push(a_hf_id.hf_id);
            }
            for d_hf_id in he_d_hf_id_list.get(index).unwrap() {
                core_he.d_hf_ids.push(d_hf_id.hf_id);
            }

            core_list.push(core_he);
        }

        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::historical_events::dsl::*;
        let query = historical_events.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "type" => type_,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "type" => {type_: Option<String>},
                "year" => {year: Option<i32>},
                "seconds72" => {seconds72: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HistoricalEvent, df_st_core::HistoricalEvent> for HistoricalEvent {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.year.add_missing_data(&source.year);
        self.seconds72.add_missing_data(&source.seconds72);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEvent> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEvent) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.year.add_missing_data(&source.year);
        self.seconds72.add_missing_data(&source.seconds72);
    }
}

impl PartialEq<HistoricalEvent> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEvent) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEvent {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.id == other.id
    }
}
