use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, EntityPopulation};
use crate::schema::entity_population_races;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "entity_population_races"]
#[belongs_to(EntityPopulation)]
pub struct EntityPopulationRace {
    pub id: i32,
    pub world_id: i32,
    pub entity_population_id: i32,
    pub race: Option<String>,
    pub amount: Option<i32>,
}

impl EntityPopulationRace {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityPopulationRace, EntityPopulationRace> for EntityPopulationRace {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for entity_population in core_world.entity_populations.values() {
            for race in &entity_population.races {
                let new_id: i32 = world.entity_population_races.len() as i32;
                world.entity_population_races.push(EntityPopulationRace {
                    id: new_id,
                    entity_population_id: entity_population.id,
                    race: Some(race.clone()),
                    amount: None,
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_population_races: &[EntityPopulationRace]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_population_races::table)
            .values(entity_population_races)
            .on_conflict((
                entity_population_races::id,
                entity_population_races::world_id,
            ))
            .do_update()
            .set((
                entity_population_races::entity_population_id
                    .eq(excluded(entity_population_races::entity_population_id)),
                entity_population_races::race.eq(excluded(entity_population_races::race)),
                entity_population_races::amount.eq(excluded(entity_population_races::amount)),
            ))
            .execute(conn)
            .expect("Error saving entity_population_races");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_population_races: &[EntityPopulationRace]) {
        diesel::insert_into(entity_population_races::table)
            .values(entity_population_races)
            .execute(conn)
            .expect("Error saving entity_population_races");
    }

    /// Get a list of entity_population_races from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityPopulationRace>, Error> {
        use crate::schema::entity_population_races::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_population_races.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "entity_population_id" => entity_population_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "entity_population_id" => entity_population_id,
                "race" => race,
                "amount" => amount,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityPopulationRace>, Error> {
        use crate::schema::entity_population_races::dsl::*;
        let query = entity_population_races;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        let query = query
            .filter(entity_population_id.eq(id_filter.get("entity_population_id").unwrap_or(&0)));
        Ok(query.first::<EntityPopulationRace>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["id", "entity_population_id", "race", "amount"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityPopulationRace],
        core_list: Vec<EntityPopulationRace>,
    ) -> Result<Vec<EntityPopulationRace>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_population_races::dsl::*;
        let query = entity_population_races
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "entity_population_id" => entity_population_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "entity_population_id" => {entity_population_id: i32},
                "race" => {race: Option<String>},
                "amount" => {amount: Option<i32>},
            };},
        };
    }
}
