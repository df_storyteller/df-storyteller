use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::entity_populations;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

mod entity_population_race;
pub use entity_population_race::*;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "entity_populations"]
pub struct EntityPopulation {
    pub id: i32,
    pub world_id: i32,
    pub civ_id: Option<i32>,
}

impl EntityPopulation {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityPopulation, EntityPopulation> for EntityPopulation {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {}

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_populations: &[EntityPopulation]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_populations::table)
            .values(entity_populations)
            .on_conflict((entity_populations::id, entity_populations::world_id))
            .do_update()
            .set((entity_populations::civ_id.eq(excluded(entity_populations::civ_id)),))
            .execute(conn)
            .expect("Error saving entity_populations");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_populations: &[EntityPopulation]) {
        diesel::insert_into(entity_populations::table)
            .values(entity_populations)
            .execute(conn)
            .expect("Error saving entity_populations");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityPopulation>, Error> {
        use crate::schema::entity_populations::dsl::*;
        let query = entity_populations;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<EntityPopulation>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityPopulation>, Error> {
        use crate::schema::entity_populations::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_populations.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "civ_id" => civ_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "civ_id" => civ_id,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id", "civ_id"],
            _ => vec!["id", "civ_id"],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[EntityPopulation],
        mut core_list: Vec<df_st_core::EntityPopulation>,
    ) -> Result<Vec<df_st_core::EntityPopulation>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add EntityPopulationRace
        let race_list = EntityPopulationRace::belonging_to(db_list)
            .filter(crate::schema::entity_population_races::world_id.eq(world_id))
            .load::<EntityPopulationRace>(conn)?
            .grouped_by(db_list);

        let core_list2: Vec<df_st_core::EntityPopulation> = db_list
            .iter()
            .zip(race_list)
            .map(|(entity_pop, entity_pop_races)| {
                let mut core_entity_pop = df_st_core::EntityPopulation::default();
                core_entity_pop.add_missing_data(entity_pop);
                for entity_pop_race in entity_pop_races {
                    if let Some(race) = entity_pop_race.race {
                        core_entity_pop.races.push(race);
                    }
                }
                core_entity_pop
            })
            .collect();
        core_list.add_missing_data(&core_list2);
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_populations::dsl::*;
        let query = entity_populations.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "civ_id" => civ_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "civ_id" => {civ_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityPopulation, df_st_core::EntityPopulation> for EntityPopulation {
    fn add_missing_data(&mut self, source: &df_st_core::EntityPopulation) {
        self.id.add_missing_data(&source.id);
        self.civ_id.add_missing_data(&source.civ_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityPopulation, EntityPopulation> for df_st_core::EntityPopulation {
    fn add_missing_data(&mut self, source: &EntityPopulation) {
        self.id.add_missing_data(&source.id);
        self.civ_id.add_missing_data(&source.civ_id);
    }
}

impl PartialEq<EntityPopulation> for df_st_core::EntityPopulation {
    fn eq(&self, other: &EntityPopulation) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::EntityPopulation> for EntityPopulation {
    fn eq(&self, other: &df_st_core::EntityPopulation) -> bool {
        self.id == other.id
    }
}
