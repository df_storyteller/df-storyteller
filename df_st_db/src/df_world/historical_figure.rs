use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::historical_figures;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;
use std::convert::TryInto;

mod hf_entity_link;
mod hf_entity_position_link;
mod hf_entity_reputation;
mod hf_entity_squad_link;
mod hf_honor_entity;
mod hf_honor_id;
mod hf_interaction_knowledge;
mod hf_intrigue_actor;
mod hf_intrigue_plot;
mod hf_journey_pet;
mod hf_link;
mod hf_plot_actor;
mod hf_relationship_profile_hf;
mod hf_site_link;
mod hf_site_property;
mod hf_skill;
mod hf_sphere;
mod hf_vague_relationship;
pub use hf_entity_link::HFEntityLink;
pub use hf_entity_position_link::HFEntityPositionLink;
pub use hf_entity_reputation::HFEntityReputation;
pub use hf_entity_squad_link::HFEntitySquadLink;
pub use hf_honor_entity::HFHonorEntity;
pub use hf_honor_id::HFHonorID;
pub use hf_interaction_knowledge::HFInteractionKnowledge;
pub use hf_intrigue_actor::HFIntrigueActor;
pub use hf_intrigue_plot::HFIntriguePlot;
pub use hf_journey_pet::HFJourneyPets;
pub use hf_link::HFLink;
pub use hf_plot_actor::HFPlotActor;
pub use hf_relationship_profile_hf::HFRelationshipProfileHF;
pub use hf_site_link::HFSiteLink;
pub use hf_site_property::HFSiteProperty;
pub use hf_skill::HFSkill;
pub use hf_sphere::HFSpheres;
pub use hf_vague_relationship::HFVagueRelationship;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "historical_figures"]
pub struct HistoricalFigure {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub race: Option<String>,
    pub race_id: Option<String>,
    pub caste: Option<String>,
    pub appeared: Option<i32>,
    pub birth_year: Option<i32>,
    pub birth_seconds72: Option<i32>,
    pub death_year: Option<i32>,
    pub death_seconds72: Option<i32>,
    pub associated_type: Option<String>,
    pub deity: Option<String>,
    pub goal: Option<String>,
    pub ent_pop_id: Option<i32>,
    pub active_interaction: Option<String>,
    pub force: Option<bool>,
    pub current_identity_id: Option<i32>,
    pub holds_artifact: Option<i32>,
    pub used_identity_id: Option<i32>,
    pub animated: Option<bool>,
    pub animated_string: Option<String>,
    pub adventurer: Option<bool>,
}

impl HistoricalFigure {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalFigure, HistoricalFigure> for HistoricalFigure {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for historical_figure in core_world.historical_figures.values() {
            // Add entity_link list
            for entity_link in &historical_figure.entity_link {
                let mut db_entity_link = HFEntityLink::new();
                db_entity_link.add_missing_data(entity_link);
                db_entity_link.hf_id = historical_figure.id;
                world.hf_entity_links.push(db_entity_link);
            }
            // Add entity_position_link list
            for entity_position_link in &historical_figure.entity_position_link {
                let mut db_entity_pos_link = HFEntityPositionLink::new();
                let new_id: i32 = world.hf_entity_position_links.len().try_into().unwrap();
                db_entity_pos_link.add_missing_data(entity_position_link);
                db_entity_pos_link.hf_id = historical_figure.id;
                db_entity_pos_link.id = new_id;
                world.hf_entity_position_links.push(db_entity_pos_link);
            }
            // Add entity_position_link list
            for site_link in &historical_figure.site_link {
                let mut db_site_links = HFSiteLink::new();
                db_site_links.add_missing_data(site_link);
                db_site_links.hf_id = historical_figure.id;
                world.hf_site_links.push(db_site_links);
            }
            // Add skill list
            for skill in &historical_figure.skills {
                let mut db_skill = HFSkill::new();
                db_skill.add_missing_data(skill);
                db_skill.hf_id = historical_figure.id;
                world.hf_skills.push(db_skill);
            }
            // Add relationship_profile_hf list
            for core_item in &historical_figure.relationship_profile_hf {
                let mut db_item = HFRelationshipProfileHF::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_relationship_profile_hf.push(db_item);
            }
            // Add intrigue_actor list
            for core_item in &historical_figure.intrigue_actor {
                let mut db_item = HFIntrigueActor::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_intrigue_actors.push(db_item);
            }
            // Add intrigue_plot list
            for intrigue_plot in &historical_figure.intrigue_plot {
                let mut db_intrigue_plot = HFIntriguePlot::new();
                db_intrigue_plot.add_missing_data(intrigue_plot);
                db_intrigue_plot.hf_id = historical_figure.id;
                let local_id = db_intrigue_plot.local_id;
                world.hf_intrigue_plots.push(db_intrigue_plot);

                // Add plot_actor list
                for core_item in &intrigue_plot.plot_actor {
                    let mut db_item = HFPlotActor::new();
                    db_item.add_missing_data(core_item);
                    db_item.hf_id = historical_figure.id;
                    db_item.local_id = local_id;
                    world.hf_plot_actors.push(db_item);
                }
            }
            // Add entity_reputation list
            for core_item in &historical_figure.entity_reputation {
                let mut db_item = HFEntityReputation::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_entity_reputations.push(db_item);
            }
            // Add vague_relationship list
            for core_item in &historical_figure.vague_relationship {
                let mut db_item = HFVagueRelationship::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_vague_relationships.push(db_item);
            }
            // Add entity_squad_link list
            for core_item in &historical_figure.entity_squad_link {
                let mut db_item = HFEntitySquadLink::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_entity_squad_links.push(db_item);
            }
            // Add hf_links list
            for core_item in &historical_figure.links {
                let mut db_item = HFLink::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_links.push(db_item);
            }
            // Add honor_entity list
            for honor_entity in &historical_figure.honor_entity {
                let mut db_honor_entity = HFHonorEntity::new();
                db_honor_entity.add_missing_data(honor_entity);
                db_honor_entity.hf_id = historical_figure.id;
                let entity_id = db_honor_entity.entity_id;
                world.hf_honor_entities.push(db_honor_entity);

                // Add honor_id list
                for core_item in &honor_entity.honor_id {
                    let mut db_item = HFHonorID::new();
                    db_item.hf_id = historical_figure.id;
                    db_item.entity_id = entity_id;
                    db_item.honor_id = *core_item;
                    world.hf_honor_ids.push(db_item);
                }
            }
            // Add site_property list
            for core_item in &historical_figure.site_property {
                let mut db_item = HFSiteProperty::new();
                db_item.add_missing_data(core_item);
                db_item.hf_id = historical_figure.id;
                world.hf_site_properties.push(db_item);
            }
            // Add sphere list
            for core_item in &historical_figure.sphere {
                let mut db_item = HFSpheres::new();
                db_item.hf_id = historical_figure.id;
                db_item.sphere = core_item.clone();
                world.hf_spheres.push(db_item);
            }
            // Add interaction_knowledge list
            for core_item in &historical_figure.interaction_knowledge {
                let mut db_item = HFInteractionKnowledge::new();
                db_item.hf_id = historical_figure.id;
                db_item.interaction_knowledge = core_item.clone();
                world.hf_interaction_knowledges.push(db_item);
            }
            // Add journey_pet list
            for core_item in &historical_figure.journey_pet {
                let mut db_item = HFJourneyPets::new();
                db_item.hf_id = historical_figure.id;
                db_item.journey_pet = core_item.clone();
                world.hf_journey_pets.push(db_item);
            }
        }
    }

    #[cfg(feature = "postgres")]
    #[rustfmt::skip]
    fn insert_into_db(conn: &DbConnection, historical_figures: &[HistoricalFigure]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_figures::table)
            .values(historical_figures)
            .on_conflict((historical_figures::id, historical_figures::world_id))
            .do_update()
            .set((
                historical_figures::name.eq(excluded(historical_figures::name)),
                historical_figures::race.eq(excluded(historical_figures::race)),
                historical_figures::race_id.eq(excluded(historical_figures::race_id)),
                historical_figures::caste.eq(excluded(historical_figures::caste)),
                historical_figures::appeared.eq(excluded(historical_figures::appeared)),
                historical_figures::birth_year.eq(excluded(historical_figures::birth_year)),
                historical_figures::birth_seconds72.eq(excluded(historical_figures::birth_seconds72)),
                historical_figures::death_year.eq(excluded(historical_figures::death_year)),
                historical_figures::death_seconds72.eq(excluded(historical_figures::death_seconds72)),
                historical_figures::associated_type.eq(excluded(historical_figures::associated_type)),
                historical_figures::deity.eq(excluded(historical_figures::deity)),
                historical_figures::goal.eq(excluded(historical_figures::goal)),
                historical_figures::ent_pop_id.eq(excluded(historical_figures::ent_pop_id)),
                historical_figures::active_interaction.eq(excluded(historical_figures::active_interaction)),
                historical_figures::force.eq(excluded(historical_figures::force)),
                historical_figures::current_identity_id.eq(excluded(historical_figures::current_identity_id)),
                historical_figures::holds_artifact.eq(excluded(historical_figures::holds_artifact)),
                historical_figures::used_identity_id.eq(excluded(historical_figures::used_identity_id)),
                historical_figures::animated.eq(excluded(historical_figures::animated)),
                historical_figures::animated_string.eq(excluded(historical_figures::animated_string)),
                historical_figures::adventurer.eq(excluded(historical_figures::adventurer)),
            ))
            .execute(conn)
            .expect("Error saving historical_figures");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_figures: &[HistoricalFigure]) {
        diesel::insert_into(historical_figures::table)
            .values(historical_figures)
            .execute(conn)
            .expect("Error saving historical_figures");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalFigure>, Error> {
        use crate::schema::historical_figures::dsl::*;
        let query = historical_figures;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<HistoricalFigure>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<HistoricalFigure>, Error> {
        use crate::schema::historical_figures::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = historical_figures.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "appeared" => appeared,
                "birth_year" => birth_year,
                "birth_seconds72" => birth_seconds72,
                "death_year" => death_year,
                "death_seconds72" => death_seconds72,
                "ent_pop_id" => ent_pop_id,
                "current_identity_id" => current_identity_id,
                "holds_artifact" => holds_artifact,
                "used_identity_id" => used_identity_id,
            ],
            string_filter,
            [
                "name" => name,
                "race" => race,
                "race_id" => race_id,
                "caste" => caste,
                "associated_type" => associated_type,
                "deity" => deity,
                "goal" => goal,
                "active_interaction" => active_interaction,
                "animated_string" => animated_string,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "race" => race,
                "race_id" => race_id,
                "caste" => caste,
                "appeared" => appeared,
                "birth_year" => birth_year,
                "birth_seconds72" => birth_seconds72,
                "death_year" => death_year,
                "death_seconds72" => death_seconds72,
                "associated_type" => associated_type,
                "deity" => deity,
                "goal" => goal,
                "ent_pop_id" => ent_pop_id,
                "active_interaction" => active_interaction,
                "force" => force,
                "current_identity_id" => current_identity_id,
                "holds_artifact" => holds_artifact,
                "used_identity_id" => used_identity_id,
                "animated" => animated,
                "animated_string" => animated_string,
                "adventurer" => adventurer,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec![
                "id",
                "appeared",
                "birth_year",
                "birth_seconds72",
                "death_year",
                "death_seconds72",
                "ent_pop_id",
                "current_identity_id",
                "holds_artifact",
                "used_identity_id",
            ],
            _ => vec![
                "id",
                "name",
                "race",
                "race_id",
                "caste",
                "appeared",
                "birth_year",
                "birth_seconds72",
                "death_year",
                "death_seconds72",
                "associated_type",
                "deity",
                "goal",
                "ent_pop_id",
                "active_interaction",
                "force",
                "current_identity_id",
                "holds_artifact",
                "used_identity_id",
                "animated",
                "animated_string",
                "adventurer",
            ],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[HistoricalFigure],
        _core_list: Vec<df_st_core::HistoricalFigure>,
    ) -> Result<Vec<df_st_core::HistoricalFigure>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add HFEntityLink
        let hf_entity_links_list = HFEntityLink::belonging_to(db_list)
            .filter(crate::schema::hf_entity_links::world_id.eq(world_id))
            .load::<HFEntityLink>(conn)?
            .grouped_by(db_list);
        // Add HFEntityPositionLink
        let hf_entity_pos_links_list = HFEntityPositionLink::belonging_to(db_list)
            .filter(crate::schema::hf_entity_position_links::world_id.eq(world_id))
            .load::<HFEntityPositionLink>(conn)?
            .grouped_by(db_list);
        // Add HFSiteLink
        let hf_site_links_list = HFSiteLink::belonging_to(db_list)
            .filter(crate::schema::hf_site_links::world_id.eq(world_id))
            .load::<HFSiteLink>(conn)?
            .grouped_by(db_list);
        // Add HFSkill
        let hf_skills_list = HFSkill::belonging_to(db_list)
            .filter(crate::schema::hf_skills::world_id.eq(world_id))
            .load::<HFSkill>(conn)?
            .grouped_by(db_list);
        // Add HFRelationshipProfileHF
        let hf_rel_pro_hf_list = HFRelationshipProfileHF::belonging_to(db_list)
            .filter(crate::schema::hf_relationship_profile_hf::world_id.eq(world_id))
            .load::<HFRelationshipProfileHF>(conn)?
            .grouped_by(db_list);
        // Add HFIntrigueActor
        let hf_intrigue_actors_list = HFIntrigueActor::belonging_to(db_list)
            .filter(crate::schema::hf_intrigue_actors::world_id.eq(world_id))
            .load::<HFIntrigueActor>(conn)?
            .grouped_by(db_list);
        // Add HFIntriguePlot
        let hf_intrigue_plots_list = HFIntriguePlot::belonging_to(db_list)
            .filter(crate::schema::hf_intrigue_plots::world_id.eq(world_id))
            .load::<HFIntriguePlot>(conn)?
            .grouped_by(db_list);
        // Add HFPlotActor
        let hf_plot_actors_list = HFPlotActor::belonging_to(db_list)
            .filter(crate::schema::hf_plot_actors::world_id.eq(world_id))
            .load::<HFPlotActor>(conn)?
            .grouped_by(db_list);
        // Add HFEntityReputation
        let hf_entity_reputations_list = HFEntityReputation::belonging_to(db_list)
            .filter(crate::schema::hf_entity_reputations::world_id.eq(world_id))
            .load::<HFEntityReputation>(conn)?
            .grouped_by(db_list);
        // Add HFVagueRelationship
        let hf_vague_relationships_list = HFVagueRelationship::belonging_to(db_list)
            .filter(crate::schema::hf_vague_relationships::world_id.eq(world_id))
            .load::<HFVagueRelationship>(conn)?
            .grouped_by(db_list);
        // Add HFEntitySquadLink
        let hf_entity_squad_links_list = HFEntitySquadLink::belonging_to(db_list)
            .filter(crate::schema::hf_entity_squad_links::world_id.eq(world_id))
            .load::<HFEntitySquadLink>(conn)?
            .grouped_by(db_list);
        // Add HFLink
        let hf_links_list = HFLink::belonging_to(db_list)
            .filter(crate::schema::hf_links::world_id.eq(world_id))
            .load::<HFLink>(conn)?
            .grouped_by(db_list);
        // Add HFHonorEntity
        let hf_honor_entities_list = HFHonorEntity::belonging_to(db_list)
            .filter(crate::schema::hf_honor_entities::world_id.eq(world_id))
            .load::<HFHonorEntity>(conn)?
            .grouped_by(db_list);
        // Add HFSiteProperty
        let hf_site_properties_list = HFSiteProperty::belonging_to(db_list)
            .filter(crate::schema::hf_site_properties::world_id.eq(world_id))
            .load::<HFSiteProperty>(conn)?
            .grouped_by(db_list);
        // Add HFSpheres
        let hf_spheres_list = HFSpheres::belonging_to(db_list)
            .filter(crate::schema::hf_spheres::world_id.eq(world_id))
            .load::<HFSpheres>(conn)?
            .grouped_by(db_list);
        // Add HFInteractionKnowledge
        let hf_interaction_knowledge_list = HFInteractionKnowledge::belonging_to(db_list)
            .filter(crate::schema::hf_interaction_knowledges::world_id.eq(world_id))
            .load::<HFInteractionKnowledge>(conn)?
            .grouped_by(db_list);
        // Add HFJourneyPets
        let hf_journey_pets_list = HFJourneyPets::belonging_to(db_list)
            .filter(crate::schema::hf_journey_pets::world_id.eq(world_id))
            .load::<HFJourneyPets>(conn)?
            .grouped_by(db_list);
        // Add HFHonorID
        let hf_honor_ids_list = HFHonorID::belonging_to(db_list)
            .filter(crate::schema::hf_honor_ids::world_id.eq(world_id))
            .load::<HFHonorID>(conn)?
            .grouped_by(db_list);

        // Merge all
        let mut core_list: Vec<df_st_core::HistoricalFigure> = Vec::new();
        for (index, hf) in db_list.iter().enumerate() {
            let mut core_hf = df_st_core::HistoricalFigure::default();
            core_hf.add_missing_data(hf);

            for entity_link in hf_entity_links_list.get(index).unwrap() {
                core_hf
                    .entity_link
                    .add_missing_data(&vec![entity_link.clone()]);
            }
            for entity_pos_link in hf_entity_pos_links_list.get(index).unwrap() {
                core_hf
                    .entity_position_link
                    .add_missing_data(&vec![entity_pos_link.clone()]);
            }
            for site_link in hf_site_links_list.get(index).unwrap() {
                core_hf.site_link.add_missing_data(&vec![site_link.clone()]);
            }
            for skill in hf_skills_list.get(index).unwrap() {
                core_hf.skills.add_missing_data(&vec![skill.clone()]);
            }
            for item in hf_rel_pro_hf_list.get(index).unwrap() {
                core_hf
                    .relationship_profile_hf
                    .add_missing_data(&vec![item.clone()]);
            }
            for item in hf_intrigue_actors_list.get(index).unwrap() {
                core_hf.intrigue_actor.add_missing_data(&vec![item.clone()]);
            }
            for item in hf_intrigue_plots_list.get(index).unwrap() {
                let local_id = item.local_id;
                let mut intrigue_plot = df_st_core::HFIntriguePlot::default();
                intrigue_plot.add_missing_data(item);
                for item in hf_plot_actors_list.get(index).unwrap() {
                    if local_id == item.local_id {
                        intrigue_plot
                            .plot_actor
                            .add_missing_data(&vec![item.clone()]);
                    }
                }
                core_hf.intrigue_plot.push(intrigue_plot);
            }
            for item in hf_entity_reputations_list.get(index).unwrap() {
                core_hf
                    .entity_reputation
                    .add_missing_data(&vec![item.clone()]);
            }
            for item in hf_vague_relationships_list.get(index).unwrap() {
                core_hf
                    .vague_relationship
                    .add_missing_data(&vec![item.clone()]);
            }
            for item in hf_entity_squad_links_list.get(index).unwrap() {
                core_hf
                    .entity_squad_link
                    .add_missing_data(&vec![item.clone()]);
            }
            for item in hf_links_list.get(index).unwrap() {
                core_hf.links.add_missing_data(&vec![item.clone()]);
            }
            for item in hf_honor_entities_list.get(index).unwrap() {
                let entity_id = item.entity_id;
                let mut honor_entity = df_st_core::HFHonorEntity::default();
                honor_entity.add_missing_data(item);
                for item in hf_honor_ids_list.get(index).unwrap() {
                    if entity_id == item.entity_id {
                        honor_entity.honor_id.push(item.honor_id);
                    }
                }
                core_hf.honor_entity.push(honor_entity);
            }
            for item in hf_site_properties_list.get(index).unwrap() {
                core_hf.site_property.add_missing_data(&vec![item.clone()]);
            }
            for item in hf_spheres_list.get(index).unwrap() {
                core_hf.sphere.push(item.sphere.clone());
            }
            for item in hf_interaction_knowledge_list.get(index).unwrap() {
                core_hf
                    .interaction_knowledge
                    .push(item.interaction_knowledge.clone());
            }
            for item in hf_journey_pets_list.get(index).unwrap() {
                core_hf.journey_pet.push(item.journey_pet.clone());
            }
            // for item in hf_honor_ids_list.get(index).unwrap(){
            //     core_hf.skills.add_missing_data(&vec![item.clone()]);
            // }

            core_list.push(core_hf);
        }

        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::historical_figures::dsl::*;
        let query = historical_figures.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "appeared" => appeared,
                "birth_year" => birth_year,
                "birth_seconds72" => birth_seconds72,
                "death_year" => death_year,
                "death_seconds72" => death_seconds72,
                "ent_pop_id" => ent_pop_id,
                "current_identity_id" => current_identity_id,
                "holds_artifact" => holds_artifact,
                "used_identity_id" => used_identity_id,
            ],
            string_filter,
            [
                "name" => name,
                "race" => race,
                "race_id" => race_id,
                "caste" => caste,
                "associated_type" => associated_type,
                "deity" => deity,
                "goal" => goal,
                "active_interaction" => active_interaction,
                "animated_string" => animated_string,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "race" => {race: Option<String>},
                "race_id" => {race_id: Option<String>},
                "caste" => {caste: Option<String>},
                "appeared" => {appeared: Option<i32>},
                "birth_year" => {birth_year: Option<i32>},
                "birth_seconds72" => {birth_seconds72: Option<i32>},
                "death_year" => {death_year: Option<i32>},
                "death_seconds72" => {death_seconds72: Option<i32>},
                "associated_type" => {associated_type: Option<String>},
                "deity" => {deity: Option<String>},
                "goal" => {goal: Option<String>},
                "ent_pop_id" => {ent_pop_id: Option<i32>},
                "active_interaction" => {active_interaction: Option<String>},
                "force" => {force: Option<bool>},
                "current_identity_id" => {current_identity_id: Option<i32>},
                "holds_artifact" => {holds_artifact: Option<i32>},
                "used_identity_id" => {used_identity_id: Option<i32>},
                "animated" => {animated: Option<bool>},
                "animated_string" => {animated_string: Option<String>},
                "adventurer" => {adventurer: Option<bool>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HistoricalFigure, df_st_core::HistoricalFigure> for HistoricalFigure {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalFigure) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.race.add_missing_data(&source.race);
        self.race_id.add_missing_data(&source.race_id);
        self.caste.add_missing_data(&source.caste);
        self.appeared.add_missing_data(&source.appeared);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_seconds72.add_missing_data(&source.birth_seconds72);
        self.death_year.add_missing_data(&source.death_year);
        self.death_seconds72.add_missing_data(&source.death_seconds72);
        self.associated_type.add_missing_data(&source.associated_type);
        self.deity.add_missing_data(&source.deity);
        self.goal.add_missing_data(&source.goal);
        self.ent_pop_id.add_missing_data(&source.ent_pop_id);
        self.active_interaction.add_missing_data(&source.active_interaction);
        self.force.add_missing_data(&source.force);
        self.current_identity_id.add_missing_data(&source.current_identity_id);
        self.holds_artifact.add_missing_data(&source.holds_artifact);
        self.used_identity_id.add_missing_data(&source.used_identity_id);
        self.animated.add_missing_data(&source.animated);
        self.animated_string.add_missing_data(&source.animated_string);
        self.adventurer.add_missing_data(&source.adventurer);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalFigure, HistoricalFigure> for df_st_core::HistoricalFigure {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &HistoricalFigure) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.race.add_missing_data(&source.race);
        self.race_id.add_missing_data(&source.race_id);
        self.caste.add_missing_data(&source.caste);
        self.appeared.add_missing_data(&source.appeared);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_seconds72.add_missing_data(&source.birth_seconds72);
        self.death_year.add_missing_data(&source.death_year);
        self.death_seconds72.add_missing_data(&source.death_seconds72);
        self.associated_type.add_missing_data(&source.associated_type);
        self.deity.add_missing_data(&source.deity);
        self.goal.add_missing_data(&source.goal);
        self.ent_pop_id.add_missing_data(&source.ent_pop_id);
        self.active_interaction.add_missing_data(&source.active_interaction);
        self.force.add_missing_data(&source.force);
        self.current_identity_id.add_missing_data(&source.current_identity_id);
        self.holds_artifact.add_missing_data(&source.holds_artifact);
        self.used_identity_id.add_missing_data(&source.used_identity_id);
        self.animated.add_missing_data(&source.animated);
        self.animated_string.add_missing_data(&source.animated_string);
        self.adventurer.add_missing_data(&source.adventurer);
    }
}

impl PartialEq<HistoricalFigure> for df_st_core::HistoricalFigure {
    fn eq(&self, other: &HistoricalFigure) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::HistoricalFigure> for HistoricalFigure {
    fn eq(&self, other: &df_st_core::HistoricalFigure) -> bool {
        self.id == other.id
    }
}
