use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_hf_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_hf_ids"]
#[primary_key(en_id, hf_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityHFID {
    pub en_id: i32,
    pub hf_id: i32,
    pub world_id: i32,
}

impl EntityHFID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityHFID, EntityHFID> for EntityHFID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_hf_ids: &[EntityHFID]) {
        diesel::insert_into(entity_hf_ids::table)
            .values(entity_hf_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving entity_hf_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_hf_ids: &[EntityHFID]) {
        diesel::insert_into(entity_hf_ids::table)
            .values(entity_hf_ids)
            .execute(conn)
            .expect("Error saving entity_hf_ids");
    }

    /// Get a list of EntityHFID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityHFID>, Error> {
        use crate::schema::entity_hf_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_hf_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "hf_id" => hf_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityHFID>, Error> {
        use crate::schema::entity_hf_ids::dsl::*;
        let query = entity_hf_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        Ok(query.first::<EntityHFID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["en_id", "hf_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityHFID],
        core_list: Vec<EntityHFID>,
    ) -> Result<Vec<EntityHFID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_hf_ids::dsl::*;
        let query = entity_hf_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "hf_id" => {hf_id: i32},
            };},
        };
    }
}

impl PartialEq for EntityHFID {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.hf_id == other.hf_id
    }
}

impl Hash for EntityHFID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.hf_id.hash(state);
    }
}
