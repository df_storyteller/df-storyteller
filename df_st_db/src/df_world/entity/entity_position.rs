use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_positions;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_positions"]
#[primary_key(en_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityPosition {
    pub en_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub name_male: Option<String>,
    pub name_female: Option<String>,
    pub spouse: Option<String>,
    pub spouse_male: Option<String>,
    pub spouse_female: Option<String>,
}

impl EntityPosition {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityPosition, EntityPosition> for EntityPosition {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_positions: &[EntityPosition]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_positions::table)
            .values(entity_positions)
            .on_conflict((
                entity_positions::en_id,
                entity_positions::local_id,
                entity_positions::world_id,
            ))
            .do_update()
            .set((
                entity_positions::name.eq(excluded(entity_positions::name)),
                entity_positions::name_male.eq(excluded(entity_positions::name_male)),
                entity_positions::name_female.eq(excluded(entity_positions::name_female)),
                entity_positions::spouse.eq(excluded(entity_positions::spouse)),
                entity_positions::spouse_male.eq(excluded(entity_positions::spouse_male)),
                entity_positions::spouse_female.eq(excluded(entity_positions::spouse_female)),
            ))
            .execute(conn)
            .expect("Error saving entity_positions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_positions: &[EntityPosition]) {
        diesel::insert_into(entity_positions::table)
            .values(entity_positions)
            .execute(conn)
            .expect("Error saving entity_positions");
    }

    /// Get a list of EntityPosition from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityPosition>, Error> {
        use crate::schema::entity_positions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_positions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "local_id" => local_id,
                "name" => name,
                "name_male" => name_male,
                "name_female" => name_female,
                "spouse" => spouse,
                "spouse_male" => spouse_male,
                "spouse_female" => spouse_female,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityPosition>, Error> {
        use crate::schema::entity_positions::dsl::*;
        let query = entity_positions;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<EntityPosition>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "en_id",
            "local_id",
            "name",
            "name_male",
            "name_female",
            "spouse",
            "spouse_male",
            "spouse_female",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityPosition],
        core_list: Vec<df_st_core::EntityPosition>,
    ) -> Result<Vec<df_st_core::EntityPosition>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_positions::dsl::*;
        let query = entity_positions.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "local_id" => {local_id: i32},
                "name" => {name: Option<String>},
                "name_male" => {name_male: Option<String>},
                "name_female" => {name_female: Option<String>},
                "spouse" => {spouse: Option<String>},
                "spouse_male" => {spouse_male: Option<String>},
                "spouse_female" => {spouse_female: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityPosition, df_st_core::EntityPosition> for EntityPosition {
    fn add_missing_data(&mut self, source: &df_st_core::EntityPosition) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.name_male.add_missing_data(&source.name_male);
        self.name_female.add_missing_data(&source.name_female);
        self.spouse.add_missing_data(&source.spouse);
        self.spouse_male.add_missing_data(&source.spouse_male);
        self.spouse_female.add_missing_data(&source.spouse_female);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityPosition, EntityPosition> for df_st_core::EntityPosition {
    fn add_missing_data(&mut self, source: &EntityPosition) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.name_male.add_missing_data(&source.name_male);
        self.name_female.add_missing_data(&source.name_female);
        self.spouse.add_missing_data(&source.spouse);
        self.spouse_male.add_missing_data(&source.spouse_male);
        self.spouse_female.add_missing_data(&source.spouse_female);
    }
}

impl PartialEq for EntityPosition {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.local_id == other.local_id
    }
}

impl Hash for EntityPosition {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityPosition> for df_st_core::EntityPosition {
    fn eq(&self, other: &EntityPosition) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityPosition> for EntityPosition {
    fn eq(&self, other: &df_st_core::EntityPosition) -> bool {
        self.local_id == other.local_id
    }
}
