use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_honors;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_honors"]
#[primary_key(en_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityHonor {
    pub en_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub gives_precedence: Option<i32>,
    pub requires_any_melee_or_ranged_skill: Option<bool>,
    pub required_skill_ip_total: Option<i32>,
    pub required_battles: Option<i32>,
    pub required_years: Option<i32>,
    pub required_skill: Option<String>,
    pub required_kills: Option<i32>,
    pub exempt_ep_id: Option<i32>,
    pub exempt_former_ep_id: Option<i32>,
    pub granted_to_everybody: Option<bool>,
}

impl EntityHonor {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityHonor, EntityHonor> for EntityHonor {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_honors: &[EntityHonor]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_honors::table)
            .values(entity_honors)
            .on_conflict((
                entity_honors::en_id,
                entity_honors::local_id,
                entity_honors::world_id,
            ))
            .do_update()
            .set((
                entity_honors::name.eq(excluded(entity_honors::name)),
                entity_honors::gives_precedence.eq(excluded(entity_honors::gives_precedence)),
                entity_honors::requires_any_melee_or_ranged_skill
                    .eq(excluded(entity_honors::requires_any_melee_or_ranged_skill)),
                entity_honors::required_skill_ip_total
                    .eq(excluded(entity_honors::required_skill_ip_total)),
                entity_honors::required_battles.eq(excluded(entity_honors::required_battles)),
                entity_honors::required_years.eq(excluded(entity_honors::required_years)),
                entity_honors::required_skill.eq(excluded(entity_honors::required_skill)),
                entity_honors::required_kills.eq(excluded(entity_honors::required_kills)),
                entity_honors::exempt_ep_id.eq(excluded(entity_honors::exempt_ep_id)),
                entity_honors::exempt_former_ep_id.eq(excluded(entity_honors::exempt_former_ep_id)),
                entity_honors::granted_to_everybody
                    .eq(excluded(entity_honors::granted_to_everybody)),
            ))
            .execute(conn)
            .expect("Error saving entity_honors");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_honors: &[EntityHonor]) {
        diesel::insert_into(entity_honors::table)
            .values(entity_honors)
            .execute(conn)
            .expect("Error saving entity_honors");
    }

    /// Get a list of EntityHonor from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityHonor>, Error> {
        use crate::schema::entity_honors::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_honors.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "local_id" => local_id,
                "name" => name,
                "gives_precedence" => gives_precedence,
                "requires_any_melee_or_ranged_skill" => requires_any_melee_or_ranged_skill,
                "required_skill_ip_total" => required_skill_ip_total,
                "required_battles" => required_battles,
                "required_years" => required_years,
                "required_skill" => required_skill,
                "required_kills" => required_kills,
                "exempt_ep_id" => exempt_ep_id,
                "exempt_former_ep_id" => exempt_former_ep_id,
                "granted_to_everybody" => granted_to_everybody,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityHonor>, Error> {
        use crate::schema::entity_honors::dsl::*;
        let query = entity_honors;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<EntityHonor>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "en_id",
            "local_id",
            "name",
            "gives_precedence",
            "requires_any_melee_or_ranged_skill",
            "required_skill_ip_total",
            "required_battles",
            "required_years",
            "required_skill",
            "required_kills",
            "exempt_ep_id",
            "exempt_former_ep_id",
            "granted_to_everybody",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityHonor],
        core_list: Vec<df_st_core::EntityHonor>,
    ) -> Result<Vec<df_st_core::EntityHonor>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_honors::dsl::*;
        let query = entity_honors.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "local_id" => {local_id: i32},
                "name" => {name: Option<String>},
                "gives_precedence" => {gives_precedence: Option<i32>},
                "requires_any_melee_or_ranged_skill" => {requires_any_melee_or_ranged_skill: Option<bool>},
                "required_skill_ip_total" => {required_skill_ip_total: Option<i32>},
                "required_battles" => {required_battles: Option<i32>},
                "required_years" => {required_years: Option<i32>},
                "required_skill" => {required_skill: Option<String>},
                "required_kills" => {required_kills: Option<i32>},
                "exempt_ep_id" => {exempt_ep_id: Option<i32>},
                "exempt_former_ep_id" => {exempt_former_ep_id: Option<i32>},
                "granted_to_everybody" => {granted_to_everybody: Option<bool>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityHonor, df_st_core::EntityHonor> for EntityHonor {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::EntityHonor) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.gives_precedence.add_missing_data(&source.gives_precedence);
        self.requires_any_melee_or_ranged_skill.add_missing_data(&source.requires_any_melee_or_ranged_skill);
        self.required_skill_ip_total.add_missing_data(&source.required_skill_ip_total);
        self.required_battles.add_missing_data(&source.required_battles);
        self.required_years.add_missing_data(&source.required_years);
        self.required_skill.add_missing_data(&source.required_skill);
        self.required_kills.add_missing_data(&source.required_kills);
        self.exempt_ep_id.add_missing_data(&source.exempt_ep_id);
        self.exempt_former_ep_id.add_missing_data(&source.exempt_former_ep_id);
        self.granted_to_everybody.add_missing_data(&source.granted_to_everybody);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityHonor, EntityHonor> for df_st_core::EntityHonor {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &EntityHonor) {
        self.local_id.add_missing_data(&source.local_id);
        self.name.add_missing_data(&source.name);
        self.gives_precedence.add_missing_data(&source.gives_precedence);
        self.requires_any_melee_or_ranged_skill.add_missing_data(&source.requires_any_melee_or_ranged_skill);
        self.required_skill_ip_total.add_missing_data(&source.required_skill_ip_total);
        self.required_battles.add_missing_data(&source.required_battles);
        self.required_years.add_missing_data(&source.required_years);
        self.required_skill.add_missing_data(&source.required_skill);
        self.required_kills.add_missing_data(&source.required_kills);
        self.exempt_ep_id.add_missing_data(&source.exempt_ep_id);
        self.exempt_former_ep_id.add_missing_data(&source.exempt_former_ep_id);
        self.granted_to_everybody.add_missing_data(&source.granted_to_everybody);
    }
}

impl PartialEq for EntityHonor {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.local_id == other.local_id
    }
}

impl Hash for EntityHonor {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityHonor> for df_st_core::EntityHonor {
    fn eq(&self, other: &EntityHonor) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityHonor> for EntityHonor {
    fn eq(&self, other: &df_st_core::EntityHonor) -> bool {
        self.local_id == other.local_id
    }
}
