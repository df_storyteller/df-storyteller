use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_occasion_schedules;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_occasion_schedules"]
#[primary_key(en_id, en_occ_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityOccasionSchedule {
    pub en_id: i32,
    pub en_occ_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,
    pub reference: Option<i32>,
    pub reference2: Option<i32>,
}

impl EntityOccasionSchedule {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityOccasionSchedule, EntityOccasionSchedule>
    for EntityOccasionSchedule
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_occasion_schedules: &[EntityOccasionSchedule]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_occasion_schedules::table)
            .values(entity_occasion_schedules)
            .on_conflict((
                entity_occasion_schedules::en_id,
                entity_occasion_schedules::en_occ_id,
                entity_occasion_schedules::local_id,
                entity_occasion_schedules::world_id,
            ))
            .do_update()
            .set((
                entity_occasion_schedules::type_.eq(excluded(entity_occasion_schedules::type_)),
                entity_occasion_schedules::item_type
                    .eq(excluded(entity_occasion_schedules::item_type)),
                entity_occasion_schedules::item_subtype
                    .eq(excluded(entity_occasion_schedules::item_subtype)),
                entity_occasion_schedules::reference
                    .eq(excluded(entity_occasion_schedules::reference)),
                entity_occasion_schedules::reference2
                    .eq(excluded(entity_occasion_schedules::reference2)),
            ))
            .execute(conn)
            .expect("Error saving entity_occasion_schedules");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_occasion_schedules: &[EntityOccasionSchedule]) {
        diesel::insert_into(entity_occasion_schedules::table)
            .values(entity_occasion_schedules)
            .execute(conn)
            .expect("Error saving entity_occasion_schedules");
    }

    /// Get a list of EntityOccasionSchedule from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityOccasionSchedule>, Error> {
        use crate::schema::entity_occasion_schedules::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_occasion_schedules.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "local_id" => local_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "local_id" => local_id,
                "type" => type_,
                "item_type" => item_type,
                "item_subtype" => item_subtype,
                "reference" => reference,
                "reference2" => reference2,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityOccasionSchedule>, Error> {
        use crate::schema::entity_occasion_schedules::dsl::*;
        let query = entity_occasion_schedules;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        let query = query.filter(en_occ_id.eq(id_filter.get("en_occ_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<EntityOccasionSchedule>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "en_id",
            "en_occ_id",
            "local_id",
            "type",
            "item_type",
            "item_subtype",
            "reference",
            "reference2",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityOccasionSchedule],
        core_list: Vec<df_st_core::EntityOccasionSchedule>,
    ) -> Result<Vec<df_st_core::EntityOccasionSchedule>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_occasion_schedules::dsl::*;
        let query = entity_occasion_schedules
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "en_occ_id" => {en_occ_id: i32},
                "local_id" => {local_id: i32},
                "type" => {type_: Option<String>},
                "item_type" => {item_type: Option<String>},
                "item_subtype" => {item_subtype: Option<String>},
                "reference" => {reference: Option<i32>},
                "reference2" => {reference2: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityOccasionSchedule, df_st_core::EntityOccasionSchedule> for EntityOccasionSchedule {
    fn add_missing_data(&mut self, source: &df_st_core::EntityOccasionSchedule) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.reference.add_missing_data(&source.reference);
        self.reference2.add_missing_data(&source.reference2);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityOccasionSchedule, EntityOccasionSchedule>
    for df_st_core::EntityOccasionSchedule
{
    fn add_missing_data(&mut self, source: &EntityOccasionSchedule) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.reference.add_missing_data(&source.reference);
        self.reference2.add_missing_data(&source.reference2);
    }
}

impl PartialEq for EntityOccasionSchedule {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id
            && self.en_occ_id == other.en_occ_id
            && self.local_id == other.local_id
    }
}

impl Hash for EntityOccasionSchedule {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.en_occ_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityOccasionSchedule> for df_st_core::EntityOccasionSchedule {
    fn eq(&self, other: &EntityOccasionSchedule) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityOccasionSchedule> for EntityOccasionSchedule {
    fn eq(&self, other: &df_st_core::EntityOccasionSchedule) -> bool {
        self.local_id == other.local_id
    }
}
