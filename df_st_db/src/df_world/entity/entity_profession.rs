use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_professions;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_professions"]
#[primary_key(en_id, profession)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityProfession {
    pub en_id: i32,
    pub profession: String,
    pub world_id: i32,
}

impl EntityProfession {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityProfession, EntityProfession> for EntityProfession {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_professions: &[EntityProfession]) {
        diesel::insert_into(entity_professions::table)
            .values(entity_professions)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving entity_professions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_professions: &[EntityProfession]) {
        diesel::insert_into(entity_professions::table)
            .values(entity_professions)
            .execute(conn)
            .expect("Error saving entity_professions");
    }

    /// Get a list of EntityProfession from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityProfession>, Error> {
        use crate::schema::entity_professions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_professions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "profession" => profession,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityProfession>, Error> {
        use crate::schema::entity_professions::dsl::*;
        let query = entity_professions;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        Ok(query.first::<EntityProfession>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["en_id", "profession"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityProfession],
        core_list: Vec<EntityProfession>,
    ) -> Result<Vec<EntityProfession>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_professions::dsl::*;
        let query = entity_professions.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "profession" => {profession: String},
            };},
        };
    }
}

impl PartialEq for EntityProfession {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.profession == other.profession
    }
}

impl Hash for EntityProfession {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.profession.hash(state);
    }
}
