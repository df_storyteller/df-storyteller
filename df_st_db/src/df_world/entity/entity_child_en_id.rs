use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_child_en_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_child_en_ids"]
#[primary_key(en_id, child_en_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityChildENID {
    pub en_id: i32,
    pub child_en_id: i32,
    pub world_id: i32,
}

impl EntityChildENID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityChildENID, EntityChildENID> for EntityChildENID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_child_en_ids: &[EntityChildENID]) {
        diesel::insert_into(entity_child_en_ids::table)
            .values(entity_child_en_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving entity_child_en_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_child_en_ids: &[EntityChildENID]) {
        diesel::insert_into(entity_child_en_ids::table)
            .values(entity_child_en_ids)
            .execute(conn)
            .expect("Error saving entity_child_en_ids");
    }

    /// Get a list of EntityChildENID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<EntityChildENID>, Error> {
        use crate::schema::entity_child_en_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_child_en_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "child_en_id" => child_en_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityChildENID>, Error> {
        use crate::schema::entity_child_en_ids::dsl::*;
        let query = entity_child_en_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        Ok(query.first::<EntityChildENID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["en_id", "child_en_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityChildENID],
        core_list: Vec<EntityChildENID>,
    ) -> Result<Vec<EntityChildENID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_child_en_ids::dsl::*;
        let query = entity_child_en_ids
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "child_en_id" => {child_en_id: i32},
            };},
        };
    }
}

impl PartialEq for EntityChildENID {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.child_en_id == other.child_en_id
    }
}

impl Hash for EntityChildENID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.child_en_id.hash(state);
    }
}
