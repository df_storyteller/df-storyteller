use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Coordinate, DBDFWorld};
use crate::schema::regions;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

mod regions_force;
pub use regions_force::*;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "regions"]
pub struct Region {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub type_: Option<String>,
    pub evilness: Option<String>,
}

impl Region {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Region, Region> for Region {
    /// Add Coordinates to the world that are part of Region
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for region in core_world.regions.values() {
            for coord in &region.coords {
                let new_id: i32 = world.coordinates.len() as i32;
                world.coordinates.push(Coordinate {
                    id: new_id,
                    x: coord.x,
                    y: coord.y,
                    region_id: Some(region.id),
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, regions: &[Region]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(regions::table)
            .values(regions)
            .on_conflict((regions::id, regions::world_id))
            .do_update()
            .set((
                regions::name.eq(excluded(regions::name)),
                regions::type_.eq(excluded(regions::type_)),
                regions::evilness.eq(excluded(regions::evilness)),
            ))
            .execute(conn)
            .expect("Error saving regions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, regions: &[Region]) {
        diesel::insert_into(regions::table)
            .values(regions)
            .execute(conn)
            .expect("Error saving regions");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Region>, Error> {
        use crate::schema::regions::dsl::*;
        let query = regions;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<Region>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<Region>, Error> {
        use crate::schema::regions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = regions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "type" => type_,
                "evilness" => evilness,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "type" => type_,
                "evilness" => evilness,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id"],
            _ => vec!["id", "name", "type", "evilness"],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[Region],
        _core_list: Vec<df_st_core::Region>,
    ) -> Result<Vec<df_st_core::Region>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add Coordinates
        let coord_list = Coordinate::belonging_to(db_list)
            .filter(crate::schema::coordinates::world_id.eq(world_id))
            .load::<Coordinate>(conn)?
            .grouped_by(db_list);

        // Add RegionForce
        let region_force_list = RegionForce::belonging_to(db_list)
            .filter(crate::schema::regions_forces::world_id.eq(world_id))
            .load::<RegionForce>(conn)?
            .grouped_by(db_list);

        // Merge all
        let mut core_list: Vec<df_st_core::Region> = Vec::new();
        for (index, region) in db_list.iter().enumerate() {
            let mut core_region = df_st_core::Region::default();
            core_region.add_missing_data(region);

            core_region
                .coords
                .add_missing_data(coord_list.get(index).unwrap());

            for force_id in region_force_list.get(index).unwrap() {
                core_region.force_ids.push(force_id.force_id);
            }

            core_list.push(core_region);
        }

        // There is currently no data created before this
        // function that is not in this object.
        // So just swapping it saves a bit of time.
        // core_list.add_missing_data(&core_list2);
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::regions::dsl::*;
        let query = regions.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
                "type" => type_,
                "evilness" => evilness,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "type" => {type_: Option<String>},
                "evilness" => {evilness: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<Region, df_st_core::Region> for Region {
    fn add_missing_data(&mut self, source: &df_st_core::Region) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
        self.evilness.add_missing_data(&source.evilness);
    }
}

/// From DB to Core
impl Filler<df_st_core::Region, Region> for df_st_core::Region {
    fn add_missing_data(&mut self, source: &Region) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
        self.evilness.add_missing_data(&source.evilness);
    }
}

impl PartialEq<Region> for df_st_core::Region {
    fn eq(&self, other: &Region) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Region> for Region {
    fn eq(&self, other: &df_st_core::Region) -> bool {
        self.id == other.id
    }
}
