use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, WrittenContent};
use crate::schema::wc_style;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "wc_style"]
#[primary_key(written_content_id, local_id)]
#[belongs_to(WrittenContent)]
pub struct WCStyle {
    pub written_content_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub label: Option<String>,
    pub weight: Option<i32>,
}

impl WCStyle {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::WCStyle, WCStyle> for WCStyle {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for written_content in core_world.written_contents.values() {
            for style in &written_content.style {
                let mut db_style = WCStyle::new();
                db_style.add_missing_data(style);
                db_style
                    .written_content_id
                    .add_missing_data(&written_content.id);
                world.wc_styles.push(db_style);
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, wc_style: &[WCStyle]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(wc_style::table)
            .values(wc_style)
            .on_conflict((
                wc_style::written_content_id,
                wc_style::local_id,
                wc_style::world_id,
            ))
            .do_update()
            .set((
                wc_style::label.eq(excluded(wc_style::label)),
                wc_style::weight.eq(excluded(wc_style::weight)),
            ))
            .execute(conn)
            .expect("Error saving WCStyle");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, wc_style: &[WCStyle]) {
        diesel::insert_into(wc_style::table)
            .values(wc_style)
            .execute(conn)
            .expect("Error saving WCStyle");
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<WCStyle>, Error> {
        use crate::schema::wc_style::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = wc_style.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("written_content_id").is_some() {
            let query = query
                .filter(written_content_id.eq(id_filter.get("written_content_id").unwrap_or(&0)));
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {Ok(order_by!{
                    order_by, asc, query, conn,
                    "written_content_id" => written_content_id,
                    "local_id" => local_id,
                    "label" => label,
                    "weight" => weight,
                })},
            }
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {Ok(order_by!{
                    order_by, asc, query, conn,
                    "written_content_id" => written_content_id,
                    "local_id" => local_id,
                    "label" => label,
                    "weight" => weight,
                })},
            }
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<WCStyle>, Error> {
        use crate::schema::wc_style::dsl::*;
        let query = wc_style;
        let query =
            query.filter(written_content_id.eq(id_filter.get("written_content_id").unwrap_or(&0)));
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<WCStyle>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["written_content_id", "local_id", "label", "weight"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[WCStyle],
        core_list: Vec<df_st_core::WCStyle>,
    ) -> Result<Vec<df_st_core::WCStyle>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::wc_style::dsl::*;
        let query = wc_style.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("written_content_id").is_some() {
            let query = query
                .filter(written_content_id.eq(id_filter.get("written_content_id").unwrap_or(&0)));
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {group_by!{
                    group_by_opt, query, conn,
                    "written_content_id" => {written_content_id: i32},
                    "local_id" => {local_id: i32},
                    "label" => {label: Option<String>},
                    "weight" => {weight: Option<i32>},
                };},
            };
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {
                    group_by!{
                        group_by_opt, query, conn,
                        "written_content_id" => {written_content_id: i32},
                        "local_id" => {local_id: i32},
                        "label" => {label: Option<String>},
                        "weight" => {weight: Option<i32>},
                    };
                },
            };
        }
    }
}

/// From Core to DB
impl Filler<WCStyle, df_st_core::WCStyle> for WCStyle {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::WCStyle) {
        self.written_content_id.add_missing_data(&source.written_content_id);
        self.local_id.add_missing_data(&source.local_id);
        self.label.add_missing_data(&source.label);
        self.weight.add_missing_data(&source.weight);
    }
}

/// From DB to Core
impl Filler<df_st_core::WCStyle, WCStyle> for df_st_core::WCStyle {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &WCStyle) {
        self.written_content_id.add_missing_data(&source.written_content_id);
        self.local_id.add_missing_data(&source.local_id);
        self.label.add_missing_data(&source.label);
        self.weight.add_missing_data(&source.weight);
    }
}

impl PartialEq<df_st_core::WCStyle> for WCStyle {
    fn eq(&self, other: &df_st_core::WCStyle) -> bool {
        self.written_content_id == other.written_content_id && self.local_id == other.local_id
    }
}

impl PartialEq<WCStyle> for df_st_core::WCStyle {
    fn eq(&self, other: &WCStyle) -> bool {
        self.written_content_id == other.written_content_id && self.local_id == other.local_id
    }
}

impl PartialEq<WCStyle> for WCStyle {
    fn eq(&self, other: &Self) -> bool {
        self.written_content_id == other.written_content_id && self.local_id == other.local_id
    }
}
