use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, WrittenContent};
use crate::schema::wc_reference;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "wc_reference"]
#[primary_key(written_content_parent_id, local_id)]
#[belongs_to(WrittenContent, foreign_key = "written_content_parent_id")]
pub struct WCReference {
    pub written_content_parent_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub type_id: Option<i32>,

    pub written_content_id: Option<i32>,
    pub he_id: Option<i32>,
    pub site_id: Option<i32>,
    pub poetic_form_id: Option<i32>,
    pub musical_form_id: Option<i32>,
    pub dance_form_id: Option<i32>,
    pub hf_id: Option<i32>,
    pub entity_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub subregion_id: Option<i32>,
}

impl WCReference {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::WCReference, WCReference> for WCReference {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for written_content in core_world.written_contents.values() {
            for reference in &written_content.reference {
                let mut db_reference = WCReference::new();
                db_reference.add_missing_data(reference);
                db_reference
                    .written_content_parent_id
                    .add_missing_data(&written_content.id);
                world.wc_references.push(db_reference);
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, wc_reference: &[WCReference]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(wc_reference::table)
            .values(wc_reference)
            .on_conflict((
                wc_reference::written_content_parent_id,
                wc_reference::local_id,
                wc_reference::world_id,
            ))
            .do_update()
            .set((
                wc_reference::type_.eq(excluded(wc_reference::type_)),
                wc_reference::type_id.eq(excluded(wc_reference::type_id)),
                wc_reference::he_id.eq(excluded(wc_reference::he_id)),
                wc_reference::site_id.eq(excluded(wc_reference::site_id)),
                wc_reference::poetic_form_id.eq(excluded(wc_reference::poetic_form_id)),
                wc_reference::musical_form_id.eq(excluded(wc_reference::musical_form_id)),
                wc_reference::dance_form_id.eq(excluded(wc_reference::dance_form_id)),
                wc_reference::hf_id.eq(excluded(wc_reference::hf_id)),
                wc_reference::entity_id.eq(excluded(wc_reference::entity_id)),
                wc_reference::artifact_id.eq(excluded(wc_reference::artifact_id)),
                wc_reference::subregion_id.eq(excluded(wc_reference::subregion_id)),
            ))
            .execute(conn)
            .expect("Error saving WCReference");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, wc_reference: &[WCReference]) {
        diesel::insert_into(wc_reference::table)
            .values(wc_reference)
            .execute(conn)
            .expect("Error saving WCReference");
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<WCReference>, Error> {
        use crate::schema::wc_reference::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = wc_reference.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("written_content_parent_id").is_some() {
            let query = query.filter(
                written_content_parent_id
                    .eq(id_filter.get("written_content_parent_id").unwrap_or(&0)),
            );
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {Ok(order_by!{
                    order_by, asc, query, conn,
                    "written_content_parent_id" => written_content_parent_id,
                    "local_id" => local_id,
                    "type" => type_,
                    "type_id" => type_id,
                    "written_content_id" => written_content_id,
                    "he_id" => he_id,
                    "site_id" => site_id,
                    "poetic_form_id" => poetic_form_id,
                    "musical_form_id" => musical_form_id,
                    "dance_form_id" => dance_form_id,
                    "hf_id" => hf_id,
                    "entity_id" => entity_id,
                    "artifact_id" => artifact_id,
                    "subregion_id" => subregion_id,
                })},
            }
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {Ok(order_by!{
                    order_by, asc, query, conn,
                    "written_content_parent_id" => written_content_parent_id,
                    "local_id" => local_id,
                    "type" => type_,
                    "type_id" => type_id,
                    "written_content_id" => written_content_id,
                    "he_id" => he_id,
                    "site_id" => site_id,
                    "poetic_form_id" => poetic_form_id,
                    "musical_form_id" => musical_form_id,
                    "dance_form_id" => dance_form_id,
                    "hf_id" => hf_id,
                    "entity_id" => entity_id,
                    "artifact_id" => artifact_id,
                    "subregion_id" => subregion_id,
                })},
            }
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<WCReference>, Error> {
        use crate::schema::wc_reference::dsl::*;
        let query = wc_reference;
        let query = query.filter(
            written_content_parent_id.eq(id_filter.get("written_content_parent_id").unwrap_or(&0)),
        );
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(local_id.eq(id_filter.get("local_id").unwrap_or(&0)));
        Ok(query.first::<WCReference>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec![
            "written_content_parent_id",
            "local_id",
            "type",
            "type_id",
            "written_content_id",
            "he_id",
            "site_id",
            "poetic_form_id",
            "musical_form_id",
            "dance_form_id",
            "hf_id",
            "entity_id",
            "artifact_id",
            "subregion_id",
        ]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[WCReference],
        core_list: Vec<df_st_core::WCReference>,
    ) -> Result<Vec<df_st_core::WCReference>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::wc_reference::dsl::*;
        let query = wc_reference.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("written_content_parent_id").is_some() {
            let query = query.filter(
                written_content_parent_id
                    .eq(id_filter.get("written_content_parent_id").unwrap_or(&0)),
            );
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {group_by!{
                    group_by_opt, query, conn,
                    "written_content_parent_id" => {written_content_parent_id: i32},
                    "local_id" => {local_id: i32},
                    "type" => {type_: Option<String>},
                    "type_id" => {type_id: Option<i32>},
                    "written_content_id" => {written_content_id: Option<i32>},
                    "he_id" => {he_id: Option<i32>},
                    "site_id" => {site_id: Option<i32>},
                    "poetic_form_id" => {poetic_form_id: Option<i32>},
                    "musical_form_id" => {musical_form_id: Option<i32>},
                    "dance_form_id" => {dance_form_id: Option<i32>},
                    "hf_id" => {hf_id: Option<i32>},
                    "entity_id" => {entity_id: Option<i32>},
                    "artifact_id" => {artifact_id: Option<i32>},
                    "subregion_id" => {subregion_id: Option<i32>},
                };},
            };
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {group_by!{
                    group_by_opt, query, conn,
                    "written_content_parent_id" => {written_content_parent_id: i32},
                    "local_id" => {local_id: i32},
                    "type" => {type_: Option<String>},
                    "type_id" => {type_id: Option<i32>},
                    "written_content_id" => {written_content_id: Option<i32>},
                    "he_id" => {he_id: Option<i32>},
                    "site_id" => {site_id: Option<i32>},
                    "poetic_form_id" => {poetic_form_id: Option<i32>},
                    "musical_form_id" => {musical_form_id: Option<i32>},
                    "dance_form_id" => {dance_form_id: Option<i32>},
                    "hf_id" => {hf_id: Option<i32>},
                    "entity_id" => {entity_id: Option<i32>},
                    "artifact_id" => {artifact_id: Option<i32>},
                    "subregion_id" => {subregion_id: Option<i32>},
                };},
            };
        }
    }
}

/// From Core to DB
impl Filler<WCReference, df_st_core::WCReference> for WCReference {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::WCReference) {
        self.written_content_parent_id.add_missing_data(&source.written_content_parent_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.type_id.add_missing_data(&source.type_id);
        self.written_content_id.add_missing_data(&source.written_content_id);
        self.he_id.add_missing_data(&source.he_id);
        self.site_id.add_missing_data(&source.site_id);
        self.poetic_form_id.add_missing_data(&source.poetic_form_id);
        self.musical_form_id.add_missing_data(&source.musical_form_id);
        self.dance_form_id.add_missing_data(&source.dance_form_id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.subregion_id.add_missing_data(&source.subregion_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::WCReference, WCReference> for df_st_core::WCReference {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &WCReference) {
        self.written_content_parent_id.add_missing_data(&source.written_content_parent_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.type_id.add_missing_data(&source.type_id);
        self.written_content_id.add_missing_data(&source.written_content_id);
        self.he_id.add_missing_data(&source.he_id);
        self.site_id.add_missing_data(&source.site_id);
        self.poetic_form_id.add_missing_data(&source.poetic_form_id);
        self.musical_form_id.add_missing_data(&source.musical_form_id);
        self.dance_form_id.add_missing_data(&source.dance_form_id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.subregion_id.add_missing_data(&source.subregion_id);
    }
}

impl PartialEq<df_st_core::WCReference> for WCReference {
    fn eq(&self, other: &df_st_core::WCReference) -> bool {
        self.written_content_parent_id == other.written_content_parent_id
            && self.local_id == other.local_id
    }
}

impl PartialEq<WCReference> for df_st_core::WCReference {
    fn eq(&self, other: &WCReference) -> bool {
        self.written_content_parent_id == other.written_content_parent_id
            && self.local_id == other.local_id
    }
}

impl PartialEq<WCReference> for WCReference {
    fn eq(&self, other: &Self) -> bool {
        self.written_content_parent_id == other.written_content_parent_id
            && self.local_id == other.local_id
    }
}
