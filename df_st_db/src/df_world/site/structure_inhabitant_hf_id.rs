use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure, Site};
use crate::schema::structure_inhabitant_hf_ids;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "structure_inhabitant_hf_ids"]
#[primary_key(site_id, structure_local_id, inhabitant_hf_id)]
#[belongs_to(Site, foreign_key = "site_id")]
#[belongs_to(HistoricalFigure, foreign_key = "inhabitant_hf_id")]
pub struct StructureInhabitantHFID {
    pub site_id: i32,
    pub structure_local_id: i32,
    pub inhabitant_hf_id: i32,
    pub world_id: i32,
}

impl StructureInhabitantHFID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<StructureInhabitantHFID, StructureInhabitantHFID> for StructureInhabitantHFID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        structure_inhabitant_hf_ids: &[StructureInhabitantHFID],
    ) {
        diesel::insert_into(structure_inhabitant_hf_ids::table)
            .values(structure_inhabitant_hf_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving structure_inhabitant_hf_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        structure_inhabitant_hf_ids: &[StructureInhabitantHFID],
    ) {
        diesel::insert_into(structure_inhabitant_hf_ids::table)
            .values(structure_inhabitant_hf_ids)
            .execute(conn)
            .expect("Error saving structure_inhabitant_hf_ids");
    }

    /// Get a list of StructureInhabitantHFID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<StructureInhabitantHFID>, Error> {
        use crate::schema::structure_inhabitant_hf_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = structure_inhabitant_hf_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "inhabitant_hf_id" => inhabitant_hf_id,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "inhabitant_hf_id" => inhabitant_hf_id,
            })},
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<StructureInhabitantHFID>, Error> {
        use crate::schema::structure_inhabitant_hf_ids::dsl::*;
        let query = structure_inhabitant_hf_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(site_id.eq(id_filter.get("site_id").unwrap_or(&0)));
        let query =
            query.filter(structure_local_id.eq(id_filter.get("structure_local_id").unwrap_or(&0)));
        let query =
            query.filter(inhabitant_hf_id.eq(id_filter.get("inhabitant_hf_id").unwrap_or(&0)));
        Ok(query.first::<StructureInhabitantHFID>(conn).optional()?)
    }

    fn match_field_by(_match_by: MatchBy) -> Vec<&'static str> {
        vec!["site_id", "structure_local_id", "inhabitant_hf_id"]
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[StructureInhabitantHFID],
        core_list: Vec<StructureInhabitantHFID>,
    ) -> Result<Vec<StructureInhabitantHFID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        _string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        _id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::structure_inhabitant_hf_ids::dsl::*;
        let query = structure_inhabitant_hf_ids
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "inhabitant_hf_id" => inhabitant_hf_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "site_id" => {site_id: i32},
                "structure_local_id" => {structure_local_id: i32},
                "inhabitant_hf_id" => {inhabitant_hf_id: i32},
            };},
        };
    }
}

impl PartialEq for StructureInhabitantHFID {
    fn eq(&self, other: &Self) -> bool {
        self.site_id == other.site_id
            && self.structure_local_id == other.structure_local_id
            && self.inhabitant_hf_id == other.inhabitant_hf_id
    }
}

impl Hash for StructureInhabitantHFID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.site_id.hash(state);
        self.structure_local_id.hash(state);
        self.inhabitant_hf_id.hash(state);
    }
}
