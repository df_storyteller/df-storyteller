use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::{Coordinate, DBDFWorld};
use crate::schema::landmasses;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;
use std::hash::{BuildHasher, Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "landmasses"]
pub struct Landmass {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub coord_1_id: Option<i32>,
    pub coord_2_id: Option<i32>,
}

impl Landmass {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Landmass, Landmass> for Landmass {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for landmass in core_world.landmasses.values() {
            // hash landmass for identifier
            let mut hasher = world.landmasses.hasher().build_hasher();
            landmass.hash(&mut hasher);
            let hash = hasher.finish();

            if let Some(coord) = &landmass.coord_1 {
                let new_id: i32 = world.coordinates.len() as i32;
                world.coordinates.push(Coordinate {
                    id: new_id,
                    x: coord.x,
                    y: coord.y,
                    ..Default::default()
                });
                if let Some(db_landmass) = world.landmasses.get_mut(&hash) {
                    db_landmass.coord_1_id = Some(new_id);
                } else {
                    log::warn!(
                        "Landmass not found, Can not update coord_1_id. Please report this."
                    );
                }
            }
            if let Some(coord) = &landmass.coord_2 {
                let new_id: i32 = world.coordinates.len() as i32;
                world.coordinates.push(Coordinate {
                    id: new_id,
                    x: coord.x,
                    y: coord.y,
                    ..Default::default()
                });
                if let Some(db_landmass) = world.landmasses.get_mut(&hash) {
                    db_landmass.coord_2_id = Some(new_id);
                } else {
                    log::warn!(
                        "Landmass not found, Can not update coord_2_id. Please report this."
                    );
                }
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, landmasses: &[Landmass]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(landmasses::table)
            .values(landmasses)
            .on_conflict((landmasses::id, landmasses::world_id))
            .do_update()
            .set((
                landmasses::name.eq(excluded(landmasses::name)),
                landmasses::coord_1_id.eq(excluded(landmasses::coord_1_id)),
                landmasses::coord_2_id.eq(excluded(landmasses::coord_2_id)),
            ))
            .execute(conn)
            .expect("Error saving landmasses");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, landmasses: &[Landmass]) {
        diesel::insert_into(landmasses::table)
            .values(landmasses)
            .execute(conn)
            .expect("Error saving landmasses");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Landmass>, Error> {
        use crate::schema::landmasses::dsl::*;
        let query = landmasses;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<Landmass>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<Landmass>, Error> {
        use crate::schema::landmasses::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = landmasses.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id"],
            _ => vec!["id", "name"],
        }
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[Landmass],
        mut core_list: Vec<df_st_core::Landmass>,
    ) -> Result<Vec<df_st_core::Landmass>, Error> {
        use crate::schema::coordinates;
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        let landmass_ids: Vec<i32> = db_list.iter().map(|landmass| landmass.id).collect();
        // Add coordinates
        let coord_1_list = coordinates::table
            .inner_join(landmasses::table.on(coordinates::id.nullable().eq(landmasses::coord_1_id)))
            .filter(crate::schema::coordinates::world_id.eq(world_id))
            .filter(landmasses::id.eq_any(landmass_ids.clone()))
            .load::<(Coordinate, Landmass)>(conn)?;
        let coord_2_list = coordinates::table
            .inner_join(landmasses::table.on(coordinates::id.nullable().eq(landmasses::coord_2_id)))
            .filter(crate::schema::coordinates::world_id.eq(world_id))
            .filter(landmasses::id.eq_any(landmass_ids))
            .load::<(Coordinate, Landmass)>(conn)?;

        core_list = core_list
            .into_iter()
            .map(|mut landmass| {
                let mut coord_1: Option<df_st_core::Coordinate> = None;
                for (coord, lm) in &coord_1_list {
                    if &landmass == lm {
                        coord_1.add_missing_data(&Some(coord.clone()));
                        break;
                    }
                }
                landmass.coord_1 = coord_1;
                let mut coord_2: Option<df_st_core::Coordinate> = None;
                for (coord, lm) in &coord_2_list {
                    if &landmass == lm {
                        coord_2.add_missing_data(&Some(coord.clone()));
                        break;
                    }
                }
                landmass.coord_2 = coord_2;
                landmass
            })
            .collect();
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::landmasses::dsl::*;
        let query = landmasses.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "name" => name,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<Landmass, df_st_core::Landmass> for Landmass {
    fn add_missing_data(&mut self, source: &df_st_core::Landmass) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
    }
}

/// From DB to Core
impl Filler<df_st_core::Landmass, Landmass> for df_st_core::Landmass {
    fn add_missing_data(&mut self, source: &Landmass) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
    }
}

impl PartialEq<Landmass> for df_st_core::Landmass {
    fn eq(&self, other: &Landmass) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Landmass> for Landmass {
    fn eq(&self, other: &df_st_core::Landmass) -> bool {
        self.id == other.id
    }
}
