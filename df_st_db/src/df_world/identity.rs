use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::identities;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "identities"]
pub struct Identity {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub hf_id: Option<i32>,
    pub race: Option<String>,
    pub caste: Option<String>,
    pub birth_year: Option<i32>,
    pub birth_second: Option<i32>,
    pub profession: Option<String>,
    pub entity_id: Option<i32>,
    pub nemesis_id: Option<i32>,
}

impl Identity {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Identity, Identity> for Identity {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, identities: &[Identity]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(identities::table)
            .values(identities)
            .on_conflict((identities::id, identities::world_id))
            .do_update()
            .set((
                identities::name.eq(excluded(identities::name)),
                identities::hf_id.eq(excluded(identities::hf_id)),
                identities::race.eq(excluded(identities::race)),
                identities::caste.eq(excluded(identities::caste)),
                identities::birth_year.eq(excluded(identities::birth_year)),
                identities::birth_second.eq(excluded(identities::birth_second)),
                identities::profession.eq(excluded(identities::profession)),
                identities::entity_id.eq(excluded(identities::entity_id)),
                identities::nemesis_id.eq(excluded(identities::nemesis_id)),
            ))
            .execute(conn)
            .expect("Error saving identities");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, identities: &[Identity]) {
        diesel::insert_into(identities::table)
            .values(identities)
            .execute(conn)
            .expect("Error saving identities");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Identity>, Error> {
        use crate::schema::identities::dsl::*;
        let query = identities;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<Identity>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<Identity>, Error> {
        use crate::schema::identities::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = identities.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "hf_id" => hf_id,
                "birth_year" => birth_year,
                "birth_second" => birth_second,
                "entity_id" => entity_id,
                "nemesis_id" => nemesis_id,
            ],
            string_filter,
            [
                "name" => name,
                "race" => race,
                "caste" => caste,
                "profession" => profession,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "hf_id" => hf_id,
                "race" => race,
                "caste" => caste,
                "birth_year" => birth_year,
                "birth_second" => birth_second,
                "profession" => profession,
                "entity_id" => entity_id,
                "nemesis_id" => nemesis_id,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec![
                "id",
                "hf_id",
                "birth_year",
                "birth_second",
                "entity_id",
                "nemesis_id",
            ],
            _ => vec![
                "id",
                "name",
                "hf_id",
                "race",
                "caste",
                "birth_year",
                "birth_second",
                "profession",
                "entity_id",
                "nemesis_id",
            ],
        }
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[Identity],
        core_list: Vec<df_st_core::Identity>,
    ) -> Result<Vec<df_st_core::Identity>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::identities::dsl::*;
        let query = identities.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
                "hf_id" => hf_id,
                "birth_year" => birth_year,
                "birth_second" => birth_second,
                "entity_id" => entity_id,
                "nemesis_id" => nemesis_id,
            ],
            string_filter,
            [
                "name" => name,
                "race" => race,
                "caste" => caste,
                "profession" => profession,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "hf_id" => {hf_id: Option<i32>},
                "race" => {race: Option<String>},
                "caste" => {caste: Option<String>},
                "birth_year" => {birth_year: Option<i32>},
                "birth_second" => {birth_second: Option<i32>},
                "profession" => {profession: Option<String>},
                "entity_id" => {entity_id: Option<i32>},
                "nemesis_id" => {nemesis_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<Identity, df_st_core::Identity> for Identity {
    fn add_missing_data(&mut self, source: &df_st_core::Identity) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.hf_id.add_missing_data(&source.hf_id);
        self.race.add_missing_data(&source.race);
        self.caste.add_missing_data(&source.caste);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_second.add_missing_data(&source.birth_second);
        self.profession.add_missing_data(&source.profession);
        self.entity_id.add_missing_data(&source.entity_id);
        self.nemesis_id.add_missing_data(&source.nemesis_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::Identity, Identity> for df_st_core::Identity {
    fn add_missing_data(&mut self, source: &Identity) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.hf_id.add_missing_data(&source.hf_id);
        self.race.add_missing_data(&source.race);
        self.caste.add_missing_data(&source.caste);
        self.birth_year.add_missing_data(&source.birth_year);
        self.birth_second.add_missing_data(&source.birth_second);
        self.profession.add_missing_data(&source.profession);
        self.entity_id.add_missing_data(&source.entity_id);
        self.nemesis_id.add_missing_data(&source.nemesis_id);
    }
}

impl PartialEq<Identity> for df_st_core::Identity {
    fn eq(&self, other: &Identity) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Identity> for Identity {
    fn eq(&self, other: &df_st_core::Identity) -> bool {
        self.id == other.id
    }
}
