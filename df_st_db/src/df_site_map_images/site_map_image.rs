use crate::db_object::{DBObject, MatchBy, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::site_map_images;
use crate::DbConnection;
use anyhow::Error;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "site_map_images"]
pub struct SiteMapImage {
    pub id: i32,
    pub world_id: i32,
    pub data: Vec<u8>,
    pub format: String,
}

impl SiteMapImage {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::SiteMapImage, SiteMapImage> for SiteMapImage {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, site_map_images: &[SiteMapImage]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(site_map_images::table)
            .values(site_map_images)
            .on_conflict((site_map_images::id, site_map_images::world_id))
            .do_update()
            .set((
                site_map_images::data.eq(excluded(site_map_images::data)),
                site_map_images::format.eq(excluded(site_map_images::format)),
            ))
            .execute(conn)
            .expect("Error saving site_map_images");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, site_map_images: &[SiteMapImage]) {
        diesel::insert_into(site_map_images::table)
            .values(site_map_images)
            .execute(conn)
            .expect("Error saving site_map_images");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<SiteMapImage>, Error> {
        use crate::schema::site_map_images::dsl::*;
        let query = site_map_images;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        Ok(query.first::<SiteMapImage>(conn).optional()?)
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<SiteMapImage>, Error> {
        use crate::schema::site_map_images::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = site_map_images.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "format" => format,
            ],
            {Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "data" => data,
                "format" => format,
            })},
        }
    }

    fn match_field_by(match_by: MatchBy) -> Vec<&'static str> {
        match match_by {
            MatchBy::IntFilterBy => vec!["id"],
            _ => vec!["id", "data", "format"],
        }
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[SiteMapImage],
        core_list: Vec<df_st_core::SiteMapImage>,
    ) -> Result<Vec<df_st_core::SiteMapImage>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        string_filter: HashMap<String, String>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
        id_list: Option<Vec<i32>>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::site_map_images::dsl::*;
        let query = site_map_images.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            id_list => id,
            [
                "id" => id,
            ],
            string_filter,
            [
                "format" => format,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "data" => {data: Vec<u8>},
                "format" => {format: String},
            };},
        };
    }
}

/// From Core to DB
impl Filler<SiteMapImage, df_st_core::SiteMapImage> for SiteMapImage {
    fn add_missing_data(&mut self, source: &df_st_core::SiteMapImage) {
        self.id.add_missing_data(&source.id);
        self.data = source.data.clone();
        self.format.add_missing_data(&source.format);
    }
}

/// From DB to Core
impl Filler<df_st_core::SiteMapImage, SiteMapImage> for df_st_core::SiteMapImage {
    fn add_missing_data(&mut self, source: &SiteMapImage) {
        self.id.add_missing_data(&source.id);
        self.data = source.data.clone();
        self.format.add_missing_data(&source.format);
    }
}

impl PartialEq<SiteMapImage> for df_st_core::SiteMapImage {
    fn eq(&self, other: &SiteMapImage) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::SiteMapImage> for SiteMapImage {
    fn eq(&self, other: &df_st_core::SiteMapImage) -> bool {
        self.id == other.id
    }
}
