use df_st_core::{Fillable, Filler};
use df_st_derive::Fillable;
use indexmap::IndexMap;
use std::fmt;

mod artifact;
mod creature;
mod dance_form;
mod entity;
mod entity_population;
mod historical_era;
mod historical_event;
mod historical_event_collection;
mod historical_figure;
mod identity;
mod landmass;
mod links;
mod mountain_peak;
mod musical_form;
mod poetic_form;
mod positions;
mod region;
mod river;
mod site;
mod underground_region;
mod world_construction;
mod world_info;
mod written_content;

use crate::db_object::DBObject;
use crate::DbConnection;
pub use artifact::Artifact;
pub use creature::*;
pub use dance_form::DanceForm;
pub use entity::{
    Entity, EntityChildENID, EntityHFID, EntityHonor, EntityLink, EntityOccasion,
    EntityOccasionSchedule, EntityOccasionScheduleFeature, EntityPosition,
    EntityPositionAssignment, EntityProfession, EntityWeapon, EntityWorshipID,
};
pub use entity_population::{EntityPopulation, EntityPopulationRace};
pub use historical_era::HistoricalEra;
pub use historical_event::*;
pub use historical_event_collection::*;
pub use historical_figure::*;
pub use identity::Identity;
use indicatif::ProgressBar;
pub use landmass::Landmass;
pub use links::{LinkHEEntity, LinkHEHF, LinkHESite};
pub use mountain_peak::MountainPeak;
pub use musical_form::MusicalForm;
pub use poetic_form::PoeticForm;
pub use positions::{Coordinate, Path, Rectangle};
pub use region::{Region, RegionForce};
pub use river::River;
pub use site::{Site, SiteProperty, Structure, StructureCopiedArtifactID, StructureInhabitantHFID};
pub use underground_region::UndergroundRegion;
pub use world_construction::WorldConstruction;
pub use world_info::DFWorldInfo;
pub use written_content::{WCReference, WCStyle, WrittenContent};

#[derive(Clone, Fillable, Default, PartialEq)]
pub struct DBDFWorld {
    pub world_info: DFWorldInfo,
    // Below all imported values are `IndexMap<u64,...>`
    // All locally created items are `Vec<...>`
    // This improves efficiency and speed of development.
    // The effects of these changes are larges on bigger worlds
    // where the lists are larger.
    pub coordinates: Vec<Coordinate>,
    pub rectangles: Vec<Rectangle>,
    pub paths: Vec<Path>,
    pub regions: IndexMap<u64, Region>,
    pub regions_forces: Vec<RegionForce>,
    pub underground_regions: IndexMap<u64, UndergroundRegion>,
    pub sites: IndexMap<u64, Site>,
    pub structures: Vec<Structure>,
    pub structure_copied_artifact_ids: Vec<StructureCopiedArtifactID>,
    pub structure_inhabitant_hf_ids: Vec<StructureInhabitantHFID>,
    pub sites_properties: Vec<SiteProperty>,
    pub artifacts: IndexMap<u64, Artifact>,
    pub world_constructions: IndexMap<u64, WorldConstruction>,
    pub entity_populations: IndexMap<u64, EntityPopulation>,
    pub entity_population_races: Vec<EntityPopulationRace>,
    pub historical_eras: IndexMap<u64, HistoricalEra>,
    pub historical_events: IndexMap<u64, HistoricalEvent>,
    pub historical_events_a_a: IndexMap<u64, HistoricalEventAA>,
    pub historical_events_b_b: IndexMap<u64, HistoricalEventBB>,
    pub historical_events_c_c: IndexMap<u64, HistoricalEventCC>,
    pub historical_events_d_d1: IndexMap<u64, HistoricalEventDD1>,
    pub historical_events_d_d2: IndexMap<u64, HistoricalEventDD2>,
    pub historical_events_e_g: IndexMap<u64, HistoricalEventEG>,
    pub historical_events_h_h: IndexMap<u64, HistoricalEventHH>,
    pub historical_events_i_i: IndexMap<u64, HistoricalEventII>,
    pub historical_events_j_m: IndexMap<u64, HistoricalEventJM>,
    pub historical_events_n_o: IndexMap<u64, HistoricalEventNO>,
    pub historical_events_p_p: IndexMap<u64, HistoricalEventPP>,
    pub historical_events_q_r: IndexMap<u64, HistoricalEventQR>,
    pub historical_events_s_s1: IndexMap<u64, HistoricalEventSS1>,
    pub historical_events_s_s2: IndexMap<u64, HistoricalEventSS2>,
    pub historical_events_t_t: IndexMap<u64, HistoricalEventTT>,
    pub historical_events_u_w: IndexMap<u64, HistoricalEventUW>,
    pub he_circumstances: Vec<HECircumstance>,
    pub he_reasons: Vec<HEReason>,
    pub he_bodies_hf_ids: Vec<HEBodiesHFID>,
    pub he_competitor_hf_ids: Vec<HECompetitorHFID>,
    pub he_conspirator_hf_ids: Vec<HEConspiratorHFID>,
    pub he_expelled_creatures: Vec<HEExpelledCreature>,
    pub he_expelled_hf_ids: Vec<HEExpelledHFID>,
    pub he_expelled_numbers: Vec<HEExpelledNumber>,
    pub he_expelled_pop_ids: Vec<HEExpelledPopID>,
    pub he_groups_hf_ids: Vec<HEGroupsHFID>,
    pub he_implicated_hf_ids: Vec<HEImplicatedHFID>,
    pub he_joining_en_ids: Vec<HEJoiningENID>,
    pub he_pets: Vec<HEPet>,
    pub he_a_hf_ids: Vec<HEAHFID>,
    pub he_d_hf_ids: Vec<HEDHFID>,
    pub historical_figures: IndexMap<u64, HistoricalFigure>,
    pub hf_entity_links: Vec<HFEntityLink>,
    pub hf_entity_position_links: Vec<HFEntityPositionLink>,
    pub hf_site_links: Vec<HFSiteLink>,
    pub hf_skills: Vec<HFSkill>,
    pub hf_relationship_profile_hf: Vec<HFRelationshipProfileHF>,
    pub hf_intrigue_actors: Vec<HFIntrigueActor>,
    pub hf_intrigue_plots: Vec<HFIntriguePlot>,
    pub hf_plot_actors: Vec<HFPlotActor>,
    pub hf_entity_reputations: Vec<HFEntityReputation>,
    pub hf_vague_relationships: Vec<HFVagueRelationship>,
    pub hf_entity_squad_links: Vec<HFEntitySquadLink>,
    pub hf_links: Vec<HFLink>,
    pub hf_honor_entities: Vec<HFHonorEntity>,
    pub hf_site_properties: Vec<HFSiteProperty>,
    pub hf_spheres: Vec<HFSpheres>,
    pub hf_interaction_knowledges: Vec<HFInteractionKnowledge>,
    pub hf_journey_pets: Vec<HFJourneyPets>,
    pub hf_honor_ids: Vec<HFHonorID>,
    pub dance_forms: IndexMap<u64, DanceForm>,
    pub musical_forms: IndexMap<u64, MusicalForm>,
    pub poetic_forms: IndexMap<u64, PoeticForm>,
    pub identities: IndexMap<u64, Identity>,
    pub landmasses: IndexMap<u64, Landmass>,
    pub rivers: IndexMap<u64, River>,
    pub written_contents: IndexMap<u64, WrittenContent>,
    pub wc_styles: Vec<WCStyle>,
    pub wc_references: Vec<WCReference>,
    pub mountain_peaks: IndexMap<u64, MountainPeak>,
    pub entities: IndexMap<u64, Entity>,
    pub entity_honors: Vec<EntityHonor>,
    pub entity_links: Vec<EntityLink>,
    pub entity_positions: Vec<EntityPosition>,
    pub entity_position_assignments: Vec<EntityPositionAssignment>,
    pub entity_occasions: Vec<EntityOccasion>,
    pub entity_occasion_schedules: Vec<EntityOccasionSchedule>,
    pub entity_occasion_schedule_features: Vec<EntityOccasionScheduleFeature>,
    pub entity_worship_ids: Vec<EntityWorshipID>,
    pub entity_weapons: Vec<EntityWeapon>,
    pub entity_professions: Vec<EntityProfession>,
    pub entity_hf_ids: Vec<EntityHFID>,
    pub entity_child_en_ids: Vec<EntityChildENID>,
    pub historical_event_collections: IndexMap<u64, HistoricalEventCollection>,
    pub historical_event_collections_a_c: IndexMap<u64, HistoricalEventCollectionAC>,
    pub historical_event_collections_d_z: IndexMap<u64, HistoricalEventCollectionDZ>,
    pub hec_he_ids: Vec<HECHEID>,
    pub hec_related_ids: Vec<HECRelatedID>,
    pub hec_individual_mercs: Vec<HECIndividualMerc>,
    pub hec_noncom_hf_ids: Vec<HECNoncomHFID>,
    pub hec_outcomes: Vec<HECOutcome>,
    pub hec_attacking_hf_ids: Vec<HECAttackingHFID>,
    pub hec_defending_hf_ids: Vec<HECDefendingHFID>,
    pub hec_a_support_merc_hf_ids: Vec<HECASupportMercHFID>,
    pub hec_d_support_merc_hf_ids: Vec<HECDSupportMercHFID>,
    pub creatures: IndexMap<u64, Creature>,
    pub creatures_a_g: IndexMap<u64, CreatureAG>,
    pub creatures_h_h_1: IndexMap<u64, CreatureHH1>,
    pub creatures_h_h_2: IndexMap<u64, CreatureHH2>,
    pub creatures_l_z: IndexMap<u64, CreatureLZ>,
    pub creature_biomes_1: Vec<CreatureBiome1>,
    pub creature_biomes_2: Vec<CreatureBiome2>,

    pub links_he_hf: IndexMap<u64, LinkHEHF>,
    pub links_he_site: IndexMap<u64, LinkHESite>,
    pub links_he_entity: IndexMap<u64, LinkHEEntity>,
}

macro_rules! progress_code(
    { $progress_bar:ident, $($label:expr => $code:block),* $(,)* } => {
        {
            if let Some(progress_bar) = $progress_bar{
                let labels = vec![$($label),*];
                progress_bar.set_length(labels.len() as u64);
                $(
                    progress_bar.set_message($label);
                    progress_bar.inc(1);
                    $code
                )*
                progress_bar.finish_with_message("Done ✔️");
            } else {
                $(
                    $code
                )*
            }
        }
     };
);

macro_rules! set_world_ids_indexmap(
    { $self:ident, $world_id:ident: [$($lists:ident),* $(,)*] } => {
        {
            $(
                for item in $self.$lists.values_mut(){
                    item.world_id = $world_id;
                }
            )*
        }
     };
);

macro_rules! set_world_ids_vec(
    { $self:ident, $world_id:ident: [$($lists:ident),* $(,)*] } => {
        {
            $(
                for item in $self.$lists.iter_mut(){
                    item.world_id = $world_id;
                }
            )*
        }
     };
);

impl DBDFWorld {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_world_id(&mut self, world_id: i32) {
        self.world_info.id = world_id;
        set_world_ids_indexmap!(self, world_id: [regions, underground_regions, sites,
            artifacts, world_constructions, entity_populations, historical_eras, historical_events,
            historical_events_a_a, historical_events_b_b, historical_events_c_c, historical_events_d_d1,
            historical_events_d_d2, historical_events_e_g, historical_events_h_h,
            historical_events_i_i, historical_events_j_m,
            historical_events_n_o, historical_events_p_p, historical_events_q_r, historical_events_s_s1,
            historical_events_s_s2, historical_events_t_t, historical_events_u_w, historical_figures,
            dance_forms, musical_forms, poetic_forms, identities, landmasses, rivers, written_contents,
            mountain_peaks, entities,
            historical_event_collections, historical_event_collections_a_c,
            historical_event_collections_d_z, creatures, creatures_a_g, creatures_h_h_1, creatures_h_h_2,
            creatures_l_z, links_he_hf, links_he_site, links_he_entity,
        ]);
        set_world_ids_vec!(self, world_id: [coordinates, rectangles, paths, regions_forces,
            structures, structure_copied_artifact_ids, structure_inhabitant_hf_ids,
            sites_properties, entity_population_races, he_circumstances, he_reasons,
            he_bodies_hf_ids, he_competitor_hf_ids, he_conspirator_hf_ids, he_expelled_creatures,
            he_expelled_hf_ids, he_expelled_numbers, he_expelled_pop_ids, he_groups_hf_ids,
            he_implicated_hf_ids, he_joining_en_ids, he_pets, he_a_hf_ids, he_d_hf_ids,
            hf_entity_links, hf_entity_position_links, hf_site_links, hf_skills,
            hf_relationship_profile_hf,
            hf_intrigue_actors, hf_intrigue_plots, hf_plot_actors, hf_entity_reputations,
            hf_vague_relationships, hf_entity_squad_links, hf_links, hf_honor_entities,
            hf_site_properties, hf_spheres, hf_interaction_knowledges, hf_journey_pets, hf_honor_ids,
            wc_styles, wc_references, entity_honors, entity_links, entity_positions, entity_position_assignments,
            entity_occasions, entity_occasion_schedules, entity_occasion_schedule_features,
            entity_worship_ids, entity_weapons, entity_professions, entity_hf_ids, entity_child_en_ids,
            hec_he_ids, hec_related_ids, hec_individual_mercs, hec_noncom_hf_ids, hec_outcomes,
            hec_attacking_hf_ids, hec_defending_hf_ids, hec_a_support_merc_hf_ids,
            hec_d_support_merc_hf_ids, creature_biomes_1, creature_biomes_2,
        ]);
    }

    pub fn insert_into_db(&self, conn: &DbConnection, progress_bar: Option<ProgressBar>) {
        progress_code! { progress_bar,
            "World" => {DFWorldInfo::insert_into_db(conn, &([self.world_info.clone()]));},
            "Region" => {Region::insert_into_db_chunked_indexmap(conn, &self.regions);},
            "RegionForce" => {RegionForce::insert_into_db_chunked(conn, &self.regions_forces);},
            "UndergroundRegion" => {UndergroundRegion::insert_into_db_chunked_indexmap(conn, &self.underground_regions);},
            "Coordinate" => {Coordinate::insert_into_db_chunked(conn, &self.coordinates);},
            "River" => {River::insert_into_db_chunked_indexmap(conn, &self.rivers);},
            "Path" => {Path::insert_into_db_chunked(conn, &self.paths);},
            "Rectangle" => {Rectangle::insert_into_db_chunked(conn, &self.rectangles);},
            "Site" => {Site::insert_into_db_chunked_indexmap(conn, &self.sites);},
            "Structure" => {Structure::insert_into_db_chunked(conn, &self.structures);},
            "StructureCopiedArtifactID" => {StructureCopiedArtifactID::insert_into_db_chunked(conn, &self.structure_copied_artifact_ids);},
            "StructureInhabitantHFID" => {StructureInhabitantHFID::insert_into_db_chunked(conn, &self.structure_inhabitant_hf_ids);},
            "SiteProperty" => {SiteProperty::insert_into_db_chunked(conn, &self.sites_properties);},
            "Artifact" => {Artifact::insert_into_db_chunked_indexmap(conn, &self.artifacts);},
            "WorldConstruction" => {WorldConstruction::insert_into_db_chunked_indexmap(conn, &self.world_constructions);},
            "EntityPopulation" => {EntityPopulation::insert_into_db_chunked_indexmap(conn, &self.entity_populations);},
            "EntityPopulationRace" => {EntityPopulationRace::insert_into_db_chunked(conn, &self.entity_population_races);},
            "HistoricalEra" => {HistoricalEra::insert_into_db_chunked_indexmap(conn, &self.historical_eras);},
            "HistoricalEvent" => {HistoricalEvent::insert_into_db_chunked_indexmap(conn, &self.historical_events);},
            "HistoricalEventAA" => {HistoricalEventAA::insert_into_db_chunked_indexmap(conn, &self.historical_events_a_a);},
            "HistoricalEventBB" => {HistoricalEventBB::insert_into_db_chunked_indexmap(conn, &self.historical_events_b_b);},
            "HistoricalEventCC" => {HistoricalEventCC::insert_into_db_chunked_indexmap(conn, &self.historical_events_c_c);},
            "HistoricalEventDD1" => {HistoricalEventDD1::insert_into_db_chunked_indexmap(conn, &self.historical_events_d_d1);},
            "HistoricalEventDD2" => {HistoricalEventDD2::insert_into_db_chunked_indexmap(conn, &self.historical_events_d_d2);},
            "HistoricalEventEG" => {HistoricalEventEG::insert_into_db_chunked_indexmap(conn, &self.historical_events_e_g);},
            "HistoricalEventHH" => {HistoricalEventHH::insert_into_db_chunked_indexmap(conn, &self.historical_events_h_h);},
            "HistoricalEventII" => {HistoricalEventII::insert_into_db_chunked_indexmap(conn, &self.historical_events_i_i);},
            "HistoricalEventJM" => {HistoricalEventJM::insert_into_db_chunked_indexmap(conn, &self.historical_events_j_m);},
            "HistoricalEventNO" => {HistoricalEventNO::insert_into_db_chunked_indexmap(conn, &self.historical_events_n_o);},
            "HistoricalEventPP" => {HistoricalEventPP::insert_into_db_chunked_indexmap(conn, &self.historical_events_p_p);},
            "HistoricalEventQR" => {HistoricalEventQR::insert_into_db_chunked_indexmap(conn, &self.historical_events_q_r);},
            "HistoricalEventSS1" => {HistoricalEventSS1::insert_into_db_chunked_indexmap(conn, &self.historical_events_s_s1);},
            "HistoricalEventSS2" => {HistoricalEventSS2::insert_into_db_chunked_indexmap(conn, &self.historical_events_s_s2);},
            "HistoricalEventTT" => {HistoricalEventTT::insert_into_db_chunked_indexmap(conn, &self.historical_events_t_t);},
            "HistoricalEventUW" => {HistoricalEventUW::insert_into_db_chunked_indexmap(conn, &self.historical_events_u_w);},
            "HECircumstance" => {HECircumstance::insert_into_db_chunked(conn, &self.he_circumstances);},
            "HEReason" => {HEReason::insert_into_db_chunked(conn, &self.he_reasons);},
            "HEBodiesHFID" => {HEBodiesHFID::insert_into_db_chunked(conn, &self.he_bodies_hf_ids);},
            "HECompetitorHFID" => {HECompetitorHFID::insert_into_db_chunked(conn, &self.he_competitor_hf_ids);},
            "HEConspiratorHFID" => {HEConspiratorHFID::insert_into_db_chunked(conn, &self.he_conspirator_hf_ids);},
            "HEExpelledCreature" => {HEExpelledCreature::insert_into_db_chunked(conn, &self.he_expelled_creatures);},
            "HEExpelledHFID" => {HEExpelledHFID::insert_into_db_chunked(conn, &self.he_expelled_hf_ids);},
            "HEExpelledNumber" => {HEExpelledNumber::insert_into_db_chunked(conn, &self.he_expelled_numbers);},
            "HEExpelledPopID" => {HEExpelledPopID::insert_into_db_chunked(conn, &self.he_expelled_pop_ids);},
            "HEGroupsHFID" => {HEGroupsHFID::insert_into_db_chunked(conn, &self.he_groups_hf_ids);},
            "HEImplicatedHFID" => {HEImplicatedHFID::insert_into_db_chunked(conn, &self.he_implicated_hf_ids);},
            "HEJoiningENID" => {HEJoiningENID::insert_into_db_chunked(conn, &self.he_joining_en_ids);},
            "HEPet" => {HEPet::insert_into_db_chunked(conn, &self.he_pets);},
            "HistoricalFigure" => {HistoricalFigure::insert_into_db_chunked_indexmap(conn, &self.historical_figures);},
            "HFEntityLink" => {HFEntityLink::insert_into_db_chunked(conn, &self.hf_entity_links);},
            "HFEntityPositionLink" => {HFEntityPositionLink::insert_into_db_chunked(conn, &self.hf_entity_position_links);},
            "HFSiteLink" => {HFSiteLink::insert_into_db_chunked(conn, &self.hf_site_links);},
            "HFSkill" => {HFSkill::insert_into_db_chunked(conn, &self.hf_skills);},
            "HFRelationshipProfileHF" => {HFRelationshipProfileHF::insert_into_db_chunked(conn, &self.hf_relationship_profile_hf);},
            "HFIntrigueActor" => {HFIntrigueActor::insert_into_db_chunked(conn, &self.hf_intrigue_actors);},
            "HFIntriguePlot" => {HFIntriguePlot::insert_into_db_chunked(conn, &self.hf_intrigue_plots);},
            "HFPlotActor" => {HFPlotActor::insert_into_db_chunked(conn, &self.hf_plot_actors);},
            "HFEntityReputation" => {HFEntityReputation::insert_into_db_chunked(conn, &self.hf_entity_reputations);},
            "HFVagueRelationship" => {HFVagueRelationship::insert_into_db_chunked(conn, &self.hf_vague_relationships);},
            "HFEntitySquadLink" => {HFEntitySquadLink::insert_into_db_chunked(conn, &self.hf_entity_squad_links);},
            "HFLink" => {HFLink::insert_into_db_chunked(conn, &self.hf_links);},
            "HFHonorEntity" => {HFHonorEntity::insert_into_db_chunked(conn, &self.hf_honor_entities);},
            "HFSiteProperty" => {HFSiteProperty::insert_into_db_chunked(conn, &self.hf_site_properties);},
            "HFSpheres" => {HFSpheres::insert_into_db_chunked(conn, &self.hf_spheres);},
            "HFInteractionKnowledge" => {HFInteractionKnowledge::insert_into_db_chunked(conn, &self.hf_interaction_knowledges);},
            "HFJourneyPets" => {HFJourneyPets::insert_into_db_chunked(conn, &self.hf_journey_pets);},
            "HFHonorID" => {HFHonorID::insert_into_db_chunked(conn, &self.hf_honor_ids);},
            "DanceForm" => {DanceForm::insert_into_db_chunked_indexmap(conn, &self.dance_forms);},
            "MusicalForm" => {MusicalForm::insert_into_db_chunked_indexmap(conn, &self.musical_forms);},
            "PoeticForm" => {PoeticForm::insert_into_db_chunked_indexmap(conn, &self.poetic_forms);},
            "Identity" => {Identity::insert_into_db_chunked_indexmap(conn, &self.identities);},
            "Landmass" => {Landmass::insert_into_db_chunked_indexmap(conn, &self.landmasses);},
            "WrittenContent" => {WrittenContent::insert_into_db_chunked_indexmap(conn, &self.written_contents);},
            "WCStyle" => {WCStyle::insert_into_db_chunked(conn, &self.wc_styles);},
            "WCReference" => {WCReference::insert_into_db_chunked(conn, &self.wc_references);},
            "MountainPeak" => {MountainPeak::insert_into_db_chunked_indexmap(conn, &self.mountain_peaks);},
            "Entity" => {Entity::insert_into_db_chunked_indexmap(conn, &self.entities);},
            "EntityHonor" => {EntityHonor::insert_into_db_chunked(conn, &self.entity_honors);},
            "EntityLink" => {EntityLink::insert_into_db_chunked(conn, &self.entity_links);},
            "EntityPosition" => {EntityPosition::insert_into_db_chunked(conn, &self.entity_positions);},
            "EntityPositionAssignment" => {EntityPositionAssignment::insert_into_db_chunked(conn, &self.entity_position_assignments);},
            "EntityOccasion" => {EntityOccasion::insert_into_db_chunked(conn, &self.entity_occasions);},
            "EntityOccasionSchedule" => {EntityOccasionSchedule::insert_into_db_chunked(conn, &self.entity_occasion_schedules);},
            "EntityOccasionScheduleFeature" => {EntityOccasionScheduleFeature::insert_into_db_chunked(conn, &self.entity_occasion_schedule_features);},
            "EntityWorshipID" => {EntityWorshipID::insert_into_db_chunked(conn, &self.entity_worship_ids);},
            "EntityWeapon" => {EntityWeapon::insert_into_db_chunked(conn, &self.entity_weapons);},
            "EntityProfession" => {EntityProfession::insert_into_db_chunked(conn, &self.entity_professions);},
            "EntityHFID" => {EntityHFID::insert_into_db_chunked(conn, &self.entity_hf_ids);},
            "EntityChildENID" => {EntityChildENID::insert_into_db_chunked(conn, &self.entity_child_en_ids);},
            "HistoricalEventCollection" => {HistoricalEventCollection::insert_into_db_chunked_indexmap(conn, &self.historical_event_collections);},
            "HistoricalEventCollectionAC" => {HistoricalEventCollectionAC::insert_into_db_chunked_indexmap(conn, &self.historical_event_collections_a_c);},
            "HistoricalEventCollectionDZ" => {HistoricalEventCollectionDZ::insert_into_db_chunked_indexmap(conn, &self.historical_event_collections_d_z);},
            "HECHEID" => {HECHEID::insert_into_db_chunked(conn, &self.hec_he_ids);},
            "HECRelatedID" => {HECRelatedID::insert_into_db_chunked(conn, &self.hec_related_ids);},
            "HECIndividualMerc" => {HECIndividualMerc::insert_into_db_chunked(conn, &self.hec_individual_mercs);},
            "HECNoncomHFID" => {HECNoncomHFID::insert_into_db_chunked(conn, &self.hec_noncom_hf_ids);},
            "HECOutcome" => {HECOutcome::insert_into_db_chunked(conn, &self.hec_outcomes);},
            "HECAttackingHFID" => {HECAttackingHFID::insert_into_db_chunked(conn, &self.hec_attacking_hf_ids);},
            "HECDefendingHFID" => {HECDefendingHFID::insert_into_db_chunked(conn, &self.hec_defending_hf_ids);},
            "HECASupportMercHFID" => {HECASupportMercHFID::insert_into_db_chunked(conn, &self.hec_a_support_merc_hf_ids);},
            "HECDSupportMercHFID" => {HECDSupportMercHFID::insert_into_db_chunked(conn, &self.hec_d_support_merc_hf_ids);},
            "HEAHFID" => {HEAHFID::insert_into_db_chunked(conn, &self.he_a_hf_ids);},
            "HEDHFID" => {HEDHFID::insert_into_db_chunked(conn, &self.he_d_hf_ids);},
            "Creature" => {Creature::insert_into_db_chunked_indexmap(conn, &self.creatures);},
            "CreatureAG" => {CreatureAG::insert_into_db_chunked_indexmap(conn, &self.creatures_a_g);},
            "CreatureHH1" => {CreatureHH1::insert_into_db_chunked_indexmap(conn, &self.creatures_h_h_1);},
            "CreatureHH2" => {CreatureHH2::insert_into_db_chunked_indexmap(conn, &self.creatures_h_h_2);},
            "CreatureLZ" => {CreatureLZ::insert_into_db_chunked_indexmap(conn, &self.creatures_l_z);},
            "CreatureBiome1" => {CreatureBiome1::insert_into_db_chunked(conn, &self.creature_biomes_1);},
            "CreatureBiome2" => {CreatureBiome2::insert_into_db_chunked(conn, &self.creature_biomes_2);},
            // Links
            "LinkHEHF" => {LinkHEHF::insert_into_db_chunked_indexmap(conn, &self.links_he_hf);},
            "LinkHESite" => {LinkHESite::insert_into_db_chunked_indexmap(conn, &self.links_he_site);},
            "LinkHEEntity" => {LinkHEEntity::insert_into_db_chunked_indexmap(conn, &self.links_he_entity);},
        }
    }
}

#[allow(dead_code)]
fn skip_long_list<T: std::fmt::Debug>(i: usize, x: T, len: usize) -> Option<String> {
    if i <= 2 || i >= len - 3 {
        Some(format!("{:#?}", x))
    } else if i == 3 {
        Some(format!("...skipped {} items...", len - 6))
    } else {
        None
    }
}

/// From Core to DB (World data)
impl Filler<DBDFWorld, df_st_core::DFWorld> for DBDFWorld {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &df_st_core::DFWorld) {
        self.world_info.add_missing_data(&source.world_info);
        // Regions
        self.regions.add_missing_data(&source.regions);
        Region::add_missing_data_advanced(source, self);
        RegionForce::add_missing_data_advanced(source, self);
        // Sites
        self.sites.add_missing_data(&source.sites);
        Site::add_missing_data_advanced(source, self);
        Structure::add_missing_data_advanced(source, self);
        SiteProperty::add_missing_data_advanced(source, self);
        // UndergroundRegion
        self.underground_regions.add_missing_data(&source.underground_regions);
        UndergroundRegion::add_missing_data_advanced(source, self);
        // Artifact
        self.artifacts.add_missing_data(&source.artifacts);
        Artifact::add_missing_data_advanced(source, self);
        // WorldConstruction
        self.world_constructions.add_missing_data(&source.world_constructions);
        WorldConstruction::add_missing_data_advanced(source, self);
        // EntityPopulation
        self.entity_populations.add_missing_data(&source.entity_populations);
        EntityPopulation::add_missing_data_advanced(source, self);
        EntityPopulationRace::add_missing_data_advanced(source, self);
        // HistoricalEra
        self.historical_eras.add_missing_data(&source.historical_eras);
        // HistoricalEvent
        // TODO add HE for missing IDs
        self.historical_events.add_missing_data(&source.historical_events);
        self.historical_events_a_a.add_missing_data(&source.historical_events);
        self.historical_events_b_b.add_missing_data(&source.historical_events);
        self.historical_events_c_c.add_missing_data(&source.historical_events);
        self.historical_events_d_d1.add_missing_data(&source.historical_events);
        self.historical_events_d_d2.add_missing_data(&source.historical_events);
        self.historical_events_e_g.add_missing_data(&source.historical_events);
        self.historical_events_h_h.add_missing_data(&source.historical_events);
        self.historical_events_i_i.add_missing_data(&source.historical_events);
        self.historical_events_j_m.add_missing_data(&source.historical_events);
        self.historical_events_n_o.add_missing_data(&source.historical_events);
        self.historical_events_p_p.add_missing_data(&source.historical_events);
        self.historical_events_q_r.add_missing_data(&source.historical_events);
        self.historical_events_s_s1.add_missing_data(&source.historical_events);
        self.historical_events_s_s2.add_missing_data(&source.historical_events);
        self.historical_events_t_t.add_missing_data(&source.historical_events);
        self.historical_events_u_w.add_missing_data(&source.historical_events);
        HistoricalEvent::add_missing_data_advanced(source, self);
        // HistoricalFigure
        // TODO add HF for missing IDs
        self.historical_figures.add_missing_data(&source.historical_figures);
        HistoricalFigure::add_missing_data_advanced(source, self);
        // DanceForm
        self.dance_forms.add_missing_data(&source.dance_forms);
        // MusicalForm
        self.musical_forms.add_missing_data(&source.musical_forms);
        // PoeticForm
        self.poetic_forms.add_missing_data(&source.poetic_forms);
        // Identity
        self.identities.add_missing_data(&source.identities);
        // Landmass
        self.landmasses.add_missing_data(&source.landmasses);
        Landmass::add_missing_data_advanced(source, self);
        // River
        self.rivers.add_missing_data(&source.rivers);
        River::add_missing_data_advanced(source, self);
        // WrittenContent
        self.written_contents.add_missing_data(&source.written_contents);
        WrittenContent::add_missing_data_advanced(source, self);
        WCStyle::add_missing_data_advanced(source, self);
        WCReference::add_missing_data_advanced(source, self);
        // MountainPeak
        self.mountain_peaks.add_missing_data(&source.mountain_peaks);
        MountainPeak::add_missing_data_advanced(source, self);
        // Entity
        self.entities.add_missing_data(&source.entities);
        Entity::add_missing_data_advanced(source, self);
        // HistoricalEventCollection
        self.historical_event_collections.add_missing_data(&source.historical_event_collections);
        self.historical_event_collections_a_c.add_missing_data(&source.historical_event_collections);
        self.historical_event_collections_d_z.add_missing_data(&source.historical_event_collections);
        HistoricalEventCollection::add_missing_data_advanced(source, self);
        // Creature
        self.creatures.add_missing_data(&source.creatures);
        self.creatures_a_g.add_missing_data(&source.creatures);
        self.creatures_h_h_1.add_missing_data(&source.creatures);
        self.creatures_h_h_2.add_missing_data(&source.creatures);
        self.creatures_l_z.add_missing_data(&source.creatures);
        Creature::add_missing_data_advanced(source, self);
        // Links
        self.links_he_hf.add_missing_data(&source.links_he_hf);
        self.links_he_site.add_missing_data(&source.links_he_site);
        self.links_he_entity.add_missing_data(&source.links_he_entity);
    }
}

impl fmt::Debug for DBDFWorld {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("DBDFWorld")
            .field("world", &self.world_info)
            // .field("regions", &self.regions)
            // .field("coordinates", &self.coordinates.iter().enumerate()
            // .filter_map(|(i, x)| {
            //     skip_long_list(i,x,self.coordinates.len())
            // })
            // .collect::<Vec<String>>())
            // .field("region_coordinates", &self.regions_coordinates.iter().enumerate()
            // .filter_map(|(i, x)| {
            //     skip_long_list(i,x,self.regions_coordinates.len())
            // })
            // .collect::<Vec<String>>())
            // .field("regions_forces", &self.regions_forces.iter().enumerate()
            // .filter_map(|(i, x)| {
            //     skip_long_list(i,x,self.regions_forces.len())
            // })
            // .collect::<Vec<String>>())
            .field("sites", &self.sites)
            .finish()
    }
}
